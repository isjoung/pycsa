# cython: profile=False, boundscheck=False, wraparound=False, nonecheck=False, language_level=3
"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors:

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import numpy as np
cimport numpy as np

cdef double BIG = np.finfo('d').max

cdef class Symmetric2d:
    """
    This class is designed to handle symmetric 2d distance matrix.
    To save memory, only half of the actual 2d matrix is stored in one-dimensional array.
    The one-dimensional array is same as the array extracted from a 2d symmetric matrix(mat)
    using the following method: mat[np.triu_indices(nsize,k=1)]
    """
    cdef double [:] mat # symmetric 2d matrix serialized in one-dimension
    cdef int matsize # one-dimension size
    cdef int nelem # number of elements (= matsize*(matsize-1)/2)

    def __init__(self, int size, double [:] matrix=None):
        """
        This instantiates the object

        Parameters
        ----------
        self : Symmetric2d
        size : the size of one dimension of the 2d matrix
        matrix : serialized one-dimensional matrix, optional
            if it is not given, -1 is filled in the matrix by default

        Returns
        -------
        None
        """

        if size <= 0:
            ValueError('Error: wrong matrix size')
        self.set_size(size)

        if matrix is not None:
            self.mat = matrix
        else:
            self.create_array()

    cdef set_size(self,int size):
        self.matsize = size
        self.nelem = int(size*(size-1)/2)

    cdef void create_array(self):
        """ This creates empty array (filled with -1.0) """
        cdef int i
        self.mat = np.empty(self.nelem,dtype=np.double)
        for i in range(self.nelem):
            self.mat[i] = -1.0

    def average(self):
        cdef int i
        cdef double avg = 0.0
        for i in range(self.nelem):
            avg += self.mat[i]
        avg /= self.nelem
        return avg

    def average_min(self):
        cdef int i
        cdef double avg = 0.0
        for i in range(self.matsize):
            avg += self.amin_column(i)
        avg /= self.matsize
        return avg

    def max_min(self):
        cdef int i
        cdef double val
        cdef double maxval

        maxval = self.amin_column(0)
        for i in range(1,self.matsize):
            val = self.amin_column(i)
            if val > maxval:
                maxval = val
        return maxval

    def amin(self):
        cdef int i
        cdef double val
        cdef double minval = self.mat[0]
        for i in range(1,self.nelem):
            val = self.mat[i]
            if val < minval:
                minval = val
        return minval

    def amax(self):
        cdef int i
        cdef double val
        cdef double maxval = self.mat[0]
        for i in range(1,self.nelem):
            val = self.mat[i]
            if val > maxval:
                maxval = val
        return maxval

    def aminmax(self):
        cdef int i
        cdef double val
        cdef double minval = self.mat[0]
        cdef double maxval = self.mat[0]
        for i in range(1,self.nelem):
            val = self.mat[i]
            if val < minval:
                minval = val
            if val > maxval:
                maxval = val
        return minval, maxval

    cpdef double amin_column(self,int i):
        """
        This finds the minimum value of the i-th column

        Parameters
        ----------
        self : Symmetric2d
        i : column index

        Returns
        -------
        minval : minimum value
        """

        cdef int j
        cdef double val
        cdef double minval

        if i == 0:
            minval = self.get_elem(i,1)
        else:
            minval = self.get_elem(i,0)

        for j in range(1,self.matsize):
            if i == j:
                continue
            val = self.get_elem(i,j)
            if val < minval:
                minval = val
        return minval

    def amin_mask(self, long [:] mask):
        """
        This finds the minimum value excluding columns and rows specified in
        the input

        Parameters
        ----------
        self : Symmetric2d
        mask : numpy array holding indexes of the columns and rows to be excluded

        Returns
        -------
        minval : minimum value
        """
        cdef np.uint8_t [:] allowed_ind = np.ones(self.matsize,dtype=np.uint8)
        cdef int i, j
        cdef int msize = mask.size
        cdef double minval = BIG

        for i in range(msize):
            allowed_ind[mask[i]] = 0

        for i in range(self.matsize-1):
            if allowed_ind[i] == 0:
                continue
            for j in range(i+1,self.matsize):
                if allowed_ind[j] == 0:
                    continue
                val = self.get_elem(i,j)
                if val < minval:
                    minval = val

        return minval

    def adjust_size(self, int newsize):
        cdef Symmetric2d news2d
        cdef int i, j
        cdef double val
        cdef int csize

        if newsize == self.matsize:
            return

        news2d = Symmetric2d(newsize)
        if newsize > self.matsize:
            csize = self.matsize
        else:
            csize = newsize
        for i in range(csize-1):
            for j in range(i+1,csize):
                val = self.get_elem(i,j)
                news2d.set_elem(i,j,val)

        self.mat = news2d.mat
        self.nelem = news2d.nelem
        self.matsize = news2d.matsize

    def set_column(self, int icol, double [:] colval):
        """
        This replaces values in the column and row. Note that the value falls on
        the diagonal line is ignored.

        Parameters
        ----------
        self : Symmetric2d
        icol : the index of the column
        mask : numpy array with values for the column and row

        Returns
        -------
        None
        """
        cdef int i
        for i in range(self.matsize):
            self.set_elem(i,icol,colval[i])

    def remove_column(self, int icol):
        """
        This removes one column specified in icol

        Parameters
        ----------
        self : Symmetric2d
        icol : the index of the column to be removed
        mask : numpy array with values for the column and row

        Returns
        -------
        None
        """
        cdef Symmetric2d news2d
        cdef int i, j
        cdef int newi, newj
        cdef double val

        news2d = Symmetric2d(self.matsize-1)

        for i in range(self.matsize):
            if i < icol:
                newi = i
            elif i == icol:
                continue
            else:
                newi = i - 1
            for j in range(i+1,self.matsize):
                if j < icol:
                    newj = j
                elif j == icol:
                    continue
                else:
                    newj = j - 1
                val = self.get_elem(i,j)
                news2d.set_elem(newi, newj, val)

        self.mat = news2d.mat
        self.nelem = news2d.nelem
        self.matsize = news2d.matsize

    def get_matsize(self):
        return self.matsize

    def get_2dmat(self):
        """
        This returns numpy 2d matrix.

        Returns
        -------
        2d-ndarray
        """
        cdef np.ndarray [np.double_t,ndim=2] mat
        cdef double val
        mat = np.empty((self.matsize,self.matsize),dtype=np.double)
        for i in range(self.matsize):
            for j in range(self.matsize):
                val = self.get_elem(i,j)
                mat[i,j] = val
                mat[j,i] = val
        return mat

    def get_1dmat(self):
        """
        This returns numpy 1d matrix (only the upper part of the symmetric 2d matrix)

        Returns
        -------
        1d-ndarray
        """

        # copy self.mat into ret
        cdef np.ndarray [np.double_t,ndim=1] ret = np.empty(self.nelem,dtype=np.double)
        cdef int i
        for i in range(self.nelem):
            ret[i] = self.mat[i]

        return ret

    cpdef double get_elem(self, int i, int j):
        cdef int ind
        if i == j:
            return 0.0
        else:
            ind = self.get_index(i,j)
        return self.mat[ind]

    cpdef int set_elem(self, int i, int j, double val):
        cdef int ind
        if i == j:
            return 1
        else:
            ind = self.get_index(i,j)
        self.mat[ind] = val
        return 0

    cdef int get_index(self, int i, int j):
        cdef int ind
        if i < j:
            ind = int(i*(2*self.matsize-1-i)/2) + (j-i-1)
        else:
            ind = int(j*(2*self.matsize-1-j)/2) + (i-j-1)
        return ind

    def mask_closer_elem(self, int ind1, int ind2, np.ndarray [np.uint8_t,ndim=1] mask):
        """
        This turns off the mask (size of elements) if the element in the index of the mask
        is closer to the element in ind1 than the one in ind2.

        Returns
        -------
        None
        """
        cdef int ind3

        for ind3 in range(self.matsize):
            if mask[ind3] == 0:
                continue
            dist_13 = self.get_elem(ind1,ind3)
            dist_23 = self.get_elem(ind2,ind3)
            if dist_13 < dist_23:
                mask[ind3] = 0

    def sort_column(self, int ind):
        """
        This returns the sorted indexes of a column by the distance

        Returns
        -------
        1d-array
        """
        cdef np.ndarray [np.double_t,ndim=1] column = np.empty(self.matsize,dtype=np.double)
        cdef np.ndarray [np.int_t,ndim=1] sortind
        cdef int i

        for i in range(self.matsize):
            column[i] = self.get_elem(i,ind)

        sortind = np.argsort(column)
        return sortind

    cpdef int argmin_column(self,int i):
        """
        This finds the index of the minimum value of the i-th column

        Parameters
        ----------
        self : Symmetric2d
        i : column index

        Returns
        -------
        minind : minimum index
        """

        cdef int j
        cdef double val
        cdef double minval
        cdef int minind

        if i == 0:
            minval = self.get_elem(i,1)
            minind = 1
        else:
            minval = self.get_elem(i,0)
            minind = 0

        for j in range(1,self.matsize):
            if i == j:
                continue
            val = self.get_elem(i,j)
            if val < minval:
                minval = val
                minind = j
        return minind

    cpdef int argmax_column(self,int i):
        """
        This finds the index of the maximum value of the i-th column

        Parameters
        ----------
        self : Symmetric2d
        i : column index

        Returns
        -------
        maxind : maximum index
        """

        cdef int j
        cdef double val
        cdef double maxval
        cdef int maxind

        if i == 0:
            maxval = self.get_elem(i,1)
            maxind = 1
        else:
            maxval = self.get_elem(i,0)
            maxind = 0

        for j in range(1,self.matsize):
            if i == j:
                continue
            val = self.get_elem(i,j)
            if val > maxval:
                maxval = val
                maxind = j
        return maxind
