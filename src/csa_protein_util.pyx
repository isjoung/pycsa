# cython: language_level=3
"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Jong Yun Kim, Sang Jun Park, Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from libc.math cimport sqrt
cimport numpy as np
import numpy as np

def get_ca_adaptive_potential_list(list new_bvar,
    np.ndarray [np.int_t,ndim=1] ca_index,
    np.ndarray [np.double_t,ndim=2] ca_adaptive_potential,
    double ca_ap_cut):

    cdef int iv
    cdef double [:,:] var
    cdef int nbank = len(new_bvar)
    cdef np.ndarray [np.double_t,ndim=1] ret = np.zeros(nbank)

    for iv in range(nbank):
        var = new_bvar[iv]
        ret[iv] += get_ca_adaptive_potential(var,ca_index,ca_adaptive_potential,ca_ap_cut)

    return ret

def get_ca_adaptive_potential(
    double [:,:] var,
    np.ndarray [np.int_t,ndim=1] ca_index,
    np.ndarray [np.double_t,ndim=2] ca_adaptive_potential,
    double ca_ap_cut):

    cdef int i, j, ica, jca, nca
    cdef double dij
    cdef double ret = 0.0

    nca = len(ca_index)
    for ica in range(nca-1):
        i = ca_index[ica]
        for jca in range(ica+1,nca):
            j = ca_index[jca]
            dij = sqrt( (var[i,0]-var[j,0])**2 + (var[i,1]-var[j,1])**2 + (var[i,2]-var[j,2])**2 )
            if dij < ca_ap_cut:
                ret += ca_adaptive_potential[ica,jca]     
            else:
                ret += ca_adaptive_potential[jca,ica] 

    return ret

def add_ca_adaptive_potential(
    double [:,:] var,
    np.ndarray [np.int_t,ndim=1] ca_index,
    np.ndarray [np.double_t,ndim=2] ca_adaptive_potential,
    double ca_ap_cut,
    double ca_ap_inc):

    cdef int i, j, ica, jca, nca
    cdef double dij
    cdef double ret = 0.0

    nca = len(ca_index)
    for ica in range(nca-1):
        i = ca_index[ica]
        for jca in range(ica+1,nca):
            j = ca_index[jca]
            dij = sqrt( (var[i,0]-var[j,0])**2 + (var[i,1]-var[j,1])**2 + (var[i,2]-var[j,2])**2 )
            if dij < ca_ap_cut:
                ca_adaptive_potential[ica,jca] += ca_ap_inc
            else:
                ca_adaptive_potential[jca,ica] += ca_ap_inc

    return ret
