# cython: language_level=3
"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Jong Yun Kim, Sang Jun Park, Keehyung Joo,
Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from libc.math cimport exp, sqrt, log
from libc.float cimport DBL_MAX
cimport numpy as np
import numpy as np

def get_biased_bscore(object self):
    cdef int nbank = len(self.new_bvar)
    cdef np.ndarray [np.double_t,ndim=1] new_bscore_real = self.new_bscore_real
    cdef int bp_sigma_type = self.p['bp_sigma_type']
    cdef double bp_sigma = self.p['bp_sigma']
    cdef double bp_max = self.bp_max
    cdef double sigma2, sigma2_low, sigma2_max
    cdef np.ndarray [np.double_t,ndim=1] new_bscore = new_bscore_real.copy()
    cdef int i, j
    cdef double bp_distance, bp_sum, bp
    cdef np.ndarray [np.double_t,ndim=1] bp_score
    cdef np.ndarray [np.uint8_t,ndim=1] smask

    if bp_sigma_type == 0:
        bp_distance = self.dcut
    elif bp_sigma_type == 1:
        bp_distance = 1.0  # fixed distance
    elif bp_sigma_type == 2:
        bp_distance = self.min_dcut
        
    sigma2 = 2.0 * ( bp_sigma * bp_distance )**2 

    sigma2_low = 0.0
    sigma2_max = DBL_MAX
    bp_sum = 0.0
    bp_score = np.zeros(nbank,dtype=np.double)
    smask = np.zeros(nbank,dtype=np.uint8)
    if bp_sigma_type != 3 and bp_sigma_type != 4:
        smask[self.seed_mask] = 1
    for i in range(nbank):
        if smask[i]:
            continue
        bp = 0.0
        for j in range(nbank):
            if i == j or new_bscore_real[i] < new_bscore_real[j]:
                continue
            bp += exp(-self.bank_distance.get_elem(i,j)**2/sigma2)
        bp *= bp_max
        bp_score[i] = bp
        bp_sum += bp

    for i in range(nbank):
        new_bscore[i] += bp_score[i]

    return new_bscore, sigma2, bp_sum

def add_biased_bscore(object self):
    cdef double imscore_real
    cdef double bp_max
    cdef double bp_sigma2
    cdef np.ndarray [np.double_t,ndim=1] mb_distance
    cdef np.ndarray [np.double_t,ndim=1] bscore_real
    cdef np.ndarray [np.double_t,ndim=1] bscore = self.new_bscore
    cdef np.ndarray [np.double_t,ndim=1] new_bscore = bscore.copy()
    cdef int bsize, i

    imscore_real = self.imscore_real
    bp_max = self.bp_max 
    bp_sigma2 = self.bp_sigma2
    mb_distance = self.mb_distance
    bscore_real = self.new_bscore_real
    bsize = mb_distance.size
    for i in range(bsize):
        if imscore_real > bscore_real[i]:
            continue
        new_bscore[i] += bp_max * exp( -mb_distance[i]**2 / bp_sigma2 )

    return new_bscore
