# cython: language_level=3
"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors:

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import numpy as np
cimport numpy as np
from libc.math cimport M_PI, sin, cos, sqrt, acos, atan2, abs
import sys

cdef double r2d = 180.0/M_PI
cdef double d2r = M_PI/180.0

def get_cartesian(np.ndarray[np.int32_t,ndim=2] iz, np.ndarray[np.double_t] bond,
                  np.ndarray[np.double_t] angle, np.ndarray[np.double_t] torsion):
    cdef np.ndarray[np.double_t] angrad = angle * d2r
    cdef np.ndarray[np.double_t] torrad = torsion * d2r
    cdef int natom = bond.size
    cdef unsigned int i
    cdef int ca1
    cdef np.ndarray[np.double_t,ndim=2] coord = np.empty([natom,3],dtype=np.double)  
    cdef np.ndarray[np.uint8_t,ndim=1] ccheck = np.zeros(natom,dtype=np.uint8)
    cdef unsigned int ia,ib,ic
    cdef double vab[3]
    cdef double vbc[3]
    cdef double vt[3]
    cdef double vu[3]
    cdef double vabn, vbcn, cosine, sine
    cdef double sinang, cosang, sintor, costor
    cdef bint update

    # atom 0
    coord[0,0] = 0.0
    coord[0,1] = 0.0
    coord[0,2] = 0.0
    ccheck[0] = True
    # atom 1
    coord[1,0] = 0.0
    coord[1,1] = 0.0
    coord[1,2] = bond[1]
    ccheck[1] = True
    # atom 2
    ca1 = iz[0,2] - 1
    if ca1 == 0:
        coord[2,0] = bond[2] * sin(angrad[2])
        coord[2,1] = 0.0
        coord[2,2] = bond[2] * cos(angrad[2])
    else:
        coord[2,0] = bond[2] * sin(angrad[2])
        coord[2,1] = 0.0
        coord[2,2] = bond[1] - bond[2] * cos(angrad[2])
    ccheck[2] = True

    while True:
        update = False
        for i in xrange(3,natom):
            if ccheck[i]:
                continue
            ia = iz[0,i] - 1
            ib = iz[1,i] - 1
            ic = iz[2,i] - 1
            if not ccheck[ia] or not ccheck[ib] or not ccheck[ic]:
                continue
            ccheck[i] = True
            update = True
            vab[0] = coord[ia,0] - coord[ib,0]
            vab[1] = coord[ia,1] - coord[ib,1]
            vab[2] = coord[ia,2] - coord[ib,2]
            vabn = norm(vab)
            if vabn == 0.0:
                print('WARN: zmatrix: zero bond distance %d %d'%(ia+1,ib+1))
                vabn = 1e-10
            vab[0] /= vabn
            vab[1] /= vabn
            vab[2] /= vabn
            vbc[0] = coord[ib,0] - coord[ic,0]
            vbc[1] = coord[ib,1] - coord[ic,1]
            vbc[2] = coord[ib,2] - coord[ic,2]
            vbcn = max(norm(vbc),1e-10)
            if vbcn == 0.0:
                print('WARN: zmatrix: zero bond distance %d %d'%(ib+1,ic+1))
                vbcn = 1e-10
            vbc[0] /= vbcn
            vbc[1] /= vbcn
            vbc[2] /= vbcn
            cross(vbc,vab,vt)
            cosine = inner(vab,vbc) 
            if abs(cosine) > 1.0:
                print('WARN: zmatrix: undefined torsion %d %d %d %d'%(i+1,ia+1,ib+1,ic+1))
                cosine = 1.0
            sine = 1.0 - cosine**2
            if sine < 1e-30:
                sine = 1e-30
            else:
                sine = sqrt(sine)
            vt[0] /= sine
            vt[1] /= sine
            vt[2] /= sine
            cross(vt,vab,vu)

            sinang = sin(angrad[i])
            cosang = cos(angrad[i])
            sintor = sin(torrad[i])
            costor = cos(torrad[i])
            coord[i,0] = coord[ia,0] + bond[i] * ( sinang*costor*vu[0] + sinang*sintor*vt[0] - cosang*vab[0] ) 
            coord[i,1] = coord[ia,1] + bond[i] * ( sinang*costor*vu[1] + sinang*sintor*vt[1] - cosang*vab[1] ) 
            coord[i,2] = coord[ia,2] + bond[i] * ( sinang*costor*vu[2] + sinang*sintor*vt[2] - cosang*vab[2] ) 
        if not update:
            raise ValueError('IZ probably has cyclic references')
        if np.all(ccheck):
            break

    return coord

def get_internal(np.ndarray[np.double_t,ndim=2,mode='c'] coord,connect):
    cdef int natom = len(connect)
    cdef np.ndarray[np.double_t] bond = np.zeros(natom,dtype=np.double)
    cdef np.ndarray[np.double_t] angle = np.zeros(natom,dtype=np.double)
    cdef np.ndarray[np.double_t] torsion = np.zeros(natom,dtype=np.double)
    cdef np.ndarray[np.int32_t,ndim=2] iz = np.zeros((3,natom),dtype=np.int32)

    cdef double c1[3]
    cdef double c2[3]
    cdef double xyz0[3]
    cdef double xyz1[3]
    cdef double xyz2[3]
    cdef double xyz3[3]
    cdef int ca1, ca2, ca3

    # atom 1
    c1 = &coord[0,0]
    c2 = &coord[1,0]
    bond[1] = calc_distance(c1,c2)

    # atom 2
    xyz0 = &coord[2,0]
    ca1 = find_connect(2,2,connect,(2,))
    xyz1 = &coord[ca1,0]
    bond[2] = calc_distance(xyz1,xyz0)
    ca2 = find_connect(ca1,2,connect,(2,ca1))
    xyz2 = &coord[ca2,0]
    angle[2] = calc_angle(xyz2,xyz1,xyz0) * r2d
    iz[0,2] = ca1 + 1
    iz[1,2] = ca2 + 1

    for i in xrange(3,natom):
        xyz0 = &coord[i,0]
        ca1 = find_connect(i,i,connect,(i,))
        xyz1 = &coord[ca1,0]
        bond[i] = calc_distance(xyz1,xyz0)
        ca2 = find_connect(ca1,i,connect,(i,ca1))
        xyz2 = &coord[ca2,0]
        angle[i] = calc_angle(xyz2,xyz1,xyz0) * r2d
        ca3 = find_connect(ca2,i,connect,(i,ca1,ca2))
        xyz3 = &coord[ca3,0]
        torsion[i] = calc_torsion(xyz3,xyz2,xyz1,xyz0) * r2d
        iz[0,i] = ca1 + 1
        iz[1,i] = ca2 + 1
        iz[2,i] = ca3 + 1

    return iz,bond,angle,torsion

cpdef int find_connect(int i, int c0,connect,exclude):
    cdef int c
    for c in connect[i]: # assuming connect[i] is already sorted
        if c-1 < c0 and c-1 not in exclude:
            return c-1
    for c in xrange(c0-1,-1,-1):
        if c not in exclude:
            return c
    print('ERROR: failed to find a connect')
    sys.exit()

cdef double calc_distance( double* c1, double* c2 ):
    cdef double dx = c1[0] - c2[0]
    cdef double dy = c1[1] - c2[1]
    cdef double dz = c1[2] - c2[2]
    return sqrt( dx*dx + dy*dy + dz*dz )

cdef double calc_angle( double* c1, double* c2, double* c3 ):
    cdef double v1[3]
    cdef double v2[3]
    cdef double ac
    v1[0] = c1[0] - c2[0]
    v1[1] = c1[1] - c2[1]
    v1[2] = c1[2] - c2[2]
    v2[0] = c3[0] - c2[0]
    v2[1] = c3[1] - c2[1]
    v2[2] = c3[2] - c2[2]
    ac = inner(v1,v2) / ( norm(v1) * norm(v2) )
    if ( ac > 1.0 ):
        return 0.0
    elif ( ac < -1.0 ):
        return -M_PI 
    else:
        return acos(ac) 

cdef double calc_torsion( double* c1, double* c2, double* c3, double* c4 ):
    cdef double v1[3]
    cdef double v2[3]
    cdef double v3[3]
    cdef double cp12[3]
    cdef double cp23[3]
    cdef double tv[3]
    cdef double nv2
    v1[0] = c2[0] - c1[0]
    v1[1] = c2[1] - c1[1]
    v1[2] = c2[2] - c1[2]
    v2[0] = c3[0] - c2[0]
    v2[1] = c3[1] - c2[1]
    v2[2] = c3[2] - c2[2]
    v3[0] = c4[0] - c3[0]
    v3[1] = c4[1] - c3[1]
    v3[2] = c4[2] - c3[2]
    cross(v1,v2,cp12)
    cross(v2,v3,cp23)
    nv2 = norm(v2)
    tv[0] = nv2*v1[0]
    tv[1] = nv2*v1[1]
    tv[2] = nv2*v1[2]
    return atan2( inner(tv,cp23), inner(cp12,cp23) )

cdef double inner(double* v1, double* v2):
    return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2]

cdef double norm(double* v):
    return sqrt( v[0]*v[0] + v[1]*v[1] + v[2]*v[2] )

cdef void cross(double* v1, double* v2, double* vout):
    vout[0] = v1[1]*v2[2] - v1[2]*v2[1]
    vout[1] = v1[2]*v2[0] - v1[0]*v2[2]
    vout[2] = v1[0]*v2[1] - v1[1]*v2[0]
