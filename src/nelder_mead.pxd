import numpy as np
cimport numpy as np

ctypedef double (*cfptr)(np.ndarray [np.double_t,ndim=1] v)
cdef cfptr nm_function
cdef int maxiter
cdef double [:] max_param
cdef double [:] min_param
cdef double perturb_ratio
cdef double xtol

cdef tuple minimize_neldermead(np.ndarray [np.double_t,ndim=1] x0)
