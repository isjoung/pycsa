# cython: language_level=3
"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: 

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import numpy as np
from copy import deepcopy

cimport numpy as np
from libc.math cimport sqrt, M_PI, acos, atan2

def exchange_chain_coord(chains,amap,var1,var2):
    if ( len(chains) == 1 ):
        return var2.copy()
    
    newvar = var1.copy()
    # rms fit
    for chain_atom_ind in chains:
        #chain_atom_ind = get_atom_index(chain,amap)
        newvar = exchange_coord_car(chain_atom_ind,newvar,var2)

    return newvar

def exchange_coord_car(exind,var1,var2):
    rmsd, fitvar = calc_rmsd(var1[exind,:],var2[exind,:])
    newvar = var1.copy()
    newvar[exind,:] = fitvar
    return newvar

def exchange_coord_int(exind,
    # try to use exchange_coord_int2 instead
    np.ndarray [np.int32_t,ndim=2] iz1,
    np.ndarray [np.double_t,ndim=1] tor1,
    np.ndarray [np.double_t,ndim=1] ang1,
    np.ndarray [np.double_t,ndim=1] bond1,
    np.ndarray [np.double_t,ndim=1] tor2,
    np.ndarray [np.double_t,ndim=1] ang2,
    np.ndarray [np.double_t,ndim=1] bond2):

    cdef int ei, i
    cdef int natom = tor1.size
    cdef np.ndarray [np.double_t,ndim=1] tor_new = tor1.copy()
    cdef np.ndarray [np.double_t,ndim=1] ang_new = ang1.copy()
    cdef np.ndarray [np.double_t,ndim=1] bond_new = bond1.copy()
    cdef np.ndarray [np.uint8_t,ndim=1] exind_mask = np.zeros(natom,dtype=np.uint8)
    cdef double del_tor

    for i in exind:
        exind_mask[i] = True

    for ei in exind:
        del_tor = tor2[ei] - tor_new[ei]
        tor_new[ei] = tor2[ei]
        for i in range(natom):
            if iz1[0,ei] == iz1[0,i] and iz1[1,ei] == iz1[1,i] and exind_mask[i] == False:
                tor_new[i] += del_tor
                exind_mask[i] = True

        ang_new[ei] = ang2[ei]
        bond_new[ei] = bond2[ei]

    return tor_new, ang_new, bond_new

def exchange_coord_int2(
    np.ndarray [np.uint8_t,ndim=1] exind_mask,
    np.ndarray [np.int32_t,ndim=2] iz1,
    np.ndarray [np.double_t,ndim=1] tor1,
    np.ndarray [np.double_t,ndim=1] ang1,
    np.ndarray [np.double_t,ndim=1] bond1,
    np.ndarray [np.double_t,ndim=1] tor2,
    np.ndarray [np.double_t,ndim=1] ang2,
    np.ndarray [np.double_t,ndim=1] bond2):

    cdef int ei, i
    cdef int natom = tor1.size
    cdef np.ndarray [np.double_t,ndim=1] tor_new = tor1.copy()
    cdef np.ndarray [np.double_t,ndim=1] ang_new = ang1.copy()
    cdef np.ndarray [np.double_t,ndim=1] bond_new = bond1.copy()
    cdef np.ndarray [np.uint8_t,ndim=1] exind_mask_copy = exind_mask.copy()
    cdef double del_tor

    for ei in range(natom):
        if not exind_mask_copy[ei]:
            continue
        del_tor = tor2[ei] - tor_new[ei]
        tor_new[ei] = tor2[ei]
        for i in range(natom):
            if iz1[0,ei] == iz1[0,i] and iz1[1,ei] == iz1[1,i] and exind_mask_copy[i] == False:
                tor_new[i] += del_tor
                exind_mask_copy[i] = True

        ang_new[ei] = ang2[ei]
        bond_new[ei] = bond2[ei]

    return tor_new, ang_new, bond_new

def add_torsion(exind, int ei, double del_tor,
    np.ndarray [np.int32_t,ndim=2] iz,
    np.ndarray [np.double_t,ndim=1] tor,
    np.ndarray [np.uint8_t,ndim=1] update = None):

    # add del_tor to the atoms that have the same connectivity as atom index, ei

    cdef int i
    cdef int iz0 = iz[0,ei]
    cdef int iz1 = iz[1,ei]

    if update is None:
        for i in exind:
            if iz[0,i] == iz0 and iz[1,i] == iz1:
                tor[i] += del_tor
    else:
        for i in exind:
            if iz[0,i] == iz0 and iz[1,i] == iz1:
                tor[i] += del_tor
                update[i] = True

def get_atom_index(resind,amap):
    # get atom indexes of the residues in resind
    # try to use get_atom_mask instead of this subroutine
    cdef int ri, an

    atom_index = []
    for ri in resind:
        resatoms = amap['data'][ri+1]
        for ra in resatoms.values():
            an = ra[0]
            atom_index.append(an-1)
    return atom_index

def get_atom_mask(resind,amap):
    cdef int ri, an
    cdef np.ndarray [np.uint8_t,ndim=1] atom_mask = np.zeros(amap['natom'],dtype=np.uint8)
    
    for ri in resind:
        resatoms = amap['data'][ri+1]
        for ra in resatoms.values():
            an = ra[0]
            atom_mask[an-1] = 1
    return atom_mask

def pick_partner(avail_partner,ind1,bank_dist=None,n_best=20):
    if bank_dist is not None:
        # pick best
        n_avail = len(avail_partner)
        if n_avail <= n_best:
            ret = np.random.choice(avail_partner)
        else:
            aind = np.argpartition(bank_dist[ind1,avail_partner],n_best)[:n_best]
            ret = np.random.choice(np.array(avail_partner)[aind])
    else:
        ret = np.random.choice(avail_partner)
    return ret

def get_available_partner(int nbank,int ind1,seed_mask=None,int exclusive_partner=0): 
    cdef np.ndarray [np.uint8_t,ndim=1] ap
    cdef list ap_ind # return value
    cdef int nind
    cdef int i, j
    cdef int nep

    if exclusive_partner:
        nep = exclusive_partner
        if ind1 < nbank-nep:
            ap = np.zeros(nbank,dtype=np.uint8)
            for i in range(nbank-nep,nbank):
                ap[i] = 1
        else:
            ap = np.ones(nbank,dtype=np.uint8)
            for i in range(nbank-nep,nbank):
                ap[i] = 0
    else:
        ap = np.ones(nbank,dtype=np.uint8)
        ap[ind1] = 0
        if seed_mask is not None:
            for i in seed_mask:
                ap[i] = 0

    # count the number of indexes
    nind = 0
    for i in range(nbank):
        if ap[i]:
            nind += 1 
    assert( nind > 0 )

    ap_ind = [None] * nind
    j = 0
    for i in range(nbank):
        if ap[i]:
            ap_ind[j] = i
            j += 1

    return ap_ind

def split_integer(num,size):
    input_num = int(num)
    if ( size == 1 ): return [input_num]
    ret = np.random.rand(size)
    ret = ( ret / np.sum(ret) * input_num ).round()
    ret[-1] = input_num - np.sum(ret[:-1])
    print(ret) 

def plane_fit(coord,weight=None):
    c = coord.copy()
    center = None
    mat = None
    if ( weight ):
        center = np.sum( c * weight, axis=0 ) / np.sum(weight)
        c -= center
        mat = c * weight
    else:
        center = np.average( c, axis=0 )
        mat = c - center

    ( r1, s, r2 ) = np.linalg.svd(mat)
    coeff = r2[-1,:]

    return coeff, center

cpdef tuple rms_fit(np.ndarray [np.double_t,ndim=2] ref_c, np.ndarray [np.double_t,ndim=2] c,w=None):
    cdef int ncoord = ref_c.shape[0]
    cdef int naxis = ref_c.shape[1]
    cdef double sum_w
    cdef np.ndarray [np.double_t,ndim=1] ref_trans = np.zeros(naxis,dtype=np.double) 
    cdef np.ndarray [np.double_t,ndim=1] c_trans = np.zeros(naxis,dtype=np.double) 
    cdef np.ndarray [np.double_t,ndim=2] ref_c_orig = np.empty((ncoord,naxis),dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=2] c_orig = np.empty((ncoord,naxis),dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=2] C = np.zeros((naxis,naxis),dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=2] r1
    cdef np.ndarray [np.double_t,ndim=1] s
    cdef np.ndarray [np.double_t,ndim=2] r2
    cdef np.ndarray [np.double_t,ndim=2] U = np.zeros((naxis,naxis),dtype=np.double)
    cdef int i, j, k

    if ( w != None ):
        sum_w = np.sum(w)

        # move geometric center to the origin
        ref_trans = np.sum( ref_c * w[:,np.newaxis], axis=0 ) / sum_w
        ref_c_orig = ref_c - ref_trans
        c_trans = np.sum( c*w[:,np.newaxis], axis=0 ) / sum_w
        c_orig = c - c_trans

        # covariance matrix
        C = np.dot( (c_orig*w[:,np.newaxis]).T, ref_c_orig ) / ( sum_w/w.size )
    else:
        # cythonized
        #ref_trans = average( ref_c, axis=0 )
        #ref_c = ref_c - ref_trans
        #c_trans = average( c, axis=0 )
        #c = c - c_trans
        # move geometric center to the origin
        for i in range(ncoord):
            for j in range(naxis):
                ref_trans[j] += ref_c[i,j] 
        for j in range(naxis):
            ref_trans[j] /= ncoord
        for i in range(ncoord):
            for j in range(naxis):
                ref_c_orig[i,j] = ref_c[i,j] - ref_trans[j]

        for i in range(ncoord):
            for j in range(naxis):
                c_trans[j] += c[i,j]
        for j in range(naxis):
            c_trans[j] /= ncoord
        for i in range(ncoord):
            for j in range(naxis):
                c_orig[i,j] = c[i,j] - c_trans[j]

        # covariance matrix
        # cythonized
        #C = np.dot( c_orig.T, ref_c_orig )
        for i in range(naxis):
            for j in range(naxis):
                for k in range(ncoord):
                    C[i,j] += c_orig[k,i] * ref_c_orig[k,j]

    # Singular Value Decomposition
    ( r1, s, r2 ) = np.linalg.svd(C)

    # compute sign (remove mirroring)
    if ( np.linalg.det(C) < 0.0 ):
        i = naxis - 1
        for j in range(naxis):
            r2[i,j] *= -1.0
    # cythonized
    #U = np.dot( r1, r2 )
    for i in range(naxis):
        for j in range(naxis):
            for k in range(naxis):
                U[i,j] += r1[i,k] * r2[k,j]

    return ( c_trans, U, ref_trans )

cpdef tuple calc_rmsd(np.ndarray [np.double_t,ndim=2] c1, np.ndarray [np.double_t,ndim=2] c2, w=None):
    cdef int ncoord = c1.shape[0]
    cdef int naxis = c1.shape[1]
    cdef np.ndarray [np.double_t,ndim=1] c_trans
    cdef np.ndarray [np.double_t,ndim=2] U
    cdef np.ndarray [np.double_t,ndim=1] ref_trans
    cdef np.ndarray [np.double_t,ndim=2] new_c2 = np.zeros((ncoord,naxis),dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=1] tcoord = np.empty(naxis,dtype=np.double)
    cdef int i, j, k
    cdef double rmsd

    ( c_trans, U, ref_trans ) = rms_fit( c1, c2, w )

    # cythonized
    # new_c2 = np.dot( c2-c_trans, U ) + ref_trans
    for i in range(ncoord):
        for j in range(naxis):
            tcoord[j] = c2[i,j] - c_trans[j]
        for j in range(naxis):
            for k in range(naxis):
                new_c2[i,j] += tcoord[k] * U[k,j]
            new_c2[i,j] += ref_trans[j]

    rmsd = 0.0
    if ( w != None ):
        rmsd = np.sqrt( np.sum( np.sum( ( c1 - new_c2 )**2, axis=1 ) * w ) / np.sum(w) )
    else:
        # cythonized
        # rmsd2 = np.sqrt( np.average( np.sum( ( c1 - new_c2 )**2, axis=1 ) ) )
        for i in range(ncoord):
            for j in range(naxis):
                rmsd += (c1[i,j] - new_c2[i,j])**2
        rmsd = np.sqrt(rmsd/ncoord)

    return rmsd, new_c2

cpdef fit_coord(np.ndarray [np.double_t,ndim=2] c1, np.ndarray [np.double_t,ndim=2] c2, w=None):
    cdef int ncoord = c1.shape[0]
    cdef int naxis = c1.shape[1]
    cdef np.ndarray [np.double_t,ndim=1] c_trans
    cdef np.ndarray [np.double_t,ndim=2] U
    cdef np.ndarray [np.double_t,ndim=1] ref_trans
    cdef np.ndarray [np.double_t,ndim=2] new_c2 = np.zeros((ncoord,naxis),dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=1] tcoord = np.empty(naxis,dtype=np.double)
    cdef int i, j, k

    ( c_trans, U, ref_trans ) = rms_fit( c1, c2, w )

    # cythonized
    # new_c2 = np.dot( c2-c_trans, U ) + ref_trans
    for i in range(ncoord):
        for j in range(naxis):
            tcoord[j] = c2[i,j] - c_trans[j]
        for j in range(naxis):
            for k in range(naxis):
                new_c2[i,j] += tcoord[k] * U[k,j]
            new_c2[i,j] += ref_trans[j]

    return new_c2

def correct_torsion(tor):
    # adjust values of tor to be in the range from -180 to 180
    tor = np.mod(tor,360.0) # 0 to 360.0
    if tor.size == 1:
        if tor>180.0:
            tor -= 360.0
    else:
        mask = (tor>180.0)
        tor[mask] -= 360.0
    return tor

def correct_angle(ang):
    ang = np.mod(ang,360.0) # 0 to 360.0
    if ang.size == 1:
        if ang>180.0:
            ang = 360.0 - ang
    else:
        mask = (ang>180.0)
        ang[mask] = 360.0 - ang[mask]
    return ang

cpdef double distance(np.ndarray [np.double_t,ndim=1] c1, np.ndarray [np.double_t,ndim=1] c2):
    cdef int ndim = c1.size
    cdef double dc, dist
    cdef int i

    dist = 0.0
    for i in range(ndim):
        dc = c1[i] - c2[i]
        dist += dc*dc
    return sqrt(dist)

cpdef double angle(np.ndarray [np.double_t,ndim=1] c1, np.ndarray [np.double_t,ndim=1] c2, np.ndarray [np.double_t,ndim=1] c3):
    cdef int ndim = c1.size 
    cdef np.ndarray [np.double_t,ndim=1] v1 = np.empty(ndim,dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=1] v2 = np.empty(ndim,dtype=np.double)
    cdef int i
    cdef double ac

    for i in range(ndim):
        v1[i] = c1[i] - c2[i]
        v2[i] = c3[i] - c2[i]
    ac = inner(v1,v2) / ( norm(v1) * norm(v2) )
    if ac > 1.0:
        return 0.0
    elif ac < -1.0:
        return -M_PI 
    else:
        return acos(ac) 

cpdef double torsion(
    np.ndarray [np.double_t,ndim=1] c1,
    np.ndarray [np.double_t,ndim=1] c2,
    np.ndarray [np.double_t,ndim=1] c3,
    np.ndarray [np.double_t,ndim=1] c4):

    cdef int ndim = c1.size 
    cdef np.ndarray [np.double_t,ndim=1] v1 = np.empty(ndim,dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=1] v2 = np.empty(ndim,dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=1] v3 = np.empty(ndim,dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=1] cp12 = np.empty(ndim,dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=1] cp23 = np.empty(ndim,dtype=np.double)
    cdef int i

    for i in range(ndim): 
        v1[i] = c2[i] - c1[i]
        v2[i] = c3[i] - c2[i]
        v3[i] = c4[i] - c3[i]

    v1 = c2 - c1
    v2 = c3 - c2
    v3 = c4 - c3
    cross(v1,v2,cp12) 
    cross(v2,v3,cp23)
    return atan2( norm(v2) * inner(v1,cp23), inner(cp12,cp23) )

cdef double inner(np.ndarray [np.double_t,ndim=1] c1, np.ndarray [np.double_t,ndim=1] c2):
    cdef int ndim = c1.size
    cdef double ret

    ret = 0.0
    for i in range(ndim):
        ret += c1[i] * c2[i]
    return ret

cdef double norm(np.ndarray [np.double_t,ndim=1] c):
    cdef int ndim = c.size
    cdef int i
    cdef double ret

    ret = 0.0
    for i in range(ndim):
        ret += c[i]*c[i]
    return sqrt(ret)

cdef void cross(double [:] v1, double [:] v2, double [:] vout):
    vout[0] = v1[1]*v2[2] - v1[2]*v2[1]
    vout[1] = v1[2]*v2[0] - v1[0]*v2[2]
    vout[2] = v1[0]*v2[1] - v1[1]*v2[0]
