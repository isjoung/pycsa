"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import print_function
from __future__ import absolute_import

from builtins import str
from builtins import map
from builtins import range
import sys
from .csa_molecule import csa_molecule
from .csa import mpicomm, mpirank, mpisize, MPI, dict_merge
from . import tinker_util as tu
from perturb_util import calc_rmsd, fit_coord
import datetime
import numpy as np

class csa_TINKER(csa_molecule):
    """csa for optimizing conformations of TINKER molecules"""

    def unpack_min_output(self,min_output,i_list):
        for ind in range(len(i_list)):
            i = i_list[ind]
            mo = min_output[ind]
            score, minvar, decomp_ene, nee = mo
            self.mvar.append(minvar)
            self.mscore.append(score)
            try:
                self.perturb_info.append(self.perturb_info_backup[i])
            except:
                pass
            self.decomp_mscore.append(decomp_ene)
            try:
                self.minimize_nee += nee
            except:
                self.minimize_nee = nee
            self.print_log("m%d score: %.5E"%(len(self.mvar),score), level=3)

    def minimizef(self,var,rinfo=None):
        grdmin = self.p['grdmin']
        freq = self.p['report_min']
        min_method = self.p['min_method'] 

        # maxiter
        if self.rbank_minimization:
            maxiter = self.p['rbank_min_maxiter']
        else:
            maxiter = self.p['bank_min_maxiter']

        if rinfo: 
            tu.tinker.py_restrain.use_py_restrain = True # turn on
            if 'dist' in rinfo:
                tu.tinker.py_restrain.set_py_dist_restrain(*rinfo['dist'])
                tu.tinker.py_restrain.use_py_dist_restrain = True
            if 'tor' in rinfo:
                tu.tinker.py_restrain.set_py_tor_restrain(*rinfo['tor'])
                tu.tinker.py_restrain.use_py_tor_restrain = True

        if ( min_method == 'tinker' ): 
            tu.tinker.minima.maxiter = maxiter
            tu.setxyz(var) # give xyz to tinker
            # callback for printing out minimization progress
            def report_min(ncyc,f,xx):
                if ( freq ):
                    if ( ncyc % freq == 0 ):
                        print("proc %d ncycle %d energy %f"%(mpirank,ncyc,f))
            if tu.tinker.minima.maxiter >= 0:
                min_energy, ncalls = tu.tinker_minimize(grdmin,report_min)
            else:
                min_energy = tu.tinker.energy()
                ncalls = 1
            if rinfo:
                tu.tinker.py_restrain.use_py_restrain = False # turn off
                min_energy, ncalls2 = tu.tinker_minimize(grdmin,report_min)
                ncalls += ncalls2
            minvar = tu.getxyz() # take xyz from tinker
            decomp_energy = tu.get_ecomp()
        elif ( min_method == 'lbfgsb' ):
            # callback for printing out minimization progress
            def report_min(ncyc,f,xx):
                if ( freq ):
                    if ( ncyc % freq == 0 ):
                        print("proc %d ncycle %d energy %f"%(mpirank,ncyc,f))
            if self.p['neb']:
                var = np.array(var)
                tu.tinker.neb.set_neb_coord(var)
                minvar = np.empty_like(var)
                min_energy, ncalls = tu.tinker_minimize_lbfgsb(grdmin,report_min)
            else:
                tu.setxyz(var) # give xyz to tinker
                min_energy, ncalls = tu.tinker_minimize_lbfgsb(grdmin,report_min)
            if rinfo:
                tu.tinker.py_restrain.use_py_restrain = False # turn off
                if self.p['neb']:
                    min_energy, ncalls2 = tu.tinker_minimize_lbfgsb(minvar,grdmin,report_min)
                else:
                    min_energy, ncalls2 = tu.tinker_minimize_lbfgsb(minvar,grdmin,report_min)
                ncalls += ncalls2
            if self.p['neb']:
                minvar = np.asfortranarray(minvar)
                tu.tinker.neb.get_neb_coord(minvar)
                neb_ene = np.empty(self.p['neb_nseq']*2+5)
                tu.tinker.neb.get_neb_energy(neb_ene)
                decomp_energy = (self.decomp_energy_name,tuple(neb_ene))
            else:
                minvar = tu.getxyz() # take xyz from tinker
                decomp_energy = tu.get_ecomp()

        elif ( min_method == 'monte' ):
            tu.setxyz(var) # give xyz to tinker
            # callback for printing out monte progress
            def report_mon(ncyc,f,x,y,z):
                if ( freq ):
                    if ( ncyc % freq == 0 ):
                        print("proc %d ncycle %d energy %f"%(mpirank,ncyc,f))
            min_energy, ncalls = tu.tinker_monte(self.p['monte_torsmove'],self.p['monte_size'],self.p['temperature'],report_mon)
            minvar = tu.getxyz() # take xyz from tinker
            decomp_energy = tu.get_ecomp()
        else:
            self.print_error('Unknown minimization method %s'%min_method)

        if rinfo:
            tu.tinker.py_restrain.use_py_restrain = False # turn off

        if self.p['metadynamics']:
            # remove adaptive potential
            min_energy -= tu.tinker.py_energy.py_ene

        # fortran array to C array
        if np.isfortran(minvar):
            minvar = np.ascontiguousarray(minvar)
        return min_energy, minvar, decomp_energy, ncalls

    def __init__(self,param={}):
        p = {
            # tinker
            'xyzfile': 'model.xyz',
            'xyzmapfile': 'model.map',
            'init_bank': None,

            'bank_min_maxiter': 1000, # maximum iteration step in bank minimization (if 0, unlimited)
            'rbank_min_maxiter': 1000, # maximum iteration step in rbank minimization (if 0, unlimited)

            # log
            'write_xyz': 2, # writes xyz files 0:No, 1: Yes, 2: write arc
            'old_style_history': False,

            # minimization method
            'min_method': 'tinker',  # tinker/lbfgsb/monte
            'monte_torsmove': False, # torsional move or not (Monte)
            'monte_size': 0.01,      # step size for Monte

            'native_xyz': None,
        }

        self.p = dict_merge(p,param)

        # init for tinker
        tu.tinker_init(self.p['xyzfile'])
        self.initvar = tu.readxyz(self.p['xyzfile'])

        # init for csa parameter
        super(csa_TINKER,self).__init__(self.p)

    def init_accessor(self):
        super(csa_TINKER,self).init_accessor()

        # turn on py_energy
        if self.p['metadynamics']:
            tu.tinker.py_energy.use_py_energy = True
            def py_energy_function():
                var = tu.getxyz()
                ap_distance = self.get_adaptive_potential_distance(var)
                ap_ind = self.get_ap_bin_index(ap_distance)
                ene = self.get_adaptive_potential(ap_ind)
                # energy
                tu.tinker.py_energy.py_ene = ene
                # derivative
                #natom = self.xyzmap['natom']
                #tu.tinker.py_energy.d_py_ene[:,:natom] = 0.0
                return

            # callback energy function
            tu.tinker.py_energy_function = py_energy_function

        if not self.p['neb']:
            self.decomp_energy_name = tu.get_ecomp()[0]
            self.decomp_energy_name = tuple(self.decomp_energy_name)

    def read_native_var(self):
        # native xyz
        if ( self.p['native_xyz'] ):
            if isinstance(self.p['native_xyz'],list) or isinstance(self.p['native_xyz'],tuple):
                self.native_var = []
                for nxyz in self.p['native_xyz']:
                    self.native_var.append( tu.readxyz(nxyz) )
            else:
                self.native_var = [ tu.readxyz(self.p['native_xyz']) ]
            tu.setxyz(self.initvar) 

        if self.p['neb']:
            self.native_score = []
            for nv in self.native_var:
                tu.setxyz(nv)
                self.native_score.append(tu.tinker.energy())

    def read_connect(self):
        connect = []
        for i in range(tu.tinker.atoms.n):
            n_con = tu.tinker.couple.n12[i]
            connect.append(tu.tinker.couple.i12[:n_con,i])
        return connect

    def read_xyzmap(self):
        mapfile = self.p['xyzmapfile']
        try:
            f = open(mapfile,'r')
        except:
            self.print_error( "cannot read %s"%(mapfile) )
            return
        self.print_log( "Reading %s"%(mapfile), level=4 )

        # xyzmap dict
        # ['title']: title
        # ['nres']: nres
        # ['seq']: sequence
        # ['natom']: natom
        # ['data'][resnum][atomname]: list of columns
        # ['resrange'][resnum]: tuple of atom index range

        # read headlines
        self.xyzmap = {}
        self.xyzmap['title'] = f.readline()
        self.xyzmap['nres'] = int( f.readline().strip().split()[0] )
        self.xyzmap['seq'] = f.readline().strip() 
        self.xyzmap['natom'] = int( f.readline().strip().split()[0] )
        self.xyzmap['connect'] = self.read_connect()

        # read atom lines
        data = {}
        raw_data = []
        for i in range(self.xyzmap['natom']):
            line = f.readline().strip()
            c = line.split()
            c[0] = int(c[0]) # serial number in xyz file
            c[1] = int(c[1]) # serial number in pdb file
            c[4] = int(c[4]) # residue number
            c[5] = int(c[5]) # DFIRE residue index
            c[6] = int(c[6]) # DFIRE atom index
            try:
                c[7] = int(c[7]) # molecule number
            except:
                pass
            raw_data.append(c)
            if ( c[4] not in data ):
                data[c[4]] = {}
            data[c[4]][c[2]] = c # residue number -> atom name = serial number of xyz
        self.xyzmap['data'] = data  
        self.xyzmap['raw_data'] = raw_data

        # resrange
        resrange = {}
        atom_index = 0
        for res in sorted(data):
            end_index = atom_index + len(data[res])
            resrange[res] = ( atom_index, end_index )
            atom_index = end_index 
        self.xyzmap['resrange'] = resrange

    def __del__(self):
        # final for tinker
        tu.tinker.final()

    def savexyz(self,prefix,vars,energy=None):
        var_size = len(vars)
        outfmt = '%s%0' + '%d'%(max(int(np.log10(var_size))+1,3)) + 'd.xyz'
        for i in range(len(vars)):
            outfile = outfmt%(prefix,i+1)
            self.print_log('Writing %s'%(outfile),level=5)

            natom = int(tu.tinker.atoms.n)
            name = np.transpose(tu.tinker.atmtyp.name[:,:natom]).view('S3')
            atmtype = tu.tinker.atoms.atmtype
            i12 = tu.tinker.couple.i12
            n12 = tu.tinker.couple.n12
            coord = vars[i]

            with open(outfile,'w') as xyzf:
                if self.p['neb']:
                    for iseq in range(self.p['neb_nseq']):
                        co = coord[iseq]
                        if energy is not None:
                            xyzf.write( '%6d  Total Energy= %f\n' % (natom,energy[i]) )
                        else:
                            xyzf.write( '%6d\n' % (natom) )
                        for j in range(natom):
                            xyzf.write( '%6d  %-3s%12.6f%12.6f%12.6f%6d' % ( j+1, name[j].tostring(), co[j,0], co[j,1], co[j,2], atmtype[j] ) )
                            for k in range(n12[j]):
                                xyzf.write( '%6d' % (i12[k,j]) )
                            xyzf.write('\n')
                else:
                    if energy is not None:
                        xyzf.write( '%6d  Total Energy= %f\n' % (natom,energy[i]) )
                    else:
                        xyzf.write( '%6d\n' % (natom) )
                    for j in range(natom):
                        xyzf.write( '%6d  %-3s%12.6f%12.6f%12.6f%6d' % ( j+1, name[j].tostring(), coord[j,0], coord[j,1], coord[j,2], atmtype[j] ) )
                        for k in range(n12[j]):
                            xyzf.write( '%6d' % (i12[k,j]) )
                        xyzf.write('\n')

    def savearc(self,prefix,vars,energy=None):
        var_size = len(vars)
        outfile = prefix+".arc"
        with open(outfile,'w') as xyzf:
            self.print_log('Writing %s'%(outfile),level=5)
            natom = int(tu.tinker.atoms.n)
            name = np.transpose(tu.tinker.atmtyp.name[:,:natom]).view('S3')
            atmtype = tu.tinker.atoms.atmtype
            i12 = tu.tinker.couple.i12
            n12 = tu.tinker.couple.n12

            for i in range(len(vars)):
                coord = vars[i]
                if self.p['neb']:
                    for iseq in range(self.p['neb_nseq']):
                        co = coord[iseq]
                        if energy is not None:
                            xyzf.write( '%6d  Total Energy= %f\n' % (natom,energy[i]) )
                        else:
                            xyzf.write( '%6d\n' % (natom) )
                        for j in range(natom):
                            xyzf.write( '%6d  %-3s%12.6f%12.6f%12.6f%6d' % ( j+1, name[j].tostring(), co[j,0], co[j,1], co[j,2], atmtype[j] ) )
                            for k in range(n12[j]):
                                xyzf.write( '%6d' % (i12[k,j]) )
                            xyzf.write('\n')
                else:
                    if energy is not None:
                        xyzf.write( '%6d  Total Energy= %f\n' % (natom,energy[i]) )
                    else:
                        xyzf.write( '%6d\n' % (natom) )
                    for j in range(natom):
                        xyzf.write( '%6d  %-3s%12.6f%12.6f%12.6f%6d' % ( j+1, name[j].tostring(), coord[j,0], coord[j,1], coord[j,2], atmtype[j] ) )
                        for k in range(n12[j]):
                            xyzf.write( '%6d' % (i12[k,j]) )
                        xyzf.write('\n')

    def report_iterate(self):
        # save bank conformations
        if ( mpirank != 0 ): return
        if ( self.p['write_diff'] and self.niter % self.p['write_diff'] == 0 ):
            self.write_diff()
        if ( self.p['write_bank'] ):
            if ( self.p['write_pdb'] ):
                self.savepdb('b',self.bvar,self.bscore)
            if ( self.p['record_gmin'] and self.update_bvar[self.minscore_ind] >= 0 ):
                # gmin updated
                self.write_pdb('gmin%05d.pdb'%self.niter,self.bvar[self.minscore_ind],energy=self.minscore)
            if ( self.p['write_xyz']==1 ):
                self.savexyz('b',self.bvar,self.bscore)
            elif ( self.p['write_xyz']==2 ):
                self.savearc('b',self.bvar,self.bscore)
            if ( self.p['write_score_profile'] ):
                self.write_score_profile()
        if ( not self.history_fh ): return
        self.write_history_file()

    def read_archive_coord(self):
        arcfile = self.p['init_bank']
        # add conformations from the archive file to rbank
        varlist = []
        f = open(arcfile,'r')
        line = f.readline()
        natom = int(line[0:6])
        count = 1
        coord = []
        head_read = True
        for line in f:
            count += 1
            m = count % (natom+1)
            if ( head_read ):
                coord.append( [ float(line[11:23]), float(line[23:35]), float(line[35:47]) ] )
            else:
                head_read = True
            if ( m == 0 ): # end of frame
                head_read = False
                varlist.append(np.array(coord))
                coord = []
        f.close()
        return varlist

    def report_create_firstbank(self):
        # save initial random conformations
        if ( mpirank != 0 ): return
        if ( self.p['write_bank'] ):
            if ( self.p['write_pdb'] ):
                self.savepdb('r',self.rvar,self.rscore)
            if ( self.p['write_xyz']==1 ):
                self.savexyz('r',self.rvar,self.rscore)
            elif ( self.p['write_xyz']==2 ):
                self.savearc('r',self.rvar,self.rscore)
            if ( self.p['write_score_profile'] ):
                self.write_score_profile()
        if ( not self.history_fh ): return

        # write old style history
        if self.p['history_file'] and self.p['old_style_history']:
            self.history_fh.write('#rand_conf_type: %s\n'%self.p['rand_conf_type'])
            if self.p['rand_conf_type'] == 0:
                self.history_fh.write('#rand_param: %s\n'%self.p['rand_param0'])
            elif self.p['rand_conf_type'] == 1:
                self.history_fh.write('#rand_param: %s\n'%self.p['rand_param1'])
            elif self.p['rand_conf_type'] == 2:
                self.history_fh.write('#rand_param: %s\n'%self.p['rand_param2'])
            elif self.p['rand_conf_type'] == 'backtor':
                self.history_fh.write('#rand_param: %s\n'%self.p['rand_param_backtor'])
            self.history_fh.write('#dcut: %f\n'%self.dcut)
            avg_dist = self.bank_distance.average()
            min_dist, max_dist = self.bank_distance.aminmax()
            self.history_fh.write('#average distance: %f\n'%avg_dist)
            self.history_fh.write('#min distance: %f\n'%min_dist)
            self.history_fh.write('#max distance: %f\n'%max_dist)
            self.history_fh.write('#min_dcut: %f\n'%self.min_dcut)
            self.history_fh.write('#energies of bank confs: %s\n'%' '.join(map(str,sorted(self.bscore))))
            self.history_fh.flush()

        # write the first history
        self.write_history_file()

    def write_history_file_header(self):
        if mpirank != 0:
            return
        if self.p['old_style_history']:
            self.history_fh.write('#Datetime: %s\n'%(datetime.datetime.now()))
            self.history_fh.write('#nbank: %d\n'%self.p['nbank'])
            for key in sorted(self.p):
                if key[:8] == 'perturb_':
                    self.history_fh.write('#%s: %s\n'%(key,self.p[key]))
            self.history_fh.write('#nbank_add: %d\n'%self.p['nbank_add'])
            self.history_fh.write('#nseed: %d\n'%self.p['nseed'])
            self.history_fh.write('#dcut_reduce: %f\n'%self.p['dcut_reduce'])
            self.history_fh.write('#dcut1: %f\n'%self.p['dcut1'])
            self.history_fh.write('#dcut2: %f\n'%self.p['dcut2'])
            self.history_fh.write('#dist_method: %s\n'%self.p['dist_method'])
            self.history_fh.write('#natom: %d\n'%self.xyzmap['natom'])
            self.history_fh.write('#nseq: %d\n'%self.xyzmap['nres'])
            tu.setxyz(self.initvar)
            tu.tinker.energy()
            decomp_energy = tu.get_ecomp()
            for i in range(len(decomp_energy[0])):
                self.history_fh.write('#decomp energy %s: %.2f\n'%(decomp_energy[0][i],decomp_energy[1][i]))
        pre = str(self.p['profile_precision'])
        fmt = '%6s %3s %10s %4s %4s %' + pre + 's %' + pre + 's %15s %4s %4s\n' 
        self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_energy','max_energy','nee','iuse','nbk'))
        self.history_fh.flush()

    def finalize(self):
        if self.p['history_file'] and self.p['old_style_history'] and mpirank == 0:
            self.history_fh.write('#e of final bank: %s\n'%' '.join(map(str,sorted(self.bscore))))
            self.history_fh.write('#Datetime: %s\n'%(datetime.datetime.now()))
        super(csa_TINKER,self).finalize()

    def check_param(self):
        super(csa_TINKER,self).check_param()
        # neb
        if self.p['neb'] and self.p['min_method'] != 'lbfgsb':
            self.print_error( "Only 'min_method: lbfgsb' supports 'neb'" )

    def get_energy(self,var):
        tu.setxyz(var) 
        return tu.tinker.energy()

    def initialize_neb(self):
        var0 = self.native_var[ self.p['neb_end'][0] ]
        ene0 = self.get_energy(var0)
        var1 = self.native_var[ self.p['neb_end'][1] ]
        ene1 = self.get_energy(var1)
        if self.p['neb_fit'] is None:
            fit_ind = np.arange(self.xyzmap['natom'])
        else:
            fit_ind = np.unique(self.p['neb_fit'])
        if self.p['neb_rms'] is None:
            rms_ind = np.arange(self.xyzmap['natom'])
        else:
            rms_ind = np.unique(self.p['neb_rms'])
        tu.tinker.neb.initialize_neb(self.p['neb_nseq'],self.p['neb_force'][0],self.p['neb_force'][1],
            self.p['neb_rcut'],self.p['neb_force'][2],var0,var1,ene0,ene1,fit_ind,rms_ind)
        self.neb_initialized = True 
