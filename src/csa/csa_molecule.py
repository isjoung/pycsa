"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

from builtins import str
from builtins import range
from .csa import csa, MPI, mpicomm, mpirank, mpisize, dict_merge
from . import mol_util
from . import mol_rand
from . import protein_rand
from . import mol_perturb
import perturb_util as pu
import os
import numpy as np
from copy import deepcopy
import zmatrix_c
import neb_util
try:
    import tmscore
    USE_TMSCORE_MODULE = True
except:
    import subprocess
    import tempfile
    USE_TMSCORE_MODULE = False

def calc_dihedral_distance(var1,var2):
    ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)
    ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)
    tor_diff = np.absolute( ztors1 - ztors2 )
    mask = (tor_diff>180.0)
    tor_diff[mask] = 360.0 - tor_diff[mask]
    return float(np.sum(tor_diff))

def write_temp_CA_pdb(ca):
    tpdb = tempfile.NamedTemporaryFile(suffix='.pdb')
    with open(tpdb.name,'w') as f:
        for i, c  in enumerate(ca):
            resn = i+1
            x, y, z = c
            print(f'ATOM   {resn:4d} CA   GLY  {resn:4d}    {x:8.3f}{y:8.3f}{z:8.3f}', file=f)
    return tpdb


class csa_molecule(csa):
    """csa base class for optimizing molecular conformation"""

    def randvarf(self):
        if self.p['neb']:
            nconf = self.p['neb_nseq']
            nseq = nconf

            ind0, ind1 = self.p['neb_end']
            var0 = self.native_var[ind0] # end-state0
            var1 = self.native_var[ind1] # end-state1
            natom = var0.shape[0]
            iz0, bond0, angle0, torsion0 = mol_util.getint(var0)
            iz1, bond1, angle1, torsion1 = mol_util.getint(var1)

            bond_new = (bond0+bond1)*0.5
            angle_new = (angle0+angle1)*0.5

            exchange_mask = (np.random.random(natom)<0.6).astype(np.uint8) # 60%
            ex_tor = np.random.random(natom)*360.0 - 180.0 # center

            tor_center, _, _ = pu.exchange_coord_int2(exchange_mask,iz0,torsion0,angle0,bond0,ex_tor,angle0,bond0) # this is the center

            newvar_list = [None] * nseq
            seq0 = int(nseq/2)
            for iseq in range(seq0): # var0 to center 
                gap = tor_center - torsion0
                gap -= np.floor(gap/360.0) * 360.0
                gap[gap>180.0] -= 360.0
                tor_new = torsion0 + gap*iseq/float(seq0)
                mol_util.setint(iz0,bond_new,angle_new,tor_new)
                newvar_list[iseq] = mol_util.getxyz()
            seq1 = nseq - seq0
            for iseq in range(seq1): # center to var1
                gap = torsion1 - tor_center
                gap -= np.floor(gap/360.0) * 360.0
                gap[gap>180.0] -= 360.0
                tor_new = tor_center + gap*iseq/float(seq1)
                mol_util.setint(iz0,bond_new,angle_new,tor_new)
                newvar_list[seq0+iseq] = mol_util.getxyz()

            newvar = np.array(newvar_list)
        else:
            if self.p['rand_init_bank']:
                if ( not len(self.init_bank_ind) ):
                    # reset init_bank_ind
                    self.init_bank_ind = list(range(len(self.init_bank)))
                # pick one from init_bank
                ind = np.random.randint(len(self.init_bank_ind))
                ind = self.init_bank_ind.pop(ind)
                ivar = self.init_bank[ind]
            else:
                ivar = self.initvar
            newvar = self.randomize_conf(ivar,nconf=None)

        return newvar

    def randomize_conf(self,ivar,nconf=None):
        rand_conf_type = self.p['rand_conf_type']
        if rand_conf_type == 0:
            newvar = self.rand_type0.randomize(ivar,multiconf=nconf)
        elif rand_conf_type == 1:
            newvar = self.rand_type1.randomize(ivar,multiconf=nconf)
        elif rand_conf_type == 'backtor':
            newvar = self.rand_backtor.randomize(ivar)
        elif rand_conf_type == 'receptor_ligand':
            newvar = self.rand_receptor_ligand.randomize(ivar)
        else:
            raise ValueError('unknown rand_conf_type %s'%str(rand_conf_type))
        return newvar

    def perturbvarf(self,ind1):
        natom = self.xyzmap['natom']
        newvars = []
        perturb_info = []
        iuse = self.count_unused_bank()
        if self.p['neb']:
            nconf = self.p['neb_nseq']
        else:
            nconf = None

        # int_cross
        nperturb = self.p['perturb_int_cross']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_int_cross.perturb(self.bvar,self.rvar,ind1,nperturb=nperturb,multiconf=nconf)
            newvars += newvar
            perturb_info += pinfo

        # int_residue
        nperturb = self.p['perturb_int_residue']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_int_residue.perturb(self.bvar,self.rvar,ind1,nperturb=nperturb,multiconf=nconf)
            newvars += newvar
            perturb_info += pinfo

        # car_residue
        nperturb = self.p['perturb_car_residue']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_car_residue.perturb(self.bvar,self.rvar,ind1,nperturb=nperturb,multiconf=nconf)
            newvars += newvar
            perturb_info += pinfo

        # torsion
        nperturb = self.p['perturb_torsion']['nperturb']
        if nperturb:
            if ( self.ncycle == 0 and iuse > len(self.bvar) - len(self.seed) - self.p['perturb_torsion']['n_new_bank_cut'] ):
                nmin, nmax = self.p['perturb_torsion']['n_range0']
                ( newvar, pinfo ) = self.perturb_torsion.perturb(self.bvar,self.rvar,ind1,src='rr',nperturb=nperturb,nmin=nmin,nmax=nmax,multiconf=nconf)
                newvars += newvar
                perturb_info += pinfo
            else:
                br_ratio = self.p['perturb_torsion']['br_ratio']
                n1 = int( nperturb * br_ratio )
                n2 = nperturb - n1
                nmin, nmax = self.p['perturb_torsion']['n_range1']
                ( newvar, pinfo ) = self.perturb_torsion.perturb(self.bvar,self.rvar,ind1,src='br',nperturb=n1,nmin=nmin,nmax=nmax,multiconf=nconf)
                newvars += newvar
                perturb_info += pinfo
                ( newvar, pinfo ) = self.perturb_torsion.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=n2,nmin=nmin,nmax=nmax,multiconf=nconf)
                newvars += newvar
                perturb_info += pinfo

        # car_cross
        nperturb = self.p['perturb_car_cross']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_car_cross.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=nperturb,multiconf=nconf)
            newvars += newvar
            perturb_info += pinfo

        # type_randint
        nperturb = self.p['perturb_randint']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_randint.perturb(self.bvar,self.rvar,ind1,src='b',nperturb=nperturb,multiconf=nconf)
            newvars += newvar
            perturb_info += pinfo

        # type_randcar
        nperturb = self.p['perturb_randcar']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_randcar.perturb(self.bvar,self.rvar,ind1,src='b',nperturb=nperturb,multiconf=nconf)
            newvars += newvar
            perturb_info += pinfo

        # trans_rot
        nperturb = self.p['perturb_trans_rot']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_trans_rot.perturb(self.bvar,self.rvar,ind1,src='b',nperturb=nperturb,multiconf=nconf)
            newvars += newvar
            perturb_info += pinfo

        # protein_perturb_backtor
        nperturb = self.p['protein_perturb_backtor']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.protein_perturb_backtor.perturb(self.bvar,ind1,nperturb=nperturb)
            newvars += newvar
            perturb_info += pinfo

        # protein_perturb_backtor_cg
        nperturb = self.p['protein_perturb_backtor_cg']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.protein_perturb_backtor_cg.perturb(self.bvar,ind1,nperturb=nperturb)
            newvars += newvar
            perturb_info += pinfo

        # type_average
        nperturb = self.p['perturb_average']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_average.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=nperturb,multiconf=nconf)
            newvars += newvar
            perturb_info += pinfo

        # native_torsion
        np = self.p['perturb_native_torsion']['nperturb']
        if np:
            native_ind = self.p['perturb_native_torsion']['native_ind']
            if native_ind is None:
                native_ind = list(range(len(self.native_var)))
            for ni in native_ind:
                ( newvar, pinfo ) = self.perturb_native_torsion.perturb(self.bvar,self.native_var,ind1,ni,src='b',nperturb=np,multiconf=nconf)
                newvars += newvar
                perturb_info += pinfo

        # native_car_cross
        np = self.p['perturb_native_car_cross']['nperturb']
        if np:
            native_ind = self.p['perturb_native_car_cross']['native_ind']
            if native_ind is None:
                native_ind = list(range(len(self.native_var)))
            for ni in native_ind:
                ( newvar, pinfo ) = self.perturb_native_car_cross.perturb(self.bvar,self.native_var,ind1,ni,src='b',nperturb=np,multiconf=nconf)
                newvars += newvar
                perturb_info += pinfo

        if self.p['neb']:
            newvar, pinfo = self.perturbvarf_multiconf(ind1)
            newvars += newvar
            perturb_info += pinfo

        return ( newvars, perturb_info )

    def perturbvarf_multiconf(self,ind1):
        newvars = []
        perturb_info = []
        # crossover
        nperturb = self.p['multiconf_perturb_cross']['nperturb']
        if nperturb:
            newvar, pinfo = self.multiconf_perturb_cross.perturb(self.bvar,self.rvar,ind1)
            newvars += newvar
            perturb_info += pinfo
        # residue exchange
        nperturb = self.p['multiconf_perturb_residue']['nperturb']
        if nperturb:
            iuse = self.count_unused_bank()
            if ( self.ncycle == 0 and iuse > len(self.bvar) - len(self.seed) - self.p['multiconf_perturb_residue']['n_new_bank_cut'] ):
                newvar, pinfo = self.multiconf_perturb_residue.perturb(self.bvar,self.rvar,ind1,src='rr',nperturb=nperturb)
                newvars += newvar
                perturb_info += pinfo
            else:
                br_ratio = self.p['multiconf_perturb_residue']['br_ratio']
                n1 = int( nperturb * br_ratio )
                n2 = nperturb - n1
                newvar, pinfo = self.multiconf_perturb_residue.perturb(self.bvar,self.rvar,ind1,src='br',nperturb=n1)
                newvars += newvar
                perturb_info += pinfo
                newvar, pinfo = self.multiconf_perturb_residue.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=n2)
                newvars += newvar
                perturb_info += pinfo
        # 2opt
        nperturb = self.p['multiconf_perturb_2opt']['nperturb']
        if nperturb:
            newvar, pinfo = self.multiconf_perturb_2opt.perturb(self.bvar,self.rvar,ind1)
            newvars += newvar
            perturb_info += pinfo
        # average
        nperturb = self.p['multiconf_perturb_average']['nperturb']
        if nperturb:
            nv0 = self.native_var[ self.p['neb_end'][0] ]
            nv1 = self.native_var[ self.p['neb_end'][1] ]
            newvar, pinfo = self.multiconf_perturb_average.perturb(self.bvar,self.rvar,nv0,nv1,ind1)
            newvars += newvar
            perturb_info += pinfo

        return newvars, perturb_info

    def minimize_init(self):
        super(csa_molecule,self).minimize_init()
        self.decomp_mscore = []

    def pack_min_input(self,i_list):
        min_input = []
        for i in i_list:
            try:
                rinfo = self.perturb_info[i][2] # restrain info
            except:
                rinfo = None
            min_input.append( (self.pvar[i],rinfo) )
            # destroy self.pvar[i] to save memory
            self.pvar[i] = None
        return min_input

    def unpack_min_output(self,min_output,i_list):
        for ind in range(len(i_list)):
            i = i_list[ind]
            mo = min_output[ind]
            score, minvar, decomp_ene = mo
            self.mvar.append(minvar)
            self.mscore.append(score)
            try:
                self.perturb_info.append(self.perturb_info_backup[i])
            except:
                pass
            self.decomp_mscore.append(decomp_ene)
            self.print_log("m%d score: %.5E"%(len(self.mvar),score), level=3)

    def comparef(self,var1,var2):
        if self.p['neb']:
            dist = 0.0
            for i in range(self.p['neb_nseq']):
                dist += self.get_distance(var1[i],var2[i])
        else:
            dist = self.get_distance(var1,var2)
        return dist

    def get_distance(self,var1,var2):
        if self.p['dist_method'] is None:
            return 0.0
        elif ( self.p['dist_method'] == 'rmsd' ):
            # rmsd
            try:
                distance, newvar2 = pu.calc_rmsd(var1,var2)
            except:
                distance =99.9
        elif self.p['dist_method'] == 'tmscore':
            distance = self.calc_tmscore(var1, var2)
        else:
            distance = calc_dihedral_distance(var1,var2)

        return distance

    def eval_update_single(self,i):
        if self.p['update_cut'] is not None:
            if (     self.p['update_cut_method'] == 'rmsd'
                 and self.calc_rmsd_valid_atom(self.mvar[i],self.native_var[0]) > self.p['update_cut']):
                self.print_log( "m%d is removed by rmsd cut"%i, level=3 )
                return
            elif ( self.p['update_cut_method'] == 'tmscore'
                   and self.calc_tmscore(self.mvar[i],self.native_var[0],native=True) < self.p['update_cut']
            ):
                self.print_log( "m%d is removed by TMscore cut"%i, level=3 )
                return

        # tmsocre_cut is deprecated but still works for backward compatibility
        # Use update_cut and update_cut_method instead
        tmscore_cut = self.p['tmscore_cut']
        if ( tmscore_cut is not None
             and self.calc_tmscore(self.mvar[i], self.native_var[0],
                 native=True) < tmscore_cut
        ):
            self.print_log( "m%d is removed by TMscore cut"%i, level=3 )
            return

        super(csa_molecule,self).eval_update_single(i)

    def eval_update_final(self):
        super(csa_molecule,self).eval_update_final()

        if mpirank == 0:
            count = {} # number of trials generated by the perturb_name
            accept = {} # number of accepted trials generated by the perturb_name
            counter = [ None ] * len(self.update_mvar) # residue source counter
            for i in range(len(self.update_mvar)):
                # calculate acceptace ratio
                try:
                    # dict-style perturb_info
                    pname = self.perturb_info[i]['name']
                except:
                    pname = self.perturb_info[i][0]
                status = self.update_mvar[i]
                if ( pname not in count ):
                    count[pname] = 0
                if ( pname not in accept ):
                    accept[pname] = 0
                if ( status >= 0 ):
                    accept[pname] += 1
                count[pname] += 1

                if isinstance(self.perturb_info[i],dict):
                    # dict-style perturb_info
                    counter[i] = self.perturb_info[i]['ratio']
                else:
                    res_source = self.perturb_info[i][1]
                    counter[i] = {}
                    for rs in res_source:
                        try:
                            rs = tuple(rs)
                        except:
                            rs = (None,None)
                        if len(rs) == 3:
                            # ( src, bind, ratio )
                            counter[i][(rs[0],rs[1])] = rs[2]
                        else:
                            # len(rs) == 2
                            # ( src, bind )
                            if ( rs[1] == None ): continue
                            try:
                                counter[i][rs] += 1
                            except:
                                counter[i][rs] = 1

            # print acceptance ratio
            for pname in sorted( count.keys() ):
                if ( count[pname] == 0 ):
                    ratio = 0.0
                else:
                    ratio = float(accept[pname]) / float(count[pname])
                try:
                    self.accept_ratio[pname] = ratio
                except:
                    self.accept_ratio = {}
                    self.accept_ratio[pname] = ratio
                self.print_log( "acceptance ratio of %s is %f"%(pname,ratio), rank=mpirank )

            # print detailed perturb info
            nbscore = len(self.bscore)
            s = []
            ds = []
            for i in range(len(self.update_bvar)):
                mi = self.update_bvar[i]
                if ( mi < 0 ): continue
                pi = self.perturb_info[mi]
                pname = pi[0]
                res_source = pi[1]
                cmi = counter[mi]
                nres = float(len(res_source))
                line = 'new b%d: %s'%(i+1,pname)
                if isinstance(self.perturb_info[mi],dict):
                    # dict-style perturb_info
                    for key in sorted(cmi):
                        bank_type, bank_ind = key
                        line += ' %s%d(%.3f)'%(bank_type,bank_ind+1,cmi[key])
                else:
                    for rs in sorted(cmi):
                        try:
                            #line += ' %s%d(%.3f)'%(rs[0],rs[1]+1,cmi[rs]/nres)
                            line += ' %s%d(%.3f)'%(rs[0],rs[1]+1,cmi[rs])
                        except:
                            #line += ' %s%s(%.3f)'%(rs[0],str(rs[1]),cmi[rs]/nres)
                            line += ' %s%s(%.3f)'%(rs[0],str(rs[1]),cmi[rs])
                if ( i < nbscore ):
                    s.append(self.mscore[mi]) 
                    delta_score = self.bscore[i]-self.mscore[mi]
                    ds.append(delta_score)
                    line += ' S(%.7E) dS(%.7E)'%(self.mscore[mi],delta_score)
                else:
                    line += ' S(%.7E)'%(self.mscore[mi])
                self.print_log(line)
            if s:
                average_s = np.average(s)
            else:
                average_s = 0.0
            if ds:
                average_ds = np.average(ds)
            else:
                average_ds = 0.0
            try:
                self.print_log('Average S: %.3f dS: %.3f'%(average_s,average_ds))
            except:
                print('ERROR')
                print(average_s, average_ds)

    def write_bank_history(self):
        if ( mpirank == 0 and self.bank_history_fh ):
            if self.bank_history_header is False:
                self.write_bank_history_file_header()
                self.bank_history_header = True
            f = self.bank_history_fh
            fmt = ' %' + str(self.p['bank_history_precision']) + '.' + str(self.p['bank_history_precision']-7) + 'E'
            #try:
            #    e_name = self.decomp_energy_name
            #except:
            #    e_name = self.decomp_bscore[0][0]
            e_name = self.decomp_bscore[0][0]
            for mi in range(len(self.update_mvar)):
                bi = self.update_mvar[mi]
                if ( bi < 0 ): continue
                f.write('%6d %4d'%(self.niter,bi+1))
                if self.p['neb']:
                    for nv in self.native_var:
                        for iseq in range(self.p['neb_nseq']):
                            dist = self.get_bank_history_distance(self.mvar[mi][iseq],nv)
                            f.write(fmt%dist)
                else:
                    for nv in self.native_var:
                        dist = self.get_bank_history_distance(self.mvar[mi],nv)
                        f.write(fmt%dist)
                for i in range(len(e_name)):
                    try:
                        f.write(fmt%self.decomp_mscore[mi][1][i])
                    except:
                        f.write(' ' + '0' * self.p['bank_history_precision'])
                f.write('\n')
            f.flush()

    def get_bank_history_distance(self,var1,var2):
        if self.p['bank_history_dist_method'] == 'rmsd':
            dist = self.calc_rmsd_valid_atom(var1,var2)
        elif self.p['bank_history_dist_method'] == 'rmsd_ca':
            dist = self.calc_rmsd_ca(var1,var2)
        elif self.p['bank_history_dist_method'] == 'rmsd_ha':
            ha_index = self.get_HA_index()
            dist = pu.calc_rmsd(var1[ha_index,:],var2[ha_index,:])[0]
        elif self.p['bank_history_dist_method'] == 'tmscore':
            dist = self.calc_tmscore(var1, var2, native=True)
        else:
            self.print_error("Unknown bank_history_distance: %s"%self.p['bank_history_dist_method']) 
        return dist

    def write_rbank_history(self,nexclude=0):
        nbvar2 = len(self.bvar)
        if mpirank == 0 and self.bank_history_fh:
            if self.bank_history_header is False:
                self.write_bank_history_file_header()
                self.bank_history_header = True
            f = self.bank_history_fh
            fmt = ' %' + str(self.p['bank_history_precision']) + '.' + str(self.p['bank_history_precision']-7) + 'E'
            #try:
            #    e_name = self.decomp_energy_name
            #except:
            #    e_name = self.decomp_bscore[0][0]
            e_name = self.decomp_bscore[0][0]
            for bi in range(nexclude,nbvar2):
                f.write('%6d %4d'%(self.niter,bi+1))
                if self.p['neb']:
                    for nv in self.native_var:
                        for iseq in range(self.p['neb_nseq']):
                            dist = self.get_bank_history_distance(self.bvar[bi][iseq],nv)
                            f.write(fmt%dist)
                else:
                    for nv in self.native_var:
                        dist = self.get_bank_history_distance(self.bvar[bi],nv)
                        f.write(fmt%dist)
                for i in range(len(e_name)):
                    try:
                        f.write(fmt%self.decomp_bscore[bi][1][i])
                    except:
                        f.write(' ' + '0' * self.p['bank_history_precision'])
                f.write('\n')
            f.flush()

    def update_bank(self):
        n_delta = super(csa_molecule,self).update_bank()
        if ( mpirank == 0 ):
            #n_delta = len(self.update_bvar) - len(self.decomp_bscore)
            if n_delta:
                self.decomp_bscore += [ None ] * n_delta
            # update decompsed energy
            try:
                self.decomp_bscore # exists?
            except:
                self.decomp_bscore = [ None ] * len(self.bvar)
            for i in range( len(self.update_bvar) ):
                ind = self.update_bvar[i]
                if ind < 0: continue
                self.decomp_bscore[i] = self.decomp_mscore[ind]

        return n_delta

    def perturbvar(self):
        super(csa_molecule,self).perturbvar()

        if ( mpirank != 0 ): return

        if ( self.p['minimize_seed'] ):
            # add seed conformations to the perturbed conformation list
            for ind1 in self.seed:
                ( newvar, pinfo ) = self.mol_perturb_type0.perturb(self.bvar,self.rvar,ind1)
                self.mvar += newvar
                self.perturb_info += pinfo
        if self.p['minimize_inv_seed']:
            # add inversed seed conformations to the perturbed conformation list
            for ind1 in self.seed:
                ( newvar, pinfo ) = self.mol_perturb_inv.perturb(self.bvar,self.rvar,ind1)
                self.mvar += newvar
                self.perturb_info += pinfo
        if self.p['fix_residue'] is not None or self.p['fix_index'] is not None:
            self.fix_mvar_residue()

    def randvar(self,nconf=None,varlist=None):
        if ( self.p['rand_init_bank'] ):
            # store init bank into self.init_bank for randomization
            #self.init_bank = read_archive_coord( self.p['init_bank'] ) 
            self.init_bank = self.read_archive_coord()
            self.init_bank_ind = list(range(len(self.init_bank))) 
        super(csa_molecule,self).randvar(nconf=nconf,varlist=varlist)
        if ( self.p['rand_init_bank'] ):
            # remove unnecessary variables
            del self.init_bank
            del self.init_bank_ind
        #if self.p['neb']:
        #    # reorder multiconf
        #    nv0 = self.native_var[self.p['neb_end'][0]]
        #    nv1 = self.native_var[self.p['neb_end'][1]]
        #    for i in xrange(len(self.mvar)):
        #        mvar = self.mvar[i]
        #        nconf = len(mvar)
        #        d0 = np.empty(nconf)
        #        d1 = np.empty(nconf)
        #        for iconf in xrange(nconf):
        #            d0[iconf], _ = pu.calc_rmsd(nv0,mvar[iconf])
        #            d1[iconf], _ = pu.calc_rmsd(nv1,mvar[iconf])
        #        ind = np.argsort(d0-d1)
        #        newmvar = [None]*nconf
        #        for iconf in xrange(nconf):
        #            newmvar[iconf] = mvar[ind[iconf]]
        #        self.mvar[i] = newmvar
        if self.p['fix_residue'] is not None or self.p['fix_index'] is not None:
            self.fix_mvar_residue()

    def fix_mvar_residue(self):
        fixind = self.fixind
        nonfix_mask = np.ones( self.xyzmap['natom'], dtype=bool )
        nonfix_mask[fixind] = False
        for i in range(len(self.mvar)):
            if self.p['neb']:
                imv = self.mvar[i]
                for iseq in range(self.p['neb_nseq']):
                    var1 = imv[iseq]
                    var2 = self.initvar
                    # rmsd fit
                    c_trans, U, ref_trans = pu.rms_fit(var2[nonfix_mask,:],var1[nonfix_mask,:])
                    newvar = np.dot( var1 - c_trans, U ) + ref_trans 
                    newvar[fixind,:] = var2[fixind,:]
                    self.mvar[i][iseq] = newvar
            else:
                var1 = self.mvar[i]
                var2 = self.initvar
                # rmsd fit
                c_trans, U, ref_trans = pu.rms_fit(var2[nonfix_mask,:],var1[nonfix_mask,:])
                newvar = np.dot( var1 - c_trans, U ) + ref_trans 
                newvar[fixind,:] = var2[fixind,:]
                self.mvar[i] = newvar
   
    def __init__(self,param={}):
        p = {
            # basic csa parameters
            'nbank': 50,
            'nbank_add': 30,
            'nseed': 30,

            # termination
            'icmax': 3,  # If ncycle reaches icmax, iteration stops
            'iucut': 10, # the number of bank update is below iucut, ncycle increments 

            # measuring distances between conformations
            # dihedral: dihedral distance
            # rmsd: root-mean-square distance
            'dist_method': 'dihedral',

            # dcut
            'dcut_reduce': 0.997252158427478, # dcut is reduced by dcut_reduce every iteration 0.4**(1./333) = 0.997...

            # restart
            'restart_file': 'csa_restart.dat',
            'save_restart': 1,
            'restart_mode': 2,

            'init_bank': None,
            'include_init_bank': True, # add init_bank to rbank
            'rand_init_bank': False, # randomize conformations in init_bank. if False, the conformations are
                                     # added without perturbation
            'grdmin': 0.1, # minimum RMS gradient per atom 

            # log
            'report_min': 0, # report minimization step frequency
            'history_file': 'history', # history filename
            'append_history': False, # to force appending to the existing history file
            'write_pdb': True, # writes pdb files
            'append_coord': False, # append coordinates to bank pdb files
            'write_xyz': False,
            'write_score_profile': True, # writes energy profile
            'profile_file': 'profile.energy',
            'record_gmin': False, # save gmin###.pdb

            # randomize/rbank
            # random conformation parameters
            'rand_conf_type': 0,
            # rand_conf_type == 0: numbers are sigmas of bond, angle, and torsions
            #     if angle or torsion is None, it is randomly perturbed
            # rand_conf_type == 1: numbers are maximum distances of x, y, and z
            # rand_conf_type == 2: numbers are sigma distances of x, y and z
            'rand_param0': { 'sigma_bond': 0.1, 'sigma_angle': 10.0, 'sigma_torsion': None, 'discrete_omega': True, 'residue_ratio': 1.0, 'res_index': None },
            'rand_param1': { 'max_x': 3.0, 'max_y': 3.0, 'max_z': 3.0, 'res_index': None },
            'rand_param_receptor_ligand': { 'ligand_mol': 2, 'receptor_max_coord': 1.0, 'ligand_max_coord': 10.0, 'rand_radius': 20.0 },

            # perturbation
            'perturb_int_cross': { 'nperturb': 0, 'res_index': None, 'partner_mask': [] },
            'perturb_int_residue': { 'nperturb': 0, 'res_index': None, 'partner_mask': [] },
            'perturb_car_residue': { 'nperturb': 0, 'res_index': None, 'partner_mask': [] },
            'perturb_torsion': { 'nperturb': 0, 'res_index': None, 'n_range0': (3,7), 'n_range1': (1,5), 'partner_mask': [], 'br_ratio': 0.5, 'n_new_bank_cut': 5 },
            'perturb_car_cross': { 'nperturb': 0, 'res_index': None, 'partner_mask': [], 'nmin': 1, 'nmax': None },
            'perturb_randint': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': None, 'max_ratio': 0.02,
                                 'sigma_bond': 0.001, 'sigma_angle': 0.001, 'sigma_torsion': 5.0 },
            'perturb_randcar': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': None, 'max_ratio': 0.02, 'sigma': 0.5 },
            'perturb_trans_rot': { 'nperturb': 0, 'res_index': None, 'sigma_trans': 0.5, 'sigma_rot': 10.0 },
            'protein_perturb_backtor': { 'nperturb': 0, 'res_index': None, 'nmax': 3, 'nmin': 1, 'backtor_file': None, 'sigma_torsion': 1.0, 'weighted_frag': False, 'max_energy_diff': None },
            'protein_perturb_backtor_cg': { 'nperturb': 0, 'res_index': None, 'nmax': 1, 'nmin': 1, 'backtor_file': None, 'sigma_torsion': 1.0, 'sigma_angle': 1.0, 'weighted_frag': False, 'max_energy_diff': None },
            'perturb_average': { 'nperturb': 0, 'partner_mask': [] },
            'perturb_native_torsion': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': None, 'max_ratio': 0.5, 'native_ind': None },
            'perturb_native_car_cross': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': None, 'max_ratio': 0.5, 'native_ind': None },

            # multiconf perturb
            'multiconf_perturb_cross' : { 'nperturb': 0, 'nmin': 1, 'max_ratio': 0.4, 'max_shift_ratio': 0.3, 'partner_mask': [] },
            'multiconf_perturb_residue' : { 'nperturb': 0, 'nmin': 1, 'nmax': 3, 'partner_mask': [], 'max_shift_ratio': 0.3, 'n_new_bank_cut': 5, 'br_ratio': 0.1 },
            'multiconf_perturb_2opt' : { 'nperturb': 0 },
            'multiconf_perturb_average' : { 'nperturb': 0 },

            # perturb
            'minimize_seed': True, # seed variables are added to the perturbed variable lists
            'minimize_inv_seed': False, # inversed seed variables are added to the perturbed variable lists
            'minimize_chiral': False,
            'weighted_partner': False,

            # minimization method
            'temperature': 0.0,
            'boltzmann_const': 1.9872066e-3,

            # fix residue
            'fix_residue': None,
            'fix_index': None,

            # record bank energy and distance history
            'bank_history': None,
            'bank_history_dist_method': 'rmsd',

            # update is rejected if the condition is met
            'update_cut': None,
            'update_cut_method': 'rmsd',
            # tmscore cut (deprecated DON'T USE)
            'tmscore_cut': None,

            # metadynamics
            'ap_dist_method': 'rmsd',

            # nudged elastic band (NEB)
            'neb_fit': None, # atom index list or None, None means 'all atoms'
            'neb_rms': None, # atom index list or None, None means 'all atoms'
        }

        self.p = dict_merge(p,param)

        # init for neb
        self.neb_initialized = False

        # init for csa parameter
        super(csa_molecule,self).__init__(self.p)

    def init_accessor(self):
        self.read_xyzmap()
        super(csa_molecule,self).init_accessor()

        # read xyzmapfile
        mol_util.set_connect(self.xyzmap['connect'])

        # initialize randomize types
        self.rand_type0 = mol_rand.mol_rand_type0(self.p['rand_param0'],self.xyzmap)
        self.rand_type1 = mol_rand.mol_rand_type1(self.p['rand_param1'],self.xyzmap)
        #self.protein_rand_backtor = protein_rand.protein_rand_backtor(self.p['protein_rand_backtor'],self.xyzmap,self.torsion_angle)
        if self.p['rand_conf_type'] == 'receptor_ligand':
            self.rand_receptor_ligand = mol_rand.mol_rand_receptor_ligand(self.p['rand_param_receptor_ligand'],self.xyzmap) 

        # perturb type0
        # type0 simply add the seed conformation
        self.mol_perturb_type0 = mol_perturb.mol_perturb_type0({'nperturb':1},self.xyzmap)

        # multiconf perturb
        if self.p['neb']:
            self.multiconf_perturb_cross = mol_perturb.multiconf_perturb_cross(self.p['multiconf_perturb_cross'],self.p['neb_nseq'])
            self.multiconf_perturb_residue = mol_perturb.multiconf_perturb_residue(self.p['multiconf_perturb_residue'],self.p['neb_nseq'])
            self.multiconf_perturb_2opt = mol_perturb.multiconf_perturb_2opt(self.p['multiconf_perturb_2opt'],self.p['neb_nseq'])
            self.multiconf_perturb_average = mol_perturb.multiconf_perturb_average(self.p['multiconf_perturb_average'],self.p['neb_nseq'])

        # initialize perturb types
        self.perturb_int_cross = mol_perturb.mol_perturb_int_cross(self.p['perturb_int_cross'],self.xyzmap)
        self.perturb_int_residue = mol_perturb.mol_perturb_int_residue(self.p['perturb_int_residue'],self.xyzmap)
        self.perturb_car_residue = mol_perturb.mol_perturb_car_residue(self.p['perturb_car_residue'],self.xyzmap)
        self.perturb_torsion = mol_perturb.mol_perturb_torsion(self.p['perturb_torsion'],self.xyzmap)
        self.perturb_car_cross = mol_perturb.mol_perturb_car_cross(self.p['perturb_car_cross'],self.xyzmap)
        self.perturb_randint = mol_perturb.mol_perturb_randint(self.p['perturb_randint'],self.xyzmap)
        self.perturb_randcar = mol_perturb.mol_perturb_randcar(self.p['perturb_randcar'],self.xyzmap)
        self.perturb_trans_rot = mol_perturb.mol_perturb_trans_rot(self.p['perturb_trans_rot'],self.xyzmap)
        self.perturb_average = mol_perturb.mol_perturb_average(self.p['perturb_average'],self.xyzmap)
        self.perturb_native_torsion = mol_perturb.mol_perturb_native_torsion(self.p['perturb_native_torsion'],self.xyzmap)
        self.perturb_native_car_cross = mol_perturb.mol_perturb_native_car_cross(self.p['perturb_native_car_cross'],self.xyzmap)
        self.protein_perturb_backtor = mol_perturb.protein_perturb_backtor(self.p['protein_perturb_backtor'],self.xyzmap,energy_func=None)
        self.protein_perturb_backtor_cg = mol_perturb.protein_perturb_backtor_cg(self.p['protein_perturb_backtor_cg'],self.xyzmap,energy_func=None)

        # perturb inverse
        self.mol_perturb_inv = mol_perturb.mol_perturb_inv({'nperturb':1},self.xyzmap)

        # neb
        if self.p['neb'] and not self.neb_initialized:
            self.initialize_neb()

        # read the names of the decomposed energies
        if self.p['neb']:
            self.decomp_energy_name = [None] * (self.p['neb_nseq']*2+5)
            self.decomp_energy_name[0] = 'total'
            self.decomp_energy_name[1] = 'esum'
            self.decomp_energy_name[2] = 'ssum1' # spring ene1: chain length difference
            self.decomp_energy_name[3] = 'ssum2' # spring ene2: absolute chain length
            self.decomp_energy_name[4] = 'ssum3' # spring ene3: self-avoiding term
            ind = 5
            for i in range(self.p['neb_nseq']):
                self.decomp_energy_name[ind] = 'e%d'%i # individual total energy
                ind += 1
                self.decomp_energy_name[ind] = 's%d'%i # individual spring energy
                ind += 1
            self.decomp_energy_name = tuple(self.decomp_energy_name)

        # fix index
        self.init_fixind()


    def init_fixind(self):
        fixind = []
        if self.p['fix_residue'] is not None:
            for resind in self.p['fix_residue']:
                resnum = resind + 1
                resatoms = self.xyzmap['data'][resnum]
                for atomname in resatoms:
                    fixind.append( resatoms[atomname][0] - 1 )
        if self.p['fix_index'] is not None:
            fixind += list(self.p['fix_index'])
        self.fixind = np.array(sorted(set(fixind)))


    def write_history_file(self):
        if hasattr(self,'minimize_nee'):
            pre = self.p['profile_precision']
            pre2 = pre - 7
            numfmt = '%' + str(pre) + '.' + str(pre2) + 'E'
            self.print_log( "Writing history", level=4 )
            fmt = "%6d %3d %10.4E %4d %4d " + numfmt + " " + numfmt + " %15d %4d %4d\n"
            self.history_fh.write(fmt
                %(self.niter,self.ncycle,self.dcut,self.minscore_ind+1,self.maxscore_ind+1,self.minscore,self.maxscore,self.minimize_nee,self.count_unused_bank(),self.count_bank()))
            self.history_fh.flush()
        else:
            super(csa_molecule,self).write_history_file()

    def savepdb(self,prefix,vars,energy=None,decomp_energy=None):
        var_size = len(vars)
        outfmt = '%s%0' + '%d'%(max(int(np.log10(var_size))+1,3)) + 'd.pdb'
        if self.p['append_coord']:
            mode = 'a'
        else:
            mode = 'w'
        for i in range(var_size):
            outfile = outfmt%(prefix,i+1)
            try:
                ene = energy[i]
            except:
                ene = None
            try:
                decomp_ene = []
                for iseq in range(self.p['neb_nseq']):
                    decomp_ene.append(decomp_energy[i][1][iseq*2+5])
            except:
                decomp_ene = None
            self.write_pdb(outfile,vars[i],energy=ene,decomp_energy=decomp_ene,mode=mode)
        if ( energy ):
            imin = energy.index(min(energy))
            self.write_pdb('gmin.pdb',vars[imin],energy=energy[imin],mode=mode)

    def write_pdb(self,pdbfile,var,energy=None,decomp_energy=None,mode='w'):
        self.print_log('Writing %s'%(pdbfile),level=5)
        try:
            sorted_amap = self.sorted_amap
        except:
            sorted_amap = sorted( self.xyzmap['raw_data'], key=lambda x: x[1] )
            self.sorted_amap = sorted_amap
        with open(pdbfile,mode) as pdbf:
            if energy is not None:
                pdbf.write('REMARK  Total Energy= %f\n'%energy)
                if hasattr(self,'niter'):
                    pdbf.write('REMARK  Iteration %d\n'%self.niter)
            if self.p['neb']:
                for iseq in range(self.p['neb_nseq']):
                    pdbf.write('REMARK  SEQ %4d\n'%(iseq+1))
                    if decomp_energy is not None:
                        pdbf.write('REMARK  Frame Energy= %f\n'%decomp_energy[iseq])
                    va = var[iseq]
                    for j in sorted_amap:
                        keyword = 'ATOM' 
                        xyz = va[j[0]-1,:]
                        if ( len(j[2]) <= 3 ):
                            atmname = ' ' + j[2]
                        else:
                            atmname = j[2]
                        pdbf.write('%-6s%5d %-4s %-3s  %4d    %8.3f%8.3f%8.3f\n'%(
                            keyword,
                            j[1],
                            atmname,
                            j[3],
                            j[4],
                            xyz[0],
                            xyz[1],
                            xyz[2],
                        ))
                    pdbf.write('END\n')
            else:
                for j in sorted_amap:
                    keyword = 'ATOM' 
                    xyz = var[j[0]-1,:]
                    if ( len(j[2]) <= 3 ):
                        atmname = ' ' + j[2]
                    else:
                        atmname = j[2]
                    pdbf.write('%-6s%5d %-4s %-3s  %4d    %8.3f%8.3f%8.3f\n'%(
                        keyword,
                        j[1],
                        atmname,
                        j[3],
                        j[4],
                        xyz[0],
                        xyz[1],
                        xyz[2],
                    ))
                pdbf.write('END\n')

    def write_history_file_header(self):
        pre = str(self.p['profile_precision'])
        fmt = '%6s %3s %10s %4s %4s %' + pre + 's %' + pre + 's %15s %4s %4s\n' 
        self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_energy','max_energy','nee','iuse','nbk'))
        self.history_fh.flush()

    def restart_key_list(self):
        return super(csa_molecule,self).restart_key_list() + [ 'decomp_bscore', 'minimize_nee' ]

    def pick_rbank(self,npick):
        sel_idx_list = super(csa_molecule,self).pick_rbank(npick)
        if mpirank == 0:
            for x in sel_idx_list:
                self.decomp_bscore.append( deepcopy(self.decomp_mscore[x]) ) 
        return sel_idx_list

    def sort_mvar(self):
        ind = super(csa_molecule,self).sort_mvar()
        if mpirank == 0:
            self.decomp_mscore = [ self.decomp_mscore[x] for x in ind ]

    def remove_bank(self,r_ind):
        b_ind = np.ones(len(self.bvar),dtype=bool)
        b_ind[r_ind] = False
        b_ind = np.where(b_ind)[0].tolist()
        if mpirank == 0:
            self.decomp_bscore = [ self.decomp_bscore[bi] for bi in b_ind ]
            self.decomp_mscore = self.decomp_bscore

        b_ind = super(csa_molecule,self).remove_bank(r_ind)

        return b_ind

    def write_bank_history_file_header(self):
        f = self.bank_history_fh
        fsize = self.p['bank_history_precision']
        fmt = ' %' + str(fsize) + 's'
        distname = self.get_bank_history_distance_name()
        f.write("#niter bank")
        if self.p['neb']:
            for i in range(len(self.native_var)):
                for j in range(self.p['neb_nseq']):
                    dn = '%d,%d%s'%(i,j,distname)
                    dn = dn[:fsize]
                    f.write(fmt%dn)
        else:
            for i in range(len(self.native_var)):
                f.write(fmt%distname[:fsize])
        e_name = self.decomp_bscore[0][0]
        for i in range(len(e_name)):
            f.write(fmt%e_name[i])
        f.write('\n')
        f.flush()

    def get_bank_history_distance_name(self):
        if self.p['bank_history_dist_method'] == 'rmsd':
            name = 'rmsd'
        elif self.p['bank_history_dist_method'] == 'rmsd_ha':
            name = 'rmsdHA'
        elif self.p['bank_history_dist_method'] == 'rmsd_ca':
            name = 'rmsdCA'
        elif self.p['bank_history_dist_method'] == 'tmscore':
            name = 'TMscore'
        else:
            self.print_error("Unknown bank_history_distance: %s"%self.p['bank_history_dist_method']) 
        return name

    def get_valid_atom_index(self):
        try:
            valid_atom_index = self.valid_atom_index
            return valid_atom_index
        except:
            valid_atom_index = []
            for al in self.xyzmap['raw_data']:
                ai = al[0]-1
                try:
                    # if native_var is defined, check the coordinate
                    if self.native_var[0][ai,0] >= 9999.0: # null value?
                        continue
                except:
                    pass
                valid_atom_index.append(ai)
            self.valid_atom_index = valid_atom_index
        return valid_atom_index

    def get_HA_index(self):
        try:
            ha_index = self.ha_index
            return ha_index
        except:
            ha_index = []
            for ai in self.get_valid_atom_index():
                if self.xyzmap['raw_data'][ai][2][0] != 'H':
                    ha_index.append(ai)
            self.ha_index = ha_index
        return ha_index

    def get_CA_index(self):
        try:
            ca_index = self.ca_index
            return ca_index
        except:
            ca_index = []
            for ai in self.get_valid_atom_index():
                if self.xyzmap['raw_data'][ai][2] == 'CA':
                    ca_index.append(ai)
            self.ca_index = ca_index
        return ca_index

    def get_heavy_index(self):
        """ return the indexes of heavy atoms (not hydrogen) """
        try:
            heavy_index = self.heavy_index
            return heavy_index
        except:
            heavy_index = []
            for ai in self.get_valid_atom_index():
                if self.xyzmap['raw_data'][ai][2][0] == 'H':
                    continue
                heavy_index.append(ai)
            self.heavy_index = heavy_index
        return heavy_index

    def calc_rmsd_valid_atom(self,var1,var2):
        a_index = self.get_valid_atom_index()
        c1 = var1[a_index,:] 
        c2 = var2[a_index,:]
        dist = pu.calc_rmsd(c1,c2)[0]
        return dist

    def calc_tmscore(self, var1, var2, native=False):
        if np.any( np.isnan(var1) ) or np.any( np.isnan(var2) ):
            return 0.0
        ca_index = self.get_CA_index()
        if native:
            if ( hasattr(self,'native_pdb_index')
                    and self.native_pdb_index is not None ):
                ca_index = np.intersect1d( np.array(ca_index),
                        self.native_pdb_index )
        ca1 = var1[ca_index,:]
        ca2 = var2[ca_index,:]

        if USE_TMSCORE_MODULE:
            tm = tmscore.tmscore(ca1,ca2)[0]
        else:
            p1 = write_temp_CA_pdb(ca1)
            p2 = write_temp_CA_pdb(ca2)
            try:
                p = subprocess.Popen( ['tmscore', p1.name, p2.name],
                        stdout=subprocess.PIPE )
            except FileNotFoundError:
                self.print_error("Cannot call 'tmscore'")
            else:
                p.wait()
                tm = 0.0
                for line in p.stdout:
                    line = line.decode()
                    if line.startswith('TM-score'):
                        tm = float(line.split()[2])

        return tm

    def calc_rmsd_ca(self,var1,var2):
        ca_index = self.get_CA_index()
        ca1 = var1[ca_index,:]
        ca2 = var2[ca_index,:]
        dist = pu.calc_rmsd(ca1,ca2)[0]
        return dist

    def write_score_profile(self):
        #try:
        #    e_name = self.decomp_energy_name
        #except:
        #    e_name = self.decomp_bscore[0][0]
        e_name = self.decomp_bscore[0][0]
        f = open(self.p['profile_file'],'w')
        f.write('#bnk') 
        fsize = self.p['bank_history_precision']
        fmt = ' %' + str(fsize) + 's'

        distname = self.get_bank_history_distance_name()
        if self.p['neb']:
            if hasattr(self,'native_var'):
                for i in range(len(self.native_var)):
                    for j in range(self.p['neb_nseq']):
                        dn = '%d,%d%s'%(i,j,distname)
                        dn = dn[:fsize]
                        f.write(fmt%dn)
        else:
            if hasattr(self,'native_var'):
                for i in range(len(self.native_var)):
                    f.write(fmt%distname[:fsize])

#        distname = self.get_bank_history_distance_name()
#        f.write(' %12s' %distname)

        for i in range(len(e_name)):
            f.write(' %12s'%e_name[i])
        f.write('\n')

        fmt = ' %' + str(self.p['bank_history_precision']) + '.' + str(self.p['bank_history_precision']-7) + 'E'
        for j in range(len(self.bvar)):
            f.write('%4d'%(j+1))
            if self.p['neb']:
                if hasattr(self,'native_var'):
                    for nv in self.native_var:
                        for iseq in range(self.p['neb_nseq']):
                            dist = self.get_bank_history_distance(self.bvar[j][iseq],nv)
                            f.write(fmt%dist)
            else:
                if hasattr(self,'native_var'):
                    for nv in self.native_var:
                        dist = self.get_bank_history_distance(self.bvar[j],nv)
                        f.write(fmt%dist)

            for i in range(len(e_name)):
                try:
                    f.write(' %12.5E'%self.decomp_bscore[j][1][i])
                except:
                    f.write(' 000000000000')
            f.write('\n')
        f.close()

    def get_adaptive_potential_distance(self,var):
        distance = np.empty(len(self.p['ap_range']),dtype=float)
        for i in range(len(distance)):
            if self.p['ap_dist_method'] == 'rmsd':
                distance[i] = pu.calc_rmsd(var,self.native_var[i])[0]
            elif self.p['ap_dist_method'] == 'dihedral':
                # calculates dihedral angle distance
                ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var)
                ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(self.native_var[i])

                tor_diff = np.absolute( ztors1 - ztors2 )
                ind = np.where(tor_diff>180.0)
                tor_diff[ind] = 360.0 - tor_diff[ind]
                distance[i] = np.sum(tor_diff)
            else:
                self.print_error("Unknown ap_distance: %s"%self.p['ap_dist_method']) 
        return distance

    def minimize_chiral(self,var):
        # protein only
        # internal cooordinates
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var)

        xyzmap_data = self.xyzmap['data']
        # remove D-form residues
        res_num = list(xyzmap_data.keys())
        for rn in res_num:
            try:
                num_CA = xyzmap_data[rn]['CA'][0]
                c_CA = var[num_CA-1,:]
            except:
                continue
            try:
                num_N = xyzmap_data[rn]['N'][0]
                c_N = var[num_N-1,:]
            except:
                continue
            try:
                num_C = xyzmap_data[rn]['C'][0]
                c_C = var[num_C-1,:]
            except:
                continue
            try:
                num_CB = xyzmap_data[rn]['CB'][0]
                ind_CB = num_CB-1 
                c_CB = var[ind_CB,:]
            except:
                continue

            # calculate torsion CA-N-C-CB
            v1 = c_N - c_CA
            v2 = c_C - c_N
            v3 = c_CB - c_C
            if np.inner( v1, np.cross(v2,v3) ) > 0.0: # L-form
                continue

            if iz1[0,ind_CB] == num_CA and iz1[1,ind_CB] == num_N:
                num_a1 = num_CA
                num_a2 = num_N
                rotang = 120.0
            elif iz1[0,ind_CB] == num_CA and iz1[1,ind_CB] == num_C: 
                num_a1 = num_CA
                num_a2 = num_C
                rotang = -120.0
            else:
                # Not TINKER-style z-matrix.
                # try modifying iz
                iz1[0,ind_CB] = num_CA
                iz1[1,ind_CB] = num_N
                a1 = iz1[0,ind_CB] - 1
                a2 = iz1[1,ind_CB] - 1
                a3 = zmatrix_c.find_connect(a2,ind_CB,self.xyzmap['connect'],(ind_CB,a1,a2))
                iz1[2,ind_CB] = a3 + 1
                zbond1[ind_CB] = pu.distance(var[ind_CB,:],var[a1,:])
                zang1[ind_CB] = pu.angle(var[ind_CB,:],var[a1,:],var[a2,:])*180/np.pi
                ztors1[ind_CB] = pu.torsion(var[ind_CB,:],var[a1,:],var[a2,:],var[a3,:])*180.0/np.pi
                num_a1 = num_CA
                num_a2 = num_N
                rotang = 120.0
            num_a3 = iz1[2,ind_CB]

            # HA exists?
            try:
                ind_HA = xyzmap_data[rn]['HA'][0]-1
            except:
                ind_HA = None
            if ind_HA is None:
                # HA does not exist
                # rotate the torsion angle of CB by 120.0
                ztors1[ind_CB] += rotang
            else:
                # swap torsion angles of CB and HA
                if iz1[0,ind_HA] != num_a1 or iz1[1,ind_HA] != num_a2 or iz1[2,ind_HA] != num_a3:
                    # Not TINKER-style z-matrix.
                    # try modifying iz
                    iz1[0,ind_HA] = num_a1
                    iz1[1,ind_HA] = num_a2
                    iz1[2,ind_HA] = num_a3
                    a1 = num_a1 - 1
                    a2 = num_a2 - 1
                    a3 = num_a3 - 1
                    zbond1[ind_HA] = pu.distance(var[ind_HA,:],var[a1,:])
                    zang1[ind_HA] = pu.angle(var[ind_HA,:],var[a1,:],var[a2,:])*180/np.pi
                    ztors1[ind_HA] = pu.torsion(var[ind_HA,:],var[a1,:],var[a2,:],var[a3,:])*180.0/np.pi
                ztors1[ind_CB] += rotang
                ztors1[ind_HA] -= rotang
                #ztors1[ind_CB], ztors1[ind_HA] = ztors1[ind_HA], ztors1[ind_CB]

        mol_util.setint(iz1,zbond1,zang1,ztors1)
        newvar = mol_util.getxyz()
        # rms fit
        _, var = pu.calc_rmsd(var,newvar)
        ene = self.get_energy(var)

        return ene, var, 1

    def report_create_firstbank(self):
        # save initial random conformations
        if ( mpirank != 0 ): return
        if ( self.p['write_bank'] ):
            if ( self.p['write_pdb'] ):
                self.savepdb('r',self.rvar,self.rscore)
            if ( self.p['write_score_profile'] ):
                self.write_score_profile()
        if ( not self.history_fh ): return
        self.write_history_file()

    def add_rbank(self,nbank=None,nbank_add=None,varlist=None):
        if hasattr(self,'perturb_info'):
            del self.perturb_info

        if self.p['init_bank'] is not None and self.p['include_init_bank']:
            if varlist is None:
                varlist = []
            arcvar = self.read_archive_coord()
            varlist += arcvar
            self.print_log('%d conformations were read from the archive'%len(arcvar))

        try:
            self.decomp_bscore
        except:
            self.decomp_bscore = []

        super(csa_molecule,self).add_rbank(nbank=nbank,nbank_add=nbank_add,varlist=varlist)

    def initialize_neb(self):
        neb_util.initialize_neb(self)
