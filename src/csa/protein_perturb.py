"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import division
from __future__ import absolute_import

from builtins import range
import sys
from .mol_perturb import mol_perturb, mol_perturb_int_cross, mol_perturb_torsion, mol_perturb_randint, mol_perturb_car_cross
import numpy as np
from .perturb_util import get_available_partner, exchange_coord_int, pick_partner, correct_angle, add_torsion, get_atom_index, exchange_chain_coord
from .csa import dict_merge
from copy import deepcopy
from . import mol_util
from . import protein_frag_assembly as pfa

class protein_perturb_type1(mol_perturb_int_cross):
    pname = 'type1'

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,bank_dist=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
            nbank = len(rvar)
        else:
            var1 = bvar[ind1]
            nbank = len(bvar)
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        res_index = self.residue_index
        n_res_index = res_index.size
        nres = self.xyzmap['nres']

        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        ap = get_available_partner(nbank,ind1,self.p['partner_mask'])
        xyzmap_data = self.xyzmap['data']
        for i in range(nperturb):
            ind2 = pick_partner(ap,ind1,bank_dist=bank_dist)
            if ( src[1] == 'r' ):
                var2 = rvar[ind2]
            else:
                var2 = bvar[ind2]   
            ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)

            # determine the size of exchange
            #esize = np.random.choice(self.esize_weight.size,p=self.esize_weight) + self.nmin
            esize = np.random.randint(self.nmax-self.nmin+1) + self.nmin

            # residue indices to be exchanged
            istart_ind = np.random.randint(n_res_index-esize+1)
            istop_ind = istart_ind + esize

            # exchange residue index
            exresind = res_index[istart_ind:istop_ind]

            # exchange atom index
            exind = []
            res_source = [ [src[0],ind1] for x in range(nres) ]
            for resind in exresind:
                if ( not resind in res_index ): continue
                resnum = resind + 1
                res_source[resind] = [src[1],ind2]
                resatoms = xyzmap_data[resnum]
                for atomname in resatoms:
                    exind.append( resatoms[atomname][0] - 1 )
                # remove 'N' in the current residue
                exind.remove( xyzmap_data[resind+1]['N'][0]-1 )
                # try adding 'N' in the next residue of the last residue
                try:
                    exind.append( xyzmap_data[resind+2]['N'][0]-1 )
                except:
                    pass
            tor_new, ang_new, bond_new = exchange_coord_int(exind,iz1,ztors1,zang1,zbond1,ztors2,zang2,zbond2)
            # save int 
            # assuming iz1 and iz2 are identical
            mol_util.setint(iz1,bond_new,ang_new,tor_new)
            newvar = exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())
            newvars.append(newvar)
            perturb_info.append( (self.pname,res_source) )

        return newvars, perturb_info

class protein_perturb_type2(mol_perturb):
    pname = 'type2'

    # ind1 from bvar
    # ind2 from bvar
    # ind1 takes discontinuous residues from ind2
    # with 1/3 chance, it takes all of a residue
    # with 1/3 chance, it takes the backbone of a residue
    # with 1/3 chance, it takes the sidechain of a residue
    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'res_index': None,
            'nmax': None,
            'nmin': 1,
            'partner_mask': [],
        }
        self.p = dict_merge(p,param)
        super(protein_perturb_type2,self).__init__(self.p,amap)

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,bank_dist=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
        else:
            var1 = bvar[ind1]
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        res_index = self.residue_index
        nres = res_index.size
        # number of exchanged residues
        if ( nperturb == None ):    
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        nmax = self.p['nmax']
        nmin = self.p['nmin']
        if ( nmax == None ): nmax = int(( nres - 1 ) / 2)
        if ( nmax < nmin ): nmax = nmin
        if ( src[1] == 'r' ):
            nbank = len(rvar)
        else:
            nbank = len(bvar)
        ap = get_available_partner(nbank,ind1,self.p['partner_mask'])
        for i in range(nperturb):
            ind2 = pick_partner(ap,ind1,bank_dist=bank_dist)
            if ( src[1] == 'r' ):
                var2 = rvar[ind2]
            else:
                var2 = bvar[ind2]   
            ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)
            # calculate number of exchange atoms
            nres_change = int( np.random.rand(1) * ( nmax - nmin + 1 ) ) + nmin
            # exchange res index
            exresind = np.random.choice(res_index,nres_change,replace=False)
            # exchange atom index
            exind = []
            res_source = [ [src[0],ind1] for x in range(self.xyzmap['nres']) ]
            for resind in exresind:
                resnum = resind + 1
                res_source[resind] = [src[1],ind2]
                rn = np.random.randint(3)
                resatoms = self.xyzmap['data'][resnum]
                if ( rn == 0 ): # 1/3 chance
                    # exchange all
                    for atomname in resatoms:
                        if ( atomname == 'N' ): continue
                        exind.append( resatoms[atomname][0] - 1 )
                    try:
                        # add 'N' in the next residue
                        exind.append( self.xyzmap['data'][resnum+1]['N'][0] - 1 )
                    except:
                        pass
                elif ( rn == 1 ): # 1/3 chance
                    # exchange backbone only
                    for atomname in ( 'H', 'CA', 'CB', 'HA', 'HA2', 'HA3', 'C', 'O' ):
                        try:
                            exind.append( resatoms[atomname][0] - 1 )
                        except:
                            pass
                    try:
                        # add 'N' in the next residue
                        exind.append( self.xyzmap['data'][resnum+1]['N'][0] - 1 )
                    except:
                        pass
                else: # 1/3 chance
                    # exchange sidechain only
                    forbidden_atoms = set( ('N', 'H', 'CA', 'CB', 'HA', 'HA2', 'HA3', 'C', 'O') )
                    for atomname in resatoms:
                        if ( atomname in forbidden_atoms ): continue
                        exind.append( resatoms[atomname][0] - 1 )
            # exchange
            tor_new, ang_new, bond_new = exchange_coord_int(exind,iz1,ztors1,zang1,zbond1,ztors2,zang2,zbond2)
            # save int 
            # assuming iz1 and iz2 are identical
            mol_util.setint(iz1,bond_new,ang_new,tor_new)
            newvar = exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())
            newvars.append(newvar)
            perturb_info.append( (self.pname,res_source) )

        return newvars, perturb_info

class protein_perturb_type3(mol_perturb_torsion):
    pname = 'type3'

class protein_perturb_type4(mol_perturb):
    pname = 'type4'

    # slightly perturb angles and dihedrals of the partner
    def __init__(self,param,amap,tor):
        p = {
            'nperturb': 1,
            'sigma_bond': 0.1,
            'sigma_angle': 5.0,
            'sigma_torsion': 10.0,
            'max_ratio': 0.05,
            'res_index': None,
            'partner_mask': [],
        }
        self.p = dict_merge(p,param)
        super(protein_perturb_type4,self).__init__(self.p,amap)

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,bank_dist=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
        else:
            var1 = bvar[ind1]
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        res_index = self.residue_index
        nres = res_index.size
        # number of exchanged residues
        if ( nperturb == None ):    
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        max_ratio = self.p['max_ratio']
        sigma_bond = self.p['sigma_bond']
        sigma_angle = self.p['sigma_angle']
        sigma_torsion = self.p['sigma_torsion']
        natom = self.xyzmap['natom']
        if ( src[1] == 'r' ):
            nbank = len(rvar)
        else:
            nbank = len(bvar)
        ap = get_available_partner(nbank,ind1,self.p['partner_mask'])
        xyzmap_data = self.xyzmap['data']
        for i in range(nperturb):
            ind2 = pick_partner(ap,ind1,bank_dist=bank_dist)
            if ( src[1] == 'r' ):
                var2 = rvar[ind2]
            else:
                var2 = bvar[ind2]   
            ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)
            # determine exchange ratio
            ex_ratio = np.random.rand(1) * max_ratio
            # calculate number of exchange atoms
            nres_change = int( ( nres - 1 ) * ex_ratio ) + 1
            # exchange res index
            exresind = np.random.choice(res_index,nres_change,replace=False)
            # exchange atom index
            exind = []
            res_source = [ [src[0],ind1] for x in range(self.xyzmap['nres']) ]
            for resind in exresind:
                resnum = resind + 1
                res_source[resind] = [src[1],ind2] # if perturbed
                resatoms = xyzmap_data[resnum]
                atomname_list = list(resatoms.keys())
                atomname_list.remove('N')
                for atomname in atomname_list:
                    exind.append( resatoms[atomname][0] - 1 )
                # try adding 'N' in the next residue
                try:
                    exind.append( xyzmap_data[resnum+1]['N'][0]-1 )
                except:
                    pass
            perturbed = [ False for x in range(len(exind)) ]
            # exchange
            tor_new = ztors1.copy()
            ang_new = zang1.copy()
            bond_new = zbond1.copy()
            for i in range(len(exind)):
                if ( perturbed[i] == True ): continue
                ia1 = iz1[0,i]
                ia2 = iz1[1,i]
                exind_sel = [i]
                perturbed[i] = True
                for j in range(i+1,len(exind)):
                    if ( perturbed[j] == True ): continue
                    if ( ia1 == iz1[0,j] and ia2 == iz1[1,j] ):
                        exind_sel.append(j)
                        perturbed[j] = True
                if ( sigma_torsion ):
                    sigma_tor = sigma_torsion
                else:
                    tor_diff = absolute( ztors1[exind_sel] - ztors2[exind_sel] )
                    sigma_tor = 180.0 - absolute( average( correct_angle(tor_diff) ) )
                    if ( sigma_tor == 0.0 ): sigma_tor = 0.1 # nonzero minimum
                tor_delta = np.random.normal(0.0,sigma_tor)
                #tor_new[exind_sel] = correct_torsion( tor_delta + ztors2[exind_sel] ) # correct_torsion may not be necessary
                tor_new[exind_sel] = tor_delta + ztors2[exind_sel]
            #ang_new[exind] = correct_angle( np.random.normal(zang2[exind],sigma_angle) ) # correct_angle may not be necessary
            ang_new[exind] = np.random.normal(zang2[exind],sigma_angle)
            bond_new[exind] = np.random.normal( zbond2[exind],sigma_bond )
            # save int assuming iz1 and iz2 are identical
            mol_util.setint(iz1,bond_new,ang_new,tor_new)
            newvar = mol_util.getxyz() 
            newvars.append(newvar)
            perturb_info.append( (self.pname,res_source) )

        return newvars, perturb_info

class protein_perturb_type5(mol_perturb_car_cross):
    pname = 'type5'

    # exchange a sphere in cartesian coordinate
    def get_ra_index(self):
        # representative atom index (the first atom of a residue)
        # CA index
        ra_index = []
        amap_data = self.xyzmap['data']
        for rind in self.residue_index:
            ra_index.append( amap_data[rind+1]['CA'][0] - 1 )
        self.ra_index = ra_index

class protein_perturb_type6(protein_perturb_type5):
    pname = 'type6'
    # same as protein_perturb_type5

class protein_perturb_type7(mol_perturb):
    pname = 'type7'

    # construct beta sheet
    def __init__(self,param,amap):
        p = {
            'nperturb': 6,
            'res_index': None,
            'nmax': 5,
            'nmin': 1,
            'distance_cut': 6.5,
            'perturb_tor': True,
        }
        self.p = dict_merge(p,param)
        super(protein_perturb_type7,self).__init__(self.p,amap)

        # remove non-protein residues
        for i in range(len(amap['seq'])):
            if ( amap['seq'][i] == 'X' ):
                try:
                    self.residue_index = delete(self.residue_index,i,0)
                except:
                    pass

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,nmin=None,nmax=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
        else:
            var1 = bvar[ind1]

        if ( self.p['perturb_tor'] ):
            ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        if ( nmax == None ): nmax = self.p['nmax']
        if ( nmin == None ): nmin = self.p['nmin']
        if ( nmin == None ): nmin = 1
        beta_residue_index, beta_n_index, beta_ca_index, beta_c_index = self.get_beta_sheet_info(var1)
        n_beta = len(beta_residue_index)
        # no beta residue
        if n_beta < 2: return
        if ( nmax == None ):
            nmax = int(n_beta/2)

        beta_n_coord = var1[beta_n_index,:]
        beta_ca_coord = var1[beta_ca_index,:]
        beta_c_coord = var1[beta_c_index,:]

        # find beta pairs
        beta_pairs = []
        all_beta_pairs = []
        all_dist2 = []
        all_parallel = []
        parallel = []
        n_beta = len(beta_residue_index)
        cut2 = self.p['distance_cut']**2
        for i in range(n_beta-1):
            for j in range(i+1,n_beta):
                ca_dist2 = np.sum( ( beta_ca_coord[i,:] - beta_ca_coord[j,:] )**2 )
                if ( abs( beta_residue_index[i] - beta_residue_index[j] ) < 3 ): continue
                all_beta_pairs.append( (beta_residue_index[i],beta_residue_index[j]) )
                all_dist2.append(ca_dist2)
                pl = (np.inner(beta_c_coord[i,:]-beta_n_coord[i,:],beta_c_coord[j,:]-beta_n_coord[j,:]) >= 0)
                all_parallel.append(pl)
                if ( ca_dist2 < cut2 ):
                    beta_pairs.append( (beta_residue_index[i],beta_residue_index[j]) )
                    parallel.append(pl)

        newvars = []
        perturb_info = []
        amap = self.xyzmap['data']
        for i in range(nperturb):
            n_pair = np.random.randint(nmin,nmax+1) 

            ex_pair = []
            ex_pair_parallel = []
            if ( not beta_pairs ):
                # pick any closest pairs
                weight = 1.0 / np.array(np.sqrt(all_dist2))
                weight /= np.sum(weight)
                ind = np.random.choice(len(weight),p=weight)
                beta_pairs.append( all_beta_pairs[ind] )
                parallel.append( all_parallel[ind] )  
                ex_pair.append(beta_pairs[0])
                ex_pair_parallel.append( all_parallel[ind] )

            bp_copy = deepcopy( beta_pairs )
            pl_copy = deepcopy( parallel )
            # select ex_pair
            while ( len(ex_pair) < n_pair ):
                avail_pair = []
                avail_pair_parallel = []
                for ibp in np.random.permutation(len(bp_copy)):
                    bp = bp_copy[ibp]
                    pl = pl_copy[ibp]
                    if ( pl ):
                        # parallel
                        for new_bp in ( (bp[0],bp[1]), (bp[0]-1,bp[1]-1), (bp[0]+1,bp[1]+1) ):
                            if (     new_bp not in bp_copy
                                 and new_bp[0] in self.residue_index 
                                 and new_bp[1] in self.residue_index
                                 and (new_bp[0]-1,new_bp[1]) not in ex_pair
                                 and (new_bp[0]+1,new_bp[1]) not in ex_pair
                                 and (new_bp[0],new_bp[1]-1) not in ex_pair
                                 and (new_bp[0],new_bp[1]+1) not in ex_pair ):
                                avail_pair.append(new_bp)
                                avail_pair_parallel.append(True)
                    else:
                        # anti-parallel
                        for new_bp in ( (bp[0],bp[1]), (bp[0]+1,bp[1]-1), (bp[0]-1,bp[1]+1) ):
                            if (     new_bp not in bp_copy
                                 and new_bp[0] in self.residue_index 
                                 and new_bp[1] in self.residue_index
                                 and (new_bp[0]-1,new_bp[1]) not in ex_pair
                                 and (new_bp[0]+1,new_bp[1]) not in ex_pair
                                 and (new_bp[0],new_bp[1]-1) not in ex_pair
                                 and (new_bp[0],new_bp[1]+1) not in ex_pair ):
                                avail_pair.append(new_bp)
                                avail_pair_parallel.append(False)
                pick = np.random.randint(len(avail_pair))
                pick_pair = avail_pair[pick]
                pl = avail_pair_parallel[pick]
                ex_pair.append(pick_pair)
                ex_pair_parallel.append(pl)
                bp_copy.append(pick_pair)
                pl_copy.append(pl)

            # prepare coordinate
            dist_type = [] # Harmonic (False) or Lorentzian (True)
            dist_pair = [] # atom1, atom2
            dist_info = [] # weight, r_left, r_right
            tor_type = [] # harmonic (false) or lorentzian (true)
            tor_atom = [] # a1, a2, a3, a4
            tor_info = [] # weight, tor_left, tor_right
            res_source = [ (src[0],ind1) for x in range(self.xyzmap['nres']) ]
            if ( self.p['perturb_tor'] ):
                tor_new = ztors1.copy()
            for iex in range(len(ex_pair)):
                bp = ex_pair[iex]
                pl = ex_pair_parallel[iex]
                try:
                    c1p = amap[bp[0]]['C'][0]
                except:
                    c1p = None 
                try:
                    n1 = amap[bp[0]+1]['N'][0]
                except:
                    n1 = None 
                try:
                    ca1 = amap[bp[0]+1]['CA'][0]
                except:
                    ca1 = None 
                try:
                    c1 = amap[bp[0]+1]['C'][0]
                except:
                    c1 = None 
                try:
                    n1n = amap[bp[0]+2]['N'][0]
                except:
                    n1n = None 

                try:
                    c2p = amap[bp[1]]['C'][0]
                except:
                    c2p = None 
                try:
                    n2 = amap[bp[1]+1]['N'][0]
                except:
                    n2 = None 
                try:
                    ca2 = amap[bp[1]+1]['CA'][0]
                except:
                    ca2 = None 
                try:
                    c2 = amap[bp[1]+1]['C'][0]
                except:
                    c2 = None 
                try:
                    n2n = amap[bp[1]+2]['N'][0]
                except:
                    n2n = None 

                # distance restraints (Ca-Ca)
                if ( ca1 != None and ca2 != None ):
                    dist_type.append(False) # harmonic
                    dist_pair.append((ca1,ca2))
                    dist_info.append((500.0,0.0,6.0)) # (0.0,6.0)

                # torsion restraints
                # -165 < phi < -75
                #   90 < psi < 180
                if ( c1p != None and n1 != None and ca1 != None and c1 != None ):
                    tor_type.append(False)
                    tor_atom.append((c1p,n1,ca1,c1))
                    tor_info.append((100.0,-165.0,-75.0))
                if ( n1 != None and ca1 != None and c1 != None and n1n != None ):
                    tor_type.append(False)
                    tor_atom.append((n1,ca1,c1,n1n))
                    tor_info.append((100.0,90.0,180.0))
                if ( c2p != None and n2 != None and ca2 != None and c2 != None ):
                    tor_type.append(False)
                    tor_atom.append((c2p,n2,ca2,c2))
                    tor_info.append((100.0,-165.0,-75.0))
                if ( n2 != None and ca2 != None and c2 != None and n2n != None ):
                    tor_type.append(False)
                    tor_atom.append((n2,ca2,c2,n2n))
                    tor_info.append((100.0,90.0,180.0))

                # perturb torsion
                if ( self.p['perturb_tor'] ):
                    mods = []
                    # phi
                    if ( c1 != None ):
                        tor = tor_new[c1-1]
                        if ( tor < -165.0 or tor > -75.0 ):
                            mods.append( (c1-1,np.random.random()*90.0-165.0) ) # (-165,-75)
                    if ( c2 != None ):
                        tor = tor_new[c2-1]
                        if ( tor < -165.0 or tor > -75.0 ):
                            mods.append( (c2-1,np.random.random()*90.0-165.0) )
                    # psi
                    if ( n1n != None ):
                        tor = tor_new[n1n-1]
                        if ( tor < 90.0 ):
                            mods.append( (n1n-1,np.random.random()*90.0+90.0) ) # (90,180)
                    if ( n2n != None ):
                        tor = tor_new[n2n-1]
                        if ( tor < 90.0 ):
                            mods.append( (n2n-1,np.random.random()*90.0+90.0) )

                    for exind,ang in mods:
                        mask = np.logical_and(iz1[0,exind]==iz1[0,:],iz1[1,exind]==iz1[1,:])
                        del_ang = ang - tor_new[exind]
                        tor_new[mask] += del_ang

                res_source[bp[0]] = ('type7',None)
                res_source[bp[1]] = ('type7',None)

            if ( self.p['perturb_tor'] ):
                mol_util.setint(iz1,zbond1,zang1,tor_new)
                newvar = mol_util.getxyz() 
            else:
                newvar = var1.copy()
            newvars.append(newvar)

            # restrain info
            dist_type = np.array( dist_type, dtype=bool )[:,np.newaxis]
            dist_pair = np.array( dist_pair, dtype=int )
            dist_info = np.array( dist_info, dtype=float )
            tor_type = np.array( tor_type, dtype=bool )[:,np.newaxis]
            tor_atom = np.array( tor_atom, dtype=int )
            tor_info = np.array( tor_info, dtype=float )
            rinfo = { 'dist': (dist_type,dist_pair,dist_info),
                      'tor' : (tor_type,tor_atom,tor_info) }
            perturb_info.append( (self.pname,res_source,rinfo) )

        return newvars, perturb_info

    def get_beta_sheet_info(self,var):
        amap = self.xyzmap['data']

        # find beta residues
        beta_residue_index = []
        for ri in range( self.xyzmap['nres'] ):
            try:
                i1 = amap[ri]['C'][0]-1
            except:
                continue
            try:
                i2 = amap[ri+1]['N'][0]-1
            except:
                continue
            try:
                i3 = amap[ri+1]['CA'][0]-1
            except:
                continue
            try:
                i4 = amap[ri+1]['C'][0]-1
            except:
                continue
            try:
                i5 = amap[ri+2]['N'][0]-1
            except:
                continue
            c1 = var[i1,:]
            c2 = var[i2,:]
            c3 = var[i3,:]
            c4 = var[i4,:]
            c5 = var[i5,:]

            v1 = c2 - c1
            v2 = c3 - c2
            v3 = c4 - c3
            v4 = c5 - c4
            cp23 = np.cross(v2,v3)
            cp34 = np.cross(v3,v4)

            phi = np.arctan2( np.inner( np.linalg.norm(v2) * v1, cp23 ), np.inner( np.cross(v1,v2), cp23 ) )*180.0/np.pi
            psi = np.arctan2( np.inner( np.linalg.norm(v3) * v2, cp34 ), np.inner( np.cross(v2,v3), cp34 ) )*180.0/np.pi
            if ( phi > -180.0 and phi < -20.0
                 and (    ( psi < 180.0 and psi > 20.0 )
                       or ( psi > -180.0 and psi < -170.0 )
                     )

               ):
                beta_residue_index.append(ri)

        beta_n_index = []
        beta_ca_index = []
        beta_c_index = []
        for bri in beta_residue_index:
            beta_n_index.append( self.xyzmap['data'][bri+1]['N'][0] - 1 )
            beta_ca_index.append( self.xyzmap['data'][bri+1]['CA'][0] - 1 )
            beta_c_index.append( self.xyzmap['data'][bri+1]['C'][0] - 1 )

        return beta_residue_index, beta_n_index, beta_ca_index, beta_c_index

class protein_perturb_exbeta(mol_perturb):
    pname = 'exbeta'

    # extend beta structure
    def __init__(self,param,amap):
        p = {
            'nperturb': 1,
            'res_index': None,
            'nmax': 3,
            'nmin': 1,
        }
        self.p = dict_merge(p,param)
        super(protein_perturb_exbeta,self).__init__(self.p,amap)

        # remove non-protein residues
        for i in range(len(amap['seq'])):
            if ( amap['seq'][i] == 'X' ):
                try:
                    self.residue_index = delete(self.residue_index,i,0)
                except:
                    pass

    def perturb(self,bvar,ind1,nperturb=None,nmin=None,nmax=None):
        var1 = bvar[ind1]
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        if ( nmax == None ): nmax = self.p['nmax']
        if ( nmin == None ): nmin = self.p['nmin']
        if ( nmin == None ): nmin = 1
        beta_residue_ind = self.get_beta_residue(var1)

        newvars = []
        perturb_info = []
        nres = self.xyzmap['nres']
        for i in range(nperturb):
            n_ex = np.random.randint(nmin,nmax+1)
            res_source = [ ('b',ind1) for x in range(nres) ]

            # pick residues
            ex_res_ind = []
            bri_copy = deepcopy( beta_residue_ind )
            ri_copy = self.residue_index.tolist()
            for iex in range(n_ex):
                avail_ind = []
                for ri in ri_copy:
                    if ( not (ri in bri_copy) ):
                        if ( ri+1 in bri_copy ):
                            avail_ind.append(ri)
                        elif ( ri-1 in bri_copy ):
                            avail_ind.append(ri)
                if len(avail_ind) == 0:
                    avail_ind = deepcopy(ri_copy)
                pick_res = np.random.choice(avail_ind)
                ex_res_ind.append(pick_res)
                bri_copy.add(pick_res)
                ri_copy.remove(pick_res)

            tor_new = ztors1.copy()
            for ir in ex_res_ind:
                res_source[ir] = ('exbeta',None)
                rnum = ir + 1
                # phi
                exind = self.xyzmap['data'][rnum]['C'][0] - 1 # phi
                mask = np.logical_and(iz1[0,exind]==iz1[0,:],iz1[1,exind]==iz1[1,:])
                phi_ang = np.random.random() * 160.0 - 180.0 # (-180,-20)
                del_phi = phi_ang - tor_new[exind]
                tor_new[mask] += del_phi

                # psi
                try:
                    exind = self.xyzmap['data'][rnum+1]['N'][0] - 1 # psi
                except:
                    exind = None
                if ( exind != None ):
                    mask = np.logical_and(iz1[0,exind]==iz1[0,:],iz1[1,exind]==iz1[1,:])
                    psi_ang = np.random.random() * 170.0 + 20.0 # (20,190)
                    del_psi = psi_ang - tor_new[exind]
                    tor_new[mask] += del_psi

            mol_util.setint(iz1,zbond1,zang1,tor_new)
            newvar = mol_util.getxyz() 
            newvars.append(newvar)
            perturb_info.append( (self.pname,res_source) )

        return newvars, perturb_info

    def get_beta_residue(self,var):
        amap = self.xyzmap['data']

        # find beta residues
        beta_residue_index = []
        for ri in range( self.xyzmap['nres'] ):
            try:
                i1 = amap[ri]['C'][0]-1
            except:
                continue
            try:
                i2 = amap[ri+1]['N'][0]-1
            except:
                continue
            try:
                i3 = amap[ri+1]['CA'][0]-1
            except:
                continue
            try:
                i4 = amap[ri+1]['C'][0]-1
            except:
                continue
            try:
                i5 = amap[ri+2]['N'][0]-1
            except:
                continue
            c1 = var[i1,:]
            c2 = var[i2,:]
            c3 = var[i3,:]
            c4 = var[i4,:]
            c5 = var[i5,:]

            v1 = c2 - c1
            v2 = c3 - c2
            v3 = c4 - c3
            v4 = c5 - c4
            cp23 = np.cross(v2,v3)
            cp34 = np.cross(v3,v4)

            phi = np.arctan2( np.inner( np.linalg.norm(v2) * v1, cp23 ), np.inner( np.cross(v1,v2), cp23 ) )*180.0/np.pi
            psi = np.arctan2( np.inner( np.linalg.norm(v3) * v2, cp34 ), np.inner( np.cross(v2,v3), cp34 ) )*180.0/np.pi
            if ( phi > -180.0 and phi < -20.0
                 and (    ( psi < 180.0 and psi > 20.0 )
                       or ( psi > -180.0 and psi < -170.0 )
                     ) ):
                beta_residue_index.append(ri)

        return set(beta_residue_index)

class protein_perturb_backtor(mol_perturb):
    pname = 'backtor'

    # construct beta sheet
    def __init__(self,param,amap,energy_func=None):
        p = {
            'nperturb': 6,
            'res_index': None,
            'nmax': 3,
            'nmin': 1,
            'backtor_file': None,
            'sigma_torsion': 1.0,
            'weighted_frag': False,
            'max_energy_diff': None,
        }
        self.p = dict_merge(p,param)
        super(protein_perturb_backtor,self).__init__(self.p,amap)

        # energy function
        if energy_func is None:
            self.max_energy_diff = None
        else:
            self.energy_func = energy_func
            self.max_energy_diff = self.p['max_energy_diff']

        # remove non-protein residues
        for i in range(len(amap['seq'])):
            if ( amap['seq'][i] == 'X' ):
                try:
                    self.residue_index = delete(self.residue_index,i,0)
                except:
                    pass

        # exchangeable atom indexes
        e_ind = get_atom_index(self.residue_index,self.xyzmap)
        self.exchange_ind = np.array(e_ind)
        
        # read backtor_file
        self.backtor, self.nfrag = pfa.read_backtor_file(self.p['backtor_file'])

        if self.backtor is not None:
            # exchangeable residue indexes
            mol_nres = self.xyzmap['nres']
            min_res = int(self.nfrag/2)
            max_res = mol_nres-int(self.nfrag/2)
            e_res_ind = []
            for ri in self.residue_index:
                if ri >= min_res and ri < max_res:
                    e_res_ind.append(ri)
            self.exchange_res_ind = e_res_ind

    def perturb(self,bvar,ind1,nperturb=None,nmin=None,nmax=None,nfrag=None):
        if nfrag is None:
            nfrag = self.nfrag
        var1 = bvar[ind1]
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        # energy check?
        if self.max_energy_diff is not None:
            ene = self.energy_func(var1)
            ene_cut = ene + self.max_energy_diff
        else:
            ene_cut = None

        nres = len(self.residue_index)
        # number of exchanged residues
        if ( nperturb == None ):    
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        if nmax is None:
            nmax = self.p['nmax']
            if nmax is None:
                nmax = ( nres - 1 ) / 2
        if nmin is None:
            nmin = self.p['nmin']
            if nmin is None:
                nmin = 1
        if nmax < nmin:
            nmax = nmin

        for i in range(nperturb):
            nres_change = np.random.randint(nmin,nmax+1)
            cur_tor_new = ztors1.copy()
            cur_res_source = [ ('b',ind1) for x in range(self.xyzmap['nres']) ]

            for ires_change in range(nres_change):
                best_ene = sys.float_info.max 
                best_tor_new = None 
                best_res_source = None
                success = False

                exresind = np.random.choice(self.exchange_res_ind,len(self.exchange_res_ind),replace=False)
                for resind in exresind:
                    tor_new = cur_tor_new.copy()
                    res_source = deepcopy(cur_res_source)
                    self.perturb_residue(resind+1,iz1,ztors1,tor_new,res_source=res_source,nfrag=nfrag)

                    if ene_cut is not None:
                        mol_util.setint(iz1,zbond1,zang1,tor_new)
                        newvar = mol_util.getxyz()
                        newene = self.energy_func(newvar)
                        if newene <= ene_cut:
                            cur_tor_new = tor_new
                            cur_res_source = res_source
                            success = True
                            break
                        else:
                            if newene < best_ene:
                                best_ene = newene
                                best_tor_new = tor_new 
                                best_res_source = res_source
                    else:
                        cur_tor_new = tor_new
                        cur_res_source = res_source
                        success = True
                        break

                if not success:
                    cur_tor_new = best_tor_new
                    cur_res_source = best_res_source
                    ene = best_ene
                    ene_cut = ene + self.max_energy_diff
                    break # energy fails, then exit immediately

            mol_util.setint(iz1,zbond1,zang1,cur_tor_new)
            newvar = mol_util.getxyz()
            newvars.append(newvar)
            perturb_info.append( (self.pname,cur_res_source) )

        return newvars, perturb_info

    def perturb_residue(self,resnum,iz1,ztors1,tor_new,res_source=None,nfrag=None):
        if nfrag is None:
            nfrag = self.nfrag
        if resnum in self.backtor:
            pfa.perturb_residue(self.backtor,self.exchange_ind,self.xyzmap['data'],resnum,iz1,ztors1,tor_new,nfrag=nfrag,sigma_torsion=self.p['sigma_torsion'],weighted_frag=self.p['weighted_frag'])

class protein_perturb_randint(mol_perturb_randint):
    pname = 'randint'

class protein_perturb_randint2(mol_perturb_randint):
    pname = 'randint2'
