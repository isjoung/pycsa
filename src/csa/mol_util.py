"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors:

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import absolute_import

from builtins import range
from zmatrix_c import get_internal, get_cartesian
from copy import deepcopy

connect = []
var = None # temporary holder

def set_connect(con):
    global connect
    n_con = len(con)
    connect = [ None ] * n_con
    for i in range(n_con):
        connect[i] = tuple(sorted(con[i]))

def getint(var):
    global connect
    ret = get_internal(var,connect)
    return ret

def setint(iz,bond,angle,torsion):
    global var
    var = get_cartesian(iz,bond,angle,torsion) 

def getxyz():
    global var
    return var

def setxyz(input_var):
    global var
    var = deepcopy(input_var)

#def init_mol_util(input):
#    global getxyz
#    global setint
#    global getint
#    global setxyz
#    if ( input == 'tinker' ):
#        mu = __import__('mol_util_tinker')
#    else:
#        mu = __import__('mol_util_interface')
#    getxyz = mu.getxyz
#    setxyz = mu.setxyz
#    getint = mu.getint
#    setint = mu.setint
