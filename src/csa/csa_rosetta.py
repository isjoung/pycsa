"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors:

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import sys
import os

# turn off some multithread algorithms
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['NUMEXPR_NUM_THREADS'] = '1'
os.environ['OMP_NUM_THREADS'] = '1'

from .csa_molecule import csa_molecule, mpicomm, mpisize, mpirank, MPI
from .csa import dict_merge
from . import mol_util
import numpy as np
import re
import neb_util
import perturb_util as pu
from copy import deepcopy
# %%
import numpy as np
import pyrosetta
from pyrosetta import pose_from_sequence ,get_fa_scorefxn, MoveMap, Pose, standard_packer_task, AtomID, pose_from_pdb
from pyrosetta.rosetta import core, protocols, numeric
try:
    from simtk.openmm import app as omapp
except:
    from openmm import app as omapp

PYROSETTA_INIT = False
if not PYROSETTA_INIT:
    pyrosetta.init()
    PYROSETTA_INIT = True


def pose_natom(pose):
    natom = 0
    for rn in range(1, pose.total_residue()+1):
        r = pose.residue(rn)
        natom += r.natoms()
    return natom


def get_score_names(scorefxn):
    decomp_names = []
    score_types = []
    for name, st in core.scoring.ScoreType.__dict__.items():
        if not isinstance(st,core.scoring.ScoreType) \
                or not scorefxn.has_nonzero_weight(st):
            continue
        decomp_names.append(name)
        score_types.append(st)

    arg = np.argsort(decomp_names)
    decomp_names = [ decomp_names[x] for x in arg ]
    score_types = [ score_types[x] for x in arg ]

    return decomp_names, score_types


def get_pose_coords(pose):
    ret = []
    for rn in range(1, pose.total_residue()+1):
        r = pose.residue(rn)
        for a in range(1, r.natoms()+1):
            c = r.xyz(a)
            #r.set_xyz(a, c)
            ret.append(c)
    return np.array(ret)


def set_pose_coords(pose, coords):
    ci = 0
    for rn in range(1, pose.total_residue()+1):
        r = pose.residue(rn)
        for a in range(1, r.natoms()+1):
            vec = r.xyz(a)
            x, y, z = coords[ci]
            vec.assign(x,y,z)
            r.set_xyz(a, vec)
            ci += 1


def read_connect(topo):
    natom = topo.getNumAtoms() 
    connect = [ [] for x in range(natom) ]
    for b in topo.bonds():
        a1 = int(b.atom1.id)
        a2 = int(b.atom2.id)
        connect[a1-1].append(a2)
        connect[a2-1].append(a1)

    return connect

class csa_rosetta(csa_molecule):
    """csa for optimizing ROSETTA molecules"""

    def __init__(self,param={}):
        p = {
            # initial pose (pdb)
            'pose': 'model.pdb',

            # minimization method
            'bank_min_maxiter': 1000,
            'rbank_min_maxiter': 1000,
            'md_maxiter': 0,
            'parallel_minimization': 3,

            'native_coord': None,
            'init_bank': None,

            # protein
            #'protein_chiral_rest': False,
            #'contact_rest': None,
            #'contact_intra_only': False,

            # positional rest
            #'positional_rest': False,
            #'positional_weight': 1.0,

            'fix_residue': None,
            'fix_index': None,

            # symmetry
            #'symmetry_rest': False,
            #'symmetry_cutoff': ( 2.0, 6.0, 0.0, 6.0 ),
            #'symmetry_weight': ( 1.0, 1.0 ),
            #'symmetry_contact_only': True,
            #'symmetry_min': False, # symmetric minimization

            # nucleic acid
            #'nucleicacid_chiral_rest': False,

            #'nonbonded_method': 'nocutoff',
            #'nonbonded_cutoff': 1.0 * unit.nanometer,
            #'implicit_solvent': 'GBn2',
            #'charge': True,

            #'pressure': None,

            # extra restraints
            #'restrain': None,
            #'restrain_weights': {},

            'verbose': 1,
            'log_caller': False,
        }

        self.p = dict_merge(p,param)
        self.verbose = self.p['verbose']

        # topology file
        self.init_pose = pose_from_pdb(self.p['pose'])
        self.initvar = get_pose_coords(self.init_pose)
        self.pose = pose_from_pdb(self.p['pose']) # container pose

        # score function
        self.score_func = get_fa_scorefxn()  

        # decomposed score names
        _temp_score_names, self.score_types = get_score_names(self.score_func)
        self.score_names = [ 'total' ] + _temp_score_names

        # min mover
        self.min_mover = protocols.minimization_packing.MinMover()

        # init for csa parameter
        super(csa_rosetta,self).__init__(self.p)

    def energy_decomposition(self, pose):
        ene = pose.energies().total_energies()
        decomp_ene = [ self.score_func(pose) ]
        for i in range(len(self.score_types)):
            decomp_ene.append(ene.get(self.score_types[i]))
        return decomp_ene

    def minimizef(self, var, perturb_name):
        set_pose_coords(self.pose, var)
        self.min_mover.apply(self.pose)
        minvar = get_pose_coords(self.pose)

        decomp_energy = self.energy_decomposition(self.pose)
        min_energy = decomp_energy[0]

        ncalls = 1

        return min_energy, minvar, (self.score_names, decomp_energy), ncalls

    def read_xyzmap(self):
        if hasattr(self,'xyzmap'):
            # already read
            return

        natom = pose_natom(self.init_pose)
        topo = omapp.PDBFile(self.p['pose']).topology
        connect = read_connect(topo)
        assert(len(connect) == natom)

        Res31 ={'ALA':'A','CYS':'C','ASP':'D','GLU':'E','PHE':'F','GLY':'G',
                'HIS':'H','ILE':'I','LYS':'K','LEU':'L','MET':'M','ASN':'N',
                'PRO':'P','GLN':'Q','ARG':'R','SER':'S','THR':'T','VAL':'V',
                'TRP':'W','TYR':'Y','ASX':'N','GLX':'Q','UNK':'X','INI':'K',
                'AAR':'R','ACE':'X','ACY':'G','AEI':'T','AGM':'R','ASQ':'D',
                'AYA':'A','BHD':'D','CAS':'C','CAY':'C','CEA':'C','CGU':'E',
                'CME':'C','CMT':'C','CSB':'C','CSD':'C','CSE':'C','CSO':'C',
                'CSP':'C','CSS':'C','CSW':'C','CSX':'C','CXM':'M','CYG':'C',
                'CYM':'C','DOH':'D','EHP':'F','FME':'M','FTR':'W','GL3':'G',
                'H2P':'H','HIC':'H','HIP':'H','HTR':'W','HYP':'P','KCX':'K',
                'LLP':'K','LLY':'K','LYZ':'K','M3L':'K','MEN':'N','MGN':'Q',
                'MHO':'M','MHS':'H','MIS':'S','MLY':'K','MLZ':'K','MSE':'M',
                'NEP':'H','NPH':'C','OCS':'C','OCY':'C','OMT':'M','OPR':'R',
                'PAQ':'Y','PCA':'Q','PHD':'D','PRS':'P','PTH':'Y','PYX':'C',
                'SEP':'S','SMC':'C','SME':'M','SNC':'C','SNN':'D','SVA':'S',
                'TPO':'T','TPQ':'Y','TRF':'W','TRN':'W','TRO':'W','TYI':'Y',
                'TYN':'Y','TYQ':'Y','TYS':'Y','TYY':'Y','YOF':'Y','FOR':'X',
                '---':'-','PTR':'Y','LCX':'K','SEC':'D','MCL':'K','LDH':'K',
                'D  ':'X',
        }

        # xyzmap dict
        # ['title']: title
        # ['nres']: nres
        # ['seq']: sequence
        # ['natom']: natom
        # ['data'][resnum][atomname]: list of columns
        # ['resrange'][resnum]: tuple of atom index range
        self.xyzmap = {}
        self.xyzmap['title'] = 'no title'
        self.xyzmap['nres'] = topo.getNumResidues()
        self.xyzmap['seq'] = ''
        for res in topo.residues():
            try:
                self.xyzmap['seq'] += Res31[res.name]
            except:
                self.xyzmap['seq'] += 'X'
        self.xyzmap['natom'] = natom
        self.xyzmap['connect'] = connect

        # read continuous molecules
        mol = { 1:1 }
        imol = 1
        size = len(connect)
        while ( len(mol) < size ):
            updated = True
            while ( updated ):
                updated = False
                for sn in range(1,natom+1):
                    if ( sn in mol ): continue
                    conn = connect[sn-1]
                    for cn in conn:
                        if ( cn in mol ):
                            mol[sn] = mol[cn]
                            updated = True
            if len(mol) < size:
                imol += 1
                for sn in range(1,natom+1):
                    if ( sn in mol ): continue
                    mol[sn] = imol
                    break

        # read atom lines
        data = {}
        raw_data = []
        for atm in topo.atoms():
            ind = atm.index
            res = atm.residue
            c = []
            c.append(ind+1) # ind0 serial number in xyz file
            c.append(ind+1) # ind1 serial number in pdb file
            c.append(atm.name) # ind2
            c.append(res.name) # ind3
            c.append(res.index+1) # ind4
            c.append(0) # ind5 null
            c.append(0) # ind6 null
            c.append(mol[ind+1]) # ind7
            raw_data.append(c)
            if ( c[4] not in data ):
                data[c[4]] = {}
            data[c[4]][c[2]] = c # residue number -> atom name = serial number of xyz
        self.xyzmap['data'] = data
        self.xyzmap['raw_data'] = raw_data

        # resrange
        resrange = {}
        atom_index = 0
        for res in sorted(data):
            end_index = atom_index + len(data[res])
            resrange[res] = ( atom_index, end_index )
            atom_index = end_index
        self.xyzmap['resrange'] = resrange

    def report_iterate(self):
        # save bank conformations
        if mpirank != 0:
            return
        if self.p['write_diff'] and self.niter % self.p['write_diff'] == 0:
            self.write_diff()
        if self.p['write_bank']:
            if self.p['write_pdb']:
                self.savepdb('b',
                        self.bvar,self.bscore,decomp_energy=self.decomp_bscore)
            if self.p['record_gmin'] \
                    and self.update_bvar[self.minscore_ind] >= 0:
                # gmin updated
                self.write_pdb('gmin%05d.pdb'%self.niter,
                        self.bvar[self.minscore_ind],energy=self.minscore)
            if self.p['write_score_profile']:
                self.write_score_profile()
        if not self.history_fh:
            return
        self.write_history_file()

    def write_history_file_header(self):
        pre = str(self.p['profile_precision'])
        fmt = '%6s %3s %10s %4s %4s %' + pre + 's %' + pre + 's %15s %4s %4s\n'
        self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_energy','max_energy','ntrial','iuse','nbk'))
        self.history_fh.flush()

    def read_native_var(self):
        # native var
        if self.p['native_coord']:
            if isinstance(self.p['native_coord'],str):
                if self.p['native_coord'].endswith('.inpcrd'):
                    inpcrd = app.AmberInpcrdFile(self.p['native_coord'])
                    self.native_atom_index = None
                    self.native_var = [ np.array( inpcrd.positions.value_in_unit(unit.angstrom) ) ]
                elif self.p['native_coord'].endswith('.pdb'):
                    pdb = app.PDBFile(self.p['native_coord'])
                    raw_var = np.array( pdb.positions.value_in_unit(unit.angstrom) )
                    top = pdb.getTopology()
                    known_atoms = {}
                    atom_ind = 0
                    for residue in top.residues():
                        resnum = int(residue.id)
                        for atom in residue.atoms():
                            known_atoms[(resnum,atom.name)] = atom_ind
                            atom_ind += 1
                    self.native_atom_index = []
                    natom = self.xyzmap['natom']
                    nvar = np.zeros((natom,3))
                    self.native_pdb_index = []
                    for resnum in self.xyzmap['data']:
                        atoms = self.xyzmap['data'][resnum]
                        for atom_name in atoms:
                            key = (resnum, atom_name)
                            if key in known_atoms:
                                ind = atoms[atom_name][0]-1
                                xyz = raw_var[known_atoms[key]]
                                nvar[ind,:] = xyz
                                self.native_pdb_index.append(ind)
                    self.native_pdb_index = np.array(self.native_pdb_index)
                    if len(self.native_pdb_index) == 0:
                        self.print_error('native pdb does not match')
                    self.native_var = [nvar]
            else:
                self.native_var = []
                for inpcrd_file in self.p['native_coord']:
                    if inpcrd_file.endswith('.inpcrd'):
                        inpcrd = app.AmberInpcrdFile(inpcrd_file)
                    elif inpcrd_file.endswith('.pdb'):
                        inpcrd = app.PDBFile(inpcrd_file)
                    self.native_var.append( np.array( inpcrd.positions.value_in_unit(unit.angstrom) ) )

    def pack_min_input(self,i_list):
        min_input = []
        for i in i_list:
            try:
                perturb_name = self.perturb_info_backup[i][0]
            except:
                perturb_name = None
            min_input.append( (self.pvar[i],perturb_name) )
            # destroy self.pvar[i] to save memory
            self.pvar[i] = None
        return min_input

    def unpack_min_output(self,min_output,i_list):
        for ind in range(len(i_list)):
            i = i_list[ind]
            mo = min_output[ind]
            score, minvar, decomp_ene, nee = mo
            self.mvar.append(minvar)
            self.mscore.append(score)
            try:
                self.perturb_info.append(self.perturb_info_backup[i])
            except:
                pass
            self.decomp_mscore.append(decomp_ene)
            try:
                self.minimize_nee += nee
            except:
                self.minimize_nee = nee
            self.print_log("m%d score: %.5E"%(len(self.mvar),score), level=3)

    def find_molecule_unit(self):
        if hasattr(self,'mol_unit_size') and hasattr(self,'n_mol_unit'):
            # already calculated
            return
        # find molecule unit size
        nmol = self.xyzmap['raw_data'][-1][7]
        atoms_per_molecule = [ 0 for x in range(nmol) ]
        for atm in self.xyzmap['raw_data']:
            ind = atm[7]-1
            atoms_per_molecule[ind] += 1
        ms0 = atoms_per_molecule[0]
        for i in range(1,nmol):
            if atoms_per_molecule[i] == ms0:
                break
        unitsize = i
        count_unit = 0
        ms0 = 0
        for i in range(unitsize):
            ms0 += atoms_per_molecule[i]
        for i in range(0,nmol,unitsize):
            msi = 0
            for j in range(i,i+unitsize):
                msi += atoms_per_molecule[j]
            if msi == ms0:
                count_unit += 1
            else:
                break
        if count_unit == 1:
            self.print_error( 'ERROR: cannot guess the molecule unit' )
        self.print_log( 'Molecule unit size: %d'%ms0 )
        self.print_log( 'The number of unit: %d'%count_unit )
        self.mol_unit_size = ms0
        self.n_mol_unit = count_unit

    def save_system(self,outfile):
        open(outfile,'w').write( mm.XmlSerializer.serialize(self.system) )

    def read_archive_coord(self):
        varlist = []
        arcfile = self.p['init_bank']
        if isinstance(arcfile,str):
            # arcfile is a filename
            # TODO
            raise ValueError
        else: # arcfile is a container?
            for af in arcfile:
                if af.endswith('.pdb'):
                    var = np.array(app.PDBFile(af).positions.value_in_unit(unit.angstrom))
                    varlist.append(var)
                else:
                    # more file types?
                    raise ValueError
        # multi conformation
        for i in range(len(varlist)):
            if varlist[i].shape[0] > self.xyzmap['natom']:
                varlist[i] = varlist[i].reshape((-1,self.xyzmap['natom'],3))

        return varlist

