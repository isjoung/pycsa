"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Jong Yun Kim, Seung Hwan Hong, Sang Jun Park, Keehyung Joo,
Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

#from future import standard_library
#standard_library.install_aliases()
import inspect
import numpy as np
import pickle
import sys
import os
import time
import datetime
from copy import copy, deepcopy
import csa_util
from symmetric2d import Symmetric2d
from scipy.cluster import hierarchy
from mpi4py import MPI

mpicomm = MPI.COMM_WORLD
mpisize = mpicomm.Get_size()
mpirank = mpicomm.Get_rank()

def dict_merge(a, b):
    '''recursively merges dict's. not just simple a['key'] = b['key'], if
    both a and b have a key who's value is a dict then dict_merge is called
    on both values and the result stored in the returned dictionary.'''
    if not isinstance(b,dict):
        return b
    result = deepcopy(a)
    for k, v in b.items():
        if k in result and isinstance(result[k], dict):
            result[k] = dict_merge(result[k], v)
        else:
            result[k] = deepcopy(v)
    return result

def read_restart_param(rfile):
    if mpirank == 0:
        try:
            with open(rfile,'rb') as f:
                restart = pickle.load(f)
            p = restart['p']
        except:
            p = {}
        mpicomm.bcast(p,root=0)
    else:
        p = mpicomm.bcast(None,root=0)
    return p

class csa(object):
    "class of conformational space anealing"

    # minimize function, which should be overriden
    def minimizef(self,var):
        # return score, minvar
        # type(score): float
        # type(minvar): object
        self.print_error('minimizef must be overriden')
        score = None
        minvar = None
        return score,minvar

    # compare function, which should be overriden
    def comparef(self,var1,var2):
        # return distance
        self.print_error('comparef must be overriden')
        distance = None
        return distance

    # function that generates radomized initial variables
    # it should be overriden
    def randvarf(self,var=None):
        self.print_error('randvarf must be overriden')
        randvar = None
        return randvar

    # function that mates two variables
    # it should be overriden
    def perturbvarf(self,ind1):
        self.print_error('perturbvarf must be overriden')
        newvar = [None]
        perturb_info = [None]
        return newvar, perturb_info

    def print_log(self,message,level=1,rank=0):
        if self.verbose < level or mpirank != rank:
            return

        if rank == 0:
            msg = message
        else:
            msg = '[rank:%d] '%rank + message

        if self.p['log_caller']:
            for i in inspect.stack():
                caller = i[3]
                if ( caller == 'print_log' ): continue
                break
            print("| %s: %s"%(caller,msg),flush=True)
        else:
            print("| %s"%msg,flush=True)

    def print_error(self,message,rank=0):
        if mpirank != rank:
            return

        for i in inspect.stack():
            caller = i[3]
            if ( caller == 'print_error' ): continue
            break
        print("| %s: ERROR: %s"%(caller,message))
        sys.exit(1)

    def __init__(self,param={}):
        # parameter dict
        p = { 'nbank': 10,  # number of bank conformations
              'max_nbank': 500, # number of max bank conformations
              'min_nbank': 10, # number of min bank conformations
              'nbank_add': 0, # number of additional conformations collected at the beginning
              'nseed': 6, # number of seeds(mating) per iteration
              'increase_bank': 0, # 0: no increase, 1: increase by egap
              'increase_bank_energy_cut': 300.0,
              'ecut_reduce': 'fix',
              'max_increase': 9999,
              'increase_bank_dcut': True, # if True, the new conf should be farther than dcut from any conf in bank

              # termination
              'maxiter': -1, # maximum iteration of csa
              'icmax': 3,  # If ncycle reaches icmax, iteration stops
              'iucut': 0, # the number of bank update is below iucut, ncycle increments
              'check_min_dist': 0,

              # dcut
              'init_dcut': None, # initial dcut
              'init_dcut_type': 0, # 0: rbank average, 1, bank average
              'reset_dcut': True,
              'dcut_reduce_method': 'exp', # exp/linear
              'dcut_reduce_steps': None,
              'dcut_reduce': 0.997252158427478, # dcut is reduced by dcut_reduce every iteration 0.4**(1./333) = 0.997...
              'dcut1': 2.0, # initial Dcut is the initial average diff / dcut1
              'dcut2': 5.0, # minimum Dcut is the initial average diff / dcut2
              'dcut_recover': 5,
              'dcut_recover_mode': 0, # 0: no recover, 1: recover when Egap increases, 2: recover when Egap decreases
              'min_dcut': None, # minimum dcut. If None, it is set by dcut2.
              'check_maxscore': True,
              'use_cdcut': 0, # if turned on, cdcut = dcut * cdcut_ratio
              'cdcut_ratio': 3.0, # relative ratio of cdcut to dcut
              'cluster_distance_ratio': 1.5, # cluster distance criterion = ravg / cluster_distance_ratio
              'cluster_update': 0, # 0: remove the top of the largest cluster, 1: remove the top of the current cluster
              'nearest_neighbor': False, # find the closest bank conformation using branch and bound method (EXPERIMENTAL)

              # max diversity
              'diversity_tol': 'max', # number/max/egap, 'max': between min/max score, 'egap': between min/(egap+min) score

              # update
              'local_update': 'normal', # normal/random/diverse, if None, local_update is not evaluated
              'update_mask': [],

              # restart
              'restart_file': 'csa_restart.dat', # restart fileanme
              'save_restart': 0, # restart save frequency. no save if zero
              'restart_mode': 1, # 0: no restart
                                 # 1: restart_file is loaded
                                 # 2: restart_file is loaded without the parameter
              'rerun': False, # if true, dcut is re-initialized

              # seeding
              'seed_mask': [], # any index in the list is not selected as a seed
              # seeding method
              # 0: original seeding method
              # 1: First, new solutions are randomly selected. Second, old solutions are randomly selected.
              #           Clustering and energy comparison is not performed.
              # 2: Solutions are randomly selected regardless of old/new status.
              # 3: similar to 0. Clustering is not performed. Lower score solutions are selected first.
              'random_seed': 0,

              # log
              'write_bank': True, # writes bank conformations
              'write_diff': False, # saving diff file frequency. no write if zero
              'write_score_profile': 1, # Saves the profile file
              'history_file': 'history', # history filename
              'bank_history': None,
              'bank_history_dist_method': None,
              'bank_history_precision': 12,
              'append_history': False, # to force appending to the existing history file
              'profile_file': 'profile.score', # filename of profile
              'profile_precision': 12,
              'log_caller': False,
              'verbose': 2,

              # update rule
              'decut': 0.1, # if the score difference is smaller than this number, the new conformations are not
                            # considered as different ones (bank_status does not reset)

              # rbank
              'pick_rbank': 'sort', # sort: lowest energy conformations
                                    # cluster: similar to seeding algorithm
                                    # order: same order (no sort)

              # temperature
              'temperature': 0.0,
              'temperature_reduce': 0.999,
              'min_temperature': 0.0,
              'boltzmann_const': 1.9872066e-3,
              'recover_temperature': False,

              # biased potential
              'biased_potential': False,
              'bp_max': 1.0e6,
              'bp_sigma': 0.3,
              'bp_sigma_type': 0, # bp_sigma is multipled to one of the followings: 0: dcut, 1: 1.0 (const), 2: min_dcut
              'bp_ratio': 0.5,
              'bp_limit': None,

              # metadynamics
              'metadynamics': False,
              'ap_range': None, # ((dmin1,dmax1,dn1),(dmin2,dmax2,dn2),...)
              'ap_inc': 0.1, # increment of the energy
              'ap_max': 1.0e5, # energy out of boundary

              # nudged elastic band
              'neb': False,
              'neb_end': (0,1),
              'neb_force': (10.0,0.0,1000.0),
              'neb_rcut': 1.0,
              'neb_nseq': 10,

              # 0: serial run
              # 1: master-slave
              # 2: equal distribution
              # 3: continuous
              'parallel_minimization': 1,
              'parallel_minimization_unit': 1, # only for parallel_minimization=3
              'smart_pmunit': 1, # determins parallel_minimization_unit automatically
              'nproc_sub': 1,  # number of processor of the MPI sub-group
              'even_subgroup': False,
            }

        self.p = dict_merge(p,param)
        self.verbose = self.p['verbose']
        self.init_parallel()
        self.init_accessor()

    def init_parallel(self):
        if hasattr(self,'mpicomm_master'):
            # if it is already initialized
            return

        # parallel_minimization == 2 does not support nproc_sub > 1
        assert( self.p['parallel_minimization'] != 2 or self.p['nproc_sub'] == 1 )

        if self.p['parallel_minimization'] == 0:
            self.subgroup_ind = 0
            self.mpicomm_sub = mpicomm
            self.mpicomm_master = mpicomm
        else:
            # number of subgroups
            if self.p['even_subgroup']:
                ngroup = np.ceil((mpisize-1)/self.p['nproc_sub'])
                if ngroup == 0:
                    groups = tuple()
                else:
                    assert( mpisize > ngroup )
                    groups = np.array_split( np.arange(1,mpisize), ngroup )
            else:
                ngroup = np.ceil(mpisize/self.p['nproc_sub'])
                assert( mpisize >= ngroup )
                groups = np.array_split( np.arange(mpisize), ngroup )
                if len(groups[0]) == 1:
                    groups.pop(0)
                else:
                    groups[0] = groups[0][1:]
            groups = [ tuple(x) for x in groups ]
            groups.insert(0,(0,))
            self.print_log('Parallel subgroups: %s'%str(groups), level=2)

            # subgroup communicator
            done = False
            for icolor in range(len(groups)):
                grp = groups[icolor]
                for irank in grp:
                    if irank == mpirank:
                        self.subgroup_ind = icolor
                        done = True
                        break
                if done:
                    break
            self.mpicomm_sub = MPI.COMM_WORLD.Split(self.subgroup_ind,mpirank)

            # define pycsa master communicator
            done = False
            for grp in groups:
                for i in range(len(grp)):
                    if mpirank != grp[i]:
                        continue
                    if i == 0:
                        # subgroup master
                        color = 0
                    else:
                        color = 1
                    done = True
                    break
                if done:
                    break
            self.mpicomm_master = MPI.COMM_WORLD.Split(color,mpirank)
            if color == 1:
                # dummy communicator, so replace it with mpicomm
                self.mpicomm_master = mpicomm

        self.mpisize_sub = self.mpicomm_sub.Get_size()
        self.mpirank_sub = self.mpicomm_sub.Get_rank()

        self.mpisize_master = self.mpicomm_master.Get_size()
        self.mpirank_master = self.mpicomm_master.Get_rank()

    def set_param(self,p):
        self.p = dict_merge(self.p,p)
        self.init_accessor()

    def init_accessor(self):
        # decorate accessors if necessary
        # This subroutine is called both when the object is instantiated
        # and parameters are reset
        self.verbose = self.p['verbose']
        self.read_native_var()

        # metadynamics
        if self.p['metadynamics']:
            self.set_adaptive_potential()

    def read_native_var(self):
        # self.native_var should be a list of native vars
        # example)
        # self.native_var[0] = NV1
        # self.native_var[1] = NV2
        # ...
        pass

    def run(self):
        if self.p['parallel_minimization'] == 0 and mpisize > 1:
            # synchronize random seed
            rseed = np.random.randint(0,2**32)
            if mpirank == 0:
                mpicomm.bcast(rseed,root=0)
            else:
                rseed = mpicomm.bcast(None,root=0)
            np.random.seed(rseed)

        if mpirank == 0 or self.p['parallel_minimization'] == 0:
            self.job_assigned = np.empty(self.mpisize_master,dtype=np.int64)
            self.job_assigned.fill(-1) # -1 means not assigned
            if self.mpisize_master > 1 and self.p['parallel_minimization'] != 0:
                # initialize parallel-related master variables
                self.slave_data_distributed = np.zeros(self.mpisize_master,dtype=np.uint8)
                self.slave_data_distributed[0] = 1
                self.pvar_index_assigned = [None] * self.mpisize_master
                self.job_received = []

        # initialize variable
        self.rbank_minimization = False

        if mpirank == 0 or self.p['parallel_minimization'] == 0:
            self.start_timer('run')
            self.read_runcheck()
            self.get_command_id()
            if self.runcheck == 1:
                if self.p['parallel_minimization'] != 0:
                    # terminate slave jobs
                    cmd_id = self.command_id['terminate']
                    if self.p['parallel_minimization'] != 0:
                        for irank in range(1,self.mpisize_master):
                            self.mpicomm_master.send((cmd_id,2),dest=irank) # termination
                self.print_log( "Iteration is terminated by runcheck" )
                sys.exit()
            self.load_restart()
            self.open_history_file()
            self.start_timer('run/firstbank')
            self.create_firstbank()
            self.cluster_bank()
            self.stop_timer('run/firstbank')
            self.start_timer('run/iterate')
            self.iterate()
            self.stop_timer('run/iterate')
            self.stop_timer('run')
        else:
            self.start_timer('run')
            self.get_command_id()
            self.run_slave()
            self.stop_timer('run')
        self.finalize()
        mpicomm.barrier()

        # runcheck termination
        if mpirank == 0:
            mpicomm.bcast(self.runcheck)
        else:
            self.runcheck = mpicomm.bcast(None)
        if self.runcheck == 1:
            sys.exit()

    def run_slave(self):
        # ONLY SLAVE PROCESSORS RUN THIS SUBROUTINE
        while True:
            self.start_timer('run/idle')
            if self.mpirank_sub == 0:
                # subgroup master receives a job
                cmd_id, data = self.mpicomm_master.recv(source=0)
            if self.mpisize_sub > 1:
                if self.mpirank_sub == 0:
                    self.mpicomm_sub.bcast((cmd_id,data),root=0)
                else:
                    cmd_id, data = self.mpicomm_sub.bcast(None,root=0)
            self.stop_timer('run/idle')
            try:
                cmd = self.command[cmd_id]
            except KeyError:
                raise ValueError('unknown command id %d'%cmd_id)
            self.print_log( "Received %s job"%cmd, level=4, rank=mpirank )
            res = self.run_command(cmd,data)
            if not res:
                break

    def run_command(self,cmd,data):
        if cmd == 'minimize':
            # local minimization
            # run minimize
            self.start_timer('run/minimize')
            output = self.minimize_slave(data)
            self.stop_timer('run/minimize')
            self.print_log( "Returned %s job"%cmd, level=4, rank=mpirank )
            self.start_timer('run/idle/minimize')
            if self.mpirank_sub == 0:
                self.mpicomm_master.send((self.mpirank_master,output),dest=0)
            self.stop_timer('run/idle/minimize')
        elif cmd == 'decorate_data':
            # decorate data
            self.start_timer('run/decorate')
            self.decorate_slave_data(data)
            self.stop_timer('run/decorate')
        elif cmd == 'terminate':
            if data == 1:
                # soft termination
                return 0
            else:
                # hard termination
                sys.exit()
        elif cmd == 'measure_distance':
            self.start_timer('run/measure_distance')
            bb_dist = [ None ] * len(data)
            for i, j in data:
                i, j = pairs[ind]
                bb_dist[ind] = self.comparef(self.bvar[i],self.bvar[j])
            self.stop_timer('run/measure_distance')
            self.start_timer('run/idle/measure_distance')
            if self.mpirank_sub == 0:
                self.mpicomm_master.send((self.mpirank_master,bb_dist),dest=0)
            self.stop_timer('run/idle/measure_distance')
        elif cmd == 'delete_data':
            self.start_timer('run/delete')
            self.delete_slave_data(data)
            self.stop_timer('run/delete')
        else:
            # unknown command
            raise ValueError('unknown command %s'%cmd)

        return 1

    def get_received_data(self,cmd_id):
        ret = []
        ind = 0
        while ind < len(self.job_received):
            if self.job_received[ind][0] == cmd_id:
                ret.append( self.job_received.pop(ind) )
            else:
                ind += 1
        return ret

    def receive_slave_data(self):
        rank, data = self.mpicomm_master.recv(source=MPI.ANY_SOURCE)
        # cmd_id, rank, data
        self.job_received.append((self.job_assigned[rank],rank,data))
        self.job_assigned[rank] = -1

    def decorate_slave_data(self,data):
        for key in data:
            self.__dict__[key] = data[key]

    def delete_slave_data(self,data):
        for key in data:
            del self.__dict__[key]

    def get_slave_work_type(self):
        work_type = [
            (0, 'minimize'),
            (1, 'decorate_data'),
            (2, 'terminate'),
            (3, 'measure_distance'),
            (4, 'delete_data'),
        ]
        return work_type

    def get_command_id(self):
        work_type = self.get_slave_work_type()
        if mpirank != 0:
            self.command = {}
            for cmd_id, cmd in work_type:
                self.command[cmd_id] = cmd
        else: # master
            self.command_id = {}
            for cmd_id, cmd in work_type:
                self.command_id[cmd] = cmd_id

    def write_bank_history_file_header(self):
        f = self.bank_history_fh
        fsize = self.p['bank_history_precision']
        fmt = ' %' + str(fsize) + 's'
        distname = self.get_bank_history_distance_name()
        f.write("#niter bank")
        if self.p['neb']:
            for i in range(len(self.native_var)):
                for j in range(self.p['neb_nseq']):
                    dn = '%d,%d%s'%(i,j,distname)
                    dn = dn[:fsize]
                    f.write(fmt%dn)
        else:
            f.write(fmt%distname[:fsize])
        f.write(fmt%'score' + '\n')
        f.flush()

    def get_bank_history_distance_name(self):
        return 'distance'

    def open_history_file(self):
        # history file handler
        hf = self.p['history_file']
        if hf is None:
            self.history_fh = None
        else:
            if ( self.p['append_history'] or ( os.path.isfile(hf) and self.restart_mode ) ):
                try:
                    self.history_fh = open(hf,'a')
                except:
                    self.print_error('failed to append %s'%hf)
                self.print_log( "open history file in append mode", level=4 )
            else:
                try:
                    self.history_fh = open(hf,'w')
                except:
                    self.print_error('failed to open %s'%hf)
                self.print_log( "open history file in new mode", level=4 )
                self.write_history_file_header()

        # bank_history file handler
        hf = self.p['bank_history']
        if hf is None:
            self.bank_history_fh = None
        else:
            if ( self.restart_mode and os.path.isfile(hf) ):
                self.bank_history_fh = open(hf,'a')
                self.print_log("open bank_history file in append mode", level=4 )
                self.bank_history_header = True
            else:
                self.bank_history_fh = open(hf,'w')
                self.print_log("open bank_history file in new mode", level=4 )
                self.bank_history_header = False

    def write_rbank_history(self,nexclude=0):
        if mpirank != 0: return

        nbvar2 = len(self.bvar)
        if self.bank_history_fh:
            if self.bank_history_header is False:
                self.write_bank_history_file_header()
                self.bank_history_header = True
            f = self.bank_history_fh
            fmt = ' %' + str(self.p['bank_history_precision']) + '.' + str(self.p['bank_history_precision']-7) + 'E'
            for bi in range(nexclude,nbvar2):
                f.write('%6d %4d'%(self.niter,bi+1))
                if self.p['neb']:
                    for nv in self.native_var:
                        for iseq in range(self.p['neb_nseq']):
                            dist = self.get_bank_history_distance(self.bvar[bi][iseq],nv)
                            f.write(fmt%dist)
                else:
                    for nv in self.native_var:
                        dist = self.get_bank_history_distance(self.bvar[bi],nv)
                        f.write(fmt%dist)
                f.write(fmt%self.bscore[bi] + '\n')
            f.flush()

    def write_bank_history(self):
        if mpirank != 0: return

        if self.bank_history_fh:
            if self.bank_history_header is False:
                self.write_bank_history_file_header()
                self.bank_history_header = True
            f = self.bank_history_fh
            fmt = ' %' + str(self.p['bank_history_precision']) + '.' + str(self.p['bank_history_precision']-7) + 'E'
            for mi in range(len(self.update_mvar)):
                bi = self.update_mvar[mi]
                if ( bi < 0 ): continue
                f.write('%6d %4d'%(self.niter,bi+1))
                if self.p['neb']:
                    for nv in self.native_var:
                        for iseq in range(self.p['neb_nseq']):
                            dist = self.get_bank_history_distance(self.mvar[mi][iseq],nv)
                            f.write(fmt%dist)
                    f.write(fmt%self.mscore[mi] + '\n')
                else:
                    for nv in self.native_var:
                        dist = self.get_bank_history_distance(self.mvar[mi],nv)
                        f.write(fmt%dist)
                    f.write(fmt%self.mscore[mi] + '\n')
            f.flush()

    def get_bank_history_distance(self,var1,var2):
        return self.comparef(var1,var2)

    def load_restart(self):
        # mode == 0: do not read the restart file
        # mode == 1: read the restart file
        # mode == 2: read the restart file except 'p'

        mode = self.p['restart_mode']
        # if csa is in the middle of repeated runs, self has 'niter'
        if not mode or hasattr(self,'niter'):
            self.restart_mode = mode
            return

        # read the restart file
        restart_file = self.p['restart_file']
        self.print_log( "loading %s for restart"%(restart_file) )
        try:
            f = open(restart_file,'rb')
        except:
            self.print_log( "cannot read the restart file, %s"%(restart_file) )
            self.restart_mode = 0
            return

        # load the restart file
        loaded_self = pickle.load(f)
        f.close()
        if mode == 2:
            try:
                del loaded_self['p']
            except:
                pass
        for key in loaded_self.keys():
            self.load_restart_key(key,loaded_self)
        if mode == 2:
            self.init_accessor()
        self.restart_mode = mode

        # destroy loaded self
        loaded_self = None

    def load_restart_key(self,key,restart_data):
        if key == 'bank_distance':
            nbank = int((1+int(np.sqrt(1+8*len(restart_data['bank_distance']))))/2)
            self.bank_distance = Symmetric2d(nbank,matrix=restart_data['bank_distance'])
        else:
            self.__dict__[key] = restart_data[key]

    def create_firstbank(self):
        self.print_log("create first banks")

        if self.restart_mode == 0:
            # this is not a continuous run
            self.niter = 1
            self.ncycle = 0
        self.check_param()

        # read bank size
        try:
            bank_size = len(self.rvar)
        except:
            bank_size = 0

        if not hasattr(self,'ntrial'):
            # counter for the number of trial solutions
            self.ntrial = 0

        # set min_dcut
        if self.p['min_dcut'] != None:
            self.min_dcut = self.p['min_dcut']

        self.set_temperature()

        # send data to minimize rbank
        self.rbank_minimization = True
        if self.p['parallel_minimization'] != 0:
            decorate_cmd_id = self.command_id['decorate_data']
            decorate_data = { 'rbank_minimization': True, 'temperature': self.temperature }
            for irank in range(1,self.mpisize_master):
                self.mpicomm_master.send((decorate_cmd_id,decorate_data),dest=irank)
        if bank_size < self.p['nbank'] or self.p['rerun']:
            # rbank is added or davg of rabnk is recalculated
            self.seed_mask = list(self.p['seed_mask']) # copy seed mask list
            self.add_rbank()
            self.compare_bb()
            self.set_dcut()
            self.set_increase_bank_energy_cut()
            self.set_bpmax()
        # unset rbank_minimization
        self.rbank_minimization = False
        if self.p['parallel_minimization'] != 0:
            decorate_data = { 'rbank_minimization': False }
            for irank in range(1,self.mpisize_master):
                self.mpicomm_master.send((decorate_cmd_id,decorate_data),dest=irank)
        if not hasattr(self,'seed_mask'):
            self.seed_mask = list(self.p['seed_mask'])
        if not hasattr(self,'bp_max'):
            self.bp_max = self.p['bp_max']
        if not hasattr(self,'update_mask'):
            self.update_mask = copy(self.p['update_mask'])

        self.find_min()
        self.find_max()

        self.report_create_firstbank()

    def set_adaptive_potential(self):
        if mpirank != 0:
            return

        if not hasattr(self,'adaptive_potential'):
            if self.p['ap_range'] is None:
                self.adaptive_potential = None
            else:
                dimension = []
                for dmin, dmax, dn in self.p['ap_range']:
                    dimension.append(dn)
                dimension = tuple(dimension)
                self.adaptive_potential = np.zeros(dimension,dtype=np.double)

    def set_increase_bank_energy_cut(self):
        self.increase_bank_energy_cut = self.p['increase_bank_energy_cut']

    def set_temperature(self):
        try:
            self.temperature
        except:
            self.temperature = self.p['temperature']
        self.print_log( "temperature: %f" % (self.temperature) )

    def set_bpmax(self):
        try:
            self.bp_max
        except:
            self.bp_max = self.p['bp_max']
        if self.bp_max is None:
            self.bp_max = np.amax(self.rscore) - np.amin(self.rscore)
        self.print_log("Initial bp_max: %f"%(self.bp_max))

    def set_dcut(self):
        if not self.p['reset_dcut'] and hasattr(self,'init_dcut'):
            self.dcut = self.init_dcut
        elif self.p['init_dcut'] is None:
            if self.p['init_dcut_type'] == 0:
                self.compare_rr()
                self.dcut = self.ravg_dist / self.p['dcut1']
            else: # type == 1
                self.dcut = self.avg_dist / self.p['dcut1']
            try:
                del self.min_dcut
            except:
                pass
        else:
            self.dcut = self.p['init_dcut']
        self.init_dcut = self.dcut
        if self.p['reset_dcut'] or not hasattr(self,'min_dcut'):
            if self.p['min_dcut'] is not None:
                self.min_dcut = self.p['min_dcut']
            else:
                if self.p['init_dcut_type'] == 0:
                    if not hasattr(self,'ravg_dist'):
                        self.compare_rr()
                    self.min_dcut = self.ravg_dist / self.p['dcut2']
                else:
                    self.min_dcut = self.avg_dist / self.p['dcut2']

        if self.p['dcut_reduce_steps']:
            if self.p['dcut_reduce_method'] == 'linear':
                self.dcut_reduce = (self.init_dcut-self.min_dcut)/self.p['dcut_reduce_steps']
            else:
                self.dcut_reduce = (self.min_dcut/self.init_dcut)**(1./self.p['dcut_reduce_steps'])
            if self.p['dcut_reduce'] is not None:
                self.print_log("WARNING: Parameter 'dcut_reduce' is ignored")
        else:
            self.dcut_reduce = self.p['dcut_reduce']
        self.print_log("Dcut reduce: %f"%(self.dcut_reduce))

        self.print_log("Initial Dcut: %f"%(self.dcut))
        self.print_log("Minimum Dcut: %f"%(self.min_dcut))
        if self.p['use_cdcut']:
            self.cdcut = self.dcut * self.p['cdcut_ratio']
            self.min_cdcut = self.min_dcut * self.p['cdcut_ratio']
            if not hasattr(self,'ravg_dist'):
                self.compare_rr()
            self.cluster_distance = self.ravg_dist / self.p['cluster_distance_ratio']
            self.print_log("Initial CDcut: %f"%(self.cdcut))

    def write_history_file_header(self):
        pre = str(self.p['profile_precision'])
        fmt = '%6s %3s %10s %4s %4s %' + pre + 's %' + pre + 's %15s %4s %4s\n'
        self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_score','max_score','ntrial','iuse','nbk'))

    def write_history_file(self):
        self.print_log( "Writing history", level=4 )
        pre0 = self.p['profile_precision']
        pre1 = pre0 - 7
        fmt_e = ' %' + str(pre0) + '.' + str(pre1) + 'E'
        fmt = '%6d %3d %10.4E %4d %4d' + fmt_e + fmt_e + ' %15d %4d %4d\n'
        self.history_fh.write(fmt
            %(self.niter,self.ncycle,self.dcut,self.minscore_ind+1,self.maxscore_ind+1,self.minscore,self.maxscore,self.ntrial,self.count_unused_bank(),self.count_bank()))
        self.history_fh.flush()

    def report_create_firstbank(self):
        if ( self.p['write_bank'] ):
            self.write_bank('r','',self.rvar,self.rscore)
        if ( self.p['write_score_profile'] ):
            self.write_score_profile(self.rscore)
        if ( not self.history_fh ): return
        self.write_history_file()

    def write_score_profile(self,scores):
        self.print_log( "writing score profile" )
        f = open(self.p['profile_file'],'w')
        fmt_h = '%4s %12s %' + str(self.p['profile_precision']) + 's\n'
        if hasattr(self,'native_var'):
            fmt_h = '%4s'
            fmt = '%4d'
            header = [ '#bnk' ]
            n_native_var = len(self.native_var)
            for i in range(n_native_var):
                fmt_h += ' %12s'
                fmt += ' %12.5E'
                header.append('distance%d'%i)
            fmt_h += ' %' + str(self.p['profile_precision']) + 's\n'
            fmt += ' %' + str(self.p['profile_precision']) + '.' + str(self.p['profile_precision']-7) + 'E\n'
            header.append('score')
            f.write(fmt_h%tuple(header))
            for i in range(len(scores)):
                line = [ i+1 ]
                for j in range(n_native_var):
                    dist = self.comparef(self.bvar[i],self.native_var[j])
                    line.append(dist)
                line.append(scores[i])
                f.write(fmt%tuple(line))
        else:
            fmt_h = '%4s %' + str(self.p['profile_precision']) + 's\n'
            fmt = '%4d %' + str(self.p['profile_precision']) + '.' + str(self.p['profile_precision']-7) + 'E\n'
            header = [ '#bnk', 'score' ]
            f.write(fmt_h%tuple(header))
            for i in range(len(scores)):
                f.write(fmt%(i+1,scores[i]))
        f.close()

    def write_bank(self,prefix,suffix,ivars,scores=None):
        for i in range(len(ivars)):
            outfile = '%s%03d%s'%(prefix,i+1,suffix)
            with open(outfile,'w') as f:
                for v in ivars[i]:
                    f.write(str(v)+'\n')
                if scores is not None:
                    f.write('\n%s\n'%str(scores[i]))
        if scores is not None:
            imin = scores.index(min(scores))
            outfile = 'gmin%s'%suffix
            with open(outfile,'w') as f:
                for v in ivars[imin]:
                    f.write(str(v)+'\n')
                f.write('\n%s\n'%str(scores[imin]))

    def iterate_init(self):
        self.print_log("csa iteration starts")

        if self.p['parallel_minimization'] != 3 or mpisize == 1:
            self.first_pm3 = False
        else:
            self.first_pm3 = True # first iteration of parallel minimization 3

        # ncycle -> See iucut, icmax for the meaning
        # niter = csa iteration step

    def terminate_iteration(self,status=1):
        self.print_log("Continuous minimization is waiting for the last iteration")
        # status:
        #    1: soft termination
        #    2: hard termination
        #self.mvar = []
        #self.perturb_info = []
        self.minimize_init()
        self.eval_update_init()
        # merge left-over
        try:
            self.pvar_index = len(self.pvar_left)
        except:
            self.pvar_index = 0
        if self.pvar_index:
            self.pvar = self.pvar_left + self.pvar
            self.perturb_info_backup = self.perturb_info_left + self.perturb_info_backup
        received = False

        while not all( self.job_assigned == -1 ):
            self.start_timer('run/iterate/minimize/parallel/idle')
            self.receive_slave_data()
            self.stop_timer('run/iterate/minimize/parallel/idle')
            _, irank, min_output = self.get_received_data(self.command_id['minimize'])[0]
            self.unpack_min_output(min_output,self.pvar_index_assigned[irank])
            self.eval_update_single(len(self.mvar)-1)
            received = True

        for irank in range(1,self.mpisize_master):
            # send terminate message
            self.mpicomm_master.send((self.command_id['terminate'],status),dest=irank)

        # finish iteration
        if received:
            self.eval_update_final()
            self.update_and_adjust_annealing()
            self.iterate_single_final(skip_check=True)
        if status > 1:
            self.finalize()
            sys.exit()

    def iterate_single_init(self):
        # old max score
        self.old_maxscore = self.maxscore

        # termination conditions

        # check maxiter
        if self.p['maxiter'] >= 0 and self.niter > self.p['maxiter']:
            self.print_log( "maxiter has reached" )
            return 1

        # check_run
        self.read_runcheck()
        if self.runcheck == 1:
            self.print_log( "Iteration is terminated by runcheck" )
            return 1

        # count the number of unused bank
        try:
            self.n_unused_bank
        except:
            self.n_unused_bank = self.count_unused_bank()

        self.print_log( "Iteration step: %d" % (self.niter) )
        return 0

    def iterate(self):
        self.iterate_init()

        while True:
            status = self.iterate_single_init()
            if status:
                break

            # start iteration
            self.prepare_seed()
            self.perturbvar()
            self.print_log("%d new conformations have been generated"%len(self.mvar))

            self.minimize_and_eval_update()
            self.update_and_adjust_annealing()

            status = self.iterate_single_final()
            if status:
                break

        if self.p['parallel_minimization'] == 3 and mpisize > 1:
            self.terminate_iteration(1)
        elif mpisize > 1 and self.p['parallel_minimization'] != 0:
            cmd_id = self.command_id['terminate']
            for irank in range(1,self.mpisize_master):
                self.mpicomm_master.send((cmd_id,1),dest=irank)

        self.iterate_final()

    def minimize_init_slave_data(self):
        data = {}
        if hasattr(self,'temperature'):
            data['temperature'] = self.temperature
        if self.p['metadynamics']:
            data['adaptive_potential'] = self.adaptive_potential

        if data:
            return data
        else:
            return None

    def update_and_adjust_annealing(self):
        self.start_timer('run/iterate/update_bank')
        self.update_bank()
        self.stop_timer('run/iterate/update_bank')
        self.reduce_bank()
        self.compare_bb()
        self.find_min()
        self.find_max()
        self.cluster_bank()
        self.adjust_increase_bank_energy_cut()
        self.adjust_dcut()
        self.adjust_temperature()
        self.report_iterate()
        if self.remove_bank_list:
            self.remove_bank(self.remove_bank_list)
            self.remove_bank_list = []

    def count_bank(self):
        try:
            nbank = len(self.bvar) - len(self.remove_bank_list)
        except:
            try:
                nbank = len(self.bvar)
            except:
                nbank = 0
        return nbank

    def minimize_and_eval_update(self):
        if self.p['parallel_minimization'] == 3 and mpisize > 1:
            self.minimize_init()
            self.eval_update_init()
            # merge left-over
            try:
                self.pvar_index = len(self.pvar_left)
            except:
                self.pvar_index = 0
            if self.pvar_index:
                self.pvar = self.pvar_left + self.pvar
                self.perturb_info_backup = self.perturb_info_left + self.perturb_info_backup
            while self.pvar_index < len(self.pvar):
                n_recv = self.minimize_continuous()
                for i in range(len(self.mvar)-n_recv,len(self.mvar)):
                    self.eval_update_single(i)
            self.eval_update_final()
            # reorganize indexes of the working jobs
            self.pvar_left = []
            self.perturb_info_left = []
            pi_index = 0
            for irank in range(1,self.mpisize_master):
                j_list = self.pvar_index_assigned[irank]
                if j_list is not None:
                    for j in j_list:
                        self.pvar_left.append( self.pvar[j] )
                        self.perturb_info_left.append( self.perturb_info_backup[j] )
                    self.pvar_index_assigned[irank] = list(range(pi_index,pi_index+len(j_list)))
                    pi_index += len(j_list)
        else:
            self.start_timer('run/iterate/minimize')
            self.minimize()
            self.stop_timer('run/iterate/minimize')
            self.start_timer('run/iterate/eval_update')
            self.eval_update()
            self.stop_timer('run/iterate/eval_update')

    def minimize_continuous(self):
        # parallel_minimization == 3
        self.start_timer('run/iterate/minimize/continuous')

        pvar_len = len(self.pvar)

        if self.p['smart_pmunit']:
            #print 'self.job_assigned', self.job_assigned
            n_job = pvar_len - self.pvar_index
            pm_unit = min( int( n_job * self.p['smart_pmunit'] / (self.mpisize_master-1) ),
                           int( n_job / max(1,(np.count_nonzero(self.job_assigned==-1)-1)) ) )
        else:
            pm_unit = self.p['parallel_minimization_unit']
        pm_unit = max(1,pm_unit)

        minimize_cmd_id = self.command_id['minimize']
        add_data = self.minimize_init_slave_data()
        for irank in range(1,self.mpisize_master):
            if self.pvar_index >= pvar_len:
                break
            if self.job_assigned[irank] == -1:
                if not self.slave_data_distributed[irank]:
                    self.slave_data_distributed[irank] = True
                    if add_data is not None:
                        self.mpicomm_master.send((self.command_id['decorate_data'],add_data),dest=irank)
                if pm_unit + self.pvar_index <= pvar_len:
                    pvar_indexes = list(range(self.pvar_index,self.pvar_index+pm_unit))
                    self.pvar_index += pm_unit
                else:
                    pvar_indexes = list(range(self.pvar_index,pvar_len))
                    self.pvar_index = pvar_len
                min_input = self.pack_min_input(pvar_indexes)
                self.mpicomm_master.send((minimize_cmd_id,min_input),dest=irank)
                self.job_assigned[irank] = minimize_cmd_id
                self.pvar_index_assigned[irank] = pvar_indexes
        # Receive jobs
        self.start_timer('run/iterate/minimize/continuous/idle')
        self.receive_slave_data()
        self.stop_timer('run/iterate/minimize/continuous/idle')
        _, src_rank, min_output = self.get_received_data(self.command_id['minimize'])[0]
        n_recv = len(self.pvar_index_assigned[src_rank])
        self.unpack_min_output(min_output,self.pvar_index_assigned[src_rank])
        self.job_assigned[src_rank] = -1
        # Send a job again
        if self.pvar_index < pvar_len:
            if not self.slave_data_distributed[src_rank]:
                if add_data is not None:
                    self.mpicomm_master.send((self.command_id['decorate_data'],add_data),dest=src_rank)
                self.slave_data_distributed[src_rank] = True
            if pm_unit + self.pvar_index <= pvar_len:
                pvar_indexes = list(range(self.pvar_index,self.pvar_index+pm_unit))
                self.pvar_index += pm_unit
            else:
                pvar_indexes = list(range(self.pvar_index,pvar_len))
                self.pvar_index = pvar_len
            min_input = self.pack_min_input(pvar_indexes)
            self.mpicomm_master.send((minimize_cmd_id,min_input),dest=src_rank)
            self.job_assigned[src_rank] = minimize_cmd_id
            self.pvar_index_assigned[src_rank] = pvar_indexes
        self.stop_timer('run/iterate/minimize/continuous')
        return n_recv

    def minimize_slave(self,data):
        min_output = []
        for min_input in data:
            min_output.append( self.minimizef(*min_input) )
        return min_output

    def iterate_final(self):
        if self.p['save_restart'] > 0:
            if self.niter % self.p['save_restart'] != 0:
                self.save_restart()
        elif self.p['save_restart'] == 0:
            self.save_restart()

        # reset
        #self.saved_seed_mask = self.seed_mask
        del self.seed_mask

        self.print_log( "csa iteration is over" )

    def iterate_single_final(self,skip_check=False):
        self.niter += 1

        if ( self.p['save_restart'] > 0 and self.niter % self.p['save_restart'] == 0 ):
            self.save_restart()

        if self.first_pm3 == True:
            self.first_pm3 = False
            return 0

        if skip_check:
            return 0

        if self.p['check_min_dist'] == 1:
            min_dist = self.bank_distance.amin()
        elif self.p['check_min_dist']: # 2
            min_dist = self.dcut
            if hasattr(self,'dcut_recover_limit'):
                min_dist = self.dcut_recover_limit

        iucut = self.p['iucut']
        if isinstance(iucut,str) and iucut[-1] == '%':
            iucut = int((len(self.bvar) - len(self.seed_mask)) * float(iucut[:-1]) / 100.0)
        if (     self.n_unused_bank <= iucut
             and (    not self.p['check_min_dist']
                   or (     self.p['check_min_dist']
                        and (    min_dist <= self.min_dcut
                              or self.dcut <= self.min_dcut ) ) ) ):
            self.ncycle += 1
            self.print_log( "The number of unused banks is below iucut. ncycle: %d"%(self.ncycle) )
            self.reset_bank_status()
        if ( self.ncycle > self.p['icmax'] ):
            self.print_log( "ncycle has exeeded icmax" )
            return 1

        return 0

    def read_runcheck(self):
        try:
            self.runcheck = int(open('runcheck','r').readline())
        except:
            self.runcheck = 0

    def reset_bank_status(self):
        self.print_log('reset bank status',level=2)
        for i in range(len(self.bank_status)):
            self.bank_status[i] = False

    def adjust_temperature(self):
        if self.first_pm3:
            return

        if self.p['recover_temperature']:
            self.temperature /= self.p['temperature_reduce']
        else:
            self.temperature *= self.p['temperature_reduce']
        if ( self.temperature < self.p['min_temperature'] ):
            self.temperature = self.p['min_temperature']
        self.print_log( "temperature: %f" % (self.temperature) )

    def adjust_dcut(self):
        if self.first_pm3:
            return

        #if not hasattr(self,'dcut_reduce'):
        #    # for the compatibility of old method
        #    self.dcut_reduce = self.p['dcut_reduce']

        self.print_log("dcut is adjusted")
        dr_mode = self.p['dcut_recover_mode']
        if self.bp_limit is not None:
            if dr_mode:
                if hasattr(self,'dcut_recover_limit'):
                    drl = self.dcut_recover_limit
                else:
                    drl = self.dcut
            if dr_mode == 1 or dr_mode == 2:
                new_dscore = self.maxscore - self.minscore
                if hasattr(self,'delta_score'):
                    old_dscore = self.delta_score
                else:
                    old_dscore = new_dscore
                if (     (    ( dr_mode == 1 and old_dscore < new_dscore )
                           or ( dr_mode == 2 and old_dscore > new_dscore ))
                     and drl >= self.dcut):
                    self.dcut_recover_limit = self.dcut
                    if self.p['dcut_reduce_method'] == 'linear':
                        self.dcut += self.dcut_reduce*self.p['dcut_recover']
                    else:
                        self.dcut /= self.dcut_reduce**self.p['dcut_recover']
                else:
                    if self.p['dcut_reduce_method'] == 'linear':
                        self.dcut -= self.dcut_reduce
                    else:
                        self.dcut *= self.dcut_reduce
                self.delta_score = new_dscore
            elif dr_mode == 3:
                if hasattr(self,'n_top_removed'):
                    ntr = self.n_top_removed
                else:
                    ntr = 0
                if ntr > 0 and drl >= self.dcut:
                    self.dcut_recover_limit = self.dcut
                    if self.p['dcut_reduce_method'] == 'linear':
                        self.dcut += self.dcut_reduce*self.p['dcut_recover']
                    else:
                        self.dcut /= self.dcut_reduce**self.p['dcut_recover']
                else:
                    if self.p['dcut_reduce_method'] == 'linear':
                        self.dcut -= self.dcut_reduce
                    else:
                        self.dcut *= self.dcut_reduce
            else:
                if self.p['dcut_reduce_method'] == 'linear':
                    self.dcut -= self.dcut_reduce
                else:
                    self.dcut *= self.dcut_reduce
            if ( self.dcut < self.min_dcut ):
                self.dcut = self.min_dcut
                if hasattr(self,'dcut_recover_limit'):
                    self.dcut_recover_limit = self.min_dcut
        self.print_log( "D_cut: %f" % (self.dcut) )
        if self.p['use_cdcut']:
            self.cdcut = self.dcut * self.p['cdcut_ratio']
            self.print_log( "CD_cut: %f" %self.cdcut )

    def restart_key_list(self):
        return [ 'p', 'bvar', 'bscore', 'rvar', 'rscore', 'ncycle', 'niter', 'init_dcut',
                 'dcut', 'min_dcut', 'cdcut', 'min_cdcut', 'bank_distance', 'bank_status',
                 'increase_bank_energy_cut', 'temperature', 'seed_mask',
                 'bp_max', 'adaptive_potential', 'delta_score', 'dcut_reduce',
                 'dcut_recover_limit', 'n_top_removed', 'ntrial', 'update_mask', 'bp_sigma2',
                 'cluster', 'n_cluster', 'cluster_distance', 'ravg_dist' ]

    def save_restart(self):
        if mpirank != 0: return

        # save restart
        restart_file = self.p['restart_file']
        self.print_log( "save %s for restart" %(restart_file), level=2 )
        savedata = {}
        for key in self.restart_key_list():
            try:
                savedata[key] = self.save_restart_key(key)
            except:
                pass
        with open(restart_file, 'wb') as f:
            pickle.dump(savedata,f,2)

    def save_restart_key(self,key):
        if key == 'bank_distance':
            return self.bank_distance.get_1dmat()
        else:
            return self.__dict__[key]

    def report_iterate(self):
        if mpirank != 0: return

        if ( self.p['write_diff'] and self.niter % self.p['write_diff'] == 0 ):
            self.write_diff()
        if ( self.p['write_bank'] ):
            self.write_bank('b','',self.bvar,self.bscore)
        if ( self.p['write_score_profile'] ):
            self.write_score_profile(self.bscore)
        if ( not self.history_fh ): return
        self.write_history_file()

    def write_diff(self):
        self.print_log("writing diff file")
        nbank = len(self.bvar)
        f = open('diff','w')
        for i in range(nbank):
            for j in range(nbank):
                diff = self.bank_distance.get_elem(i,j)
                f.write('%5d %5d %15.8E\n'%(i+1,j+1,diff))
            f.write('\n')
        f.close()

    def finalize(self):
        self.print_log("csa finalizes")

        # history file
        try:
            self.history_fh.close()
            del self.history_fh
        except:
            pass

        # bank_history file
        try:
            self.bank_history_fh.flush()
        except:
            pass

        try:
            del self.dcut_recover_limit
        except:
            pass
        try:
            del self.delta_score
        except:
            pass
        self.show_timer()

    def minimize_init(self):
        try:
            self.pvar = self.mvar
        except:
            self.pvar = []
        try:
            self.perturb_info_backup = self.perturb_info
            self.perturb_info = []
        except:
            self.perturb_info_backup = []
        self.mvar = []
        self.mscore = []

    def pack_min_input(self,i_list):
        ret = []
        for i in i_list:
            # pack i-th pvar
            ret.append( (self.pvar[i],) )
            # destroy pvar[i] to save memory
            self.pvar[i] = None
        return ret

    def minimize(self):
        if mpisize == 1 or self.p['parallel_minimization'] == 0:
            self.print_log("serial minimization",level=4)
            # minimize mvar
            self.print_log("minimizing conformations")
            self.minimize_init()
            for pvar_ind in range(len(self.pvar)):
                min_input = self.pack_min_input((pvar_ind,))
                min_output = self.minimizef(*min_input[0])
                self.unpack_min_output((min_output,),(pvar_ind,))
        else:
            self.print_log("parallel minimization",level=4)
            if self.p['parallel_minimization'] == 1 or self.rbank_minimization:
                self.minimize_parallel()
            elif self.p['parallel_minimization'] == 2:
                self.minimize_parallel_2()
            else:
                raise ValueError("Unknown parallel_minimization: %s"%str(self.p['parallel_minimization']))

    def minimize_parallel(self):
        self.start_timer('run/iterate/minimize/parallel')
        self.minimize_init()
        data = self.minimize_init_slave_data()
        #print 'self.job_assigned', self.job_assigned
        minimize_cmd_id = self.command_id['minimize']
        if len(self.pvar):
            self.pvar_index = 0
            # first job distribution
            for irank in range(1,self.mpisize_master):
                if self.job_assigned[irank] != -1:
                    continue
                # slave data distribution
                if not self.slave_data_distributed[irank]:
                    self.slave_data_distributed[irank] = True
                    if data is not None:
                        self.mpicomm_master.send((self.command_id['decorate_data'],data),dest=irank)
                if self.pvar_index >= len(self.pvar):
                    break
                min_input = self.pack_min_input((self.pvar_index,))
                self.mpicomm_master.send((minimize_cmd_id,min_input),dest=irank)
                self.job_assigned[irank] = minimize_cmd_id
                self.pvar_index_assigned[irank] = (self.pvar_index,)
                self.pvar_index += 1
            while True:
                # receive minimized var
                self.start_timer('run/iterate/minimize/parallel/idle')
                self.receive_slave_data()
                self.stop_timer('run/iterate/minimize/parallel/idle')
                _, irank, min_output = self.get_received_data(minimize_cmd_id)[0]
                self.unpack_min_output(min_output,self.pvar_index_assigned[irank])
                # give another job if left
                if self.pvar_index < len(self.pvar):
                    min_input = self.pack_min_input((self.pvar_index,))
                    self.mpicomm_master.send((minimize_cmd_id,min_input),dest=irank)
                    self.job_assigned[irank] = minimize_cmd_id
                    self.pvar_index_assigned[irank] = (self.pvar_index,)
                    self.pvar_index += 1
                else:
                    break
        # receive minimized var and send termination signal
        while not all( self.job_assigned == -1 ):
            self.start_timer('run/iterate/minimize/parallel/idle')
            self.receive_slave_data()
            self.stop_timer('run/iterate/minimize/parallel/idle')
            _, irank, min_output = self.get_received_data(minimize_cmd_id)[0]
            self.unpack_min_output(min_output,self.pvar_index_assigned[irank])
        self.stop_timer('run/iterate/minimize/parallel')

    def minimize_parallel_2(self):
        # In parallel_minimization mode 2,
        # 'mpicomm' is always same as 'self.mpicomm_master'

        self.start_timer('run/iterate/minimize/parallel2')
        self.minimize_init()

        # distribute new data
        data = self.minimize_init_slave_data()
        if data:
            cmd_id = self.command_id['decorate_data']
            for i in range(1,self.mpisize_master):
                self.mpicomm_master.send((cmd_id,data),dest=i)

        # pack before sending
        jobs = [ self.pack_min_input(x) for x in np.array_split(np.arange(len(self.pvar)),self.mpisize_master) ]

        # send jobs
        cmd_id = self.command_id['minimize']
        for i in range(1,self.mpisize_master):
            job = jobs[i-1]
            if not job:
                break
            self.mpicomm_master.send((cmd_id,job),dest=i)
            self.job_assigned[i] = cmd_id

        # master also performs minimization
        master_min_output = []
        for min_input in jobs[self.mpisize_master-1]:
            min_output = self.minimizef(*min_input)
            master_min_output.append(min_output)

        # receives all the results
        while not all( self.job_assigned == -1 ):
            self.start_timer('run/iterate/minimize/parallel2/idle')
            self.receive_slave_data()
            self.stop_timer('run/iterate/minimize/parallel2/idle')

        min_outputs = [x[2] for x in sorted( self.get_received_data(cmd_id), key=lambda x: x[1] )]
        min_outputs.append(master_min_output)
        pvar_index = 0
        for min_output in min_outputs:
            pvar_indexes = list(range(pvar_index,pvar_index+len(min_output)))
            #print 'pvar_indexes', pvar_indexes
            self.unpack_min_output(min_output,pvar_indexes)
            pvar_index += len(min_output)

        self.stop_timer('run/iterate/minimize/parallel2')

    def unpack_min_output(self,min_output,i_list):
        for ind in range(len(i_list)):
            i = i_list[ind]
            mo = min_output[ind]
            score, minvar = mo
            self.mvar.append(minvar)
            self.mscore.append(score)
            try:
                # In case of randomization, perturb_info is absent.
                self.perturb_info.append( self.perturb_info_backup[i] )
            except:
                pass
            self.print_log("m%d score: %.5E"%(len(self.mvar),score), level=3)

    def comparef_list(self,var0,ind=None):
        varlist = self.new_bvar
        # measure distance from var0 to vars in varlist
        n_varlist = len(varlist)
        distance = np.empty(n_varlist,dtype=np.double)
        if self.p['nearest_neighbor']:
            # EXPERIMENTAL
            distance[:] = -9.999e99
            if ind is None:
                indi = 0
                indj = 1 #self.bank_distance.argmin_column(indi)
            else:
                indi = ind
                indj = self.bank_distance.argmin_column(indi)
                #sort_indj = self.bank_distance.sort_column(indi)
                #jsize = sort_indj.size
                #indj = sort_indj[0]
            # cover 20% near the seed conformation
            #for i in xrange(int(jsize*0.5)):
            #    distance[i] = self.comparef(var0,varlist[i])
            # repeated cutting planes
            for repeat in range(1):
                cmask = np.ones(n_varlist,dtype=np.uint8)
                if distance[indi] < 0.0:
                    dist_i = self.comparef(var0,varlist[indi])
                    distance[indi] = dist_i
                else:
                    dist_i = distance[indi]
                if distance[indj] < 0.0:
                    dist_j = self.comparef(var0,varlist[indj])
                    distance[indj] = dist_j
                else:
                    dist_j = distance[indj]
                cmask[indi] = 0
                cmask[indj] = 0
                while True:
                    if dist_i <= dist_j:
                        self.bank_distance.mask_closer_elem(indj,indi,cmask)
                        if not np.any(cmask):
                            break
                        while not cmask[indj]:
                            indj += 1
                            if indj == n_varlist:
                                indj = 0
                        if distance[indj] < 0.0:
                            dist_j = self.comparef(var0,varlist[indj])
                            distance[indj] = dist_j
                        else:
                            dist_j = distance[indj]
                        cmask[indj] = 0
                    else:
                        self.bank_distance.mask_closer_elem(indi,indj,cmask)
                        if not np.any(cmask):
                            break
                        while not cmask[indi]:
                            indi += 1
                            if indi == n_varlist:
                                indi = 0
                        if distance[indi] < 0.0:
                            dist_i = self.comparef(var0,varlist[indi])
                            distance[indi] = dist_i
                        else:
                            dist_i = distance[indi]
                        cmask[indi] = 0
                inds = np.where(distance<0.0)[0]
                isize = len(inds)
                if isize == 0:
                    break
                elif isize == 1:
                    ind = inds[0]
                    distance[ind] = self.comparef(var0,varlist[ind])
                    break
                indi = np.argmin(np.absolute(distance))

                sort_indj = self.bank_distance.sort_column(indi)
                for k in range(1,sort_indj.size):
                    indi = sort_indj[k]
                    if distance[indi] < 0.0:
                        break
                for indj in sort_indj[k+1:]:
                    if distance[indj] < 0.0:
                        break
                #print 'indi, indj', indi, indj
                #indi, indj = np.random.choice(inds,2,replace=False)
                #print distance
                #print 'repeat'

            # FOR DEBUGGING
            #minj0 = np.argmin(np.absolute(distance))
            #min_d = distance[minj0]
            #print 'MIND', min_d
            #if minj0 != ind or min_d > self.dcut:
            #    for i in xrange(n_varlist):
            #        if distance[i] < 0.0:
            #            distance[i] = self.comparef(var0,varlist[i])
            #    minj0 = np.argmin(distance)
            #print distance
            #for i in xrange(n_varlist):
            #    if distance[i] < 0.0:
            #        distance[i] = self.comparef(var0,varlist[i])
            #print distance
            #minj1 = np.argmin(distance)
            #print 'IND', ind, 'OLD', minj1, 'NEW', minj0, distance[minj1], distance[minj0], self.dcut
            #if minj1 != minj0:
            #    print 'FAIL'
            #    raw_input()
        else:
            for i in range(n_varlist):
                distance[i] = self.comparef(var0,varlist[i])
        return distance

    def eval_update_init(self):
        # copy self.bvar and self.bscore
        self.new_bvar = copy(self.bvar)
        self.new_bscore_real = np.array(self.bscore)

        # update_bvar = [] # -1: no change, integer: replaced by the index of mvar
        # update_mvar = [] # -1: rejected, integer: replaced the index of bvar
        self.update_bvar = [ -1 for x in range(len(self.bvar)) ]
        self.update_mvar = []

        # maximum allowed energy gap
        try:
            self.max_allowed_gap = self.increase_bank_energy_cut
        except:
            self.max_allowed_gap = 0.0
        if not isinstance(self.p['diversity_tol'],str):
            self.max_allowed_gap = max(self.max_allowed_gap,self.p['diversity_tol'])

        # beta
        if self.temperature > 0.0:
            self.beta = 1.0 / ( self.temperature * self.p['boltzmann_const'] )
        else:
            self.beta = None

        # reset counter
        self.n_local_update = 0
        self.n_cluster_update = 0
        if not hasattr(self,'n_top_removed'):
            # n_top_removed is initialized in reduce_bank
            self.n_top_removed = 0
        self.n_increase = 0

    def eval_update(self):
        """
        evaluate update
        """
        self.eval_update_init()

        # prepare update_mvar
        self.update_indlist = list(range(len(self.mscore)))
        self.update_mvar = [ -1 for x in range(len(self.update_indlist)) ]

        # evaluate update one by one
        for i in self.update_indlist:
            self.eval_update_single(i)

        self.eval_update_final()

    def eval_update_final(self):
        if self.p['biased_potential']:
            try:
                self.print_log("biased potential: %f"%self.bp_sum,level=2)
            except:
                pass
        self.print_log( "n_local_update: %d"%self.n_local_update )
        self.print_log( "n_cluster_update: %d"%self.n_cluster_update )
        self.print_log( "n_top_removed: %d"%self.n_top_removed )
        self.print_log( "n_added: %d"%self.n_increase )

        if self.p['biased_potential'] and self.p['bp_limit'] is not None and self.bp_sum <= self.p['bp_limit']:
            self.bp_limit = True
            self.print_log("bp_limit is set. bp_sum: %12.5E bp_sigma2: %12.5E"%(self.bp_sum,self.bp_sigma2))
        else:
            self.bp_limit = False

        self.write_bank_history()

        if self.mpisize_master > 1 and self.mpirank_master == 0 and self.p['parallel_minimization'] != 0:
            self.slave_data_distributed.fill(False)

    def add_adaptive_potential_to_imscore(self):
        if self.p['metadynamics']:
            self.i_ap_distance = self.get_adaptive_potential_distance(self.imvar)
            self.i_ap_ind = self.get_ap_bin_index(self.i_ap_distance)
            self.imscore += self.get_adaptive_potential(self.i_ap_ind)

    def add_adaptive_potential_to_new_bscore(self):
        ndim = len(self.p['ap_range'])
        nbank = len(self.new_bvar)

        # prepare native_distance
        if not hasattr(self,'native_distance'):
            self.native_distance = np.empty((ndim,len(self.new_bvar)),dtype=np.double)
            for j in range(nbank):
                self.native_distance[:,j] = self.get_adaptive_potential_distance(self.new_bvar[j])
        ap_nvar = self.native_distance.shape[1]
        if nbank < ap_nvar: # nbank decreased
            self.native_distance = self.native_distance[:,:nbank]
        elif nbank > ap_nvar: # nbank increased
            nd = np.empty((ndim,nbank-ap_nvar),dtype=np.double)
            self.native_distance = hstack((self.native_distance,nd))
            for j in range(ap_nvar,nbank):
                self.native_distance[:,j] = self.get_adaptive_potential_distance(self.new_bvar[j])

        # get the potential
        for j in range(nbank):
            bin_ind = self.get_ap_bin_index(self.native_distance[:,j])
            self.new_bscore[j] += self.get_adaptive_potential(bin_ind)

    def skip_trial_conf(self,i):
        if ( self.beta == None and not self.p['metadynamics'] and self.p['check_maxscore'] ):
            max_allowed_score = self.maxscore_real
            mins = self.minscore_real
            if self.p['increase_bank']:
                max_allowed_score = max(max_allowed_score,mins+self.max_allowed_gap)

            # is the score of the trial conformation too high ?
            if ( self.imscore_real > max_allowed_score ):
                self.print_log( "m%d score is too high (score: %.5E > %.5E) "%(i+1,self.imscore_real,max_allowed_score), level=3 )
                return True
        return False

    def insert_new_var(self,i,j,name=True):
        self.update_bvar[j] = i
        self.update_mvar[i] = j
        #self.new_bvar[j] = deepcopy(self.imvar)
        self.new_bvar[j] = self.imvar
        self.new_bscore_real[j] = self.imscore_real
        self.updated = name
        # update bank_distance
        if self.p['nearest_neighbor']:
            # fill the negative distance
            for ind in range(self.mb_distance.size):
                if self.mb_distance[ind] < 0.0:
                    self.mb_distance[ind] = self.comparef(self.imvar,self.new_bvar[ind])
        self.bank_distance.set_column(j,self.mb_distance)
        # update cluster
        if self.p['use_cdcut']:
            if self.min_mb_distance > self.cluster_distance:
                self.n_cluster += 1
                self.cluster[j] = self.n_cluster
        if self.p['metadynamics']:
            self.native_distance[:,j] = self.i_ap_distance

    def eval_local_update(self,i,j):
        if ( not self.updated and self.min_mb_distance < self.dcut ):
            if (     self.p['biased_potential']
                 and self.imscore_real > self.new_bscore_real[j] ):
                new_bscore_j = self.new_bscore[j] + self.bp_max * np.exp(-self.mb_distance[j]**2/self.bp_sigma2)
            else:
                new_bscore_j = self.new_bscore[j]

            ims = self.imscore

            local_update = self.p['local_update']
            if ( self.imscore_real < self.maxscore_real or self.p['metadynamics'] or not self.p['check_maxscore'] ):
                if (    (     local_update == 'normal'
                          and (    ims < new_bscore_j
                                or ( self.beta != None and np.exp(self.beta*(new_bscore_j-ims)) > np.random.random() ) ) )
                     or (     local_update == 'normalrandom'
                          and (    ims < new_bscore_j
                                or np.random.random() < 0.5 ) )
                     or (     local_update == 'random'
                          and np.random.random() < 0.5 )
                     or (     local_update == 'diverse'
                          and np.amin(np.delete(self.mb_distance,j)) > self.bank_distance.amin_column(j) ) ):
                    self.n_local_update += 1
                    # replace old bank
                    self.print_log( "m%d is close to b%d and replaces it (S: %.5E, %.5E)"%(i+1,j+1,ims,new_bscore_j), level=3 )
                    self.insert_new_var(i,j,'local_update')
                    if self.p['metadynamics']:
                        self.add_adaptive_potential()
                elif self.p['metadynamics']:
                    self.add_adaptive_potential(j)

    def eval_cluster_update(self,i,j):
        if not self.updated and self.min_mb_distance >= self.dcut and self.min_mb_distance < self.cdcut:
            # add biased score if exists
            if self.p['biased_potential']:
                self._mod_new_bscore = csa_util.add_biased_bscore(self)
            else:
                self._mod_new_bscore = self.new_bscore
            # bank index of the same cluster of j
            ind = np.where( self.cluster == self.cluster[j] )[0]
            # find the bank index (k) having the worst score in the cluster
            k = ind[ np.argmax( self._mod_new_bscore[ind] ) ]
            k_score = self._mod_new_bscore[k]
            if self.p['cluster_update'] == 0:
                # remove the worst conf of the largest cluster
                # find the largest cluster
                cind = np.argmax(np.bincount(self.cluster))
                ind = np.where( self.cluster == cind )[0]
                l = ind[ np.argmax( self._mod_new_bscore[ind] ) ]
                remove_ind = l
            elif self.p['cluster_update'] == 1:
                # remove the worst conf in the cluster
                remove_ind = k
            else:
                raise ValueError( 'Unknown cluster_update: %s'%str(self.p['cluster_update']) )
            if ( self.imscore < k_score
                 or ( self.beta != None and np.exp(self.beta*(k_score-self.imscore)) > np.random.random() )):
                # replace the worst in the cluster
                self.n_cluster_update += 1
                self.insert_new_var(i,remove_ind,'cluster_update')
                self.print_log( "m%d is close to b%d cluster and replaces it (S: %.5E, %.5E)"%(i+1,k+1,self.imscore,k_score), level=3 )
            elif self.p['metadynamics']:
                self.add_adaptive_potential(k)

    def eval_remove_top(self,i):
        if self.p['use_cdcut']:
            cutd = self.cdcut
        else:
            cutd = self.dcut
        if (     not self.updated
             and (    (     self.min_mb_distance >= cutd
                        and ( self.imscore_real < self.maxscore_real or self.p['metadynamics'] or not self.p['check_maxscore'] ) )
                   or (     len(self.new_bvar) > self.p['nbank']
                        and self.p['increase_bank']
                        and (     (     self.p['metadynamics']
                                    and self.maxscore > self.energy_cut
                                    and self.imscore < self.energy_cut )
                               or (     not self.p['metadynamics']
                                    and self.maxscore_real > self.energy_cut
                                    and self.imscore_real < self.energy_cut ) ) ) ) ):
            # remove top
            if not hasattr(self,'_mod_new_bscore'):
                if self.p['biased_potential']:
                    self._mod_new_bscore = csa_util.add_biased_bscore(self)
                else:
                    self._mod_new_bscore = self.new_bscore
            if self.p['metadynamics'] or not self.p['check_maxscore']:
                j_list = np.argsort(self._mod_new_bscore)
                for j_ind in range(len(j_list)-1,-1,-1):
                    j = j_list[j_ind]
                    if j not in self.update_mask:
                        break
            else:
                j_list = np.argsort(self.new_bscore_real)
                for j_ind in range(len(j_list)-1,-1,-1):
                    j = j_list[j_ind]
                    if j not in self.update_mask:
                        break
            e_delta = self.imscore - self._mod_new_bscore[j]
            if (     self.p['biased_potential']
                 and self.imscore_real > self.new_bscore_real[j] ):
                e_delta -= self.bp_max * np.exp(-self.mb_distance[j]**2/self.bp_sigma2)
            if (    e_delta < 0.0
                 or ( self.beta != None and np.exp(self.beta*(-e_delta)) > np.random.random() )):
                self.n_top_removed += 1
                # replace the bank with the highest score
                self.print_log( "m%d replaces the highest-score b%d (S: %.5E)"%(i+1,j+1,self.imscore), level=3 )
                self.insert_new_var(i,j,'remove_top')
                if self.p['metadynamics']:
                    self.add_adaptive_potential()
            elif self.p['metadynamics']:
                self.add_adaptive_potential(j)

    def eval_increase_bank(self,i):
        nbank = len(self.new_bvar)
        if (     not self.updated and self.p['increase_bank']
             and nbank < self.p['max_nbank']
             and self.p['increase_bank']
             and ( self.maxscore_real >= self.imscore_real or self.p['metadynamics'] or not self.p['check_maxscore'] )
             and self.n_increase < self.p['max_increase']
             and (    ( self.p['increase_bank_dcut'] and self.min_mb_distance >= self.dcut )
                   or not self.p['increase_bank_dcut']
                 )
             and self.imscore <= self.energy_cut ):
            # increase the bank size
            j = len(self.update_bvar)
            self.print_log( "m%d is added to b%d (S: %.5E)"%(i+1,j+1,self.imscore), level=3 )
            # increase the size of containers
            self.update_bvar.append(None)
            self.new_bvar.append(None)
            self.new_bscore_real = np.append(self.new_bscore_real,0.0)
            self.bank_distance.adjust_size(nbank+1)
            self.mb_distance = np.append(self.mb_distance,0.0)
            self.insert_new_var(i,j,'increase_bank')
            self.n_increase += 1

    def get_adaptive_potential(self,ap_ind):
        if ap_ind is not None:
            return self.adaptive_potential[ap_ind]
        else:
            return self.p['ap_max']

    def add_adaptive_potential(self,j=-1):
        if self.p['metadynamics']:
            if j < 0:
                ap_ind = self.i_ap_ind
            else:
                ap_ind = self.get_ap_bin_index(self.native_distance[:,j])
            if ap_ind is not None:
                self.adaptive_potential[ap_ind] += self.p['ap_inc']

    def get_ap_bin_index(self,distance):
        ndim = self.adaptive_potential.ndim
        bin_ind = [None]*ndim
        ap_range = self.p['ap_range']
        for i in range(len(distance)):
            dmin, dmax, dn = ap_range[i]
            dist = distance[i]
            ind = int((dn/(dmax-dmin))*(dist-dmin))
            if ind >= 0 and ind < dn:
                bin_ind[i] = ind
            else:
                return None
        return tuple(bin_ind)

    def get_adaptive_potential_distance(self,var):
        distance = np.empty(len(self.p['ap_range']),dtype=np.double)
        for i in range(len(distance)):
            distance[i] = self.comparef(var,self.native_var[i])
        return distance

    def eval_update_single(self,i):
        self.start_timer('run/iterate/eval_update/single')
        # adjust the size of update_mvar
        if len(self.update_mvar) < len(self.mvar):
            self.update_mvar += [ -1 for x in range( len(self.mvar) - len(self.update_mvar) ) ]

        # get i-th mvar and mscore
        self.imscore_real = self.mscore[i]
        self.imvar = self.mvar[i]

        # prepare self.new_bscore
        if self.p['biased_potential']:
            self.new_bscore, self.bp_sigma2, self.bp_sum = csa_util.get_biased_bscore(self)
        else:
            self.new_bscore = self.new_bscore_real.copy()

        # metadynamics
        if self.p['metadynamics']:
            self.add_adaptive_potential_to_new_bscore()

        # real min/max score
        self.maxscore_real = np.amax(self.new_bscore_real)
        self.minscore_real = np.amin(self.new_bscore_real)
        self.maxscore = np.amax(self.new_bscore)
        self.minscore = np.amin(self.new_bscore)

        # check if the conformation should be skipped
        if self.skip_trial_conf(i):
            self.stop_timer('run/iterate/eval_update/single')
            return

        # energy_cut value for increasing/removing bank
        self.energy_cut = self.minscore_real + self.increase_bank_energy_cut

        # measure distances between the trial conf and the bank confs
        try:
            imvar_seed = self.perturb_info[i]['seed']
            print(self.perturb_info[i])
        except:
            imvar_seed = None
        self.mb_distance = self.comparef_list(self.imvar,ind=imvar_seed)
        minj = np.argmin(np.absolute(self.mb_distance))
        self.min_mb_distance = self.mb_distance[minj]
        j = minj

        # calculate the biased potential of i-th mvar
        if self.p['biased_potential']:
            mask = self.new_bscore_real <= self.imscore_real
            if self.p['nearest_neighbor']:
                # fill additional mb_distance near minj
                sigma = np.sqrt(self.bp_sigma2*0.5)
                inds = self.bank_distance.sort_column(j)
                for ind in inds:
                    if not mask[ind]:
                        continue
                    if self.mb_distance[ind] < 0:
                        dist = comparef(self.imvar,self.new_bvar[ind])
                        self.mb_distance[ind] = dist
                        if dist >= 4*sigma:
                            break
            self.imscore = self.imscore_real + self.bp_max * np.sum( np.exp(-self.mb_distance[mask]**2/self.bp_sigma2) )
        else:
            self.imscore = self.imscore_real

        # add adaptive potential to imscore
        self.add_adaptive_potential_to_imscore()

        # reset updated flag
        self.updated = False

        if self.p['local_update'] is not None:
            self.eval_local_update(i,j)
        # increase bank
        self.eval_increase_bank(i)
        if self.p['use_cdcut']:
            self.eval_cluster_update(i,j)
        # remove top
        self.eval_remove_top(i)

        try:
            del self._mod_new_bscore
        except:
            pass

        if self.updated:
            j = self.update_mvar[i]
            try:
                self.seed_mask.remove(j)
            except:
                pass
        else:
            if ( self.min_mb_distance < self.dcut ):
                self.print_log( "m%d is close to b%d but rejected (S: %.5E, %.5E) "%(i+1,minj+1,self.imscore,self.new_bscore[minj]), level=3 )
            else:
                self.print_log( "m%d is new but rejected (S: %.5E)"%(i+1,self.imscore), level=3 )
        self.stop_timer('run/iterate/eval_update/single')

    def adjust_increase_bank_energy_cut(self):
        if self.first_pm3:
            return

        min_dist = self.bank_distance.amin()

        if self.p['increase_bank']:
            updated = False
            if self.p['ecut_reduce'].startswith('max'):
                try:
                    ratio = float(self.p['ecut_reduce'][3:])
                except:
                    ratio = 1.0
                self.increase_bank_energy_cut = ( self.maxscore - self.minscore ) * ratio
                updated = True
            elif self.p['ecut_reduce'] == 'fix':
                self.increase_bank_energy_cut = self.p['increase_bank_energy_cut']
                updated = True
            else:
                if self.min_dcut < min_dist:
                    self.increase_bank_energy_cut *= self.p['ecut_reduce']
                    updated = True
            if updated:
                self.print_log( "increase_bank_energy_cut: %f"%self.increase_bank_energy_cut, level=1 )

    def update_bank(self):
        # adjust the size of bvar
        nbank = len(self.update_bvar)
        n_delta = nbank - len(self.bvar) # n of increased conf
        if n_delta:
            self.bvar += [ None for x in range(n_delta) ]
            self.bscore += [ None for x in range(n_delta) ]

        # update bank
        for i in range(nbank):
            ind = self.update_bvar[i]
            if ind < 0:
                continue
            self.bvar[i] = self.mvar[ind]
            if self.bscore[i] is not None:
                sdiff = self.bscore[i] - self.mscore[ind]
                if sdiff > self.p['decut'] or sdiff < -self.p['decut']:
                    self.bank_status[i] = False
            self.bscore[i] = self.mscore[ind]

        if n_delta:
            # adjust the size of other variables
            self.rvar += self.bvar[-n_delta:]
            self.rscore += self.bscore[-n_delta:]
            self.bank_status += [ False for x in range(n_delta) ]

        self.n_unused_bank = self.count_unused_bank()

        return n_delta

    def reduce_bank(self):
        self.remove_bank_list = []
        self.n_top_removed = 0
        if self.p['increase_bank']:
            if self.p['ecut_reduce'].startswith('max'):
                cut_energy = np.amax(self.new_bscore_real)
            else:
                cut_energy = np.amin(self.new_bscore) + self.increase_bank_energy_cut
            if np.amax(self.new_bscore) > cut_energy:
                bind = np.argsort(self.new_bscore)[self.p['min_nbank']:][::-1] # keep minimum size
                for i in bind:
                    if self.new_bscore[i] > cut_energy:
                        self.print_log('b%d is removed (bS: %.5E SCut: %.5E)'%(i+1,self.new_bscore[i],cut_energy),level=2)
                        self.n_top_removed += 1
                        self.remove_bank_list.append(i)

    def count_unused_bank(self):
        count = 0
        nbank = len(self.bvar)

        # bank_status
        if not hasattr(self,'bank_status'):
            self.bank_status = [ False for x in range(nbank) ]

        # readjust the size of bank_status
        if nbank > len(self.bank_status):
            self.bank_status += [ False for x in range(nbank-len(self.bank_status)) ]

        seed_mask = set(self.seed_mask)
        for i in range(len(self.bank_status)):
            if i in seed_mask:
                continue
            if self.bank_status[i] == False:
                count += 1

        return count

    def find_min(self):
        # minscore = minimum score among bscore
        # minscore_ind = the index of the minimum score
        try:
            ind = np.argmin(self.bscore)
        except:
            self.minscore_ind = None
            self.minscore = None
            self.print_log("no score is found",level=2)
            return
        self.minscore_ind = ind
        self.minscore = self.bscore[ind]
        self.print_log("b%d score is the lowest (score: %f)"%(ind+1,self.minscore),level=2)

    def find_max(self):
        # maxscore = maximum score among bscore
        # maxscore_ind = the index of the maximum score
        try:
            ind = np.argmax(self.bscore)
        except:
            self.maxscore = None
            self.maxscore_ind = None
            self.print_log("no score is found",level=2)
            return
        self.maxscore_ind = ind
        self.maxscore = self.bscore[ind]
        self.print_log("b%d score is the highest (score: %f)"%(ind+1,self.maxscore),level=2)

    def perturbvar(self):
        self.print_log( "creating new generations" )

        if self.mpisize_master > 1 and self.p['parallel_minimization'] != 0:
            n_minimum = max(1, ( np.count_nonzero(self.job_assigned == -1) - 1 )) # omit master
        else:
            n_minimum = 1

        # empty mvar
        self.mvar = []
        # perturb_info
        self.perturb_info = []
        n_perturb = 0

        while n_perturb < n_minimum:
            for ind1 in self.seed:
                self.print_log("creating new generations using b%d"%(ind1+1),level=3)
                ( newvar, perturb_info ) = self.perturbvarf(ind1) # result is list of var
                self.mvar += newvar
                self.perturb_info += perturb_info
            n_perturb = len(self.mvar)

        # shuffle
        indlist = np.arange(len(self.mvar))
        np.random.shuffle(indlist)
        self.mvar = [ self.mvar[i] for i in indlist ]
        self.perturb_info = [ self.perturb_info[i] for i in indlist ]
        self.ntrial += len(self.mvar)

    def prepare_seed(self):
        nbank = len(self.bvar)

        # bank_status
        if not hasattr(self,'bank_status'):
            self.bank_status = [ False for x in range(nbank) ]

        # readjust the size of bank_status
        if nbank > len(self.bank_status):
            self.bank_status += [ False for x in range(nbank-len(self.bank_status)) ]

        self.seed = []

        nseed = self.p['nseed']
        seed_mask = self.seed_mask
        if ( isinstance(nseed,str) and nseed[-1] == '%' ):
            nseed = int( ( nbank - len(seed_mask) ) * float(nseed[:-1]) / 100.0 )
        else: # integer
            nseed = self.p['nseed']

        # bank status (iuse)
        # False: new or previously updated
        # True: previously used for a seed but was not updated

        if self.p['random_seed'] == 2:
            bank_avail = np.ones(nbank,dtype=bool)
            bank_avail[seed_mask] = False
            bank_avail = np.where(bank_avail)[0].tolist()

            for i in range(nseed):
                ind = np.random.randint( len(bank_avail) )
                s = bank_avail.pop(ind)
                self.add_seed(s)

        else:
            bank_avail = np.logical_not(self.bank_status)
            bank_avail[seed_mask] = False
            bank_avail = np.where(bank_avail)[0].tolist()

            if ( len(bank_avail) == 0 ):
                # reset bank_status
                self.reset_bank_status()
                bank_avail = np.ones(nbank,dtype=bool)
                bank_avail[seed_mask] = False
                bank_avail = np.where(bank_avail)[0].tolist()

            if ( len(bank_avail) <= nseed ):
                for ind1 in bank_avail:
                    self.add_seed(ind1)
                bank_avail = np.ones(nbank,dtype=bool)
                bank_avail[seed_mask] = False
                bank_avail[self.seed] = False
                bank_avail = np.where(bank_avail)[0].tolist()
            else:
                if self.p['random_seed'] == 0 or self.p['random_seed'] == 1:
                    # take one index randomly from bank_avail
                    i = np.random.randint(len(bank_avail))
                    ind1 = bank_avail.pop(i)
                elif self.p['random_seed'] == 3:
                    # take the lowest score
                    min_score_ind = 0
                    min_score = self.bscore[bank_avail[0]]
                    for i in range(1,len(bank_avail)):
                        inda = bank_avail[i]
                        if min_score > self.bscore[inda]:
                            min_score_ind = i
                            min_score = self.bscore[inda]
                    ind1 = bank_avail.pop(min_score_ind)
                else:
                    # something is wrong
                    raise
                self.add_seed(ind1)

            while ( len(self.seed) < nseed ):
                if ( len(bank_avail) == 0 ):
                    self.print_log( "Perhaps 'nseed' is too big" )
                    break
                if ( self.p['random_seed'] == 1 ):
                    i = np.random.randint(len(bank_avail))
                    ind1 = bank_avail.pop(i)
                    self.add_seed(ind1)
                elif self.p['random_seed'] == 3:
                    min_score_ind = 0
                    min_score = self.bscore[bank_avail[0]]
                    for i in range(1,len(bank_avail)):
                        inda = bank_avail[i]
                        if min_score > self.bscore[inda]:
                            min_score_ind = i
                            min_score = self.bscore[inda]
                    ind1 = bank_avail.pop(min_score_ind)
                    self.add_seed(ind1)
                else:
                    dist_sum = {}
                    davg = 0.0
                    for ind1 in self.seed:
                        for inda in bank_avail:
                            dist = self.bank_distance.get_elem(ind1,inda)
                            try:
                                dist_sum[inda] += dist
                            except:
                                dist_sum[inda] = dist
                            davg += dist
                    davg /= len(bank_avail) # it is the average distance sum
                    min_score_ind = 0
                    min_score = self.bscore[bank_avail[0]]
                    for i in range(1,len(bank_avail)):
                        # skip index which is less than the avg dist sum
                        inda = bank_avail[i]
                        if dist_sum[inda] < davg: continue
                        if min_score > self.bscore[inda]:
                            min_score_ind = i
                            min_score = self.bscore[inda]
                    ind1 = bank_avail.pop(min_score_ind)
                    self.add_seed(ind1)

        self.print_log('Seed numbers: '+' '.join([ str(x+1) for x in sorted(self.seed) ]),level=2)
    def add_seed(self,ind1):
        self.seed.append(ind1)
        self.bank_status[ind1] = True

    def compare_rr(self):
        # measure distances between bvar
        nbank = len(self.rvar)
        dist_sum = 0.0
        self.rmin_dist = sys.float_info.max
        self.rmax_dist = -sys.float_info.max
        for i in range(nbank-1):
            irvar = self.rvar[i]
            for j in range(i+1,nbank):
                dist = self.comparef(irvar,self.rvar[j])
                dist_sum += dist
                if dist < self.rmin_dist:
                    self.rmin_dist = dist
                if dist > self.rmax_dist:
                    self.rmax_dist = dist

        # calc average/min/max distance
        self.ravg_dist = dist_sum / float(nbank*(nbank-1)/2)
        self.print_log("Average rbank distance: %f"%(self.ravg_dist))
        self.print_log("Maximum rbank distance: %f"%(self.rmax_dist))
        self.print_log("Minimum rbank distance: %f"%(self.rmin_dist))

    def compare_bb(self,parallel=False):
        self.print_log('measuring bank distances')
        if parallel:
            # EXPERIMENTAL !!
            # initiate bank_distance
            nbvar = len(self.bvar)
            if not hasattr(self,'bank_distance'):
                self.bank_distance = Symmetric2d(nbvar)

            # readjust the size of bank_distance
            if self.bank_distance.get_matsize != nbvar:
                self.bank_distance.adjust_size(nbvar)

            # measure distances between bvar
            nbank = len(self.bvar)

            missing_pair = []
            for i in range(nbank-1):
                for j in range(i+1,nbank):
                    dist = self.bank_distance.get_elem(i,j)
                    if dist < 0.0:
                        missing_pair.append((i,j))

            # check if any idle process exists
            idle = []
            for i in range(1,self.mpisize_master):
                if self.job_assigned[i] is None:
                    continue
                idle.append(i)

            # parallel calculation of the pair distances
            # reserve the last split pairs for the master job
            split_missing_pair = np.array_split(missing_pair,len(idle)+1)

            pair_list_ind = 0
            pair_list = {}
            if len(idle):
                # send bvar to the slaves
                #deco_cmd_id = self.command_id['decorate_data']
                measure_cmd_id = self.command_id['measure_distance']
                for inode in idle:
                    pairs = split_missing_pair[pair_list_ind]
                    if not pairs:
                        break
                    pair_list[inode] = pair_list_ind
                    pair_list_ind += 1
                    self.mpicomm_master.send((measure_cmd_id,pairs),dest=inode)
                    self.job_assigned[inode] = measure_cmd_id

            # master also try to measure the distance because it is urgent
            pairs = split_missing_pair[pair_list_ind]
            for ind in range(len(pairs)):
                i, j = pairs[ind]
                dist = self.comparef(self.bvar[i],self.bvar[j])
                self.bank_distance.set_elem(i,j,dist)

            if len(idle):
                # recieve jobs
                while True:
                    all_received = True
                    for inode in idle:
                        if inode not in pair_list:
                            continue
                        if self.job_assigned[inode] is not None:
                            all_received = False
                            break
                    if all_received:
                        break
                    self.receive_slave_data()

                # merge distance
                recv_data = self.get_received_data('measure_distance')
                for cmd_id, rank, bb_dist in recv_data:
                    pairs = split_missing_pair[pair_list[rank]]
                    for ind in range(len(pairs)):
                        i, j = pairs[ind]
                        d = bb_dist[ind]
                        self.bank_distance.set_elem(i,j,dist)
        else:
            # only master calculate bank_distance
            # initiate bank_distance
            nbvar = len(self.bvar)
            if not hasattr(self,'bank_distance'):
                self.bank_distance = Symmetric2d(nbvar)

            # readjust the size of bank_distance
            if self.bank_distance.get_matsize != nbvar:
                self.bank_distance.adjust_size(nbvar)

            # measure distances between bvar
            nbank = len(self.bvar)
            for i in range(nbank-1):
                for j in range(i+1,nbank):
                    dist = self.bank_distance.get_elem(i,j)
                    if dist < 0.0:
                        dist = self.comparef(self.bvar[i],self.bvar[j])
                        self.bank_distance.set_elem(i,j,dist)

        # calc average/min/max distance
        self.avg_dist = self.bank_distance.average()
        self.min_dist, self.max_dist = self.bank_distance.aminmax()
        self.print_log("Average bank distance: %f"%(self.avg_dist))
        self.print_log("Maximum bank distance: %f"%(self.max_dist))
        self.print_log("Minimum bank distance: %f"%(self.min_dist))

    def sort_mvar(self):
        self.print_log("sorting mvar")
        ind = np.argsort(self.mscore)
        sorted_mvar = [None] * len(ind)
        sorted_mscore = [None] * len(ind)
        for j in range(len(ind)):
            i = ind[j]
            sorted_mvar[j] = self.mvar[i]
            sorted_mscore[j] = self.mscore[i]
            # destroy mvar[i] to save memory
            self.mvar[i] = None
        self.mvar = sorted_mvar
        self.mscore = sorted_mscore

        return ind

    def randvar(self,nconf=None,varlist=None):
        if nconf is None:
            nconf = self.p['nbank'] + self.p['nbank_add']

        if varlist is not None:
            # TODO: need to exam
            #nconf -= len(varlist)
            #nconf = max(0,nconf)
            self.mvar = deepcopy(varlist)
        else:
            self.mvar = []

        for i in range(nconf):
            var = self.randvarf()
            self.mvar.append(var)

        self.ntrial += len(self.mvar)
        self.print_log("%d random conformations have been generated"%(nconf))

    def check_param(self):
        self.print_log("checking input parameters")

        # print input parameters
        self.print_log("Input parameters")
        for key in sorted(self.p.keys()):
            self.print_log("%s = %s"%(key,str(self.p[key])))

        # parameter check
        nbank = self.p['nbank']
        nseed = self.p['nseed']
        if ( nbank < 2 ):
            self.print_error( "nbank is less than 2" )
        if ( self.p['nbank_add'] < 0 ):
            self.print_error( "nbank_add is less than 0" )
        if ( isinstance(nseed,str) and nseed[-1] == '%' ):
            nseed_ratio = float(nseed[:-1])
            if ( nseed_ratio > 100.0 or nseed_ratio < 0.0 ):
                self.print_error( "nseed ratio is out of range" )
        else:
            if ( nseed <= 0 ):
                self.print_error( "nseed is less than 1" )
            if ( nseed > nbank ):
                self.print_log( "WARN: nseed is greater than nbank" )

        # neb and metadynamics
        if self.p['neb'] and self.p['metadynamics']:
            self.print_error("cannot turn on neb and metadynamics simultaneously")

    def start_timer(self,key):
        try:
            self.timer
        except:
            self.timer = {}
        self.print_log("%s starts at %s"%(key,time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())),level=4)
        try:
            self.timer[key][0] = time.time()
        except:
            # current_time, accumulated_time, turned_on(bool)
            self.timer[key] = [ time.time(), 0.0, True ]

    def stop_timer(self,key):
        try:
            self.timer[key]
        except:
            self.print_error("cannot find timer '%s'"%key)
            return
        self.print_log("%s stops at %s"%(key,time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())),level=4)
        self.timer[key][1] += time.time() - self.timer[key][0]
        self.timer[key][2] = False # turn off the timer

    def get_accumulated_time(self):
        try:
            self.timer
        except:
            self.timer = {}

        # calc accumulated time
        accu_wall = {}
        for key in self.timer:
            # If it is already turned on, add time
            accu_wall[key] = self.timer[key][1]
            if self.timer[key][2]:
                accu_wall[key] += time.time() - self.timer[key][0]

        return accu_wall

    def print_timer(self,accu_wall):
        keylen = 10
        wtlen = 0
        output = []
        for key in sorted(accu_wall):
            # key
            depth = key.count('/')
            shortkey = ''
            if ( depth == 0 ):
                shortkey = key
                length = len(key)
            else:
                shortkey = key.split('/')[-1]
                length = len( shortkey ) + depth*4
            if ( keylen < length ):
                keylen = length
            strkey = ''
            for i in range(depth):
                strkey += '    '
            strkey += shortkey
            # walltime
            walltime = str(datetime.timedelta(seconds=accu_wall[key]))
            length = len(walltime)
            if ( wtlen < length ):
                wtlen = length
            output.append( ( strkey, walltime ) )
        fmt = '%-'+str(keylen)+'s  %'+str(wtlen)+'s'
        self.print_log(fmt%('Subroutine','Walltime'))
        for c in output:
            self.print_log(fmt%c)

    def show_timer(self):
        if mpisize == 1:
            self.print_log("CSA Timer")

            accu_wall = self.get_accumulated_time()
            self.print_timer(accu_wall)
        else:
            self.print_log("CSA Timer")
            accu_wall = self.get_accumulated_time()

            # collect slave timer and add them
            #print mpirank, 'gather accu_wall, root=0'
            slave_time = mpicomm.gather(accu_wall,root=0)
            if mpirank == 0:
                accu_wall_slave = {}
                for i in range(1,len(slave_time)): # skip master
                    aw = slave_time[i]
                    for key in aw:
                        if key in accu_wall_slave:
                            accu_wall_slave[key] += aw[key]
                        else:
                            accu_wall_slave[key] = aw[key]

                self.print_log("==== Master processor ====")
                self.print_timer(accu_wall)
                self.print_log("==== Slave processor(s) ====")
                self.print_timer(accu_wall_slave)
                self.print_log("Number of processors used: %d"%mpisize)

    def reset_timer(self):
        self.timer = {}

    def add_rbank(self,nbank=None,nbank_add=None,varlist=None):
        # bvar = [] # bank variable array
        # rvar = [] # random bank variable array
        # mvar = [] # current variable array

        # bscore = [] # bank score
        # rscore = [] # random bank score
        # mscore = [] # current conformation score

        try:
            nbvar1 = len(self.bvar)
        except:
            nbvar1 = 0

        if not hasattr(self,'rvar'):
            self.rvar = []
        if not hasattr(self,'bvar'):
            self.bvar = []
        if not hasattr(self,'rscore'):
            self.rscore = []
        if not hasattr(self,'bscore'):
            self.bscore = []

        if nbank is None:
            nbank = self.p['nbank'] - len(self.rvar)
            if nbank < 0:
                nbank = 0

        if nbank_add is None:
            nbank_add = self.p['nbank_add']

        if varlist:
            nconf = max( nbank+nbank_add-len(varlist), 0 )
        else:
            nconf = max( nbank+nbank_add, 0 )

        # build random conformations
        self.randvar(nconf,varlist=varlist)
        # minimize
        self.minimize()
        # pick rbank
        self.pick_rbank(nbank)

        # write bank_history
        self.write_rbank_history(nbvar1)

    def pick_rbank(self,npick):
        # compare conformations
        if self.p['pick_rbank'] != 'order':
            self.sort_mvar()

        sel_idx_list = []
        if ( len(self.mvar) == 0 or npick < 1 ):
            return sel_idx_list

        if self.p['pick_rbank']=='cluster':
            self.mvar = self.rvar + self.mvar
            self.mscore = self.rscore + self.mscore

            mvar_cnt = len(self.mvar)
            rvar_cnt = len(self.rvar)

            dist_arr = np.zeros( (mvar_cnt,mvar_cnt), dtype=np.double )
            for i in range(mvar_cnt-1):
                imvar = self.mvar[i]
                for j in range(i+1,mvar_cnt):
                    dist_arr[i,j] = dist_arr[j,i] = self.comparef(imvar,self.mvar[j])

            sel_idx_list = list(range(rvar_cnt+1))
            avail_idx_list = list(range(rvar_cnt+1,mvar_cnt))
            while ( len(sel_idx_list) < npick + rvar_cnt ):
                if ( len(avail_idx_list) == 1 ):
                    min_score_ind = avail_idx_list[0]
                else:
                    da = dist_arr[sel_idx_list,:][:,avail_idx_list]
                    avg_d = np.average( da, axis=0 )
                    mask = avg_d >= np.average(avg_d)
                    min_score_ind = np.array(avail_idx_list)[mask][0]
                avail_idx_list.remove(min_score_ind)
                sel_idx_list.append(min_score_ind)

            # remove previously added indexes
            sel_idx_list = [ x-rvar_cnt for x in sel_idx_list[rvar_cnt:] ]
            self.mvar = self.mvar[rvar_cnt:]
            self.mscore = self.mscore[rvar_cnt:]
        else:
            sel_idx_list = list(range(npick))

        for x in sel_idx_list:
            try:
                self.rvar.append(self.mvar[x])
            except:
                pass
            try:
                self.rscore.append(self.mscore[x])
            except:
                pass
            try:
                self.bvar.append(self.mvar[x])
            except:
                pass
            try:
                self.bscore.append(self.mscore[x])
            except:
                pass

        return sel_idx_list

    def remove_bank(self,r_ind):
        b_ind = np.ones(len(self.bvar),dtype=bool)
        b_ind[r_ind] = False
        b_ind = np.where(b_ind)[0].tolist()

        nbank = len(self.bvar)
        self.bvar = [ self.bvar[bi] for bi in b_ind ]
        self.bscore = [ self.bscore[bi] for bi in b_ind ]
        self.rvar = [ self.rvar[bi] for bi in b_ind ]
        self.rscore = [ self.rscore[bi] for bi in b_ind ]

        upmask = np.zeros(nbank,dtype=bool)
        upmask[self.update_mask] = True
        upmask = upmask[b_ind]
        self.update_mask = np.where(upmask)[0].tolist()

        # slave processors may not have the following variables
        try:
            self.bank_status = [ self.bank_status[bi] for bi in b_ind ]
        except:
            pass
        try:
            self.bank_distance.remove_column(b_ind)
        except:
            pass
        try:
            self.update_bvar = [ self.update_bvar[bi] for bi in b_ind ]
        except:
            pass

        # update mvar for write_bank_history
        self.mvar = self.bvar
        self.update_mvar = list(range(len(self.mvar)))
        try:
            self.write_bank_history()
        except:
            pass

        return b_ind

    def add_update_mask(self,j):
        if j not in self.update_mask:
            self.update_mask.append(j)

    def calc_nperturb(self,nperturb):
        if isinstance(nperturb,int):
            return nperturb
        elif isinstance(nperturb,str) and nperturb[-1] == '%':
            nperturb = ( self.count_bank() - len(self.seed_mask) ) * float(nperturb[:-1]) / 100.0
        int_nperturb = int(nperturb)
        if int_nperturb != nperturb:
            if np.random.rand() < nperturb - int_nperturb:
                nperturb = int_nperturb + 1
            else:
                nperturb = int_nperturb
        else:
            nperturb = int_nperturb
        return nperturb

    def cluster_bank(self):
        if not self.p['use_cdcut']:
            return
        bankind = np.arange(len(self.bvar)).reshape(-1,1)
        Z = hierarchy.linkage(self.bank_distance.get_1dmat())
        self.cluster = hierarchy.fcluster(Z,self.cluster_distance,criterion='distance')
        self.n_cluster = np.amax(self.cluster)
        self.print_log('number of clusters: %d'%self.n_cluster)
