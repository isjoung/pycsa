"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import absolute_import

from builtins import range
from builtins import object
import numpy as np
from . import mol_util
import sys
from perturb_util import correct_torsion, correct_angle, get_atom_index, add_torsion

class mol_rand(object):
    def __init__(self,param,amap):
        p = {
            'res_index': None,
        }
        p.update(param)
        self.xyzmap = amap
        self.p = p

        if self.p['res_index'] is None:
            self.residue_index = np.arange(self.xyzmap['nres'])    
        else:
            self.residue_index = sort(array(self.p['res_index']))

    def randomize(self,initvar,multiconf=None):
        if multiconf is not None:
            newvar = [None] * multiconf
            for i in range(multiconf):
                newvar[i] = self.randomize_single(initvar)
        else:
            newvar = self.randomize_single(initvar)
        return newvar

class mol_rand_type0(mol_rand):
    def __init__(self,param,amap):
        p = {
            'sigma_bond': 0.1,
            'sigma_angle': 10.0,
            'sigma_torsion': 360.0,
        }
        p.update(param)
        super(mol_rand_type0,self).__init__(p,amap)

        self.sigma_bond = self.p['sigma_bond']
        self.sigma_angle = self.p['sigma_angle']
        self.sigma_torsion = self.p['sigma_torsion']
        if self.sigma_angle is None:
            self.sigma_angle = 180.0
        if self.sigma_torsion is None:
            self.sigma_torsion = 360.0

    def randomize_single(self,initvar):
        index = get_atom_index(self.residue_index,self.xyzmap)

        ( iz, zbond, zang, ztors ) = mol_util.getint(initvar)

        # torsion
        del_torsion = np.random.normal( 0.0, self.sigma_torsion, len(index) )
        perturbed = np.zeros( self.xyzmap['natom'], dtype=np.uint8 )
        for i in range(len(index)):
            ind = index[i]
            if perturbed[ind]:
                continue
            del_tor = del_torsion[i]
            add_torsion(index,ind,del_tor,iz,ztors,update=perturbed)
        # bond
        zbond[index] = np.random.normal( zbond[index], self.sigma_bond )
        # angle
        zang[index] = np.random.normal( zang[index], self.sigma_angle )

        mol_util.setint(iz,zbond,zang,ztors)
        newvar = mol_util.getxyz()

        return newvar

class mol_rand_type1(mol_rand):
    def __init__(self,param,amap):
        p = {
            'max_coord': None,
            'max_x': 3.0,
            'max_y': 3.0,
            'max_z': 3.0,
        }
        p.update(param)
        super(mol_rand_type1,self).__init__(p,amap)

    def randomize_single(self,initvar):
        max_coord = [ self.p['max_x'],
                      self.p['max_y'],
                      self.p['max_z'],
                    ]
        res_index = self.p['res_index']
        if ( res_index == None ):
            res_index = list(range( self.xyzmap['nres']))
        newvar = initvar.copy()
        index = []
        for rind in res_index:
            res_num = rind + 1
            resatoms = self.xyzmap['data'][res_num]
            for atomname in resatoms:
                index.append( resatoms[atomname][0] - 1 )
        if self.p['max_coord'] is not None:
            for i in index:
                ranvec = 2.0*np.random.rand(3) - 1.0
                while np.sum(ranvec*ranvec) >= 1.0:
                    ranvec = 2.0*np.random.rand(3) - 1.0
                newvar[i,:] += ranvec * self.p['max_coord']
        else:
            newvar[index,:] += ( np.random.rand(len(index),3) * 2.0 - 1.0 ) * max_coord
        return newvar

class mol_rand_type2(mol_rand):
    def __init__(self,param,amap):
        p = {
            'sigma_x': 1.0,
            'sigma_y': 1.0,
            'sigma_z': 1.0,
        }
        p.update(param)
        super(mol_rand_type2,self).__init__(p,amap)

    def randomize_single(self,initvar):
        igma = array( [ self.p['sigma_x'],
                         self.p['sigma_y'],
                         self.p['sigma_z'],
                       ]
                     )
        res_index = self.p['res_index']
        if ( res_index == None ):
            res_index = list(range( self.xyzmap['nres']))
        index = []
        for rind in res_index:
            res_num = rind + 1
            resatoms = self.xyzmap['data'][res_num]
            for atomname in resatoms:
                index.append( resatoms[atomname][0] - 1 )
        newvar = initvar.copy()
        sigmavar = ones((len(index),3)) * sigma
        newvar[index,:] = np.random.normal( newvar[index,:], sigmavar )
        return newvar



class mol_rand_receptor_ligand(mol_rand):

    def __init__(self,param,amap):
        p = {
            'ligand_mol': 2,
            'receptor_max_coord': 0.5,
            'ligand_max_coord': 5.0,
            'rand_radius': 10.0,
            'ligand_perturb_type': 'int',
        }
        p.update(param)
        super(mol_rand_receptor_ligand,self).__init__(p,amap)


    def randomize_single(self,initvar):
        ligand_mol = self.p['ligand_mol']
        receptor_max_coord = self.p['receptor_max_coord']
        res_index = self.p['res_index']
        if res_index == None:
            res_index = range( self.xyzmap['nres'])
        receptor_index = []
        ligand_index = []
        for rind in res_index:
            res_num = rind + 1
            resatoms = self.xyzmap['data'][res_num]
            for atomname in resatoms:
                imol = resatoms[atomname][7]
                if imol == ligand_mol: 
                    ligand_index.append(resatoms[atomname][0] - 1)
                else:
                    receptor_index.append(resatoms[atomname][0] - 1)
        receptor_index = np.array(receptor_index)
        ligand_index = np.array(ligand_index)

        # new var to be returned
        newvar = initvar.copy()

        # remove far residues from receptor_index
        ligand_center = np.mean(newvar[ligand_index,:],axis=0)
        final_receptor_index = []
        mask = np.sum((ligand_center - newvar[receptor_index,:])**2,axis=1) <= self.p['rand_radius']**2
        receptor_index = receptor_index[mask]

        # xyz randomize for receptor
        max_coord = self.p['receptor_max_coord']
        for i in receptor_index:
            ranvec = 2.0*np.random.rand(3) - 1.0
            while np.sum(ranvec*ranvec) >= 1.0:
                ranvec = 2.0*np.random.rand(3)-1.0
            newvar[i,:] += ranvec * max_coord


        n_ligand_index = ligand_index.size
        if self.p['ligand_perturb_type'] == 'int':
            # perturb internal coordinations of ligand
            ( iz, zbond, zang, ztors ) = mol_util.getint(newvar)
            ztors[ligand_index] = np.random.random(n_ligand_index) * 360.0
            mol_util.setint(iz,zbond,zang,ztors)
            ligand_var = mol_util.getxyz()[ligand_index,:]
            ligand_var -= np.mean(ligand_var,axis=0)

            # random vector
            theta = np.random.uniform(low=0.0,high=2.0*np.pi)
            phi = np.random.uniform(low=0.0,high=2.0*np.pi)
            scale = np.random.random() * self.p['ligand_max_coord']
            sin_theta = np.sin(theta)
            ranvec = np.array(( 
                sin_theta * np.cos(phi),
                sin_theta * np.sin(phi),
                np.cos(theta),
            )) * scale

            ligand_var += ligand_center + ranvec
            newvar[ligand_index,:] = ligand_var
        else:
            max_coord = self.p['ligand_max_coord']

            theta = np.random.uniform(low=0.0,high=2.0*np.pi,size=n_ligand_index)
            phi = np.random.uniform(low=0.0,high=2.0*np.pi,size=n_ligand_index)
            scale = np.random.random() * self.p['ligand_max_coord']
            sin_theta = np.sin(theta)

            newvar[ligand_index,0] = ligand_center[0] + sin_theta * np.cos(phi) * scale
            newvar[ligand_index,1] = ligand_center[1] + sin_theta * np.sin(phi) * scale
            newvar[ligand_index,2] = ligand_center[2] + np.cos(theta) * scale

        return newvar
