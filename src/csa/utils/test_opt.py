from copy import deepcopy

import numpy as np
from csa_opt import (CSACrossoverGenerator, CSAMutationGenerator, CSAOptimizer,
                     CSAVariable, LocalMinimizer, LocalMinimizerResult)
from scipy import optimize

######################################################################
# problem dependent classes
######################################################################

VARDIM = 8


class myCrossoverGenerator(CSACrossoverGenerator):
    def __init__(self, name, n_perturb, perturb_type="regular", max_ratio=0.5):
        super().__init__(name, n_perturb, perturb_type=perturb_type)
        self.max_ratio = max_ratio

    def __call__(self, var1, var2):
        # variable size
        n_opt = len(var1.var)

        # A randomly selected exchange-size
        n_exchange = np.random.randint(1, max(2, n_opt * self.max_ratio))
        exind = np.random.choice(n_opt, n_exchange, replace=False)

        newvar = deepcopy(var1.var)

        # Some of the parameters are copied from the partner solution
        newvar[exind] = var2.var[exind]

        # Let's put the name of the perturbation to the perturbation information.
        # If more information is necessary for the post-analysis, you can modify this.
        pinfo = self.name

        return myCSAVariable(newvar), pinfo


class myMutationGenerator(CSAMutationGenerator):
    def __init__(self, name, n_perturb, max_ratio=0.2):
        super().__init__(name, n_perturb)
        self.max_ratio = max_ratio

    def __call__(self, var):
        # variable size
        n_opt = len(var.var)

        # A randomly selected exchange-size
        n_exchange = np.random.randint(1, max(2, n_opt * self.max_ratio))
        exind = np.random.choice(n_opt, n_exchange, replace=False)

        # numpy array
        newvar = deepcopy(var.var)

        # Some of the parameters are copied from the partner solution
        newvar[exind] = np.random.uniform(-1, 1, n_exchange)

        # check the boundary
        for i in exind:
            if newvar[i] < -512.0:
                newvar[i] = -512.0
            elif newvar[i] > 512.0:
                newvar[i] = 512.0

        # Let's put the name of the perturbation to the perturbation information.
        # If more information is necessary for the post-analysis, you can modify this.
        pinfo = self.name

        return myCSAVariable(newvar), pinfo


class myCSAVariable(CSAVariable):
    def __init__(self, var=None):
        if var is None:
            var = self.randomize()
        self.var = var

    def randomize(self):
        var = np.random.random(VARDIM) * 1024.0 - 512.0  # [-512, 512]
        return myCSAVariable(var)

    def distance(var1, var2):
        return np.linalg.norm(var1.var - var2.var)

    def __iter__(self):
        return iter(self.var)

    crossover_generators = [
        myCrossoverGenerator("crossover1", 10, perturb_type="regular", max_ratio=0.5),
        myCrossoverGenerator("crossover2", 10, perturb_type="initial", max_ratio=0.5),
    ]
    mutation_generators = [
        myMutationGenerator("mutation", 10, max_ratio=0.2),
    ]


def energy_function(x):
    # Evaluate the Eggholder function value
    xs = x[1:]
    x0 = x[:-1]
    val = np.sum(
        -(xs + 47) * np.sin(np.sqrt(np.absolute(xs + 0.5 * x0 + 47)))
        - x0 * np.sin(np.sqrt(np.absolute(x0 - xs - 47)))
    )
    return val


class myLocalMinimizer(LocalMinimizer):
    def __init__(self, param={}):
        self.p = {
            "maxiter": 1000,
            "param_range": [(-512.0, 512.0) for i in range(VARDIM)],
        }
        self.p.update(param)

    def minimize(self, var):
        """minimize.

        Parameters
        ----------
        var : myCSAVariable
            var
        """
        # Local minimization is performed using L-BFGS-B by scipy module.
        res = optimize.minimize(
            energy_function,
            var.var,
            method="L-BFGS-B",
            bounds=self.p["param_range"],
            options={"maxiter": self.p["maxiter"]},
        )
        # Minimized score
        score = res.fun
        if np.isnan(score):
            # When the local minimization is failed, a big number is assigned instead of NAN.
            score = np.finfo("d").max / 1.0e6
        # Minimized conformation
        minvar = myCSAVariable(res.x)

        myres = LocalMinimizerResult(score, minvar)

        return myres


######################################################################


local_minimizer = myLocalMinimizer()
dummy_var = myCSAVariable()

csa_opt = CSAOptimizer(
    dummy_var,
    local_minimizer,
    param={
        "nbank": 50,  # N_B, The number of new solutions added
        "max_bank": 100,
        "nseed": 5,  # N_S, The number of seed solutions per CSA iteration
        "icmax": 3,  # The maximum number of rounds
        # If the number of new solutions is over 'iucut', CSA round stays.
        # Otherwise, the CSA round increases.
        "iucut": 10,
        # If the score improvement by a newly updated solution is less than 'decut',
        # the new solution is updated in the bank but the solution is not marked
        # as a new solution
        "decut": 0.001,
        "dcut1": 2.0,  # Initial Dcut is Davg/'dcut1'
        "dcut2": 5.0,  # Lowest Dcut is Davg/'dcut2'
        "dcut_reduce": 0.983912,  # R_D, Dcut decrement ratio
        "save_restart": 1,  # The frequency of restart file saving
        "history_file": "history",  # Optimization progress is reported on the file
        "restart_file": "csa_restart.dat",  # Restart file name
        # If the number of old solutions is over 'n_new_bank_cut',
        # a crossover is performed between first-bank(seed) and
        # first-bank, otherwise it is performed between bank(seed)
        # and first-bank
        "n_new_bank_cut": 1,
    },
)
csa_opt.run_opt()
