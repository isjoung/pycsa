import numpy as np
from csa import csa, dict_merge, read_restart_param, mpicomm, mpisize, mpirank
from perturb_util import get_available_partner


class CSAOptimizer(csa):
    """CSA wrapper class"""

    def __init__(
        self,
        csavar,
        local_minimizer,
        param={},
    ):
        """__init__.

        Parameters
        ----------
        csavar : CSAVariable
        local_minimizer : LocalMinimizer
        param : dict
        """

        self.p = param

        self.csavar = csavar
        self.local_minimizer = local_minimizer

        # Initialize other CSA parameters
        super().__init__(self.p)

    def comparef(self, var1, var2):
        # distance method is defined in the variable class
        return var1.distance(var2)

    def randvarf(self):
        # randomize method is defined in the variable class
        return self.csavar.randomize()

    def minimizef(self, var):
        # returns minimization result
        res = self.local_minimizer.minimize(var)

        # returns score and minimized variable
        return res.score, res.var

    def perturbvarf(self, ind1):
        # Generate child solutions using a seed solution.
        # 'ind1' is the index of the seed solution in the bank.

        # Prepare an empty list of new variables
        newvars = []
        # Prepare an empty list of perturbation information
        # Perturbation information is for post-analysis of the
        # CSA run. It is accessible from self.perturb_info
        perturb_info = []

        try:
            partner_mask = self.p["partner_mask"]
        except KeyError:
            partner_mask = []
        nbank = len(self.bvar)

        # crossover perturbation
        dummy_var = self.csavar
        for gen in dummy_var.crossover_generators:
            perturb_name = gen.name
            n_perturb = gen.n_perturb
            perturb_type = gen.perturb_type  # 'regular' or 'initial'

            if perturb_type == "regular":
                # bank-bank crossover
                for _ in range(n_perturb):
                    var1 = self.bvar[ind1]
                    ap = get_available_partner(nbank, ind1, partner_mask)
                    if self.p['weighted_partner']:
                        d = [ self.bank_distance.get_elem(ind1,i) for i in ap ]
                        weight = 1.0 / np.array(d)
                        weight /= weight.sum()
                        ind2 = np.random.choice(ap, p=weight)
                        var2 = self.bvar[ind2]
                    else:
                        var2 = self.bvar[np.random.choice(ap)]
                    newvar, pinfo = gen(var1, var2)
                    newvars.append(newvar)
                    perturb_info.append(pinfo)
            elif perturb_type == "initial":
                for _ in range(n_perturb):
                    # 'initial' type crossover will choose either 'rr' or 'br' crossover
                    iuse = self.count_unused_bank()  # count the number of old solutions
                    iuse_cut = (
                        len(self.bvar) - len(self.seed) - self.p["n_new_bank_cut"]
                    )

                    if self.ncycle == 0 and iuse > iuse_cut:
                        # 'rr' crossover
                        var1 = self.rvar[ind1]
                    else:
                        # 'br' crossover
                        var1 = self.bvar[ind1]
                    ap = get_available_partner(nbank, ind1, partner_mask)
                    var2 = self.rvar[np.random.choice(ap)]
                    newvar, pinfo = gen(var1, var2)
                    perturb_info.append(pinfo)
            else:
                raise ValueError("Unknown perturb_type: {}".format(perturb_type))

        # mutation perturbation
        for gen in dummy_var.mutation_generators:
            perturb_name = gen.name
            n_perturb = gen.n_perturb

            for _ in range(n_perturb):
                var = self.bvar[ind1]
                newvar, pinfo = gen(var)
                newvars.append(newvar)
                perturb_info.append(pinfo)

        return newvars, perturb_info

    def run_opt(self):
        # Try to read a restart file
        p = read_restart_param(self.p["restart_file"])
        if len(p) == 0:
            # A restart file is not read, which means that this is the first run
            # or the restart file is not readable.
            #
            # Assign default values for the following CSA parameters
            #
            # 'csa_stage' is introduced. The first number is for the size of bank
            # ie. nbank = ( csa_stage[0] + 1 ) * bank_size
            # The second number can be either 0 or 1. Except for the first stage (0,0),
            # the n-th stage has two sub-stages (n,0) and (n,1). At the stage of (n,0),
            # 'seed_mask' is set but at the stage of (n,1) 'seed_mask' is not set.
            # 'seed_mask' is not set for the first stage (0,0), too.
            p["csa_stage"] = (0, 0)
            p["nbank"] = self.p["nbank"]
            # Indexes in the 'seed_mask' is not selected as a seed nor as a partner
            # solution in crossover.
            p["seed_mask"] = []
            p["icmax"] = self.p["icmax"]

        # update parameters
        max_bank = self.p['max_bank']
        icmax_param = self.p['icmax']
        self.p = dict_merge(self.p, p)

        nbank = self.p["nbank"]
        csa_stage = self.p["csa_stage"]

        unit_nbank = int(self.p["nbank"] / (csa_stage[0] + 1))
        while nbank <= max_bank:
            # Run CSA
            self.run()

            # The next 'csa_stage'
            csa_stage = self.next_csa_stage(csa_stage)

            # Adjust the size of bank
            nbank = (csa_stage[0] + 1) * unit_nbank
            if csa_stage[1] == 0:
                # This stage is the first round of a new CSA stage
                seed_mask = range(nbank - unit_nbank)
                icmax = 0
            else:
                # This stage is the beginning of the second round of the current CSA stage
                seed_mask = []
                icmax = icmax_param

            # update CSA parameters properly for the next stage
            self.set_param(
                {
                    "nbank": nbank,
                    "icmax": icmax,
                    "seed_mask": seed_mask,
                    "partner_mask": seed_mask,
                    "csa_stage": csa_stage,
                }
            )

            if csa_stage[1] == 0:
                # reset the round(cycle) number if necessary
                self.ncycle = 0
            # Save the restart file
            self.save_restart()
            mpicomm.Barrier()

    @staticmethod
    def next_csa_stage(csa_stage):
        if csa_stage[0] == 0:
            return (1, 0)
        elif csa_stage[1] == 0:
            return (csa_stage[0], 1)
        else:
            return (csa_stage[0] + 1, 0)


class CSAPerturbationGenerator(object):
    def __init__(self, name, n_perturb):
        self.name = name
        self.n_perturb = n_perturb


class CSACrossoverGenerator(CSAPerturbationGenerator):
    def __init__(self, name, n_perturb, perturb_type="regular"):
        super().__init__(name, n_perturb)
        self.perturb_type = "regular"  # 'regular' or 'initial'

    def __call__(self, var1, var2):
        raise NotImplementedError


class CSAMutationGenerator(CSAPerturbationGenerator):
    def __init__(self, name, n_perturb):
        super().__init__(name, n_perturb)

    def __call__(self, var):
        raise NotImplementedError


class CSAVariable(object):
    def randomize(self):
        raise NotImplementedError

    def distance(var1, var2):
        raise NotImplementedError

    """
    List crossover generators

    Example:

    >>>     CrossoverGenerator('crossover', 10, 'regular')
    >>>     CrossoverGenerator('initial_crossover', 10, 'initial')
    """
    crossover_generators = [
        None,
    ]

    """
    List mutation generators

    Example:

    >>>     MutationGenerator('mutation', 10)
    """
    mutation_generators = [
        None,
    ]


# auxiliary base classes


class LocalMinimizerResult(object):
    def __init__(self, score, var):
        self.score = score
        self.var = var


class LocalMinimizer(object):
    def __init__(self, param={}):
        self.p = param

    def minimize(self, var):
        raise NotImplementedError
