"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
#from __future__ import division
#from __future__ import print_function
#from __future__ import absolute_import

from .csa import csa, mpicomm, mpirank, mpisize, dict_merge
from . import mol_util
from .mol_rand import mol_rand_type0, mol_rand_type1
from . import mol_perturb
import perturb_util as pu
import os
import numpy as np
from copy import copy, deepcopy
import zmatrix_c
from pyrosetta import init, pose_from_sequence ,get_fa_scorefxn, MoveMap, Pose, standard_packer_task, AtomID, pose_from_pdb
from pyrosetta.rosetta import core, protocols, numeric

try:
    import tmscore
except:
    pass

def get_var_from_pose(pose):
    # A unicode seq somehow causes a problem in pose_from_sequence.
    # Let's convert it to 'S1'
    var_seq = np.array(list(pose.sequence())).astype('S1')
    var_bb = np.empty((pose.total_residue()*4,3),dtype=np.double)
    ind = 0
    for r in range(1,pose.total_residue()+1):
        residue = pose.residue(r)
        assert( residue.atom_name(1) == ' N  ' )
        xyz = residue.xyz(1)
        var_bb[ind,0] = xyz[0]
        var_bb[ind,1] = xyz[1]
        var_bb[ind,2] = xyz[2]
        ind += 1
        assert( residue.atom_name(2) == ' CA ' )
        xyz = residue.xyz(2)
        var_bb[ind,0] = xyz[0]
        var_bb[ind,1] = xyz[1]
        var_bb[ind,2] = xyz[2]
        ind += 1
        assert( residue.atom_name(3) == ' C  ' )
        xyz = residue.xyz(3)
        var_bb[ind,0] = xyz[0]
        var_bb[ind,1] = xyz[1]
        var_bb[ind,2] = xyz[2]
        ind += 1
        assert( residue.atom_name(4) == ' O  ' )
        xyz = residue.xyz(4)
        var_bb[ind,0] = xyz[0]
        var_bb[ind,1] = xyz[1]
        var_bb[ind,2] = xyz[2]
        ind += 1

    return [var_seq,var_bb]

def get_pose(var):
    pose = pose_from_sequence(var[0].tostring())
    set_bb_coords(pose,var[1])
    return pose

def set_bb_coords_car(pose,coords):
    ind = 0
    for r in range(1,pose.total_residue()+1):
        residue = pose.residue(r)
        x, y, z = coords[ind]
        residue.set_xyz(1,numeric.xyzVector_double_t(x,y,z))
        ind += 1
        x, y, z = coords[ind]
        residue.set_xyz(2,numeric.xyzVector_double_t(x,y,z))
        ind += 1
        x, y, z = coords[ind]
        residue.set_xyz(3,numeric.xyzVector_double_t(x,y,z))
        ind += 1
        x, y, z = coords[ind]
        residue.set_xyz(4,numeric.xyzVector_double_t(x,y,z))
        ind += 1

def set_bb_coords(pose,coords):
    print(coords)
    print('get_pose', coords.shape)
    ( iz, zbond, zang, ztors ) = mol_util.getint(coords)

    print('iz',len(iz))
    print('zbond',len(zbond))

    zang *= np.pi/180.0 # radian

    natom = len(ztors)
    conf = pose.conformation()
    for r in range(1,pose.total_residue()):
        N_ind = 4*r-4
        CA_ind = N_ind+1
        C_ind = CA_ind+1
        O_ind = C_ind+1
        Nn_ind = O_ind+1
        CAn_ind = Nn_ind+1
        print('C_ind',C_ind)
        print('ztors[C_ind]',ztors[C_ind])
        print('r', r)
        pose.set_phi(r,ztors[C_ind])
        pose.set_psi(r,ztors[Nn_ind])
        pose.set_omega(r,ztors[CAn_ind])

        N = AtomID(1,r)
        CA = AtomID(2,r)
        C = AtomID(3,r)
        O = AtomID(4,r)
        Nn = AtomID(1,r+1)
        CAn = AtomID(2,r+1)
        conf.set_bond_length(N,CA,zbond[CA_ind])
        conf.set_bond_length(CA,C,zbond[C_ind])
        conf.set_bond_length(C,O,zbond[O_ind])
        conf.set_bond_length(C,Nn,zbond[Nn_ind])

        conf.set_bond_angle(N,CA,C,zang[C_ind])
        conf.set_bond_angle(CA,C,O,zang[O_ind])
        conf.set_bond_angle(CA,C,Nn,zang[Nn_ind])
        conf.set_bond_angle(C,Nn,CAn,zang[CAn_ind])

    # last residue
    r += 1
    N_ind = 4*r-4
    CA_ind = N_ind+1
    C_ind = CA_ind+1
    O_ind = C_ind+1
    pose.set_phi(r,ztors[C_ind])

    N = AtomID(1,r)
    CA = AtomID(2,r)
    C = AtomID(3,r)
    O = AtomID(4,r)
    conf.set_bond_length(N,CA,zbond[CA_ind])
    conf.set_bond_length(CA,C,zbond[C_ind])
    conf.set_bond_length(C,O,zbond[O_ind])

    conf.set_bond_angle(N,CA,C,zang[C_ind])
    conf.set_bond_angle(CA,C,O,zang[O_ind])

def get_score_names(scorefxn):
    decomp_names = []
    score_types = []
    for name, st in core.scoring.ScoreType.__dict__.items():
        if not isinstance(st,core.scoring.ScoreType) or not scorefxn.has_nonzero_weight(st):
            continue
        #weight = scorefxn.get_weight(st)
        decomp_names.append(name)
        score_types.append(st)

    arg = np.argsort(decomp_names)
    decomp_names = [ decomp_names[x] for x in arg ]
    score_types = [ score_types[x] for x in arg ]

    return decomp_names, score_types

def calc_dihedral_distance(var1,var2):
    ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)
    ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)
    tor_diff = np.absolute( ztors1 - ztors2 )
    mask = (tor_diff>180.0)
    tor_diff[mask] = 360.0 - tor_diff[mask]
    return float(np.sum(tor_diff))

Res13 = {'A':'ALA', 'R':'ARG', 'N':'ASN', 'D':'ASP', 'C':'CYS',
         'Q':'GLN', 'E':'GLU', 'G':'GLY', 'H':'HIS', 'I':'ILE',
         'L':'LEU', 'K':'LYS', 'M':'MET', 'F':'PHE', 'P':'PRO',
         'S':'SER', 'T':'THR', 'W':'TRP', 'Y':'TYR', 'V':'VAL','X':'UNK'}

Res31 ={'ALA':'A','CYS':'C','ASP':'D','GLU':'E','PHE':'F','GLY':'G',
        'HIS':'H','ILE':'I','LYS':'K','LEU':'L','MET':'M','ASN':'N',
        'PRO':'P','GLN':'Q','ARG':'R','SER':'S','THR':'T','VAL':'V',
        'TRP':'W','TYR':'Y','ASX':'N','GLX':'Q','UNK':'X','INI':'K',
        'AAR':'R','ACE':'X','ACY':'G','AEI':'T','AGM':'R','ASQ':'D',
        'AYA':'A','BHD':'D','CAS':'C','CAY':'C','CEA':'C','CGU':'E',
        'CME':'C','CMT':'C','CSB':'C','CSD':'C','CSE':'C','CSO':'C',
        'CSP':'C','CSS':'C','CSW':'C','CSX':'C','CXM':'M','CYG':'C',
        'CYM':'C','DOH':'D','EHP':'F','FME':'M','FTR':'W','GL3':'G',
        'H2P':'H','HIC':'H','HIP':'H','HTR':'W','HYP':'P','KCX':'K',
        'LLP':'K','LLY':'K','LYZ':'K','M3L':'K','MEN':'N','MGN':'Q',
        'MHO':'M','MHS':'H','MIS':'S','MLY':'K','MLZ':'K','MSE':'M',
        'NEP':'H','NPH':'C','OCS':'C','OCY':'C','OMT':'M','OPR':'R',
        'PAQ':'Y','PCA':'Q','PHD':'D','PRS':'P','PTH':'Y','PYX':'C',
        'SEP':'S','SMC':'C','SME':'M','SNC':'C','SNN':'D','SVA':'S',
        'TPO':'T','TPQ':'Y','TRF':'W','TRN':'W','TRO':'W','TYI':'Y',
        'TYN':'Y','TYQ':'Y','TYS':'Y','TYY':'Y','YOF':'Y','FOR':'X',
        '---':'-','PTR':'Y','LCX':'K','SEC':'D','MCL':'K','LDH':'K'}

def read_pdb(pdbfile):
    pdb = {}
    resname = {}
    with open(pdbfile,'r') as f:
        for line in f:
            record = line[:6]
            if record[:3] == 'TER':
                break
            if ( record != 'ATOM  ' and record != 'HETATM' ): continue
            atomname = line[12:16].strip()
            rname = line[17:20]
            rnum = int(line[22:26])
            resname[rnum] = rname
            x = float(line[30:38])
            y = float(line[38:46])
            z = float(line[46:54])
            if rnum not in pdb:
                pdb[rnum] = {}
            pdb[rnum][atomname] = (x,y,z)
    coords = []
    seq = []
    for rnum in sorted(pdb):
        coords.append( pdb[rnum]['N'] )
        coords.append( pdb[rnum]['CA'] )
        coords.append( pdb[rnum]['C'] )
        coords.append( pdb[rnum]['O'] )
        seq.append(Res31[resname[rnum]]) 

    return [np.array(seq),np.array(coords,dtype=np.double)]

class perturb_int_cross(mol_perturb.mol_perturb_int_cross):

    def __init__(self,param,amap):
        self.p = copy(param)
        self.p['residue_base'] = True
        super(perturb_int_cross,self).__init__(self.p,amap)

    def perturb_single(self,var1,var2):
        # determine the size of exchange
        # assume residue_base = True
        # It could be the size of either residue or atom
        esize = np.random.randint(self.nmin,self.nmax+1) # number of residues
        while True:
            mask = np.zeros(self.nres,dtype=np.uint8)
            istart = np.random.randint(self.nres-esize+1)
            istop = istart + esize
            for i in range(istart,istop):
                mask[i] = 1
            mask = np.logical_and(mask,self.use_residue)
            esize = np.count_nonzero(mask)
            if esize:
                esizes = []
                for imol in range(self.nmol):
                    count = 0
                    for ires in range(*self.mol_residue_range[imol]):
                        if mask[ires]:
                            count += 1
                    esizes.append(count)
                break

        newvar = deepcopy(var1)
        ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2[1])
        for imol in range(self.nmol):
            ( iz1, bond_new, ang_new, tor_new ) = mol_util.getint(newvar[1])
            # exchange size for each molecule
            es = esizes[imol]
            if es == 0:
                continue

            # start_index and stop_index
            r0, r1 = self.mol_residue_range[imol]
            istart_ind = np.random.randint((r1-r0)-es+1) + r0
            exresind = []
            count = 0
            ires = istart_ind
            while count < es:
                if self.use_residue[ires]:
                    count += 1
                    exresind.append(ires)
                ires += 1
                if ires == r1:
                    ires = r0
            ex_atom_mask = pu.get_atom_mask(exresind,self.xyzmap)

            tor_new, ang_new, bond_new = pu.exchange_coord_int2(ex_atom_mask,iz1,tor_new,ang_new,bond_new,ztors2,zang2,zbond2)
            # save int
            # assuming iz1 and iz2 are identical
            mol_util.setint(iz1,bond_new,ang_new,tor_new)
            newvar[1] = pu.exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())

            # exchange sequence
            newvar_seq = newvar[0]
            var2_seq = var2[0]
            for exres in exresind:
                newvar_seq[exres] = var2_seq[exres]

        return newvar, ex_atom_mask

class perturb_int_residue(mol_perturb.mol_perturb_int_residue):

    def perturb_single(self,var1,var2):
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1[1])
        ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2[1])

        newvar = [ None, None ]

        # calculate number of exchange atoms
        nres_change = np.random.randint(self.nmin,self.nmax+1)
        # exchange residue index
        exresind = np.random.choice(self.residue_index,nres_change,replace=False)
        # exchange atom index
        exind = pu.get_atom_index(exresind,self.xyzmap)
        # exchange
        tor_new, ang_new, bond_new = pu.exchange_coord_int(exind,iz1,ztors1,zang1,zbond1,ztors2,zang2,zbond2)
        # save int 
        # assuming iz1 and iz2 are identical
        mol_util.setint(iz1,bond_new,ang_new,tor_new)
        newvar_bb = pu.exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())

        # exchange sequence
        newvar_seq = copy(var1[0])
        var2_seq = var2[0]
        for exres in exresind:
            newvar_seq[exres] = var2_seq[exres]

        return [ newvar_seq, newvar_bb ], exresind

class perturb_torsion(mol_perturb.mol_perturb_torsion):

    def perturb_single(self,var1,var2):
        res_index = self.residue_index
        nres = self.xyzmap['nres']
        xyzmap_raw_data = self.xyzmap['raw_data']

        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1[1])
        ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2[1])

        # calculate number of exchange atoms
        n_exchange = min( np.random.randint(self.nmin,self.nmax+1), self.avail_atom_ind.size )
        exind = np.random.permutation(self.avail_atom_ind)[:n_exchange]
        exresind = set()
        for ei in exind:
            exresind.add( xyzmap_raw_data[ei][4] - 1 )    

        # exchange
        tor_new, ang_new, bond_new = pu.exchange_coord_int(exind,iz1,ztors1,zang1,zbond1,ztors2,zang2,zbond2)
        # save int 
        # assuming iz1 and iz2 are identical
        mol_util.setint(iz1,bond_new,ang_new,tor_new)
        newvar_bb = pu.exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())

        return [ copy(var1[0]), newvar_bb ], exresind

class perturb_car_cross(mol_perturb.mol_perturb_car_cross):

    def perturb_single(self,var1,var2):
        # calculate number of exchange atoms
        nres_change = np.random.randint(self.nmin,self.nmax+1)

        ra_center_ind = np.random.choice(self.ra_index)

        # calc distance from the representative residue 
        ra_var2 = var2[1][self.ra_index,:]
        ra_center = var2[1][ra_center_ind,:]
        ra_distance2 = np.sum( ( ra_var2 - ra_center )**2, axis=1 )

        # exchange residue index
        exresind = sorted( list(range(self.residue_index.size)), key=lambda x: ra_distance2[x] )[:nres_change]

        # exchange atom index
        exind = pu.get_atom_index(exresind,self.xyzmap)
        newvar_bb = pu.exchange_coord_car(exind,var1[1],var2[1]) 

        # exchange sequence
        newvar_seq = copy(var1[0])
        var2_seq = var2[0]
        for exres in exresind:
            newvar_seq[exres] = var2_seq[exres]

        return [ newvar_seq, newvar_bb ], exresind

class perturb_randint(mol_perturb.mol_perturb_randint):

    def perturb_single(self,var1):
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1[1])

        # energy check?
        if self.max_energy_diff is not None:
            ene = self.energy_func(var1[1])
            ene_cut = ene + self.max_energy_diff
        else:
            ene_cut = None

        nres = self.xyzmap['nres']
        xyzmap_raw_data = self.xyzmap['raw_data']

        cur_tor = ztors1.copy()
        cur_ang = zang1.copy()
        cur_bond = zbond1.copy()

        for irepeat in range(self.p['repeat']):
            best_ene = sys.float_info.max
            best_tor = None
            best_ang = None
            best_bond = None
            best_exresind = None
            success = False

            for itry in range(self.p['max_try']):
                tor_new, ang_new, bond_new, exresind = self.perturb_torsion(iz1,cur_tor,cur_ang,cur_bond,resind=True)

                if ene_cut is not None:
                    mol_util.setint(iz1,bond_new,ang_new,tor_new)
                    newvar = mol_util.getxyz()
                    newene = self.energy_func(newvar)
                    if newene <= ene_cut:
                        cur_tor = tor_new
                        cur_ang = ang_new
                        cur_bond = bond_new
                        cur_exresind = exresind
                        success = True
                        break
                    elif newene < best_ene:
                        best_ene = newene
                        best_tor = tor_new
                        best_ang = ang_new
                        best_bond = bond_new
                        best_exresind = exresind
                else:
                    cur_tor = tor_new
                    cur_ang = ang_new
                    cur_bond = bond_new
                    cur_exresind = exresind
                    success = True
                    break

            if not success:
                cur_tor = tor_new
                cur_ang = ang_new
                cur_bond = bond_new
                cur_exresind = best_exresind
                ene = best_ene 
                ene_cut = ene + self.max_energy_diff
                break # if energy fails, then exit immediately

        # save int 
        mol_util.setint(iz1,cur_bond,cur_ang,cur_tor)
        newvar_bb = pu.exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())

        return [ copy(var1[0]), newvar_bb ], cur_exresind

class csa_rosetta_design(csa):
    """csa for ROSETTA design"""

    aa20 = np.array(('A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I',
        'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V'))

    def randvarf(self):
        if self.p['rand_init_bank']:
            if not len(self.init_bank_ind):
                # reset init_bank_ind
                self.init_bank_ind = list(range(len(self.init_bank)))
            # pick one from init_bank
            ind = np.random.randint(len(self.init_bank_ind))
            ind = self.init_bank_ind.pop(ind)
            ivar = self.init_bank[ind]
        else:
            ivar = self.initvar
        newvar = self.randomize_conf(ivar)

        return newvar

    def randomize_conf(self,ivar):
        rand_conf_type = self.p['rand_conf_type']
        if rand_conf_type == 0:
            newvar_bb = self.rand_type0.randomize(ivar[1])
        elif rand_conf_type == 1:
            newvar_bb = self.rand_type1.randomize(ivar[1])
        else:
            raise ValueError('unknown rand_conf_type %s'%str(rand_conf_type))

        # randomize sequence
        nseq = len(ivar[0])
        newvar_seq = self.aa20[np.random.choice(20,nseq)]

        return [newvar_seq,newvar_bb]

    def perturbvarf(self,ind1):
        natom = self.xyzmap['natom']
        newvars = []
        perturb_info = []
        iuse = self.count_unused_bank()

        # int_cross
        nperturb = self.p['perturb_int_cross']['nperturb']
        if nperturb:
            # backbone crossover
            newvar, pinfo = self.perturb_int_cross.perturb(self.bvar,self.rvar,ind1,nperturb=nperturb)
            newvars += newvar
            perturb_info += pinfo

        # int_residue
        nperturb = self.p['perturb_int_residue']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_int_residue.perturb(self.bvar,self.rvar,ind1,nperturb=nperturb)
            newvars += newvar
            perturb_info += pinfo

        # torsion
        nperturb = self.p['perturb_torsion']['nperturb']
        if nperturb:
            if ( self.ncycle == 0 and iuse > len(self.bvar) - len(self.seed) - self.p['perturb_torsion']['n_new_bank_cut'] ):
                nmin, nmax = self.p['perturb_torsion']['n_range0']
                ( newvar, pinfo ) = self.perturb_torsion.perturb(self.bvar,self.rvar,ind1,src='rr',nperturb=nperturb,nmin=nmin,nmax=nmax)
                newvars += newvar
                perturb_info += pinfo
            else:
                br_ratio = self.p['perturb_torsion']['br_ratio']
                n1 = int( nperturb * br_ratio )
                n2 = nperturb - n1
                nmin, nmax = self.p['perturb_torsion']['n_range1']
                ( newvar, pinfo ) = self.perturb_torsion.perturb(self.bvar,self.rvar,ind1,src='br',nperturb=n1,nmin=nmin,nmax=nmax)
                newvars += newvar
                perturb_info += pinfo
                ( newvar, pinfo ) = self.perturb_torsion.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=n2,nmin=nmin,nmax=nmax)
                newvars += newvar
                perturb_info += pinfo

        # car_cross
        nperturb = self.p['perturb_car_cross']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_car_cross.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=nperturb)
            newvars += newvar
            perturb_info += pinfo

        # type_randint
        nperturb = self.p['perturb_randint']['nperturb']
        if nperturb:
            ( newvar, pinfo ) = self.perturb_randint.perturb(self.bvar,self.rvar,ind1,src='b',nperturb=nperturb)
            newvars += newvar
            perturb_info += pinfo

        return ( newvars, perturb_info )

    def minimize_init(self):
        super(csa_rosetta_design,self).minimize_init()
        self.decomp_mscore = []

    def pack_min_input(self,i_list):
        min_input = []
        for i in i_list:
            try:
                rinfo = self.perturb_info[i][2] # restrain info
            except:
                rinfo = None
            min_input.append( (self.pvar[i],rinfo) )
            # destroy self.pvar[i] to save memory
            self.pvar[i] = None
        return min_input

    def unpack_min_output(self,min_output,i_list):
        for ind in range(len(i_list)):
            i = i_list[ind]
            mo = min_output[ind]
            score, minvar, decomp_ene = mo
            self.mvar.append(minvar)
            self.mscore.append(score)
            try:
                self.perturb_info.append(self.perturb_info_backup[i])
            except:
                pass
            self.decomp_mscore.append(decomp_ene)
            self.print_log("m%d score: %.5E"%(len(self.mvar),score), level=3)

    def comparef(self,var1,var2):
        dist = self.get_distance(var1,var2)
        return dist

    def get_distance(self,var1,var2):
        if self.p['dist_method'] is None:
            return 0.0
        elif ( self.p['dist_method'] == 'rmsd' ):
            # rmsd
            try:
                bb_distance, newvar2 = pu.calc_rmsd(var1[1],var2[1])
            except:
                bb_distance =99.9
        else:
            bb_distance = calc_dihedral_distance(var1[1],var2[1])

        # sequence distance
        seq_distance = float( np.count_nonzero( var1[0] != var2[0] ) )

        distance = seq_distance + self.p['bb_distance_weight'] * bb_distance

        return distance

    def eval_update_single(self,i):
        if self.p['update_cut'] is not None:
            if (     self.p['update_cut_method'] == 'rmsd'
                 and self.calc_rmsd_valid_atom(self.mvar[i][1],self.native_var[0][1]) > self.p['update_cut']):
                self.print_log( "m%d is removed by rmsd cut"%i, level=3 )
                return
            elif (     self.p['update_cut_method'] == 'tmscore'
                   and self.calc_tmscore(self.mvar[i][1],self.native_var[0][1]) < self.p['update_cut']):
                self.print_log( "m%d is removed by TMscore cut"%i, level=3 )
                return

        super(csa_rosetta_design,self).eval_update_single(i)

    def eval_update_final(self):
        super(csa_rosetta_design,self).eval_update_final()

        if mpirank == 0:
            count = {} # number of trials generated by the perturb_name
            accept = {} # number of accepted trials generated by the perturb_name
            counter = [ None for x in range(len(self.update_mvar)) ] # residue source counter
            for i in range(len(self.update_mvar)):
                # calculate acceptace ratio
                pname = self.perturb_info[i][0]
                status = self.update_mvar[i]
                if ( pname not in count ):
                    count[pname] = 0
                if ( pname not in accept ):
                    accept[pname] = 0
                if ( status >= 0 ):
                    accept[pname] += 1
                count[pname] += 1

                res_source = self.perturb_info[i][1]
                counter[i] = {}
                for rs in res_source:
                    try:
                        rs = tuple(rs)
                    except:
                        rs = (None,None)
                    if len(rs) == 3:
                        # ( src, bind, ratio )
                        counter[i][(rs[0],rs[1])] = rs[2]
                    else:
                        # len(rs) == 2
                        # ( src, bind )
                        if ( rs[1] == None ): continue
                        try:
                            counter[i][rs] += 1
                        except:
                            counter[i][rs] = 1

            # print acceptance ratio
            for pname in sorted( count.keys() ):
                if ( count[pname] == 0 ):
                    ratio = 0.0
                else:
                    ratio = float(accept[pname]) / float(count[pname])
                try:
                    self.accept_ratio[pname] = ratio
                except:
                    self.accept_ratio = {}
                    self.accept_ratio[pname] = ratio
                self.print_log( "acceptance ratio of %s is %f"%(pname,ratio), rank=mpirank )

            # print detailed perturb info
            nbscore = len(self.bscore)
            s = []
            ds = []
            for i in range(len(self.update_bvar)):
                mi = self.update_bvar[i]
                if ( mi < 0 ): continue
                pi = self.perturb_info[mi]
                pname = pi[0]
                res_source = pi[1]
                cmi = counter[mi]
                nres = float(len(res_source))
                line = 'new b%d: %s'%(i+1,pname)
                for rs in sorted(cmi):
                    try:
                        line += ' %s%d(%.3f)'%(rs[0],rs[1]+1,cmi[rs]/nres)
                    except:
                        line += ' %s%s(%.3f)'%(rs[0],str(rs[1]),cmi[rs]/nres)
                if ( i < nbscore ):
                    s.append(self.mscore[mi]) 
                    delta_score = self.bscore[i]-self.mscore[mi]
                    ds.append(delta_score)
                    line += ' S(%.7E) dS(%.7E)'%(self.mscore[mi],delta_score)
                else:
                    line += ' S(%.7E)'%(self.mscore[mi])
                self.print_log(line)
            if s:
                average_s = np.average(s)
            else:
                average_s = 0.0
            if ds:
                average_ds = np.average(ds)
            else:
                average_ds = 0.0
            try:
                self.print_log('Average S: %.3f dS: %.3f'%(average_s,average_ds))
            except:
                print('ERROR')
                print(average_s, average_ds)

    def write_bank_history(self):
        if mpirank == 0 and self.bank_history_fh:
            if self.bank_history_header is False:
                self.write_bank_history_file_header()
                self.bank_history_header = True
            f = self.bank_history_fh
            fmt = ' %' + str(self.p['bank_history_precision']) + '.' + str(self.p['bank_history_precision']-7) + 'E'
            e_name = self.decomp_bscore[0][0]
            for mi in range(len(self.update_mvar)):
                bi = self.update_mvar[mi]
                if ( bi < 0 ): continue
                f.write('%6d %4d'%(self.niter,bi+1))
                for nv in self.native_var:
                    dist = self.get_bank_history_distance(self.mvar[mi],nv)
                    f.write(fmt%dist)
                for i in range(len(e_name)):
                    try:
                        f.write(fmt%self.decomp_mscore[mi][1][i])
                    except:
                        f.write(' ' + '0' * self.p['bank_history_precision'])
                f.write('\n')
            f.flush()

    def get_bank_history_distance(self,var1,var2):
        if self.p['bank_history_dist_method'] == 'rmsd':
            dist = self.calc_rmsd_valid_atom(var1[1],var2[1])
        elif self.p['bank_history_dist_method'] == 'rmsd_ca':
            dist = self.calc_rmsd_ca(var1[1],var2[1])
        elif self.p['bank_history_dist_method'] == 'tmscore':
            dist = self.calc_tmscore(var1[1],var2[1]) 
        else:
            self.print_error("Unknown bank_history_distance: %s"%self.p['bank_history_dist_method']) 
        return dist

    def write_rbank_history(self,nexclude=0):
        nbvar2 = len(self.bvar)
        if mpirank == 0 and self.bank_history_fh:
            if self.bank_history_header is False:
                self.write_bank_history_file_header()
                self.bank_history_header = True
            f = self.bank_history_fh
            fmt = ' %' + str(self.p['bank_history_precision']) + '.' + str(self.p['bank_history_precision']-7) + 'E'
            e_name = self.decomp_bscore[0][0]
            for bi in range(nexclude,nbvar2):
                f.write('%6d %4d'%(self.niter,bi+1))
                for nv in self.native_var:
                    dist = self.get_bank_history_distance(self.bvar[bi],nv)
                    f.write(fmt%dist)
                for i in range(len(e_name)):
                    try:
                        f.write(fmt%self.decomp_bscore[bi][1][i])
                    except:
                        f.write(' ' + '0' * self.p['bank_history_precision'])
                f.write('\n')
            f.flush()

    def update_bank(self):
        n_delta = super(csa_rosetta_design,self).update_bank()
        if mpirank == 0:
            if n_delta:
                self.decomp_bscore += [ None ] * n_delta
            # update decompsed energy
            try:
                self.decomp_bscore # exists?
            except:
                self.decomp_bscore = [ None ] * len(self.bvar)
            for i in range( len(self.update_bvar) ):
                ind = self.update_bvar[i]
                if ind < 0: continue
                self.decomp_bscore[i] = self.decomp_mscore[ind]

        return n_delta

    def perturbvar(self):
        super(csa_rosetta_design,self).perturbvar()
        if self.p['minimize_seed']:
            # add seed conformations to the perturbed conformation list
            for ind1 in self.seed:
                ( newvar, pinfo ) = self.mol_perturb_type0.perturb(self.bvar,self.rvar,ind1)
                self.mvar += newvar
                self.perturb_info += pinfo
        if self.p['fix_residue']:
            self.fix_mvar_residue()

    def randvar(self,nconf=None,varlist=None):
        if ( self.p['rand_init_bank'] ):
            # store init bank into self.init_bank for randomization
            self.init_bank = self.read_archive_coord()
            self.init_bank_ind = list(range(len(self.init_bank))) 
        super(csa_rosetta_design,self).randvar(nconf=nconf,varlist=varlist)
        if ( self.p['rand_init_bank'] ):
            # remove unnecessary variables
            del self.init_bank
            del self.init_bank_ind
        if self.p['fix_residue']:
            self.fix_mvar_residue()

    def fix_mvar_residue(self):
        fixind = []
        for resind in self.p['fix_residue']:
            resnum = resind + 1
            resatoms = self.xyzmap['data'][resnum]
            for atomname in resatoms:
                fixind.append( resatoms[atomname][0] - 1 )
        nonfix_mask = np.ones( self.xyzmap['natom'], dtype=bool )
        nonfix_mask[fixind] = False
        for i in range(len(self.mvar)):
            var1 = self.mvar[i]
            var2 = self.initvar
            # rmsd fit
            c_trans, U, ref_trans = pu.rms_fit(var2[1][nonfix_mask,:],var1[1][nonfix_mask,:])
            newvar = np.dot( var1[1] - c_trans, U ) + ref_trans 
            newvar[fixind,:] = var2[1][fixind,:]
            self.mvar[i][1] = newvar
   
    pyrosetta_initialized = False

    def __init__(self,param={}):
        p = {
            # model pdb
            'model_pdb': 'model.pdb',
            'native_coord': None,

            # basic csa parameters
            'nbank': 50,
            'nbank_add': 30,
            'nseed': 30,

            # termination
            'icmax': 3,  # If ncycle reaches icmax, iteration stops
            'iucut': 10, # the number of bank update is below iucut, ncycle increments 

            # measuring distances between conformations
            # dihedral: dihedral distance
            # rmsd: root-mean-square distance
            'dist_method': 'rmsd',
            'bb_distance_weight': 1.0,

            # dcut
            'dcut_reduce': 0.997252158427478, # dcut is reduced by dcut_reduce every iteration 0.4**(1./333) = 0.997...

            # restart
            'restart_file': 'csa_restart.dat',
            'save_restart': 1,
            'restart_mode': 2,

            'init_bank': None,
            'include_init_bank': True, # add init_bank to rbank
            'rand_init_bank': False, # randomize conformations in init_bank. if False, the conformations are
                                     # added without perturbation
            'grdmin': 0.1, # minimum RMS gradient per atom 

            # log
            'report_min': 0, # report minimization step frequency
            'history_file': 'history', # history filename
            'append_history': False, # to force appending to the existing history file
            'write_pdb': True, # writes pdb files
            'append_coord': False, # append coordinates to bank pdb files
            'write_xyz': False,
            'write_score_profile': True, # writes energy profile
            'profile_file': 'profile.energy',
            'record_gmin': False, # save gmin###.pdb

            # randomize/rbank
            # random conformation parameters
            'rand_conf_type': 0,
            # rand_conf_type == 0: numbers are sigmas of bond, angle, and torsions
            #     if angle or torsion is None, it is randomly perturbed
            # rand_conf_type == 1: numbers are maximum distances of x, y, and z
            # rand_conf_type == 2: numbers are sigma distances of x, y and z
            'rand_param0': { 'sigma_bond': 0.1, 'sigma_angle': 10.0, 'sigma_torsion': None, 'discrete_omega': True, 'residue_ratio': 1.0, 'res_index': None },
            'rand_param1': { 'max_x': 3.0, 'max_y': 3.0, 'max_z': 3.0, 'res_index': None },

            # perturbation
            'perturb_int_cross': { 'nperturb': 8, 'res_index': None, 'partner_mask': [] },
            'perturb_int_residue': { 'nperturb': 6, 'res_index': None, 'partner_mask': [] },
            'perturb_torsion': { 'nperturb': 6, 'res_index': None, 'n_range0': (3,7), 'n_range1': (1,5), 'partner_mask': [], 'br_ratio': 0.5, 'n_new_bank_cut': 5 },
            'perturb_car_cross': { 'nperturb': 0, 'res_index': None, 'partner_mask': [], 'nmin': 1, 'nmax': None },
            'perturb_randint': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': None, 'max_ratio': 0.02,
                                 'sigma_bond': 0.001, 'sigma_angle': 0.001, 'sigma_torsion': 5.0 },

            # perturb
            'minimize_seed': True, # seed variables are added to the perturbed variable lists
            'weighted_partner': False,

            # minimization method
            'temperature': 0.0,
            'boltzmann_const': 1.9872066e-3,

            # fix residue
            'fix_residue': None,

            # record bank energy and distance history
            'bank_history': None,
            'bank_history_dist_method': 'rmsd',

            # update is rejected if the condition is met
            'update_cut': None,
            'update_cut_method': 'rmsd',

            # metadynamics
            'ap_dist_method': 'rmsd',

            'verbose': 1,
            'log_caller': False,  
        }

        if self.pyrosetta_initialized == False:
            init() 
            self.pyrosetta_initialized = True

        self.p = dict_merge(p,param)
        self.verbose = self.p['verbose']

        # energy function
        self.scorefxn = get_fa_scorefxn()

        # decomposed energies
        self.decomp_energy_name, self.score_type = get_score_names(self.scorefxn)
        self.decomp_energy_name.insert(0,'total')

        # rosetta move_map
        self.move_map_all = MoveMap()
        #self.move_map_all.set_bb(False)
        self.move_map_all.set_bb(True)
        self.move_map_all.set_chi(True)
        self.move_map_all.set_nu(True)
        self.move_map_all.set_branches(True)
        self.min_mover_all = protocols.minimization_packing.MinMover()
        self.min_mover_all.movemap(self.move_map_all)

        self.move_map_fixbb = MoveMap()
        self.move_map_fixbb.set_bb(False)
        self.move_map_fixbb.set_chi(True)
        self.move_map_fixbb.set_nu(True)
        self.move_map_fixbb.set_branches(True)
        self.min_mover_fixbb = protocols.minimization_packing.MinMover()
        self.min_mover_fixbb.movemap(self.move_map_fixbb)

        self.initvar = read_pdb(self.p['model_pdb'])
        self.read_xyzmap()

        # read xyzmapfile
        mol_util.set_connect(self.xyzmap['connect'])

        ###################
        print(self.initvar)
        self.write_pdb('init.pdb',self.initvar)
        print('Done')
        ##################

        # init for csa parameter
        super(csa_rosetta_design,self).__init__(self.p)

    def minimizef(self,var,perturb_name):
        pose = get_pose(var)

        # rosetta packer
        packer = standard_packer_task(pose)
        packer.restrict_to_repacking() # turns off design
        pack_mover = protocols.simple_moves.PackRotamersMover(self.scorefxn,packer)
        self.min_mover_fixbb.apply(pose)

        # repeat pack_mover and min_mover
        prev_score = self.scorefxn(pose)
        print('INIT SCORE', prev_score)
        for repeat in range(20):
            print('REPEAT', repeat)
            test_pose = Pose()
            test_pose.assign(pose)
            packer = standard_packer_task(test_pose)
            packer.restrict_to_repacking() # turns off design
            pack_mover = protocols.simple_moves.PackRotamersMover(self.scorefxn,packer)
            pack_mover.apply(test_pose)
            test_pose.dump_pdb('before.pdb')
            self.min_mover_all.apply(test_pose)
            test_pose.dump_pdb('after.pdb')
            print('dump')
            print('INPUT')
            input()
            score = self.scorefxn(test_pose)
            print(type(score))
            if score > prev_score - 0.1:
                print('FAIL score', score)
                if score < prev_score:
                    pose = test_pose
                break
            pose = test_pose
            prev_score = score
            print('SCORE', score)
        
        min_energy = self.scorefxn(pose)
        ene = pose.energies().total_energies()
        decomp_ene = [None] * (len(self.score_type)+1)
        for i in range(len(self.score_type)):
            decomp_ene[i+1] = ene.get(self.score_type[i])
        decomp_ene[0] = min_energy

        minvar = get_var_from_pose(pose)
        print('MINVAR')
        print(minvar)

        return min_energy, minvar, (self.decomp_energy_name,decomp_ene)

    def report_iterate(self):
        # save bank conformations
        if ( mpirank != 0 ): return
        if ( self.p['write_diff'] and self.niter % self.p['write_diff'] == 0 ):
            self.write_diff()
        if ( self.p['write_bank'] ):
            if ( self.p['write_pdb'] ):
                self.savepdb('b',self.bvar,self.bscore,decomp_energy=self.decomp_bscore)
            if ( self.p['record_gmin'] and self.update_bvar[self.minscore_ind] >= 0 ):
                # gmin updated
                self.write_pdb('gmin%05d.pdb'%self.niter,self.bvar[self.minscore_ind],energy=self.minscore)
            if ( self.p['write_xyz'] ):
                self.savecrd('bank',self.bvar,self.bscore)
            if ( self.p['write_score_profile'] ):
                self.write_score_profile()
        if ( not self.history_fh ): return
        self.write_history_file()

    def read_native_var(self):
        # native var
        if self.p['native_coord']:
            if isinstance(self.p['native_coord'],str):
                self.native_var = [ read_pdb(self.p['native_coord']) ]
            else:
                self.native_var = []
                for pdb_file in self.p['native_coord']:
                    var = read_pdb(pdb_file)
                    self.native_var.append(var)

    def read_xyzmap(self):
        if hasattr(self,'xyzmap'):
            # already read
            return

        natom = self.initvar[1].shape[0] # BB only
        # N-terminal
        connect = [
            (2,),    # N
            (1,3),   # CA
            (2,4,5), # C
            (3,)     # O
        ]
        for i in range(5,natom-4,4):
            connect.append( (i-2,i+1) ) # N
            connect.append( (i,i+2) )   # CA
            connect.append( (i+1,i+3) ) # C
            connect.append( (i+2,) )    # O
        # C-terminal
        connect.append( (natom-5,natom-2) ) # N
        connect.append( (natom-3,natom-1) ) # CA
        connect.append( (natom-2,natom) )   # C
        connect.append( (natom-1,) )        # O
            
        # xyzmap dict
        # ['title']: title
        # ['nres']: nres
        # ['seq']: sequence
        # ['natom']: natom
        # ['data'][resnum][atomname]: list of columns
        # ['resrange'][resnum]: tuple of atom index range
        self.xyzmap = {}
        self.xyzmap['title'] = 'CSA rosetta design'
        self.xyzmap['nres'] = int(natom/4)
        self.xyzmap['seq'] = 'X' * self.xyzmap['nres']
        self.xyzmap['natom'] = natom
        self.xyzmap['connect'] = connect

        # read continuous molecules
        # TODO for multiple chain
        mol = { 1:1 }
        imol = 1
        size = len(connect)
        while ( len(mol) < size ):
            updated = True
            while ( updated ):
                updated = False
                for sn in range(1,natom+1):
                    if ( sn in mol ): continue
                    conn = connect[sn-1]
                    for cn in conn:
                        if ( cn in mol ):
                            mol[sn] = mol[cn]
                            updated = True
            if len(mol) < size:
                imol += 1
                for sn in range(1,natom+1):
                    if ( sn in mol ): continue
                    mol[sn] = imol
                    break

        # read atom lines
        data = {}
        raw_data = []
        serial = 0
        for r in range(len(self.initvar[0])):
            resnum = r+1
            resname = Res13[self.initvar[0][r]]
            # N
            serial += 1
            c = [ serial, serial, 'N', resname, resnum, 0, 0, mol[serial] ]
            raw_data.append(c)
            if c[4] not in data:
                data[c[4]] = {}
            data[c[4]][c[2]] = c # residue number -> atom name = serial number of xyz
            # CA
            serial += 1
            c = [ serial, serial, 'CA', resname, resnum, 0, 0, mol[serial] ]
            raw_data.append(c)
            if c[4] not in data:
                data[c[4]] = {}
            data[c[4]][c[2]] = c # residue number -> atom name = serial number of xyz
            # C
            serial += 1
            c = [ serial, serial, 'C', resname, resnum, 0, 0, mol[serial] ]
            raw_data.append(c)
            if c[4] not in data:
                data[c[4]] = {}
            data[c[4]][c[2]] = c # residue number -> atom name = serial number of xyz
            # CA
            serial += 1
            c = [ serial, serial, 'O', resname, resnum, 0, 0, mol[serial] ]
            raw_data.append(c)
            if c[4] not in data:
                data[c[4]] = {}
            data[c[4]][c[2]] = c # residue number -> atom name = serial number of xyz

        self.xyzmap['data'] = data  
        self.xyzmap['raw_data'] = raw_data

        # resrange
        resrange = {}
        atom_index = 0
        for res in sorted(data):
            end_index = atom_index + len(data[res])
            resrange[res] = ( atom_index, end_index )
            atom_index = end_index 
        self.xyzmap['resrange'] = resrange

    def init_accessor(self):
        self.read_xyzmap()
        super(csa_rosetta_design,self).init_accessor()

        # initialize randomize types
        self.rand_type0 = mol_rand_type0(self.p['rand_param0'],self.xyzmap)
        self.rand_type1 = mol_rand_type1(self.p['rand_param1'],self.xyzmap)

        # perturb type0
        # type0 simply add the seed conformation
        self.mol_perturb_type0 = mol_perturb.mol_perturb_type0({'nperturb':1},self.xyzmap)

        # initialize perturb types
        self.perturb_int_cross = perturb_int_cross(self.p['perturb_int_cross'],self.xyzmap)
        self.perturb_int_residue = perturb_int_residue(self.p['perturb_int_residue'],self.xyzmap)
        self.perturb_torsion = perturb_torsion(self.p['perturb_torsion'],self.xyzmap)
        self.perturb_car_cross = perturb_car_cross(self.p['perturb_car_cross'],self.xyzmap)
        self.perturb_randint = perturb_randint(self.p['perturb_randint'],self.xyzmap)

    def write_history_file(self):
        if hasattr(self,'minimize_nee'):
            pre = self.p['profile_precision']
            pre2 = pre - 7
            numfmt = '%' + str(pre) + '.' + str(pre2) + 'E'
            self.print_log( "Writing history", level=4 )
            fmt = "%6d %3d %10.4E %4d %4d " + numfmt + " " + numfmt + " %15d %4d %4d\n"
            self.history_fh.write(fmt
                %(self.niter,self.ncycle,self.dcut,self.minscore_ind+1,self.maxscore_ind+1,self.minscore,self.maxscore,self.minimize_nee,self.count_unused_bank(),self.count_bank()))
            self.history_fh.flush()
        else:
            super(csa_rosetta_design,self).write_history_file()

    def savepdb(self,prefix,vars,energy=None,decomp_energy=None):
        var_size = len(vars)
        outfmt = '%s%0' + '%d'%(max(int(np.log10(var_size))+1,3)) + 'd.pdb'
        if self.p['append_coord']:
            mode = 'a'
        else:
            mode = 'w'
        for i in range(var_size):
            outfile = outfmt%(prefix,i+1)
            try:
                ene = energy[i]
            except:
                ene = None
            self.write_pdb(outfile,vars[i],energy=ene,decomp_energy=None,mode=mode)
        if ( energy ):
            imin = energy.index(min(energy))
            self.write_pdb('gmin.pdb',vars[imin],energy=energy[imin],mode=mode)

    def write_pdb(self,pdbfile,var,energy=None,decomp_energy=None,mode='w'):
        self.print_log('Writing %s'%(pdbfile),level=5)
        pose = get_pose(var)
        with open(pdbfile,mode) as pdbf:
            if energy is not None:
                pdbf.write('REMARK  Total Energy= %f\n'%energy)
                if hasattr(self,'niter'):
                    pdbf.write('REMARK  Iteration %d\n'%self.niter)
            serial = 1
            for r in range(1,pose.total_residue()+1):
                residue = pose.residue(r)
                for a in range(1,pose.residue(r).natoms()+1):
                    xyz = residue.xyz(a)
                    atmname = residue.atom_name(a)
                    pdbf.write('%-6s%5d %-4s %-3s  %4d    %8.3f%8.3f%8.3f\n'%(
                        'ATOM',
                        serial,
                        residue.atom_name(a),
                        residue.name()[:3],
                        r,
                        xyz[0],
                        xyz[1],
                        xyz[2],
                    ))
                    serial += 1
            pdbf.write('END\n')

    def write_history_file_header(self):
        pre = str(self.p['profile_precision'])
        fmt = '%6s %3s %10s %4s %4s %' + pre + 's %' + pre + 's %15s %4s %4s\n' 
        self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_energy','max_energy','ntrial','iuse','nbk'))
        self.history_fh.flush()

    def restart_key_list(self):
        return super(csa_rosetta_design,self).restart_key_list() + [ 'decomp_bscore', 'minimize_nee' ]

    def pick_rbank(self,npick):
        sel_idx_list = super(csa_rosetta_design,self).pick_rbank(npick)
        if mpirank == 0:
            for x in sel_idx_list:
                self.decomp_bscore.append( deepcopy(self.decomp_mscore[x]) ) 
        return sel_idx_list

    def sort_mvar(self):
        ind = super(csa_rosetta_design,self).sort_mvar()
        if mpirank == 0:
            self.decomp_mscore = [ self.decomp_mscore[x] for x in ind ]

    def remove_bank(self,r_ind):
        b_ind = np.ones(len(self.bvar),dtype=bool)
        b_ind[r_ind] = False
        b_ind = np.where(b_ind)[0].tolist()
        if mpirank == 0:
            self.decomp_bscore = [ self.decomp_bscore[bi] for bi in b_ind ]
            self.decomp_mscore = self.decomp_bscore

        b_ind = super(csa_rosetta_design,self).remove_bank(r_ind)

        return b_ind

    def write_bank_history_file_header(self):
        f = self.bank_history_fh
        fsize = self.p['bank_history_precision']
        fmt = ' %' + str(fsize) + 's'
        distname = self.get_bank_history_distance_name()
        f.write("#niter bank")
        for i in range(len(self.native_var)):
            f.write(fmt%distname[:fsize])
        e_name = self.decomp_bscore[0][0]
        for i in range(len(e_name)):
            f.write(fmt%e_name[i])
        f.write('\n')
        f.flush()

    def get_bank_history_distance_name(self):
        if self.p['bank_history_dist_method'] == 'rmsd':
            name = 'rmsd'
        elif self.p['bank_history_dist_method'] == 'rmsd_ca':
            name = 'rmsdCA'
        elif self.p['bank_history_dist_method'] == 'tmscore':
            name = 'TMscore'
        else:
            self.print_error("Unknown bank_history_distance: %s"%self.p['bank_history_dist_method']) 
        return name

    def get_valid_atom_index(self):
        try:
            valid_atom_index = self.valid_atom_index
            return valid_atom_index
        except:
            valid_atom_index = []
            for al in self.xyzmap['raw_data']:
                ai = al[0]-1
                try:
                    # if native_var is defined, check the coordinate
                    if self.native_var[0][1][ai,0] >= 9999.0: # null value?
                        continue
                except:
                    pass
                valid_atom_index.append(ai)
            self.valid_atom_index = valid_atom_index
        return valid_atom_index

    def get_CA_index(self):
        try:
            ca_index = self.ca_index
            return ca_index
        except:
            ca_index = []
            for ai in self.get_valid_atom_index():
                if self.xyzmap['raw_data'][ai][2] == 'CA':
                    ca_index.append(ai)
            self.ca_index = ca_index
        return ca_index

    def get_heavy_index(self):
        """ return the indexes of heavy atoms (not hydrogen) """
        try:
            heavy_index = self.heavy_index
            return heavy_index
        except:
            heavy_index = []
            for ai in self.get_valid_atom_index():
                if self.xyzmap['raw_data'][ai][2][0] == 'H':
                    continue
                heavy_index.append(ai)
            self.heavy_index = heavy_index
        return heavy_index

    def calc_rmsd_valid_atom(self,var1,var2):
        a_index = self.get_valid_atom_index()
        c1 = var1[a_index,:] 
        c2 = var2[a_index,:]
        dist = pu.calc_rmsd(c1,c2)[0]
        return dist

    def calc_tmscore(self,var1,var2):
        if np.any( np.isnan(var1) ) or np.any( np.isnan(var2) ):
            return 0.0
        ca_index = self.get_CA_index()
        ca1 = var1[ca_index,:]
        ca2 = var2[ca_index,:]
        tm = tmscore.tmscore(ca1,ca2)[0]
        return tm

    def calc_rmsd_ca(self,var1,var2):
        ca_index = self.get_CA_index()
        ca1 = var1[ca_index,:]
        ca2 = var2[ca_index,:]
        dist = pu.calc_rmsd(ca1,ca2)[0]
        return dist

    def write_score_profile(self):
        e_name = self.decomp_bscore[0][0]
        f = open(self.p['profile_file'],'w')
        f.write('#bnk') 
        fsize = self.p['bank_history_precision']
        fmt = ' %' + str(fsize) + 's'

        distname = self.get_bank_history_distance_name()
        if hasattr(self,'native_var'):
            for i in range(len(self.native_var)):
                f.write(fmt%distname[:fsize])

        for i in range(len(e_name)):
            f.write(' %12s'%e_name[i])
        f.write('\n')

        fmt = ' %' + str(self.p['bank_history_precision']) + '.' + str(self.p['bank_history_precision']-7) + 'E'
        for j in range(len(self.bvar)):
            f.write('%4d'%(j+1))
            if hasattr(self,'native_var'):
                for nv in self.native_var:
                    dist = self.get_bank_history_distance(self.bvar[j],nv)
                    f.write(fmt%dist)

            for i in range(len(e_name)):
                try:
                    f.write(' %12.5E'%self.decomp_bscore[j][1][i])
                except:
                    f.write(' 000000000000')
            f.write('\n')
        f.close()

    def get_adaptive_potential_distance(self,var):
        distance = np.empty(len(self.p['ap_range']),dtype=float)
        for i in range(len(distance)):
            if self.p['ap_dist_method'] == 'rmsd':
                distance[i] = pu.calc_rmsd(var[1],self.native_var[i][1])[0]
            elif self.p['ap_dist_method'] == 'dihedral':
                # calculates dihedral angle distance
                ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var[1])
                ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(self.native_var[i][1])

                tor_diff = np.absolute( ztors1 - ztors2 )
                ind = np.where(tor_diff>180.0)
                tor_diff[ind] = 360.0 - tor_diff[ind]
                distance[i] = np.sum(tor_diff)
            else:
                self.print_error("Unknown ap_distance: %s"%self.p['ap_dist_method']) 
        return distance

    def report_create_firstbank(self):
        # save initial random conformations
        if ( mpirank != 0 ): return
        if ( self.p['write_bank'] ):
            if ( self.p['write_pdb'] ):
                self.savepdb('r',self.rvar,self.rscore)
            if ( self.p['write_score_profile'] ):
                self.write_score_profile()
        if ( not self.history_fh ): return
        self.write_history_file()

    def read_archive_coord(self):
        varlist = []
        arcfile = self.p['init_bank']
        for af in arcfile:
            var = read_pdb(af)
            varlist.append(var)

        return varlist

    def add_rbank(self,nbank=None,nbank_add=None,varlist=None):
        if hasattr(self,'perturb_info'):
            del self.perturb_info

        if self.p['init_bank'] is not None and self.p['include_init_bank']:
            if varlist is None:
                varlist = []
            arcvar = self.read_archive_coord()
            varlist += arcvar
            self.print_log('%d conformations were read from the archive'%len(arcvar))

        try:
            self.decomp_bscore
        except:
            self.decomp_bscore = []

        super(csa_rosetta_design,self).add_rbank(nbank=nbank,nbank_add=nbank_add,varlist=varlist)
