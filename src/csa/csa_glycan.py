"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import absolute_import

from builtins import range
from .csa_TINKER import csa_TINKER
from .glycan_perturb import glycan_perturb_int_cross, glycan_perturb_int_residue, glycan_perturb_torsion, glycan_perturb_randint
from .perturb_util import get_atom_index, calc_rmsd, rms_fit
from .mol_rand import mol_rand_type0, mol_rand_type1
from .csa import dict_merge
from . import mol_util
from numpy import array, empty, zeros, finfo, sort

def read_pdb(pdbfile):   # copied from csa_MODELLER
    f = open(pdbfile,'r')
    pdb = []
    for line in f:
        record = line[:6]
        if ( record != 'ATOM  ' ): continue
        serial = int( line[6:11] )
        atomname = line[12:16]
        altloc = line[16]
        resname = line[17:20]
        chain = line[21]
        resnum = int( line[22:26] )
        achar = line[26]
        x = float(line[30:38])
        y = float(line[38:46])
        z = float(line[46:54])
        try:
            occ = float( line[54:60] )
        except:
            occ = 1.0
        try:
            tfactor = float( line[60:66] )
        except:
            tfactor = 1.0
        try:
            segid = line[72:76]
        except:
            segid = ''
        try:
            elsymbol = line[76:78]
        except:
            elsymbol = ''
        try:
            charge = float(line[78:80])
        except:
            charge = 0.0
        # 0:  record
        # 1:  serial
        # 2:  atomname
        # 3:  altloc
        # 4:  resname
        # 5:  chain
        # 6:  resnum
        # 7:  achar
        # 8:  x
        # 9:  y
        # 10: z
        # 11: occ
        # 12: tfactor
        # 13: segid
        # 14: elsymbol
        # 15: charge
        pdb.append( [ record, serial, atomname, altloc, resname, chain, resnum, achar, x, y, z, occ, tfactor, segid, elsymbol, charge ] )
    f.close()
    return pdb

def read_native_pdb(pdbfile,amap):
    native_pdb = read_pdb(pdbfile)
    native_coord = {}
    for p in native_pdb:
        key=(p[6],p[2].strip())
        native_coord[key] = (p[8],p[9],p[10])

    natom = len(amap)
    var = empty((natom,3)) 
    var.fill(99999.999) # null value
    for i in range(len(amap)):
        atom_name = amap[i][2]
        res_num = amap[i][4]
        key = (res_num,atom_name)
        if ( key in native_coord ):
            var[i,:] = native_coord[key]

    return var

def read_pdb_coord(pdbfile):
    pdb = read_pdb(pdbfile)
    coord = []
    for p in pdb:
        coord.append( (p[8],p[9],p[10]) )    

    return array(coord)

class csa_glycan(csa_TINKER):

    def read_glycanmap(self):
        '''
        OUTput = {
                   1:{'ID/Filename': string,
                      'Chain/ Segment ID': string, 
                      'Glycosylation Site': list(chain, resid, resname), 
                      'Glycan Sequence': list, 'Segment Type': string'}, 
                 }
        '''
                 
        mapfile = self.p['glycanmapfile']
        count = 0
        ret = {}
        f = open(mapfile)
        try:
            f = open(mapfile,'r')
            self.print_log( "Reading %s"%(mapfile), level=4 )
        except:
            self.print_error( "cannot read %s"%(mapfile) )
            return

        for line in f:
            if line[:2] == 'ID':
               count += 1
               ret[count] = {}
               header, pdbid = line.split(':')
               ret[count][header] = pdbid.strip()
            elif line[:6] == 'Segmen':
               header, type = line.split(':')
               ret[count][header] = type.strip()
            elif line[:6] == 'Chain/':
               header, chain_ret = line.split(':')
               ret[count][header] = chain_ret.strip()
            elif line[:6] == 'Glycos':
               header, site = line.split(':')
               chain, resid, resname =  site.split('/')
               ret[count][header] = [chain.strip(), resid.strip(), resname.strip()]
            elif line[:6] == 'Glycan':
               ret[count]['Glycan Sequence'] = []
            else:
               if (line[0].isdigit()):
                  ret[count]['Glycan Sequence'].append(line.strip())
               else: raise # please check glycanmap file.
        f.close()
        self.glycanmap = ret

    #def randvarf(self):
    #    newvar = None
    #    rand_conf_type = self.p['rand_conf_type']
    #    if ( self.p['rand_init_bank'] ):
    #        if ( not len(self.init_bank_ind) ):
    #            # reset init_bank_ind
    #            self.init_bank_ind = range(len(self.init_bank))
    #        # pick one from init_bank
    #        ind = random.randint(len(self.init_bank_ind))
    #        ind = self.init_bank_ind.pop(ind)
    #        ivar = self.init_bank[ind]
    #    else:
    #        ivar = self.initvar
    #    if ( rand_conf_type == 1 ):
    #        newvar = self.rand_type1.randomize(ivar)
    #    else:
    #        newvar = self.rand_type0.randomize(ivar)
    #    return newvar

    def perturbvarf(self,ind1):
        natom = self.xyzmap['natom']

        nperturb = ( self.p['perturb_int_cross']['nperturb'],
                     self.p['perturb_int_residue']['nperturb'],
                     self.p['perturb_torsion']['nperturb'],
                     self.p['perturb_randint']['nperturb'],
                   )
        newvars = []
        perturb_info = []
        iuse = self.count_unused_bank()

        # int_cross
        if ( nperturb[0] ):
            ( newvar, pinfo ) = self.perturb_int_cross.perturb(self.bvar,self.rvar,ind1,nperturb=nperturb[0])
            newvars += newvar
            perturb_info += pinfo

        # int_residue
        if ( nperturb[1] ):
            ( newvar, pinfo ) = self.perturb_int_residue.perturb(self.bvar,self.rvar,ind1,nperturb=nperturb[1])
            newvars += newvar
            perturb_info += pinfo

        # torsion
        if ( nperturb[2] ):
            if ( self.ncycle == 0 and iuse > len(self.bvar) - len(self.seed) - self.p['perturb_torsion']['n_new_bank_cut'] ):
                nmin, nmax = self.p['perturb_torsion']['n_range0']
                ( newvar, pinfo ) = self.perturb_torsion.perturb(self.bvar,self.rvar,ind1,src='rr',nperturb=nperturb[2],nmin=nmin,nmax=nmax)
                newvars += newvar
                perturb_info += pinfo
            else:
                br_ratio = self.p['perturb_torsion']['br_ratio']
                n1 = int( nperturb[2] * br_ratio )
                n2 = nperturb[2] - n1
                nmin, nmax = self.p['perturb_torsion']['n_range1']
                ( newvar, pinfo ) = self.perturb_torsion.perturb(self.bvar,self.rvar,ind1,src='br',nperturb=n1,nmin=nmin,nmax=nmax)
                newvars += newvar
                perturb_info += pinfo
                ( newvar, pinfo ) = self.perturb_torsion.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=n2,nmin=nmin,nmax=nmax)
                newvars += newvar
                perturb_info += pinfo

        # type_randint
        if ( nperturb[3] ):
            ( newvar, pinfo ) = self.perturb_randint.perturb(self.bvar,self.rvar,ind1,src='b',nperturb=nperturb[3])
            newvars += newvar
            perturb_info += pinfo

        return ( newvars, perturb_info )

    def __init__(self,param={}):
        p = {
            # generating random conformations
            # 0: bonds, angles and torsions are perturbed by gaussian perturbation
            #    for torsion, they are phi, psi, chi1, ...
            # 1: x, y, and z are randomly moved to their maximum distance
            # 2: x, y, and z are randomly moved according to gaussian perturbation
            # backtor: take backbone torsions from backtor_file
            'rand_conf_type': 0,
            # random conformation parameters
            # rand_conf_type == 0: numbers are sigmas of bond, angle, and torsions
            #     if angle or torsion is None, it is randomly perturbed
            # rand_conf_type == 1: numbers are maximum distances of x, y, and z
            # rand_conf_type == 2: numbers are sigma distances of x, y and z
            'rand_param0': { 'sigma_bond': 0.1, 'sigma_angle': 10.0, 'sigma_torsion': None, 'discrete_omega': True, 'residue_ratio': 1.0, 'res_index': None },
            'rand_param1': { 'max_x': 3.0, 'max_y': 3.0, 'max_z': 3.0, 'res_index': None },

            # perturbation
            'perturb_int_cross': { 'nperturb': 8, 'res_index': None, 'partner_mask': [] },
            'perturb_int_residue': { 'nperturb': 6, 'res_index': None, 'partner_mask': [] },
            'perturb_torsion': { 'nperturb': 6, 'res_index': None, 'n_range0': (3,7), 'n_range1': (1,5), 'partner_mask': [], 'br_ratio': 0.5, 'n_new_bank_cut': 5 },
            'perturb_randint': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': None, 'max_ratio': 0.02 },

            'glycanmapfile': 'glycan_reader.txt',
            'native_pdb': None,
            'glycan_res_index': None,
        }

        self.p = dict_merge(p,param)
        mol_util.init_mol_util('tinker')

        # init for csa parameter
        super(csa_glycan,self).__init__(self.p)

    def init_accessor(self):
        super(csa_glycan,self).init_accessor()
        self.read_glycanmap()
        # glycan residue index
        if ( self.p['glycan_res_index'] == None ):
            self.glycan_res_index = arange(self.xyzmap['nres'])    
        else:
            self.glycan_res_index = sort(array(self.p['glycan_res_index']))

        # native pdb
        if ( self.p['native_pdb'] ):
            if isinstance(self.p['native_pdb'],list) or isinstance(self.p['native_pdb'],tuple):
                self.native_var = []
                for npdb in self.p['native_pdb']:
                    self.native_var.append( read_native_pdb(npdb,self.xyzmap['raw_data']) )
            else:
                self.native_var = [ read_native_pdb(self.p['native_pdb'],self.xyzmap['raw_data']) ]

        # initialize randomize types
        self.rand_type0 = mol_rand_type0(self.p['rand_param0'],self.xyzmap)
        self.rand_type1 = mol_rand_type1(self.p['rand_param1'],self.xyzmap)

        # initialize perturb types
        self.perturb_int_cross = glycan_perturb_int_cross(self.p['perturb_int_cross'],self.xyzmap,self.glycanmap)
        self.perturb_int_residue = glycan_perturb_int_residue(self.p['perturb_int_residue'],self.xyzmap)
        self.perturb_torsion = glycan_perturb_torsion(self.p['perturb_torsion'],self.xyzmap)
        self.perturb_randint = glycan_perturb_randint(self.p['perturb_randint'],self.xyzmap)
   
    def get_bank_history_distance(self,var1,var2):
        if self.p['bank_history_dist_method'] is 'rmsd':
            dist = self.calc_rmsd_glycan_atom(var1,var2)
        else:
            dist = super(csa_glycan,self).get_bank_history_distance(var1,var2) 
        return dist

    def calc_rmsd_glycan_atom(self,var1,var2):
        a_index = get_atom_index(self.glycan_res_index,self.xyzmap)
        c1 = var1[a_index,:]
        c2 = var2[a_index,:]
        dist = calc_rmsd(c1,c2)[0]
        return dist

    def comparef(self,var1,var2):
        if ( self.p['dist_method'] == 'rmsd' ):
            a_index = get_atom_index(self.glycan_res_index,self.xyzmap)
            try:
                distance = calc_rmsd(var1[a_index,:],var2[a_index,:])[0]
            except:
                distance =99.9
        else:
            distance = super(csa_glycan,self).comparef(var1,var2)

        return float(distance)
