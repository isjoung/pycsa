"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung and Seung Hwan Hong
Contributors: Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import numpy as np
import pickle
from .csa import *
import weight_util as wu
from perturb_util import get_available_partner
from scipy import optimize
from math import log

class csa_weight(csa):
    """csa for weight optimization"""

    def __init__(self,param={}):
        p = {
            'nbank': 50,
            'nseed': 30,
            'icmax': 4,
            'iucut': 5,
            'decut': 0.001,
            'dcut1': 2.0,
            'dcut2': 3.0, 
            'dcut_reduce': (2./3.)**(1.0/20),
            'nbank_add': 0,

            'save_restart': 1,

            'history_file': 'history',
            'append_history': False,

            'perturb_crossover': { 'nperturb': 0, 'partner_mask': [], 'max_ratio': 0.5, },
            'perturb_crossover2': { 'nperturb': 0, 'partner_mask': [], 'n_new_bank_cut': 2, 'max_ratio': 0.2, },
            'perturb_random': { 'nperturb': 0, 'nmin': 1, 'nmax': 3, 'perturb_ratio': 0.2, },

            'write_bank': False,
            'write_score_profile': True,
            'bank_history_precision': 12,
            'profile_precision': 12,

            'parallel_minimization': 2,

            'minimize_seed': False,
            'native_var': None,

            # problem specific parameters
            'data_files': [], # list of data files
            'ref_scores': None, # list of reference scores (only for score_type == 5)
            'fail_score_slope': 2.0,
            'skip_data_file': False, # skip data files if the files cannot be opened
            'decomp_params': [], # list of parameters of which the summation is the total score
            'opt_params': [], # list of parameters to be optimized (subset of decomp_params)
            'score_name': None, # the name of the score column
            'n_best': 1, # the number of best scores to be averaged
            'score_type': 1, # average score(1), best score(2), or worst score(3) of top n_best energy structures
                             # correlation coefficient(4)
            'minimize_score': True, # True if score needs to be minimized, otherwise False
            'minimize_energy': True, # True if energy needs to be minimized, otherwise False
            'harmonic_weight': 1e-8, # try to minimize the weight if possible
            'penalty_type': 0, # 0: penalty from zero, 1: penalty from 1.0
            'min_maxiter': 2000, # max iter for nelder-mead minimization
            'min_perturb_ratio': 0.1, # relative ratio to max_weight for optimization step (0,1)
            'min_tol': 0.0001, # minimum distances between solutions in nelder-mead minimization
            'min_method': 'nelder-mead',
            'max_weight': 2.0, # max_ratio for randomization > 1.0 (float or list (size of opt_param))
            'default_weight': 1.0, # default weight (float or list (size of opt_param))
            'input_data_file': 'input.dat', # input data file
        }

        self.p = dict_merge(p,param)

        # init for csa parameter
        super(csa_weight,self).__init__(self.p)

        # max_weight
        if isinstance(self.p['max_weight'],float):
            self.max_weight = np.empty(len(self.p['decomp_params']))
            self.max_weight.fill(self.p['max_weight'])
        else:
            # list or tuple
            self.max_weight = np.ones(len(self.p['decomp_params']))
            for i in range( len(self.p['opt_params']) ):
                ind = self.p['decomp_params'].index( self.p['opt_params'][i] )
                self.max_weight[ind] = self.p['max_weight'][i] 

        # default weight
        if isinstance(self.p['default_weight'],float):
            self.default_weight = np.empty(len(self.p['decomp_params']))
            self.default_weight.fill(self.p['default_weight'])
        else:
            # list or tuple
            self.default_weight = np.ones(len(self.p['decomp_params']))
            for i in range( len(self.p['opt_params']) ):
                ind = self.p['decomp_params'].index( self.p['opt_params'][i] )
                self.default_weight[ind] = self.p['default_weight'][i] 

        # opt_mask
        self.opt_mask = np.empty(len(self.p['decomp_params']),dtype=bool)
        opt_params = set(self.p['opt_params'])
        for i in range( len(self.p['decomp_params']) ):
            sp = self.p['decomp_params'][i]
            if sp in opt_params:
                self.opt_mask[i] = True
            else:
                self.opt_mask[i] = False

        # input data
        id_file = self.p['input_data_file']
        if id_file is not None and os.path.isfile(id_file):
            self.print_log('reading input data from %s'%id_file)
            f = open(id_file,'r')
            self.input_data = pickle.load(f)
            f.close()
        else:
            self.read_data_files()
            if id_file is not None and not os.path.isfile(id_file) and mpirank == 0:
                f = open(self.p['input_data_file'],'w')
                pickle.dump(self.input_data,f,2)
                f.close()

        # prepare minimization
        wu.prepare_minimization(self)

    def read_data_files(self):
        self.input_data = []
        for df in self.p['data_files']:
            if df.endswith(('.csv', '.csv.gz')):
                import pandas as pd
                try:
                    df_data = pd.read_csv(df)
                except:
                    if self.p['skip_data_file']:
                        self.print_log('WARN: cannot read %s'%df)
                        continue
                    else:
                        self.print_error('cannot read %s'%df)
                self.print_log('reading the data from %s'%df)

                score_data = df_data[self.p['score_name']].values
                sum_data = df_data[self.p['decomp_params']].values
                self.input_data.append( (df,score_data,sum_data) )
            else:
                try:
                    f = open(df,'r')
                except:
                    if self.p['skip_data_file']:
                        self.print_log('WARN: cannot read %s'%df)
                        continue
                    else:
                        self.print_error('cannot read %s'%df)

                self.print_log('reading the data from %s'%df)

                # header starts with '#' and list the column names
                param_names = f.readline()[1:].split()  # remove '#'
                f.close()

                # prepare index numbers
                try:
                    score_index = param_names.index(self.p['score_name'])
                except:
                    self.print_error("cannot find '%s' in %s"%(self.p['score_name'],df))
                sum_indexes = []
                for sp in self.p['decomp_params']:
                    try:
                        ind = param_names.index(sp)
                    except:
                        self.print_error("cannot find '%s' in %s"%(sp,df))
                    sum_indexes.append(ind)

                # read numbers
                sum_data = []
                raw_data = np.loadtxt(df,dtype='S')
                try:
                    score_data = raw_data[:,score_index].astype(float)
                    sum_data = raw_data[:,sum_indexes].astype(float)
                except:
                    self.print_log('WARN: %s has an error in the format. skipped'%df)
                    continue
                self.input_data.append( (df,score_data,sum_data) )

    def check_param(self):
        super(csa_weight,self).check_param()
        if len(self.p['decomp_params']) < 2:
            self.print_error('the length of decomp_params is less than 2')
        if self.p['score_name'] is None:
            self.print_error('score_name is not specified')
        if self.p['score_type'] == 5:
            if self.p['ref_scores'] is None:
                self.print_error('ref_score is not specified for score_type == 5')
            if len(self.p['ref_scores']) != len(self.p['data_files']):
                self.print_error('The size of ref_score does not match the size of data_files')

    def minimizef(self,var):
        mm = self.p['min_method']
        if mm == 'nelder-mead':
            score, minvar, nfe = wu.minimize_neldermead(var)
        else:
            x0 = np.log(var[self.opt_mask])
            N = len(x0)

            # param range
            param_range = [None]*len(self.max_weight)
            j = 0
            for i in range(len(self.max_weight)):
                if self.opt_mask[i]:
                    mw = log(self.max_weight[i])
                    param_range[j] = (-mw,mw)
                    j += 1
            param_range = param_range[:j]

            wu.set_global_var(var)
            opt = {'maxiter': self.p['min_maxiter']}
            if mm == 'L-BFGS-B' or mm == 'TNC' or mm == 'SLSQP': # 'SLSQP bounds' is not so reliable
                res = optimize.minimize(wu.eval_score_slice2, x0, method=mm, bounds=param_range, options=opt)
            else:
                res = optimize.minimize(wu.eval_score_slice2, x0, method=mm, options=opt)

            score = res.fun
            if np.isnan(score):
                score = BIG

            # change logarithmic x0 back to linear scale
            x0 = np.exp(res.x)
            nfe = res.nfev

            minvar = var.copy()
            minvar[self.opt_mask] = x0

        return score, minvar, nfe

    def comparef(self,var1,var2):
        distance = wu.weight_distance(var1,var2)
        return distance

    def randvarf(self):
        var = np.exp( ( np.random.random(len(self.p['decomp_params'])) * 2.0 - 1.0 ) * np.log(self.max_weight) )
        var[np.logical_not(self.opt_mask)] = 1.0
        return var

    def perturbvarf(self,ind1):
        nbank = len(self.bvar)
        newvars = []
        perturb_info = []

        # perturb_crossover
        p_param = self.p['perturb_crossover']
        for i in range(p_param['nperturb']):
            ( newvar, pinfo ) = self.perturb_crossover(ind1,'crossover',p_param['partner_mask'],src='bb',max_ratio=p_param['max_ratio'])
            newvars.append(newvar)
            perturb_info.append(pinfo)

        # perturb_crossover2
        p_param = self.p['perturb_crossover2']
        for i in range(p_param['nperturb']):
            iuse = self.count_unused_bank()
            iuse_cut = len(self.bvar)-len(self.seed)-p_param['n_new_bank_cut']
            if self.ncycle == 0 and iuse > iuse_cut:
                ( newvar, pinfo ) = self.perturb_crossover(ind1,'crossover2',p_param['partner_mask'],src='rr',max_ratio=p_param['max_ratio'])
                newvars.append(newvar)
                perturb_info.append(pinfo)
            else:
                ( newvar, pinfo ) = self.perturb_crossover(ind1,'crossover2',p_param['partner_mask'],src='br',max_ratio=p_param['max_ratio'])
                newvars.append(newvar)
                perturb_info.append(pinfo)

        # perturb_random
        p_param = self.p['perturb_random']
        for i in range(p_param['nperturb']):
            max_perturb_ratio = ( self.max_weight - 1.0 ) * p_param['perturb_ratio'] + 1.0
            ( newvar, pinfo ) = self.perturb_random(ind1,'random',max_perturb_ratio,p_param['nmin'],p_param['nmax'])
            newvars.append(newvar)
            perturb_info.append(pinfo)
       
        return newvars, perturb_info

    def write_bank(self,prefix,suffix,vars,scores=None):
        for i in range(len(vars)):
            var = vars[i]
            outfile = '%s%03d%s'%(prefix,i+1,suffix)
            self.print_weight(var,outfile)
        if ( scores ):
            imin = scores.index(min(scores))
            outfile = 'gmin%s'%suffix
            self.print_weight(vars[imin],outfile)

    def print_weight(self,var,outfile):
        decomp_params = self.p['decomp_params']
        f = open(outfile,'w')
        # weight
        f.write('# name opt_weight default_weight total_weight\n')
        for j in range(len(decomp_params)):
            total_weight = var[j] * self.default_weight[j]
            f.write('%s %f %f %f\n'%(decomp_params[j],var[j],self.default_weight[j],total_weight))
        f.write('\n')

        avg_sco, pen_sco, detail = wu.eval_score_detail(var)

        # detailed score
        f.write('# datafile avg min max pickone')
        if self.p['score_type'] == 5:
            f.write(' ref\n')
        else:
            f.write('\n')
        for i in range(len(detail)):
            d = detail[i]
            f.write('%s %f %f %f %f'%d)
            if self.p['score_type'] == 5:
                f.write(' %f\n'%self.p['ref_scores'][i])
            else:
                f.write('\n')
        f.write('\n')

        # total score
        f.write('avg_score %f\n'%avg_sco)
        f.write('penalty_score %14.7E\n'%pen_sco)
        f.write('total_score %f\n'%(avg_sco+pen_sco))

    def eval_update_final(self):
        super(csa_weight,self).eval_update_final()

        if ( mpirank == 0 ):
            # calculate acceptace ratio
            count = {} 
            accept = {}
            for i in range(len(self.update_mvar)):
                pname = self.perturb_info[i][0]
                status = self.update_mvar[i]
                if ( pname not in count ):
                    count[pname] = 0
                if ( pname not in accept ):
                    accept[pname] = 0
                if ( status > 0 ):
                    accept[pname] += 1
                count[pname] += 1
            # print acceptance ratio
            for pname in sorted( count.keys() ):
                if ( count[pname] == 0 ):
                    ratio = 0.0
                else:
                    ratio = float(accept[pname]) / float(count[pname])
                try:
                    self.accept_ratio[pname] = ratio
                except:
                    self.accept_ratio = {}
                    self.accept_ratio[pname] = ratio
                self.print_log( "acceptance ratio of %s is %f"%(pname,ratio) )
            # print detailed perturb info
            for i in range(len(self.update_bvar)):
                mi = self.update_bvar[i]
                if ( mi < 0 ): continue
                pname, src = self.perturb_info[mi]
                delta_score = self.bscore[i]-self.mscore[mi]
                line = 'new b%d: %s'%(i+1,pname)
                for s in src: 
                    line += ' %s(%.2f)'%s
                line += ' S(%.7E) dS(%.7E)'%(self.mscore[mi],delta_score)
                self.print_log(line)

    def perturbvar(self):
        super(csa_weight,self).perturbvar()

        if ( mpirank != 0 ): return

        if self.p['minimize_seed']:
            # add seed conformations to the perturbed conformation list
            for ind1 in self.seed:
                self.mvar.append(self.bvar[ind1].copy())
                self.perturb_info.append( ( 'seed', ('b%d'%(ind1+1),1.0) ) )

    def perturb_crossover(self,ind1,pname,partner_mask,src='bb',max_ratio=0.5):
        nbank = len(self.bvar)
        if src[0] == 'b':
            var1 = self.bvar[ind1]
        else:
            var1 = self.rvar[ind1]

        n_opt = np.count_nonzero(self.opt_mask)
        n_exchange = np.random.randint(1,max(2,n_opt*max_ratio))

        ap = get_available_partner(nbank,ind1,partner_mask)
        ind2 = np.random.choice(ap)
        if src[1] == 'b':
            var2 = self.bvar[ind2]
        else:
            var2 = self.bvar[ind2]

        exind = np.random.choice( np.where(self.opt_mask)[0], n_exchange, replace=False )
        newvar = var1.copy()
        newvar[exind] = var2[exind]

        pinfo = ( pname, ( ('%s%d'%(src[0],ind1+1),float(n_opt-n_exchange)/n_opt), ('%s%d'%(src[1],ind2+1),float(n_exchange)/n_opt) ) )

        return newvar, pinfo 

    def perturb_random(self,ind1,pname,max_ratio,nmin,nmax):
        var1 = self.bvar[ind1]
        max_weight = self.p['max_weight']

        n_opt = np.count_nonzero(self.opt_mask)
        n_exchange = np.random.randint(nmin,nmax+1)
        exind = np.random.choice( np.where(self.opt_mask)[0], n_exchange, replace=False )
        newvar = var1.copy()
        newvar[exind] = np.exp( np.log(newvar[exind]) + ( np.random.random(n_exchange) * 2.0 - 1.0 ) * np.log(max_ratio[exind]) )

        mask = newvar > self.max_weight 
        newvar[mask] = self.max_weight[mask]
        mask = newvar < 1.0 / self.max_weight
        newvar[mask] = 1.0 / self.max_weight[mask]

        pinfo = ( pname, ( ('b%d'%(ind1+1),float(n_opt-n_exchange)/n_opt), ) )

        return newvar, pinfo

    def unpack_min_output(self,min_output,i_list):
        for ind in range(len(i_list)):
            i = i_list[ind]
            mo = min_output[ind]
            score, minvar, nfe = mo
            self.mvar.append(minvar)
            self.mscore.append(score)
            try:
                # In case of randomization, perturb_info is absent.
                self.perturb_info.append( self.perturb_info_backup[i] )
            except:
                pass
            try:
                self.minimize_nfe += nfe
            except:
                self.minimize_nfe = nfe
            self.print_log("m%d score: %.5E"%(len(self.mvar),score), level=3)

    def restart_key_list(self):
        return super(csa_weight,self).restart_key_list() + [ 'minimize_nfe' ]

    def write_history_file_header(self):
        pre = str(self.p['profile_precision'])
        fmt = '%6s %3s %10s %4s %4s %' + pre + 's %' + pre + 's %15s %4s %4s\n' 
        self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_score','max_score','nfe','iuse','nbk'))

    def write_history_file(self):
        self.print_log( "Writing history", level=4 )
        pre0 = self.p['profile_precision']
        pre1 = pre0 - 7
        fmt_e = ' %' + str(pre0) + '.' + str(pre1) + 'E'
        fmt = '%6d %3d %10.4E %4d %4d' + fmt_e + fmt_e + ' %15d %4d %4d\n'
        self.history_fh.write(fmt
            %(self.niter,self.ncycle,self.dcut,self.minscore_ind+1,self.maxscore_ind+1,self.minscore,self.maxscore,self.minimize_nfe,self.count_unused_bank(),self.count_bank()))
        self.history_fh.flush()
