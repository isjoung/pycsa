"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: 

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

from builtins import range
import sys
import perturb_util as pu
import numpy as np

def read_backtor_file(backtor_file,cg=False):
    # read backtor_file
    try:
        f = open(backtor_file,'r')
    except:
        return None, None

    backtor = {}
    nfrag = None
    for line in f:
        c = line.split()
        if cg:
            nfrag2 = int((len(c)-3)/2) + 2
        else:
            nfrag2 = int((len(c)-3)/3)
        if nfrag is not None:
            if nfrag != nfrag2:
                print('ERROR: fragment length mismatch')
                print(line)
                sys.exit()
        nfrag = nfrag2
        resnum = int(c[0])+int(nfrag/2)
        if resnum not in backtor:
            backtor[resnum] = []
        bt_list = []
        for bt in c[2:]:
            try:
                bt_list.append( float(bt) )
            except:
                bt_list.append(-999.0) # dummy value
        backtor[resnum].append(bt_list)
    f.close()
    for key in list(backtor.keys()):
        arr = np.array( backtor[key] )
        # change the last column (corrcoef) to weight
        arr[:,-1] /= sum(arr[:,-1])
        backtor[key] = arr

    return backtor, nfrag

def perturb_residue(backtor,e_ind,xyzmap_data,resnum,iz1,ztors1,tor_new,res_source=None,nfrag=9,sigma_torsion=None,weighted_frag=False):
    if weighted_frag:
        info_ind = np.random.choice(len(weight),p=backtor[resnum][:,-1])
    else:
        info_ind = np.random.randint(backtor[resnum].shape[0])
    info = backtor[resnum][info_ind,:]
    n_ang = (info.size-1)/3 # last number is corrcoef weight
    if nfrag == n_ang:
        n_range = range(n_ang)
    elif nfrag < n_ang:
        i0 = int( (n_ang-nfrag) / 2 )
        if nfrag % 2 == 0:
            i1 = n_ang - i0 - 1
        else:
            i1 = n_ang - i0
        n_range = range(i0,i1)
    else:
        raise BaseException
    for iinfo in n_range:
        rn = resnum + iinfo - int(n_ang/2) # neighbor residue number
        if res_source is not None:
            res_source[rn-1] = ('tor',None)
        new_phi = info[iinfo*3]
        new_psi = info[iinfo*3+1]
        new_omega = info[iinfo*3+2]

        phi_ind = xyzmap_data[rn]['C'][0]-1
        if ( rn+1 in xyzmap_data ):
            psi_ind = xyzmap_data[rn+1]['N'][0]-1
            omega_ind = xyzmap_data[rn+1]['CA'][0]-1
        else:
            psi_ind = None
            omega_ind = None

        old_phi = ztors1[phi_ind]
        if new_phi == -999.0:
            new_phi = old_phi
        if sigma_torsion is not None:
            delta_phi = np.random.normal( new_phi - old_phi, sigma_torsion )
        else:
            delta_phi = new_phi - old_phi
        if psi_ind is not None:
            old_psi = ztors1[psi_ind]
            if ( new_psi == -999.0 ):
                new_psi = old_psi
            if sigma_torsion is not None:
                delta_psi = np.random.normal( new_psi - old_psi, sigma_torsion )
            else:
                delta_psi = new_psi - old_psi
        if omega_ind is not None:
            old_omega = ztors1[omega_ind]
            if ( new_omega == -999.0 ):
                new_omega = old_omega
            if sigma_torsion is not None:
                delta_omega = np.random.normal( new_omega - old_omega, sigma_torsion )
            else:
                delta_omega = new_omega - old_omega
        # phi
        pu.add_torsion(e_ind,phi_ind,delta_phi,iz1,tor_new)
        # psi
        if psi_ind is not None:
            pu.add_torsion(e_ind,psi_ind,delta_psi,iz1,tor_new)
        # omega
        if omega_ind is not None:
            pu.add_torsion(e_ind,omega_ind,delta_omega,iz1,tor_new)

def perturb_residue_cg(backtor,e_ind,xyzmap_data,resnum,iz1,ztors1,tor_new,zang1,ang_new,res_source=None,nfrag=9,sigma_torsion=None,sigma_angle=None,weighted_frag=False):
    if weighted_frag:
        info_ind = np.random.choice(len(weight),p=backtor[resnum][:,-1])
    else:
        info_ind = np.random.randint(backtor[resnum].shape[0])
    info = backtor[resnum][info_ind,:]
    n_ang = (info.size-1)/2 + 2 # last number is corrcoef weight
    if nfrag == n_ang:
        n_range = range(2,n_ang)
    elif nfrag < n_ang:
        i0 = int( (n_ang-nfrag) / 2 )
        if i0 < 2:
            i0 = 2
        if nfrag % 2 == 0:
            i1 = n_ang - i0 - 1
        else:
            i1 = n_ang - i0
        n_range = range(i0,i1)
    else:
        raise BaseException
    for iinfo in n_range:
        rn = resnum + iinfo - int(n_ang/2) # neighbor residue number
        if res_source is not None:
            res_source[rn-1] = ('tor',None)
        new_angle = info[(iinfo-2)*2]
        new_torsion = info[(iinfo-2)*2+1]

        ca3_ind = xyzmap_data[rn]['CA'][0]-1
        try:
            ca2_ind = xyzmap_data[rn-1]['CA'][0]-1
        except:
            continue
        try:
            ca1_ind = xyzmap_data[rn-2]['CA'][0]-1
        except:
            continue
        try:
            ca0_ind = xyzmap_data[rn-3]['CA'][0]-1
        except:
            continue

        # angle
        if sigma_angle is not None:
            ang_new[ca3_ind] = np.random.normal( new_angle, sigma_angle )
        else:
            ang_new[ca3_ind] = new_angle

        # torsion
        old_torsion = ztors1[ca3_ind]
        if new_torsion == -999.0:
            new_torsion = old_torsion
        if sigma_torsion is not None:
            delta_torsion = np.random.normal( new_torsion - old_torsion, sigma_torsion )
        else:
            delta_torsion = new_torsion - old_torsion
        pu.add_torsion(e_ind,ca3_ind,delta_torsion,iz1,tor_new)
