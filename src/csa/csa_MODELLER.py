"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import absolute_import

from builtins import range
from .csa_molecule import csa_molecule, mpirank
from .csa import dict_merge
from . import mol_util
from numpy import array, empty, zeros, finfo
from perturb_util import calc_rmsd
import os
import sys
from glob import glob
from copy import deepcopy
from multiprocessing import Process, Pipe

from modeller import *
from modeller.scripts import complete_pdb
from modeller.optimizers import *

log.level(output=0,notes=0,warnings=0,errors=1,memory=0)
env = environ(rand_seed=-1234)  # [-50000,-2]
env.libs.topology.read(file='$(LIB)/top_heav.lib') # read topology
env.libs.parameters.read(file='$(LIB)/par.lib') # read parameters

def read_pdb(pdbfile):
    f = open(pdbfile,'r')
    pdb = []
    for line in f:
        record = line[:6]
        if ( record != 'ATOM  ' ): continue
        serial = int( line[6:11] )
        atomname = line[12:16]
        altloc = line[16]
        resname = line[17:20]
        chain = line[21]
        resnum = int( line[22:26] )
        achar = line[26]
        x = float(line[30:38])
        y = float(line[38:46])
        z = float(line[46:54])
        try:
            occ = float( line[54:60] )
        except:
            occ = 1.0
        try:
            tfactor = float( line[60:66] )
        except:
            tfactor = 1.0
        try:
            segid = line[72:76]
        except:
            segid = ''
        try:
            elsymbol = line[76:78]
        except:
            elsymbol = ''
        try:
            charge = float(line[78:80])
        except:
            charge = 0.0
        # 0:  record
        # 1:  serial
        # 2:  atomname
        # 3:  altloc
        # 4:  resname
        # 5:  chain
        # 6:  resnum
        # 7:  achar
        # 8:  x
        # 9:  y
        # 10: z
        # 11: occ
        # 12: tfactor
        # 13: segid
        # 14: elsymbol
        # 15: charge
        pdb.append( [ record, serial, atomname, altloc, resname, chain, resnum, achar, x, y, z, occ, tfactor, segid, elsymbol, charge ] )
    f.close()
    return pdb

def read_native_pdb(pdbfile,amap):
    native_pdb = read_pdb(pdbfile)
    native_coord = {}
    for p in native_pdb:
        key=(p[6],p[2].strip())
        native_coord[key] = (p[8],p[9],p[10])

    natom = len(amap)
    var = empty((natom,3)) 
    var.fill(99999.999) # null value
    for i in range(len(amap)):
        atom_name = amap[i][2]
        res_num = amap[i][4]
        key = (res_num,atom_name)
        if key in native_coord:
            var[i,:] = native_coord[key]

    return var

def read_pdb_coord(pdbfile):
    pdb = read_pdb(pdbfile)
    coord = []
    for p in pdb:
        coord.append( (p[8],p[9],p[10]) )    

    return array(coord)

def read_connect(rsrfile):
    f = open(rsrfile,'r')
    connect_dict = {}
    for line in f:
        if line[0] == 'R' and line[12:14] == ' 1' and line[16:18] == ' 1':
            a1 = int(line[30:36])
            a2 = int(line[36:42])
            if a1 not in connect_dict:
                connect_dict[a1] = []
            connect_dict[a1].append(a2)
            if a2 not in connect_dict:
                connect_dict[a2] = []
            connect_dict[a2].append(a1)
    f.close()

    # convert dict to list
    connect = []
    for a in sorted(connect_dict):
        connect.append( connect_dict[a] )

    return connect

def set_coord(mdl,var):
    natm = mdl.natm
    for i in range(natm):
        a = mdl.atoms[i]
        a.x = var[i,0]
        a.y = var[i,1]
        a.z = var[i,2]

def get_coord(mdl):
    natm = mdl.natm
    var = []
    for i in range(natm):
        a = mdl.atoms[i]
        var.append( (a.x,a.y,a.z) )
    return array(var)

ecomp_terms = [
     physical.bond, physical.angle, physical.dihedral, physical.improper,
     physical.soft_sphere, physical.lennard_jones, physical.coulomb, physical.h_bond,
     physical.ca_distance, physical.n_o_distance, physical.phi_dihedral, physical.psi_dihedral,
     physical.omega_dihedral, physical.chi1_dihedral, physical.chi2_dihedral,
     physical.chi3_dihedral, physical.chi4_dihedral, physical.disulfide_distance,
     physical.disulfide_angle, physical.disulfide_dihedral, physical.lower_distance,
     physical.upper_distance, physical.sd_mn_distance, physical.chi5_dihedral,
     physical.phi_psi_dihedral, physical.sd_sd_distance, physical.xy_distance, 
     physical.nmr_distance, physical.nmr_distance2, physical.min_distance,
     physical.nonbond_spline, physical.accessibility, physical.density, physical.absposition,
     physical.dihedral_diff, physical.gbsa, physical.em_density, physical.saxs,
     physical.symmetry,
]

# index used in Template Based Modeling 
#TBMINDEX = [0,1,2,3,4,8,9,12,13,14,15,16,17,18,19,22,24,25]
TEMPLATE = set([8,9,22,25,17,18,19])    # CA-CA, N-O, SD-MN, SD-SD, Disulfide (distance, angle, dihedral)

def get_ecomp_name(rtype):
    name = [ 'total', 'template', 'nontemp' ]
    for i in rtype['all']:
        t = ecomp_terms[i]
        n = t._shortname
        if ( len(n) > 12 ):
            n = n[:12]
        name.append(n)
    return name

def get_ecomp(pdf,rtype):
    ecomp = []
    total_ene = 0.0
    for i in rtype['all']:
        t = ecomp_terms[i]
        e = pdf[t]
        total_ene += e
        ecomp.append(e)

    template_ene = 0.0
    for i in rtype['template']:
        t = ecomp_terms[i]
        template_ene += pdf[t]

    non_template_ene = 0.0
    for i in rtype['non_template']:
        t = ecomp_terms[i]
        non_template_ene += pdf[t]

    ret = [ total_ene, template_ene, non_template_ene ] + ecomp

    return ret

def get_restraint_type(rsrfile):
    f = open(rsrfile,'r')
    rest_type = set()
    for line in f:
        if line[0] == 'R':
            rest_type.add(int(line[14:18])-1)
    f.close()
    return tuple(sorted(rest_type))

def modeller_minimize(csa_mod,var,conn):
    set_coord(csa_mod.modeller_mdl,var)
    
    csa_mod.modeller_min.optimize(csa_mod.modeller_sel,max_iterations=csa_mod.p['min_maxiter'],output='NO_REPORT')
    minvar = get_coord(csa_mod.modeller_mdl)
    molpdf = csa_mod.modeller_sel.energy()
    min_energy = molpdf[0]
    pdf = molpdf[1]
    decomp_energy = get_ecomp(pdf,csa_mod.restraint_type)

    # return values are sent to the parent process
    conn.send( (min_energy, minvar, (csa_mod.decomp_energy_name,decomp_energy)) )
    conn.close()

class csa_MODELLER(csa_molecule):
    """csa for optimizing MODELLER molecules"""

    def minimizef(self,var,rinfo=None):
        # Process() is used to catch the error of Modeller
        parent_conn, child_conn = Pipe()
        p = Process(target=modeller_minimize,args=(self,var,child_conn))
        p.start()
        p.join()
        if p.exitcode < 0:
            # minimization failed
            minvar = deepcopy(var)
            min_energy = finfo('d').max
            return min_energy, minvar, (self.decomp_energy_name,None)
        else:
            # min_energy, minvar, (decomp_energy_name,decomp_energy)
            return parent_conn.recv()

    def __init__(self,param={}):
        p = {
            # MODELLER
            'pdbfile': 'model.pdb',
            'rsrfile': 'model.rsr',

            # minimization method
            'min_method': 'cg', # cg/newton
            'min_maxiter': 1000,

            # rbank
            'init_bank': None, # list of init bank pdbs

            'native_pdb': None,
        }

        self.p = dict_merge(p,param)
        self.initvar = read_pdb_coord(self.p['pdbfile'])

        # restraint type
        self.restraint_type = {}
        self.restraint_type['all'] = get_restraint_type(self.p['rsrfile'])
        self.restraint_type['non_template'] = []
        self.restraint_type['template'] = []
        for i in self.restraint_type['all']:
            if i in TEMPLATE:
                self.restraint_type['template'].append(i)
            else:
                self.restraint_type['non_template'].append(i)

        # init for csa parameter
        super(csa_MODELLER,self).__init__(self.p)

    def init_accessor(self):
        super(csa_MODELLER,self).init_accessor()

        # Initialize MODELLER
        self.modeller_mdl = complete_pdb(env,self.p['pdbfile'])
        self.modeller_mdl.restraints.append(file=self.p['rsrfile'])
        self.modeller_sel = selection(self.modeller_mdl)   # all atom selection
        self.modeller_rsr = self.modeller_mdl.restraints

        if self.p['min_method'] == 'newton':
            self.modeller_min = quasi_newton()
        else:
            self.modeller_min = conjugate_gradients()

        self.decomp_energy_name = get_ecomp_name(self.restraint_type)

    def read_native_var(self):
        # native pdb
        if ( self.p['native_pdb'] ):
            if isinstance(self.p['native_pdb'],list) or isinstance(self.p['native_pdb'],tuple):
                self.native_var = []
                for npdb in self.p['native_pdb']:
                    self.native_var.append( read_native_pdb(npdb,self.xyzmap['raw_data']) )
            else:
                self.native_var = [ read_native_pdb(self.p['native_pdb'],self.xyzmap['raw_data']) ]

    def read_xyzmap(self):
        pdbfile = self.p['pdbfile']
        rsrfile = self.p['rsrfile']

        connect = read_connect(rsrfile)
        natom = len(connect)

        Res31 ={'ALA':'A','CYS':'C','ASP':'D','GLU':'E','PHE':'F','GLY':'G',
                'HIS':'H','ILE':'I','LYS':'K','LEU':'L','MET':'M','ASN':'N',
                'PRO':'P','GLN':'Q','ARG':'R','SER':'S','THR':'T','VAL':'V',
                'TRP':'W','TYR':'Y','ASX':'N','GLX':'Q','UNK':'X','INI':'K',
                'AAR':'R','ACE':'X','ACY':'G','AEI':'T','AGM':'R','ASQ':'D',
                'AYA':'A','BHD':'D','CAS':'C','CAY':'C','CEA':'C','CGU':'E',
                'CME':'C','CMT':'C','CSB':'C','CSD':'C','CSE':'C','CSO':'C',
                'CSP':'C','CSS':'C','CSW':'C','CSX':'C','CXM':'M','CYG':'C',
                'CYM':'C','DOH':'D','EHP':'F','FME':'M','FTR':'W','GL3':'G',
                'H2P':'H','HIC':'H','HIP':'H','HTR':'W','HYP':'P','KCX':'K',
                'LLP':'K','LLY':'K','LYZ':'K','M3L':'K','MEN':'N','MGN':'Q',
                'MHO':'M','MHS':'H','MIS':'S','MLY':'K','MLZ':'K','MSE':'M',
                'NEP':'H','NPH':'C','OCS':'C','OCY':'C','OMT':'M','OPR':'R',
                'PAQ':'Y','PCA':'Q','PHD':'D','PRS':'P','PTH':'Y','PYX':'C',
                'SEP':'S','SMC':'C','SME':'M','SNC':'C','SNN':'D','SVA':'S',
                'TPO':'T','TPQ':'Y','TRF':'W','TRN':'W','TRO':'W','TYI':'Y',
                'TYN':'Y','TYQ':'Y','TYS':'Y','TYY':'Y','YOF':'Y','FOR':'X',
                '---':'-','PTR':'Y','LCX':'K','SEC':'D','MCL':'K','LDH':'K'}

        pdb = read_pdb(pdbfile)
        residues = {}
        for p in pdb:
            residues[p[6]] = Res31[p[4]]
        nres = len(residues)
        seq = ''
        for rn in sorted(residues):
            seq += residues[rn]

        # xyzmap dict
        # ['title']: title
        # ['nres']: nres
        # ['seq']: sequence
        # ['natom']: natom
        # ['data'][resnum][atomname]: list of columns
        # ['resrange'][resnum]: tuple of atom index range

        self.xyzmap = {}
        self.xyzmap['title'] = 'MODEL'
        self.xyzmap['nres'] = len(residues)
        self.xyzmap['seq'] = seq
        self.xyzmap['natom'] = len(pdb)
        self.xyzmap['connect'] = connect

        # read continuous molecules
        mol = { 1:1 }
        imol = 1
        size = len(connect)
        while ( len(mol) < size ):
            updated = True
            while ( updated ):
                updated = False
                for sn in range(1,natom+1):
                    if sn in mol: continue
                    conn = connect[sn-1]
                    for cn in conn:
                        if cn in mol:
                            mol[sn] = mol[cn]
                            updated = True
            if len(mol) < size:
                imol += 1
                for sn in range(1,natom+1):
                    if sn in mol: continue
                    mol[sn] = imol
                    break

        # read atom lines
        data = {}
        raw_data = []
        for i in range(len(pdb)):
            p = pdb[i]
            c = []
            c.append(i+1) # serial number
            c.append(i+1) # serial number in pdb file
            c.append(p[2].strip()) # atom name
            c.append(p[4].strip()) # res name
            c.append(p[6]) # res num
            c.append(0) # null
            c.append(0) # null
            c.append(mol[i+1])
            raw_data.append(c)
            if c[4] not in data:
                data[c[4]] = {}
            data[c[4]][c[2]] = c # residue number -> atom name = serial number of xyz
        self.xyzmap['data'] = data  
        self.xyzmap['raw_data'] = raw_data

        # resrange
        resrange = {}
        atom_index = 0
        for res in sorted(data):
            end_index = atom_index + len(data[res])
            resrange[res] = ( atom_index, end_index )
            atom_index = end_index 
        self.xyzmap['resrange'] = resrange

    def report_iterate(self):
        # save bank conformations
        if ( mpirank != 0 ): return
        if ( self.p['write_diff'] and self.niter % self.p['write_diff'] == 0 ):
            self.write_diff()
        if ( self.p['write_bank'] ):
            if ( self.p['write_pdb'] ):
                self.savepdb('b',self.bvar,self.bscore)
            if ( self.p['record_gmin'] and self.update_bvar[self.minscore_ind] >= 0 ):
                # gmin updated
                self.write_pdb('gmin%05d.pdb'%self.niter,self.bvar[self.minscore_ind],energy=self.minscore)
            if ( self.p['write_score_profile'] ):
                self.write_score_profile()
        if ( not self.history_fh ): return
        self.write_history_file()

    def write_history_file(self):
        self.print_log( "Writing history", level=4 )
        self.history_fh.write("%6d %3d %10.4E %4d %4d %14.7E %14.7E %4d %4d\n"
            %(self.niter,self.ncycle,self.dcut,self.minscore_ind+1,self.maxscore_ind+1,self.minscore,self.maxscore,self.count_unused_bank(),self.count_bank()))
        self.history_fh.flush()

    def write_history_file_header(self):
        self.history_fh.write("#niter cyc    cutdiff  imn  imx     min_energy     max_energy iuse  nbk\n")

    def read_archive_coord(self):
        varlist = []
        for pdbfile in self.p['init_bank']:
            coord = read_pdb_coord(pdbfile) 
            varlist.append(coord)
        return varlist

    def comparef(self,var1,var2):
        if ( self.p['dist_method'] == 'rmsd_ca' ):
            ca_index = self.get_CA_index()
            try:
                distance = calc_rmsd(var1[ca_index,:],var2[ca_index,:])[0]
            except:
                distance =99.9
        else:
            distance = super(csa_MODELLER,self).comparef(var1,var2)

        return float(distance)
        
