"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung, Sang Jun Park
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import absolute_import

from builtins import range
from .mol_perturb import mol_perturb, mol_perturb_int_cross, mol_perturb_int_residue, mol_perturb_torsion, mol_perturb_randint
from numpy import array, newaxis, random, sum, cross, arctan2, inner, linalg, pi, logical_and, sqrt
from .perturb_util import get_atom_index, exchange_coord_int, exchange_coord_car, get_available_partner, pick_partner, exchange_chain_coord
from .csa import dict_merge
from copy import deepcopy
from . import mol_util

class glycan_perturb_int_cross(mol_perturb_int_cross):
    pname = 'int_cross'
    def __init__(self,param,amap,glycanmap):
        p = {
            'nperturb': 1,
            'res_index': None,
            'nmax': None,
            'nmin': 1,
            'use_residue_weight':False,
        }
        self.p = dict_merge(p,param)
        super(glycan_perturb_int_cross,self).__init__(self.p,amap)

        nmax = self.p['nmax']
        nmin = self.p['nmin']
        res_index = self.residue_index
        res_index_size = res_index.size
        if ( nmax == None ): nmax = int( res_index_size * self.p['max_ratio'] )
        if ( nmin == None ): nmin = 1
        if ( nmax < nmin ): nmax = nmin
        self.nmax = nmax
        self.nmin = nmin

        conn = {}      #dict for connectivity
        for nstruc in range(1,len(glycanmap)+1):
            seq = glycanmap[nstruc]['Glycan Sequence']
            sugar = [0 for i in range(len(seq))]
            for line in seq:
                c = line.split()
                res_number = int(c[0])
                ind_tree = len([X for X in c if X == '-'])
                conn[res_number] = []
                if ind_tree == 0:
                   sugar[0] = res_number
                elif ind_tree > 0:
                   sugar[ind_tree] = res_number
                   conn[ sugar[ind_tree - 1] ].append(sugar[ind_tree])
                   conn[ sugar[ind_tree] ].append(sugar[ind_tree - 1])
        self.glycan_conn = conn

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,nmin=None,nmax=None,bank_dist=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
            nbank = len(rvar)
        else:
            var1 = bvar[ind1]
            nbank = len(bvar)
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        res_index = self.residue_index
        res_list  = res_index + 1
        nres = res_index.size
        conn = self.glycan_conn

        if ( nperturb == None ):
            nperturb = self.p['nperturb']

        nmax = self.nmax
        nmin = self.nmin
        newvars = []
        perturb_info = []
        ap = get_available_partner(nbank,ind1,self.p['partner_mask'])
        for i in range(nperturb):
            ind2 = pick_partner(ap,ind1,bank_dist=bank_dist)
            if ( src[1] == 'r' ):
                var2 = rvar[ind2]
            else:
                var2 = bvar[ind2]
            ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)
            nres_change = random.randint(nmin,nmax+1)
            exres = random.choice(res_list)
            exres_list = []
            exres_list.append(exres)

            while len(exres_list) < nres_change:
                neighbors = [self.glycan_conn[X] for X in exres_list]
                candidate = []
                for neighbor in neighbors:
                    candidate += [X for X in neighbor if X not in exres_list]

                if len(candidate) != 0:
                   exres = random.choice(candidate)
                else:
                   the_other = [X for X in res_list if X not in exres_list]
                   exres = random.choice(the_other)
                exres_list.append(exres)
            exres_list = array(exres_list)
                
            exresind = exres_list - 1
            exind = get_atom_index(exresind,self.xyzmap)
            res_source = [ [src[0],ind1] for x in range(self.xyzmap['nres']) ]
            for resnum in exres_list:
                resind = resnum - 1
                res_source[resind] = [src[1],ind2]
            # exchange
            tor_new, ang_new, bond_new = exchange_coord_int(exind,iz1,ztors1,zang1,zbond1,ztors2,zang2,zbond2)
            # save int
            mol_util.setint(iz1,bond_new,ang_new,tor_new)
            newvar = exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())
            newvars.append(newvar)
            perturb_info.append( (self.pname,res_source) )
        return newvars, perturb_info

class glycan_perturb_int_residue(mol_perturb_int_residue):
    pname = 'int_residue'
    pass

class glycan_perturb_torsion(mol_perturb_torsion):
    pname = 'torsion'
    pass
class glycan_perturb_randint(mol_perturb_randint):
    pname = 'randint'
