"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: Seung Hwan Hong, InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import absolute_import

from builtins import str
from builtins import range
from .csa_molecule import csa_molecule, mpirank
from .csa import dict_merge
from . import mol_util
from numpy import array, empty, zeros, finfo, ascontiguousarray, ones, dot
from perturb_util import calc_rmsd, rms_fit
import os
import sys
from glob import glob
import tempfile
import unres_minimize_py
import math
from copy import deepcopy

def read_pdb(pdbfile):
    f = open(pdbfile,'r')
    pdb = []
    for line in f:
        record = line[:6]
        if ( record != 'ATOM  ' ): continue
        serial = int( line[6:11] )
        atomname = line[12:16]
        altloc = line[16]
        resname = line[17:20]
        chain = line[21]
        resnum = int( line[22:26] )
        achar = line[26]
        x = float(line[30:38])
        y = float(line[38:46])
        z = float(line[46:54])
        try:
            occ = float( line[54:60] )
        except:
            occ = 1.0
        try:
            tfactor = float( line[60:66] )
        except:
            tfactor = 1.0
        try:
            segid = line[72:76]
        except:
            segid = ''
        try:
            elsymbol = line[76:78]
        except:
            elsymbol = ''
        try:
            charge = float(line[78:80])
        except:
            charge = 0.0
        # 0:  record
        # 1:  serial
        # 2:  atomname
        # 3:  altloc
        # 4:  resname
        # 5:  chain
        # 6:  resnum
        # 7:  achar
        # 8:  x
        # 9:  y
        # 10: z
        # 11: occ
        # 12: tfactor
        # 13: segid
        # 14: elsymbol
        # 15: charge
        pdb.append( [ record, serial, atomname, altloc, resname, chain, resnum, achar, x, y, z, occ, tfactor, segid, elsymbol, charge ] )
    f.close()
    return pdb

def read_native_pdb(pdbfile,amap):
    native_pdb = read_pdb(pdbfile)
    native_coord = {}
    for p in native_pdb:
        key=(p[6],p[2].strip())
        native_coord[key] = (p[8],p[9],p[10])

    natom = len(amap)
    var = empty((natom,3)) 
    var.fill(99999.999) # null value
    for i in range(len(amap)):
        atom_name = amap[i][2]
        res_num = amap[i][4]
        key = (res_num,atom_name)
        if ( key in native_coord ):
            var[i,:] = native_coord[key]

    return var

def read_pdb_coord(pdbfile):
    pdb = read_pdb(pdbfile)
    coord = []
    for p in pdb:
        coord.append( (p[8],p[9],p[10]) )    

    return array(coord)

def make_connect(pdb):
    connect = []
    natom = len(pdb)
    check_GLY = 0
    for p in pdb:
        temp = []
        if p[2]==" CA ":
            if p[6]!=1 :
                if check_GLY==0 :
                    temp = temp+[p[1]-2]
                else :
                    temp = temp+[p[1]-1]
            if p[6]!= natom :
                if p[4]!="GLY" and p[4]!="D  " : 
                    temp = temp + [p[1]+1,p[1]+2]
                    check_GLY = 0
                else : 
                    check_GLY = 1
                    temp = temp + [p[1]+1]
            else :
                if p[4]!="GLY" and p[4]!="D  ": 
                    temp = temp + [p[1]+1]
                    check_GLY = 1
        elif p[2]==" SC ":
            temp = temp+[p[1]-1]

        connect = connect + [temp]
    return connect

def read_unres_weight(filename):
    fp = open(filename,"r")
    decomp_energy_name = []
    weight = []
    while 1:
        line = fp.readline()
        if not line:
            break
        if line[0]=='\n':
            continue
 
        if line[0:4] =="NAME" :
            temp = line.split()
            model = temp[1]
        elif line[0:6] == "unres_":
            temp = line.split()
            decomp_energy_name += [temp[0][6:-7]]
            weight += [float(temp[1])]

    fp.close
    decomp_energy_name = ["total"]+decomp_energy_name

    return model,weight,decomp_energy_name

class csa_UNRES(csa_molecule):
    """csa for optimizing UNRES molecules"""

    def minimizef(self,var,rinfo=None):

        min_method=self.p['min_method']
        natom = len(var)
        if (self.super_native == True) :
            confor_new,min_energy,decomp_energy =unres_minimize_py.unres_minimize_sn(var,self.unres_spring_const,natom)
#            confor_new,min_energy,decomp_energy =unres_minimize_py.unres_minimize_sc(var,natom)

        elif (self.super_native == False) :
            if (min_method == 'sumsl') :
                confor_new,min_energy,decomp_energy =unres_minimize_py.unres_minimize_sumsl(var,natom)
            elif (min_method == 'lbfgsb') :
                confor_new,min_energy,decomp_energy =unres_minimize_py.unres_minimize(var,natom)


        if (min_energy >=100000.000 or math.isnan(min_energy) ) :
            minvar = var
            score=100000.000
        else :
            minvar = ascontiguousarray(confor_new)
            score = min_energy
        
        return min_energy, minvar, (self.decomp_energy_name,decomp_energy)

    def __init__(self,param={}):
        p = {
            # UNRES
            'pdbfile': 'model.pdb',

            # rbank
            'init_bank': None, # init bank dir

            'native_pdb': None,

            # tmscore cut
            'tmscore_cut': None,
            'min_method' : 'lbfgsb',

            'super_native': False ,
            'unres_spring_const': 10.0 ,

            'ref_str_start': 0 ,
            'ref_str_end': 0 ,
            'ref_str_num': 0 ,
        }

        model,unres_weight,self.decomp_energy_name= read_unres_weight("modelinfo.txt")
        unres_minimize_py.unres_py_init("native",unres_weight)

        self.p = dict_merge(p,param)
        self.initvar = read_pdb_coord(self.p['pdbfile'])

        self.super_native = (self.p['super_native'])
        if (self.super_native == True) :
            p2 = {'rand_conf_type':1, 'dist_method': 'rmsd_sc',}

            self.p = dict_merge(self.p,p2)
            pdbfile = self.p['native_pdb']
            self.native_pdb = read_pdb(pdbfile)
            self.unres_spring_const= self.p['unres_spring_const']


        # init for csa parameter
        super(csa_UNRES,self).__init__(self.p)

    def read_native_var(self):
        # native pdb
        if ( self.p['native_pdb'] ):
            if isinstance(self.p['native_pdb'],list) or isinstance(self.p['native_pdb'],tuple):
                self.native_var = []
                for npdb in self.p['native_pdb']:
                    self.native_var.append( read_native_pdb(npdb,self.xyzmap['raw_data']) )
            else:
                self.native_var = [ read_native_pdb(self.p['native_pdb'],self.xyzmap['raw_data']) ]

    def read_xyzmap(self):
        pdbfile = self.p['pdbfile']

        Res31 ={'ALA':'A','CYS':'C','ASP':'D','GLU':'E','PHE':'F','GLY':'G',
                'HIS':'H','ILE':'I','LYS':'K','LEU':'L','MET':'M','ASN':'N',
                'PRO':'P','GLN':'Q','ARG':'R','SER':'S','THR':'T','VAL':'V',
                'TRP':'W','TYR':'Y','ASX':'N','GLX':'Q','UNK':'X','INI':'K',
                'AAR':'R','ACE':'X','ACY':'G','AEI':'T','AGM':'R','ASQ':'D',
                'AYA':'A','BHD':'D','CAS':'C','CAY':'C','CEA':'C','CGU':'E',
                'CME':'C','CMT':'C','CSB':'C','CSD':'C','CSE':'C','CSO':'C',
                'CSP':'C','CSS':'C','CSW':'C','CSX':'C','CXM':'M','CYG':'C',
                'CYM':'C','DOH':'D','EHP':'F','FME':'M','FTR':'W','GL3':'G',
                'H2P':'H','HIC':'H','HIP':'H','HTR':'W','HYP':'P','KCX':'K',
                'LLP':'K','LLY':'K','LYZ':'K','M3L':'K','MEN':'N','MGN':'Q',
                'MHO':'M','MHS':'H','MIS':'S','MLY':'K','MLZ':'K','MSE':'M',
                'NEP':'H','NPH':'C','OCS':'C','OCY':'C','OMT':'M','OPR':'R',
                'PAQ':'Y','PCA':'Q','PHD':'D','PRS':'P','PTH':'Y','PYX':'C',
                'SEP':'S','SMC':'C','SME':'M','SNC':'C','SNN':'D','SVA':'S',
                'TPO':'T','TPQ':'Y','TRF':'W','TRN':'W','TRO':'W','TYI':'Y',
                'TYN':'Y','TYQ':'Y','TYS':'Y','TYY':'Y','YOF':'Y','FOR':'X',
                '---':'-','PTR':'Y','LCX':'K','SEC':'D','MCL':'K','LDH':'K',
                'D  ':'X'
        }

        pdb = read_pdb(pdbfile)
        connect = make_connect(pdb)
        natom = len(pdb)
        residues = {}
        for p in pdb:
            residues[p[6]] = Res31[p[4]]
        nres = len(residues)
        seq = ''
        for rn in sorted(residues):
            seq += residues[rn]

        # xyzmap dict
        # ['title']: title
        # ['nres']: nres
        # ['seq']: sequence
        # ['natom']: natom
        # ['data'][resnum][atomname]: list of columns
        # ['resrange'][resnum]: tuple of atom index range

        self.xyzmap = {}
        self.xyzmap['title'] = 'MODEL'
        self.xyzmap['nres'] = len(residues)
        self.xyzmap['seq'] = seq
        self.xyzmap['natom'] = natom
        self.xyzmap['connect'] = connect

        # read continuous molecules
        mol = { 1:1 }
        imol = 1
        size = len(connect)
        while ( len(mol) < size ):
            updated = True
            while ( updated ):
                updated = False
                for sn in range(1,natom+1):
                    if ( sn in mol ): continue
                    conn = connect[sn-1]
                    for cn in conn:
                        if ( cn in mol ):
                            mol[sn] = mol[cn]
                            updated = True
            if len(mol) < size:
                imol += 1
                for sn in range(1,natom+1):
                    if ( sn in mol ): continue
                    mol[sn] = imol
                    break

        # read atom lines
        data = {}
        raw_data = []
        for i in range(len(pdb)):
            p = pdb[i]
            c = []
            c.append(i+1) # serial number
            c.append(i+1) # serial number in pdb file
            c.append(p[2].strip()) # atom name
            c.append(p[4].strip()) # res name
            c.append(p[6]) # res num
            c.append(0) # null
            c.append(0) # null
            c.append(mol[i+1])
            raw_data.append(c)
            if ( c[4] not in data ):
                data[c[4]] = {}
            data[c[4]][c[2]] = c # residue number -> atom name = serial number of xyz
        self.xyzmap['data'] = data  
        self.xyzmap['raw_data'] = raw_data

        # resrange
        resrange = {}
        atom_index = 0
        for res in sorted(data):
            end_index = atom_index + len(data[res])
            resrange[res] = ( atom_index, end_index )
            atom_index = end_index 
        self.xyzmap['resrange'] = resrange

    def report_iterate(self):
        # save bank conformations
        if ( mpirank != 0 ): return
        if ( self.p['write_diff'] and self.niter % self.p['write_diff'] == 0 ):
            self.write_diff()
        if ( self.p['write_bank'] ):
            if ( self.p['write_pdb'] ):
                self.savepdb('b',self.bvar,self.bscore)
            if ( self.p['record_gmin'] and self.update_bvar[self.minscore_ind] >= 0 ):
                # gmin updated
                self.write_pdb('gmin%05d.pdb'%self.niter,self.bvar[self.minscore_ind],energy=self.minscore)
            if ( self.p['write_score_profile'] ):
                self.write_score_profile()
        if ( not self.history_fh ): return
        self.write_history_file()

    def write_history_file_header(self):
        pre = str(self.p['profile_precision'])
        fmt = '%6s %3s %10s %4s %4s %' + pre + 's %' + pre + 's %15s %4s %4s\n' 
        self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_score','max_score','ntrial','iuse','nbk'))

    def write_history_file(self):
        self.print_log( "Writing history", level=4 )
        pre0 = self.p['profile_precision']
        pre1 = pre0 - 7
        fmt_e = ' %' + str(pre0) + '.' + str(pre1) + 'E'
        fmt = '%6d %3d %10.4E %4d %4d' + fmt_e + fmt_e + ' %15d %4d %4d\n'
        self.history_fh.write(fmt
            %(self.niter,self.ncycle,self.dcut,self.minscore_ind+1,self.maxscore_ind+1,self.minscore,self.maxscore,self.ntrial,self.count_unused_bank(),self.count_bank()))
        self.history_fh.flush()

    def read_archive_coord(self):
        varlist = []
        for pdbfile in self.p['init_bank']:
            coord = read_pdb_coord(pdbfile) 
            varlist.append(coord)
        return varlist

    def get_SC_index(self):
        try:
            sc_index = self.sc_index
            return sc_index
        except:
            sc_index = []
            for ai in self.get_valid_atom_index():
                if self.xyzmap['raw_data'][ai][2] == 'SC':
                    sc_index.append(ai)
            self.sc_index = sc_index
        return sc_index


    def comparef(self,var1,var2):
        if ( self.p['dist_method'] == 'rmsd_ca' ):
            ca_index = self.get_CA_index()
            try:
                distance = calc_rmsd(var1[ca_index,:],var2[ca_index,:])[0]
            except:
                distance =99.9
        elif ( self.p['dist_method'] == 'rmsd_sc' ):
            sc_index = self.get_SC_index()
            try:
                distance = calc_rmsd(var1[sc_index,:],var2[sc_index,:])[0]
            except:
                distance =99.9

        else:
            distance = super(csa_UNRES,self).comparef(var1,var2)

        return float(distance)


    def perturbvar(self):
        super(csa_UNRES,self).perturbvar()
        self.fix_mvar_ca()


    def fix_mvar_ca(self):
        if ( self.p['super_native']==True ):
            fixind = []
            natom = self.xyzmap['natom']
            for j in range(0,natom):
                if (self.native_pdb[j][2]==" CA ") :
                    fixind.append(j)
            nonfix_mask = ones( self.xyzmap['natom'], dtype=bool )
            nonfix_mask[fixind] = False
            for i in range(len(self.mvar)):
                var1 = self.mvar[i]
                var2 = self.initvar
                # rmsd fit
                c_trans, U, ref_trans = rms_fit(var2[nonfix_mask,:],var1[nonfix_mask,:])
                newvar = dot( var1 - c_trans, U ) + ref_trans 
#                newvar[fixind,:] = var2[fixind,:]
                self.mvar[i] = newvar.copy()

    def write_score_profile(self):
        self.print_log( "writing energy profile" ) 
        try:
            e_name = self.decomp_energy_name
        except:
            e_name = self.decomp_bscore[0][0]
        f = open(self.p['profile_file'],'w')
        f.write('#bnk') 
        if ( self.p['native_pdb'] ):
            if ( self.p['bank_history_dist_method'] =='tmscore') :
                f.write(' TMscore')
        for i in range(len(e_name)):
            f.write(' %12s'%e_name[i])
        f.write('\n')
        for j in range(len(self.bvar)):
            f.write('%4d'%(j+1))
            if ( self.p['native_pdb'] ):
                if ( self.p['bank_history_dist_method'] =='tmscore') :
                    tmscore_native = self.calc_tmscore(self.bvar[j],self.native_var[0]) 
                    f.write(' %7.4f'%tmscore_native)
            for i in range(len(e_name)):
                try:
                    f.write(' %12.5E'%self.decomp_bscore[j][1][i])
                except:
                    f.write(' 000000000000')
            f.write('\n')
        f.close()

    def randvar(self,nconf=None,varlist=None):
        if ( nconf == None ):
            nconf = self.p['nbank'] + self.p['nbank_add']

        if ( varlist ):
            self.mvar = deepcopy(varlist)
        else:
            self.mvar = []

        istart=self.p['ref_str_start']
        iend=self.p['ref_str_end']
        tot_num=self.p['ref_str_num']
        
        for i in range(0,tot_num) :
            j = istart +i
            strj="%03d" % j
            pdbfile = "ref"+strj+".pdb"
            var = read_pdb_coord(pdbfile)
            self.mvar.append(var)

        for i in range(tot_num,nconf):
            var = self.randvarf()
            self.mvar.append(var)
        if (self.super_native == True) :
            self.fix_mvar_ca()
        self.ntrial += len(self.mvar)
        self.print_log("%d random conformations have been generated"%(nconf))
