"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors:

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import division
from __future__ import absolute_import
from builtins import str
from builtins import range
import sys
import os
# turn off some multithread algorithms
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['NUMEXPR_NUM_THREADS'] = '1'
os.environ['OMP_NUM_THREADS'] = '1'
os.environ['OPENMM_CPU_THREADS'] = '1' # should be set before openmm is loaded
from .csa_molecule import csa_molecule, mpicomm, mpisize, mpirank, MPI
from .csa import dict_merge
from . import mol_util
import numpy as np
from scipy import optimize
import re
import neb_util
import perturb_util as pu
from copy import deepcopy

from simtk import unit
try:
    import openmm as mm
    from openmm import app
except:
    from simtk.openmm import app
    from simtk import openmm as mm
import parmed

def read_connect(natom,topo):
    # prepare bond parameters
    connect = [ [] for x in range(natom) ]
    for atm1, atm2 in topo._bonds:
        ind1 = atm1.index
        ind2 = atm2.index
        connect[ind1].append(ind2+1)
        connect[ind2].append(ind1+1)
    return connect

class csa_OPENMM(csa_molecule):
    """csa for optimizing OPENMM molecules"""

    def __init__(self,param={}):
        p = {
            # topology / initial coordinates
            'topology': 'model.prmtop',
            'init_coord': 'model.inpcrd',

            # minimization method
            'min_method': 'openmm',  # openmm or lbfgsb
            'bank_min_maxiter': 1000,
            'rbank_min_maxiter': 1000,
            'md_maxiter': 0,
            'parallel_minimization': 3,

            'native_coord': None,
            'openmm_platform': None,
            'init_bank': None,

            # protein
            'protein_chiral_rest': False,
            'contact_rest': None,
            'contact_intra_only': False,

            # positional rest
            'positional_rest': False,
            'positional_weight': 1.0,

            'fix_residue': None,
            'fix_index': None,

            'softcore': False,

            # symmetry
            'symmetry_rest': False,
            'symmetry_cutoff': ( 2.0, 6.0, 0.0, 6.0 ),
            'symmetry_weight': ( 1.0, 1.0 ),
            'symmetry_contact_only': True,
            'symmetry_min': False, # symmetric minimization

            # nucleic acid
            'nucleicacid_chiral_rest': False,

            # dfire
            'dfire': False,
            'dfire_contact_cut': 6.0,

            # stap (side chain statistical potential)
            'stap': False,
            'stap_list': ('phipsi', 'phichi1', 'psichi1', 'chi1chi2'),
            'stap_weight': 1.0,

            'nonbonded_method': 'nocutoff',
            'nonbonded_cutoff': 1.0 * unit.nanometer,
            'implicit_solvent': 'GBn2',
            'charge': True,

            'pressure': None,

            # extra restraints
            'restrain': None,
            'restrain_weights': {},

            'verbose': 1,
            'log_caller': False,
        }

        self.p = dict_merge(p,param)
        self.verbose = self.p['verbose']

        # topology file
        if self.p['topology'].endswith('.prmtop'):
            # AMBER
            self.structure = app.AmberPrmtopFile(self.p['topology'])
            self.parmed_structure = parmed.load_file(self.p['topology'])
        else:
            raise ValueError('unknow topology %s'%self.p['topology'])

        # read_xyzmap should precede customized energies
        self.read_xyzmap()

        # add LJ potential if it is absent
        acoefs = self.structure._prmtop._raw_data['LENNARD_JONES_ACOEF']
        for i in range(len(acoefs)):
            acoef = float(acoefs[i])
            if acoef == 0.0:
                acoefs[i] = '1.00000000E+01'
        bcoefs = self.structure._prmtop._raw_data['LENNARD_JONES_BCOEF']
        for i in range(len(bcoefs)):
            bcoef = float(bcoefs[i])
            if bcoef == 0.0:
                bcoefs[i] = '5.00000000E+00'

        # turn off charge if necessary
        if not self.p['charge']:
            charges = self.structure._prmtop._raw_data['CHARGE']
            for i in range(len(charges)):
                charges[i] = 0.0

        # read the initial coordinates
        if self.p['init_coord'].endswith('.inpcrd'):
            # AMBER
            self.init_coord = app.AmberInpcrdFile(self.p['init_coord'])
        elif self.p['init_coord'].endswith('.pdb'):
            # pdb
            self.init_coord = app.PDBFile(self.p['init_coord'])
        else:
            raise ValueError('unknown init_coord %s'%self.p['init_coord'])
        self.initvar = np.array( self.init_coord.positions.value_in_unit(unit.angstrom) )

        # nonbonded cutoff
        nonbonded_cutoff = self.p['nonbonded_cutoff']
        if self.p['nonbonded_method'] == 'nocutoff':
            nonbonded_method = app.NoCutoff
        elif self.p['nonbonded_method'] == 'pme':
            nonbonded_method = app.PME
        elif self.p['nonbonded_method'] == 'cutoffnonperiodic':
            nonbonded_method = app.CutoffNonPeriodic
        else:
            self.print_error('Unknown nonbonded_method %s'%self.p['nonbonded_method'])
        self.nonbonded_method = nonbonded_method

        # implicit solvent
        if self.p['implicit_solvent'] == 'GBn2':
            implicit_solvent = app.GBn2
        elif self.p['implicit_solvent'] == 'OBC1':
            implicit_solvent = app.OBC1
        elif self.p['implicit_solvent'] == 'OBC2':
            implicit_solvent = app.OBC2
        elif self.p['implicit_solvent'] == 'HCT':
            implicit_solvent = app.HCT
        elif self.p['implicit_solvent'] is None:
            implicit_solvent = None
        else:
            self.print_error('Unknown implicit_solvent %s'%self.p['implicit_solvent'])

        # create an openmm system
        system = self.structure.createSystem(nonbondedMethod=nonbonded_method, nonbondedCutoff=nonbonded_cutoff,
            implicitSolvent=implicit_solvent, rigidWater=False, ewaldErrorTolerance=0.0005)

        # set force group
        self.force_group = {}
        for i in range(system.getNumForces()):
            force = system.getForce(i)
            m = re.search(r'OpenMM::(\S+)',str(force))
            name = m.group(1)
            if re.search(r'Bond',name):
                name = 'bond'
                grpnum = 0
            elif re.search(r'Angle',name):
                name = 'angle'
                grpnum = 1
            elif re.search(r'Torsion',name):
                name = 'torsion'
                grpnum = 2
            elif re.search(r'Nonbonded',name):
                name = 'nonbond'
                grpnum = 3
            elif re.search(r'GB',name):
                name = 'gb'
                grpnum = 4
            elif re.search(r'CMMotion',name):
                name = 'nonbond'
                grpnum = 3
            else:
                raise
            force.setForceGroup(grpnum)
            self.force_group[name] = grpnum

        # integrator
        integrator = mm.LangevinIntegrator(300*unit.kelvin, 1.0/unit.picosecond, 0.002*unit.picosecond)

        # barostat
        if self.p['pressure']:
            system.addForce(mm.MonteCarloBarostat(self.p['pressure']*unit.atmospheres, self.temperature*unit.kelvin, 25))

        # customized force
        self.add_extra_force(system)

        # system for softcore potential
        if not self.p['softcore']:
            self.system = system
        else:
            self.system = system
            self.system_softcore = deepcopy(system)
            # remove softcore from system
            fgrp = self.force_group['softnb']
            match = False
            for ind in range(self.system.getNumForces()):
                force = self.system.getForce(ind)
                if force.getForceGroup() == fgrp:
                    match = True
                    break
            if not match:
                raise
            self.system.removeForce(ind)
            del self.force_group['softnb']
            # remove nonbond from system_softcore
            fgrp = self.force_group['nonbond']
            while match:
                match = False
                for ind in range(self.system_softcore.getNumForces()):
                    force = self.system_softcore.getForce(ind)
                    if force.getForceGroup() == fgrp:
                        match = True
                        break
                if match:
                    self.system_softcore.removeForce(ind)

        # openmm platform
        if self.p['openmm_platform'] is None:
            self.simulation = app.Simulation(self.structure.topology, self.system, deepcopy(integrator))
            if self.p['softcore']:
                self.simulation_softcore = app.Simulation(self.structure.topology, self.system_softcore, deepcopy(integrator))
        elif self.p['openmm_platform'] == 'CPU':
            platform = mm.Platform.getPlatformByName(self.p['openmm_platform'])
            self.simulation = app.Simulation(self.structure.topology, self.system, deepcopy(integrator), platform)
            if self.p['softcore']:
                self.simulation_softcore = app.Simulation(self.structure.topology, self.system_softcore, deepcopy(integrator), platform)
            self.print_log('openmm platform: %s'%platform.getName(),rank=mpirank)
        elif self.p['openmm_platform'] == 'OpenCL':
            if self.p['parallel_minimization'] == 2 or mpisize == 1:
                platform = mm.Platform.getPlatformByName(self.p['openmm_platform'])
                self.simulation = app.Simulation(self.structure.topology, self.system, deepcopy(integrator),
                        platform, {'OpenCLDeviceIndex': str(mpirank)})
                if self.p['softcore']:
                    self.simulation_sotcore = app.Simulation(self.structure.topology, self.system_softcore, deepcopy(integrator),
                            platform, {'OpenCLDeviceIndex': str(mpirank)})
            else:
                if mpirank == 0:
                    platform = mm.Platform.getPlatformByName('CPU')
                    self.simulation = app.Simulation(self.structure.topology, self.system, deepcopy(integrator), platform)
                    if self.p['softcore']:
                        self.simulation_softcore = app.Simulation(self.structure.topology, self.system_softcore,
                                deepcopy(integrator), platform)
                else:
                    platform = mm.Platform.getPlatformByName(self.p['openmm_platform'])
                    self.simulation = app.Simulation(self.structure.topology, self.system,
                            deepcopy(integrator), platform, {'OpenCLDeviceIndex': str(mpirank-1)})
                    if self.p['softcore']:
                        self.simulation_softcore = app.Simulation(self.structure.topology, self.system_softcore,
                                deepcopy(integrator), platform, {'OpenCLDeviceIndex': str(mpirank-1)})
            self.print_log('openmm platform: %s'%platform.getName(),rank=mpirank)
        elif self.p['openmm_platform'] == 'CUDA':
            try:
                visible_dev = os.environ['CUDA_VISIBLE_DEVICES'].split(',')
            except:
                visible_dev = list(range(mpisize))
            n_visible_dev = len(visible_dev)
            if self.p['parallel_minimization'] == 2 or mpisize == 1:
                platform = mm.Platform.getPlatformByName(self.p['openmm_platform'])
                self.simulation = app.Simulation(self.structure.topology, self.system, deepcopy(integrator),
                        platform, {'CudaDeviceIndex': str(mpirank%n_visible_dev), 'CudaPrecision': 'mixed' })
                if self.p['softcore']:
                    self.simulation_softcore = app.Simulation(self.structure.topology, self.system_softcore, deepcopy(integrator),
                            platform, {'CudaDeviceIndex': str(mpirank%n_visible_dev), 'CudaPrecision': 'mixed' })
            else:
                if mpirank == 0:
                    platform = mm.Platform.getPlatformByName('CPU')
                    self.simulation = app.Simulation(self.structure.topology, self.system,
                            deepcopy(integrator), platform)
                    if self.p['softcore']:
                        self.simulation_softcore = app.Simulation(self.structure.topology, self.system_softcore,
                                deepcopy(integrator), platform)
                else:
                    platform = mm.Platform.getPlatformByName(self.p['openmm_platform'])
                    self.simulation = app.Simulation(self.structure.topology, self.system,
                            deepcopy(integrator), platform, {'CudaDeviceIndex': str(mpirank%n_visible_dev)})
                    if self.p['softcore']:
                        self.simulation_softcore = app.Simulation(self.structure.topology, self.system_softcore,
                                deepcopy(integrator), platform, {'CudaDeviceIndex': str(mpirank%n_visible_dev)})
            self.print_log('openmm platform: %s'%platform.getName(),rank=mpirank)
        else:
            raise ValueError('unknown openmm platform %s'%self.p['openmm_platform'])

        self.context = self.simulation.context
        if self.p['softcore']:
            self.context_original = self.simulation.context
            self.context_softcore = self.simulation_softcore.context
            self.context = self.context_original
        self.context.setPositions(self.init_coord.positions)
        dene_dict = self.energy_decomposition(self.context)
        self.decomp_energy_name = sorted( dene_dict.keys() )
        self.decomp_energy_name.remove('total')
        self.decomp_energy_name.insert(0,'total')

        # init for csa parameter
        super(csa_OPENMM,self).__init__(self.p)

        # min_method
        if self.p['min_method'] == 'lbfgsb' or self.p['neb']:
            # scipy's L-BFGS-B
            natom = self.xyzmap['natom']
            if self.p['neb']:
                def energy_function(input):
                    # neb_energy_force modifies 'input'
                    # Numpy's reshape preserves the reference memory space and only produces a pointer to the memory space
                    input = input.reshape((self.p['neb_nseq'],natom,3))
                    try:
                        total_ene, total_frc, _ = neb_util.neb_energy_force(self,input)
                    except:
                        total_ene = np.nan
                        total_frc = np.zeros(input.shape,dtype=np.double)
                    try:
                        self.ncalls += 1
                    except:
                        pass
                    return total_ene, -total_frc.ravel()
            else:
                if self.p['symmetry_min']:
                    self.find_molecule_unit()
                    def energy_function_symmetry(xyz):
                        var = xyz.reshape(-1,3)
                        self.enforce_symmetry(var)
                        ene, frc = self.get_energy_force(var)
                        try:
                            self.ncalls += 1
                        except:
                            pass
                        return ene, -frc.ravel()
                    self.energy_function_symmetry = energy_function_symmetry
                def energy_function(xyz):
                    var = xyz.reshape(-1,3)
                    ene, frc = self.get_energy_force(var)
                    try:
                        self.ncalls += 1
                    except:
                        pass
                    return ene, -frc.ravel()
            self.energy_function = energy_function

    def energy_decomposition(self,context):
        energies = {}
        for name, grp in list(self.force_group.items()):
            energies[name] = context.getState(getEnergy=True, groups={grp}).getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)
        energies['total'] = context.getState(getEnergy=True).getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)
        return energies

    def enforce_symmetry(self,var):
        unitsize = self.mol_unit_size
        nunit = self.n_mol_unit

        # calculate the average structure
        v0 = var[:unitsize,:]
        vavg = v0.copy()
        count = 1
        for i in range(1,nunit):
            v = var[unitsize*i:unitsize*(i+1),:]
            try:
                rmsd, vnew = pu.calc_rmsd(v0,v)
                count += 1
            except:
                self.print_log('WARNING: symmetry fit fail for unit %d'%i)
                continue
            vavg += vnew
        vavg /= count

        # refit
        for i in range(nunit):
            v = var[unitsize*i:unitsize*(i+1),:]
            try:
                rmsd, vnew = pu.calc_rmsd(v,vavg)
                var[unitsize*i:unitsize*(i+1),:] = vnew
            except:
                self.print_log('WARNING: symmetry refit fail for unit %d'%i)

    def set_force_group(self,fgrp_name):
        # if the name already exists
        fgrp_name = fgrp_name.lower()
        try:
            return self.force_group[fgrp_name]
        except:
            pass

        # determine a new force group number
        fgroup_num = max(self.force_group.values())+1

        self.force_group[fgrp_name] = fgroup_num
        return fgroup_num

    def add_extra_force(self,system):
        if self.p['protein_chiral_rest']:
            self.add_protein_chiral_rest_force(system)

        if self.p['nucleicacid_chiral_rest']:
            self.add_nucleicacid_chiral_rest_force(system)

        if self.p['dfire'] == 1:
            self.add_dfire_force(system)
        elif self.p['dfire'] == 2:
            self.add_dfire_force(system)

        if self.p['stap']:
            self.add_stap_force(system)

        if self.p['contact_rest']:
            self.add_contact_force(system)

        if self.p['symmetry_rest']:
            self.add_symmetry_force(system)

        if self.p['positional_rest']:
            self.add_positional_force(system)

        if self.p['softcore']:
            self.add_softcore_nonbonded_force(system)

        if self.p['restrain']:
            self.add_restrain_force(system)

    def add_restrain_force(self,system):
        if isinstance(self.p['restrain'],str):
            lines = open(self.p['restrain'],'r')
        else: # container
            lines = []
            for rfile in self.p['restrain']:
                lines += open(rfile,'r').readlines()
        forces = {}
        tab_dist_funcs = {}
        for line in lines:
            line = line.strip()
            if line == '' or line[0] == '#':
                continue
            c = line.split()
            if len(c) < 3:
                continue
            c[0] = c[0].upper()
            #NOTE: energy: kcal/mol, distance: A
            if c[0] == 'DISTANCE':
                # Flat-bottomed harmonic distance restraint
                # DISTANCE grpname atm1 atm2 force_const lower_d [ upper_d ]
                grpname = c[1]
                atm1 = int(c[2])-1
                atm2 = int(c[3])-1
                fconst = ( float(c[4]) * unit.kilocalorie_per_mole / unit.angstrom**2 ).value_in_unit(unit.kilojoule_per_mole / unit.nanometer**2)
                try:
                    weight = self.p['restrain_weights'][grpname]
                    fconst *= weight
                except:
                    pass
                lower_d = float(c[5]) / 10.0 # nanometer
                try:
                    upper_d = float(c[6]) / 10.0 # nanometer
                except:
                    upper_d = lower_d
                assert( upper_d >= lower_d )
                fkey = ('DISTANCE',grpname)
                if fkey in forces:
                    force = forces[fkey]
                else:
                    force = mm.CustomBondForce('k*( step(-dr0)*dr0^2 + step(dr1)*dr1^2 );dr1=r-r1;dr0=r-r0')
                    forces[fkey] = force
                    force.addPerBondParameter('k')
                    force.addPerBondParameter('r0')
                    force.addPerBondParameter('r1')
                    grp = self.set_force_group(grpname)
                    force.setForceGroup(grp)
                force.addBond(atm1,atm2,(fconst,lower_d,upper_d))
            elif c[0] in ('LORENTZ', 'DISTANCE_LORENTZ'):
                # Flat-bottomed Lorentzian distance restraint
                # LORENTZ grpname atm1 atm2 fconst sigma lower_d [ upper_d ]
                grpname = c[1]
                atm1 = int(c[2])-1
                atm2 = int(c[3])-1
                fconst = ( float(c[4]) * unit.kilocalorie_per_mole ).value_in_unit( unit.kilojoule_per_mole )
                try:
                    weight = self.p['restrain_weights'][grpname]
                    fconst *= weight
                except:
                    pass
                sigma = float(c[5]) / 10.0 # nanometer
                lower_d = float(c[6]) / 10.0 # nanometer
                try:
                    upper_d = float(c[7]) / 10.0 # nanometer
                except:
                    upper_d = lower_d
                assert( upper_d >= lower_d )
                fkey = ('LORENTZ',grpname)
                if fkey in forces:
                    force = forces[fkey]
                else:
                    force = mm.CustomBondForce('k*( step(-dr0)*(dr02/(dr02+sigma2)) + step(dr1)*(dr12/(dr12+sigma2)) );dr02=dr0^2;dr12=dr1^2;dr0=r-r0;dr1=r-r1')
                    forces[fkey] = force
                    force.addPerBondParameter('k')
                    force.addPerBondParameter('sigma2')
                    force.addPerBondParameter('r0')
                    force.addPerBondParameter('r1')
                    grp = self.set_force_group(grpname)
                    force.setForceGroup(grp)
                force.addBond(atm1,atm2,(fconst, sigma**2, lower_d,upper_d))
            elif c[0] == 'TABULAR_DISTANCE':
                # TABULAR_DISTANCE grpname xmin xmax yval1 yval2 yval3 ...
                grpname = c[1]
                atm1 = int(c[2])-1
                atm2 = int(c[3])-1
                xmin = float(c[4]) * 0.1 # x min (A -> nm)
                xmax = float(c[5]) * 0.1 # x max (A -> nm)
                yvals = ( np.array(list(map(float,c[6:]))) * unit.kilocalorie_per_mole ).value_in_unit(unit.kilojoule_per_mole)
                try:
                    weight = self.p['restrain_weights'][grpname]
                    yvals *= weight
                except:
                    pass
                fkey = ('TABULAR_DISTANCE',grpname)
                self.set_force_group(grpname)
                if fkey in tab_dist_funcs:
                    tab_func = tab_dist_funcs[fkey]
                else:
                    tab_dist_funcs[fkey] = { 'xmin': xmin, 'xmax': xmax,
                            'yvals': [], 'atom_pairs': [] }
                    tab_func = tab_dist_funcs[fkey]
                if tab_func['xmin'] != xmin:
                    self.print_error("'xmin' does not match in the restraint file")
                    self.print_error(f'LINE: {line.rstrip()}')
                if tab_func['xmax'] != xmax:
                    self.print_error("'xmax' does not match in the restraint file")
                    self.print_error(f'LINE: {line.rstrip()}')
                if len(tab_func['yvals']) and len(yvals) != len(tab_func['yvals'][0]):
                    self.print_error("'yvals' size does not match in the restraint file")
                    self.print_error(f'LINE: {line.rstrip()}')
                tab_func['atom_pairs'].append((atm1,atm2))
                tab_func['yvals'].append(yvals)
            elif c[0] == 'ANGLE':
                # Flat-bottomed harmonic angle restraint
                # ANGLE grpname atm1 atm2 atm3 force_const lower_theta [ upper_theta ]
                grpname = c[1]
                atm1 = int(c[2])-1
                atm2 = int(c[3])-1
                atm3 = int(c[4])-1
                fconst = ( float(c[5]) * unit.kilocalorie_per_mole ).value_in_unit(unit.kilojoule_per_mole)
                try:
                    weight = self.p['restrain_weights'][grpname]
                    fconst *= weight
                except:
                    pass
                lower_theta = float(c[6]) * np.pi / 180.0 # radian
                try:
                    upper_theta = float(c[7]) * np.pi / 180.0 # radian
                except:
                    upper_theta = lower_theta
                assert( lower_theta >= 0.0 and lower_theta <= np.pi )
                assert( upper_theta >= 0.0 and upper_theta <= np.pi )
                assert( upper_theta >= lower_theta )
                fkey = ('ANGLE',grpname)
                if fkey in forces:
                    force = forces[fkey]
                else:
                    force = mm.CustomAngleForce('k*( step(-dtheta0)*dtheta0^2 + step(dtheta1)*dtheta1^2 );dtheta1=theta-theta1;dtheta0=theta-theta0')
                    forces[fkey] = force
                    force.addPerAngleParameter('k')
                    force.addPerAngleParameter('theta0')
                    force.addPerAngleParameter('theta1')
                    grp = self.set_force_group(grpname)
                    force.setForceGroup(grp)
                force.addAngle(atm1,atm2,atm3,(fconst,lower_theta,upper_theta))
            elif c[0] == 'TORSION':
                # Flat-bottomed harmonic torsion angle restraint
                # TORSION grpname atm1 atm2 atm3 atm4 force_const lower_theta [ upper_theta ]
                grpname = c[1]
                atm1 = int(c[2])-1
                atm2 = int(c[3])-1
                atm3 = int(c[4])-1
                atm4 = int(c[5])-1
                fconst = ( float(c[6]) * unit.kilocalorie_per_mole ).value_in_unit(unit.kilojoule_per_mole)
                try:
                    weight = self.p['restrain_weights'][grpname]
                    fconst *= weight
                except:
                    pass
                lower_theta = float(c[7]) * np.pi / 180.0 # radian
                try:
                    upper_theta = float(c[8]) * np.pi / 180.0 # radian
                except:
                    upper_theta = lower_theta
                lower_theta = standard_radian(lower_theta)
                upper_theta = standard_radian(upper_theta)

                fkey = ('TORSION',grpname)
                if fkey in forces:
                    force = forces[fkey]
                else:
                    twopi = 2.0 * np.pi
                    force = mm.CustomTorsionForce(
                        'k*s*dtheta2;'
                        's = step(sign0 * sign1 * (theta1-theta0));'
                        'sign0=theta-theta0;'
                        'sign1=theta-theta1;'
                        'dtheta2=dtheta*dtheta;'
                        'dtheta = min(dtheta_low, dtheta_high);'
                        'dtheta_low = min('
                        '   abs(theta0-theta),'
                        f'   abs(theta0-(theta-{twopi}))'
                        ');'
                        'dtheta_high = min('
                        '   abs(theta-theta1),'
                        f'   abs((theta+{twopi})-theta1)'
                        ')'
                    )
                    forces[fkey] = force
                    force.addPerTorsionParameter('k')
                    force.addPerTorsionParameter('theta0')
                    force.addPerTorsionParameter('theta1')
                    grp = self.set_force_group(grpname)
                    force.setForceGroup(grp)
                force.addTorsion(atm1, atm2, atm3, atm4,
                        (fconst, lower_theta, upper_theta))
            elif c[0] == 'TORSION_LORENTZ':
                # Flat-bottomed lorentzian torsion angle restraint
                # TORSION_LORENTZ grpname atm1 atm2 atm3 amt4 fconst
                #         sigma_degree lower_degree [ upper_degree ]
                grpname = c[1]
                atm1 = int(c[2])-1
                atm2 = int(c[3])-1
                atm3 = int(c[4])-1
                atm4 = int(c[5])-1
                fconst = ( float(c[6]) * unit.kilocalorie_per_mole
                        ).value_in_unit( unit.kilojoule_per_mole )
                try:
                    weight = self.p['restrain_weights'][grpname]
                    fconst *= weight
                except:
                    pass
                sigma = float(c[7]) * np.pi / 180.0 # radian
                lower_theta = float(c[8]) * np.pi / 180.0 # radian
                try:
                    upper_theta = float(c[9]) * np.pi / 180.0 # radian
                except:
                    upper_theta = lower_theta
                lower_theta = standard_radian(lower_theta)
                upper_theta = standard_radian(upper_theta)
                fkey = ('TORSION_LORENTZ',grpname)
                if fkey in forces:
                    force = forces[fkey]
                else:
                    twopi = 2.0 * np.pi
                    force = mm.CustomTorsionForce(
                        'k*s*(dtheta2/(dtheta2+sigma2));'
                        's = step(sign0 * sign1 * (theta1-theta0));'
                        'sign0=theta-theta0;'
                        'sign1=theta-theta1;'
                        'dtheta2=dtheta*dtheta;'
                        'dtheta = min(dtheta_low, dtheta_high);'
                        'dtheta_low = min('
                        '   abs(theta0-theta),'
                        f'   abs(theta0-(theta-{twopi}))'
                        ');'
                        'dtheta_high = min('
                        '   abs(theta-theta1),'
                        f'   abs((theta+{twopi})-theta1)'
                        ')'
                    )
                    forces[fkey] = force
                    force.addPerTorsionParameter('k')
                    force.addPerTorsionParameter('sigma2')
                    force.addPerTorsionParameter('theta0')
                    force.addPerTorsionParameter('theta1')
                    grp = self.set_force_group(grpname)
                    force.setForceGroup(grp)
                force.addTorsion(atm1, atm2, atm3, atm4,
                        (fconst, sigma**2, lower_theta, upper_theta))
            elif c[0] == 'SYMMETRY_DISTANCE':
                # symmetry distance restraint
                # SYMMETRY_DISTANCE grpname atm1 atm2 atm3 atm4 fconst
                grpname = c[1]
                atm1 = int(c[2])-1
                atm2 = int(c[3])-1
                atm3 = int(c[4])-1
                atm4 = int(c[5])-1
                fconst = ( float(c[6]) * unit.kilocalorie_per_mole /
                        unit.angstrom**2 ).value_in_unit(
                                unit.kilojoule_per_mole / unit.nanometer**2 )
                try:
                    weight = self.p['restrain_weights'][grpname]
                    fconst *= weight
                except:
                    pass
                fkey = ('SYMMTRY_DISTANCE',grpname)
                if fkey in forces:
                    force = forces[fkey]
                else:
                    force = mm.CustomCompoundBondForce(4,
                            'k*dd2;dd2=(distance(p1,p2) - distance(p3,p4))^2')
                    forces[fkey] = force
                    force.addPerBondParameter('k')
                    grp = self.set_force_group(grpname)
                    force.setForceGroup(grp)
                force.addBond((atm1, atm2, atm3, atm4), [fconst])
            else:
                raise ValueError('unknown restraint %s'%c[0])

        # add forces into system
        for force in forces.values():
            system.addForce(force)

        # tabular distance function
        for i, fkey in enumerate(tab_dist_funcs):
            if fkey[0] == 'TABULAR_DISTANCE':
                grpname = fkey[1]
                tab = tab_dist_funcs[fkey]
                xmin = tab['xmin']
                xmax = tab['xmax']
                ytable = np.array(tab['yvals'])
                atom_pairs = tab['atom_pairs']
                grp = self.set_force_group(grpname)

                ntype, npoint = ytable.shape

                tab_function = mm.Continuous2DFunction(ntype, npoint,
                        ytable.T.ravel(), 0.0, float(ntype-1), xmin, xmax)

                fname = f'dist_2dfunc{i}'
                force = mm.CustomCompoundBondForce(2,
                        f'{fname}(itype,d);'
                        f'd=min({xmax},max({xmin},distance(p1,p2)));'
                )
                force.addPerBondParameter('itype')
                force.addTabulatedFunction(fname, tab_function)
                force.setForceGroup(grp)
                for itype in range(ntype):
                    atom_pair = atom_pairs[itype]
                    force.addBond(atom_pair, [float(itype)])
                system.addForce(force)

    def add_softcore_nonbonded_force(self,system):
        fgroup_num = self.set_force_group('softnb')
        forces = system.getForces()
        for ind in range(len(forces)):
            if isinstance(forces[ind],mm.NonbondedForce):
                break
        nonbfrc = forces[ind]

        scalpha = 0.1
        # softcore LJ-potential
        if self.p['charge']:
            force = mm.CustomNonbondedForce('eps1*eps2*(sigr6^2-sigr6)+chg1*chg2/rmod;'
                                            'sigr6=sigr2*sigr2*sigr2;'
                                            'sigr2=((sig1+sig2)/rmod)^2;'
                                            'rmod=scalpha+r;')
            force.addGlobalParameter('scalpha',scalpha)
            force.addPerParticleParameter('eps')
            force.addPerParticleParameter('sig')
            force.addPerParticleParameter('chg')
        else:
            force = mm.CustomNonbondedForce('eps1*eps2*(sigr6^2-sigr6);'
                                            'sigr6=sigr2*sigr2*sigr2;'
                                            'sigr2=((sig1+sig2)/rmod)^2;'
                                            'rmod=scalpha+r;')
            force.addGlobalParameter('scalpha',scalpha)
            force.addPerParticleParameter('eps')
            force.addPerParticleParameter('sig')
        force.setForceGroup(fgroup_num)
        force.setCutoffDistance(nonbfrc.getCutoffDistance())
        force.setNonbondedMethod(nonbfrc.getNonbondedMethod())

        # bonded force for exceptions
        # softcore LJ-potential
        if self.p['charge']:
            bforce = mm.CustomBondForce('eps*(sigr6^2-sigr6)+chg/rmod;'
                                        'sigr6=sigr2*sigr2*sigr2;'
                                        'sigr2=(sig/rmod)^2;'
                                        'rmod=scalpha+r;')
            bforce.addGlobalParameter('scalpha',scalpha)
            bforce.addPerBondParameter('eps')
            bforce.addPerBondParameter('sig')
            bforce.addPerBondParameter('chg')
        else:
            bforce = mm.CustomBondForce('eps*(sigr6^2-sigr6);'
                                        'sigr6=sigr2*sigr2*sigr2;'
                                        'sigr2=(sig/rmod)^2;'
                                        'rmod=scalpha+r;')
            bforce.addGlobalParameter('scalpha',scalpha)
            bforce.addPerBondParameter('eps')
            bforce.addPerBondParameter('sig')
        bforce.setForceGroup(fgroup_num)

        # add particle
        ONE_4PI_EPS0 = 138.935456
        SQRT_ONE_4PI_EPS0 = np.sqrt(ONE_4PI_EPS0)
        nparticle = nonbfrc.getNumParticles()
        for i in range(nparticle):
            chg, sig, eps = nonbfrc.getParticleParameters(i)
            eps = 2.0*np.sqrt(eps.value_in_unit(unit.kilojoule_per_mole))
            chg = SQRT_ONE_4PI_EPS0 * chg.value_in_unit(unit.elementary_charge)
            if self.p['charge']:
                force.addParticle((eps,sig/2.0,chg))
            else:
                force.addParticle((eps,sig/2.0))
            # delete the particle
            #nonbfrc.setParticleParameters(i, chg, 0.5, 0.0)

        # exception
        nexception = nonbfrc.getNumExceptions()
        excount = 0
        for ii in range(nexception):
            i, j, qq, ss, ee = nonbfrc.getExceptionParameters(ii)
            # delete
            #nonbfrc.setExceptionParameters(ii,i,j,qq,0.5,0.0)
            force.addExclusion(i,j)
            if ee.value_in_unit(unit.kilojoule_per_mole) == 0.0:
                continue
            if self.p['charge']:
                qq = ONE_4PI_EPS0 * qq.value_in_unit(unit.elementary_charge**2)
                bforce.addBond(i,j,(4.0*ee,ss,qq))
            else:
                bforce.addBond(i,j,(4.0*ee,ss))
            excount += 1

        # Remove the default nonbonded force
        #system.removeForce(ind)

        system.addForce(force)
        if excount:
            system.addForce(bforce)

    def add_positional_force(self,system):
        fgroup_num = self.set_force_group('position')
        topology = self.structure.topology
        w = self.p['positional_weight'] * 418.4 # kcal/mol/A^2 to kJ/mol/nm^2
        if self.p['softcore'] and False:
            pforce = mm.CustomExternalForce('k*rp2/(rp2+s2); rp2=periodicdistance(x, y, z, x0, y0, z0)^2')
            pforce.addGlobalParameter('k',1.0e8)
            pforce.addGlobalParameter('s2',1.0e8/w)
        else:
            pforce = mm.CustomExternalForce('k*(periodicdistance(x, y, z, x0, y0, z0)^2)')
            pforce.addGlobalParameter('k',w)
        pforce.setForceGroup(fgroup_num)
        pforce.addGlobalParameter('k',w)
        pforce.addPerParticleParameter('x0')
        pforce.addPerParticleParameter('y0')
        pforce.addPerParticleParameter('z0')
        if self.p['fix_residue'] is not None or self.p['fix_index'] is not None:
            if not hasattr(self,'fixind'):
                self.init_fixind()
            for ind in self.fixind:
                x0, y0, z0 = self.initvar[ind] / 10.0 # A to nm
                pforce.addParticle(int(ind),(x0,y0,z0))
            count = self.fixind.size
        else:
            count = 0
            for atom in topology.atoms():
                resname = atom.residue.name
                atmname = atom.name
                # only protein backbone
                if atmname == 'N' or atmname == 'CA' or atmname == 'C' or atmname == 'O':
                    ind = atom.index
                    x0, y0, z0 = self.initvar[ind] / 10.0 # A to nm
                    pforce.addParticle(atom.index,(x0,y0,z0))
                    count += 1
        system.addForce(pforce)
        self.print_log( 'The number of positional restraints: %d'%count )

    def add_protein_chiral_rest_force(self,system):
        fgroup_num = self.set_force_group('chiral')

        chiral_force = mm.CustomTorsionForce('k*max(step(-dtheta0)*dtheta0^2,step(dtheta1)*dtheta1^2);dtheta1=theta-theta1;dtheta0=theta-theta0')
        chiral_force.setForceGroup(fgroup_num)
        chiral_force.addGlobalParameter('k',1.0e6)
        # 25 degree = 0.436332, 45 degree = 0.785398
        chiral_force.addGlobalParameter('theta0', 0.436332) # lower
        chiral_force.addGlobalParameter('theta1', 0.785398) # upper

        amap = self.xyzmap['data']
        for nres in amap:
            aa1 = self.xyzmap['seq'][nres-1]

            # backbone
            if aa1 == 'X':
                # not a protein residue
                continue
            elif aa1 == 'G': # GLY
                atoms_list = (
                        ( 'CA', 'N', 'C', 'HA3' ),
                        ( 'CA', 'HA2', 'N', 'HA3' ),
                )
            elif aa1 == 'I': # ILE
                atoms_list = (
                        ( 'CA', 'N', 'C', 'CB' ),
                        ( 'CA', 'HA', 'N', 'CB' ),
                        ( 'CB', 'CG1', 'CG2', 'CA' ),
                        ( 'CB', 'HB', 'CG1', 'CA' ),
                )
            elif aa1 == 'T': # THR
                atoms_list = (
                        ( 'CA', 'N', 'C', 'CB' ),
                        ( 'CA', 'HA', 'N', 'CB' ),
                        ( 'CB', 'OG1', 'CG2', 'CA' ),
                        ( 'CB', 'HB', 'OG1', 'CA' ),
                )
            else:
                atoms_list = (
                        ( 'CA', 'N', 'C', 'CB' ),
                        ( 'CA', 'HA', 'N', 'CB' ),
                )

            for tor_atoms in atoms_list:
                inds = self._get_atom_inds(nres, tor_atoms)
                if _check_atom_inds(inds):
                    chiral_force.addTorsion(inds[0], inds[1], inds[2], inds[3],
                            [])

        system.addForce(chiral_force)

    def _get_atom_inds(self, nres, atomnames):
        ret = []
        for atomname in atomnames:
            try:
                ind = self.xyzmap['data'][nres][atomname][0] - 1
            except:
                ind = None
            ret.append(ind)
        return ret

    def add_nucleicacid_chiral_rest_force(self,system):
        fgroup_num = self.set_force_group('chiral')
        # C4' - C3' - O4' - C5'
        # C3' - C4' - C2' - O3'
        # C1' - C2' - N1/N9 - O4'
        # 0.08727 rad = 5 degree
        chiral_force = mm.CustomTorsionForce('1.0e6*min(theta-0.08727,0.0)^2')
        chiral_force.setForceGroup(fgroup_num)

        amap = self.xyzmap['data']
        for nres in amap:
            try:
                C4p_ind = amap[nres]["C4'"][0] - 1
            except:
                continue
            try:
                C3p_ind = amap[nres]["C3'"][0] - 1
            except:
                continue
            try:
                O4p_ind = amap[nres]["O4'"][0] - 1
            except:
                continue
            try:
                C5p_ind = amap[nres]["C5'"][0] - 1
            except:
                continue
            try:
                C2p_ind = amap[nres]["C2'"][0] - 1
            except:
                continue
            try:
                O3p_ind = amap[nres]["O3'"][0] - 1
            except:
                continue
            try:
                C1p_ind = amap[nres]["C1'"][0] - 1
            except:
                continue
            try:
                N_ind = amap[nres]["N1"][0] - 1
            except:
                try:
                    N_ind = amap[nres]["N1"][0] - 1
                except:
                    continue
            chiral_force.addTorsion(C4p_ind, C3p_ind, O4p_ind, C5p_ind)
            chiral_force.addTorsion(C3p_ind, C4p_ind, C2p_ind, O3p_ind)
            chiral_force.addTorsion(C1p_ind, C2p_ind, N_ind, O4p_ind)
        system.addForce(chiral_force)

    def add_dfire_force(self,system):
        fgroup_num = self.set_force_group('dfire')
        atom_ind = {}
        if self.p['dfire'] == 2:
            # only atoms on interface
            molind = {}
            dfire_keys = []
            amap_raw = self.xyzmap['raw_data']
            for atom in amap_raw:
                resname = atom[3]
                atmname = atom[2]
                dfire_key = dfire_atom_key(resname,atmname)
                dfire_keys.append(dfire_key)
                if dfire_key[0] == 0 or dfire_key[1] == 0:
                    continue
                # only protein atoms
                if atom[7] not in molind:
                    molind[atom[7]] = []
                molind[atom[7]].append(atom[0]-1)

            if len(molind) == 1:
                return

            contact_cut2 = self.p['dfire_contact_cut']**2
            for i in range(len(amap_raw)):
                atom = amap_raw[i]
                dfire_key = dfire_keys[i]
                if dfire_key[0] == 0 or dfire_key[1] == 0:
                    continue
                ind = atom[0]-1
                imol = atom[7]
                c0 = self.initvar[ind]
                atom_added = False
                for jmol in molind:
                    if atom_added:
                        break
                    if imol == jmol:
                        continue
                    distance2 = np.sum( ( c0 - self.initvar[molind[jmol],:] )**2, axis=1 )
                    if np.any( distance2 <= contact_cut2 ):
                        try:
                            atom_ind[dfire_key].append((atom[0]-1,atom[4]))
                        except:
                            atom_ind[dfire_key] = []
                            atom_ind[dfire_key].append((atom[0]-1,atom[4]))
                        atom_added = True
                        break
        else:
            for atom in self.xyzmap['raw_data']:
                resname = atom[3]
                atmname = atom[2]
                dfire_key = dfire_atom_key(resname,atmname)
                if dfire_key[0] == 0 or dfire_key[1] == 0:
                    continue
                try:
                    atom_ind[dfire_key].append((atom[0]-1,atom[4]))
                except:
                    atom_ind[dfire_key] = []
                    atom_ind[dfire_key].append((atom[0]-1,atom[4]))

        # read dfire table
        dfire_table = {}
        with open(os.path.join(os.environ['HOME'],'tinker','data','dfire_cspline.dat'),'r') as f:
            for line in f:
                c = line.split()
                key = (int(c[0]),int(c[1]),int(c[2]),int(c[3]))
                try:
                    dfire_table[key].append( float(c[7]) * 4.184 )
                except:
                    dfire_table[key] = []
                    dfire_table[key].append( float(c[7]) * 4.184 )

        # prepare 2D Function
        dfire_key_list = list(dfire_table.keys())
        ntype = len(dfire_key_list)
        npoint = len(dfire_table[dfire_key_list[0]])
        dfire_2d_table =  [None]*ntype
        for i in range(ntype):
            key = dfire_key_list[i]
            table1d = dfire_table[key]
            dfire_2d_table[i] = table1d
            assert(npoint == len(table1d))
        dfire_2d_table = np.array(dfire_2d_table).T.ravel()
        dfire_function = mm.Continuous2DFunction(ntype,npoint,dfire_2d_table,0.0,float(ntype-1),0.125,1.425)

        force = mm.CustomCompoundBondForce(2,
            'dfire_func(itype,d);'
            'd=min(max(distance(p1,p2),0.125),1.425);'
        )
        force.addPerBondParameter('itype')
        force.addTabulatedFunction('dfire_func',dfire_function)
        force.setForceGroup(fgroup_num)
        count = 0
        for itype in range(ntype):
            key = dfire_key_list[itype]
            key1 = (key[0],key[1])
            key2 = (key[2],key[3])
            if key1 not in atom_ind or key2 not in atom_ind:
                continue
            if key1 == key2:
                for ii in range(len(atom_ind[key1])-1):
                    i, ires = atom_ind[key1][ii]
                    for jj in range(ii+1,len(atom_ind[key2])):
                        j, jres = atom_ind[key2][jj]
                        if ires == jres:
                            continue
                        force.addBond([i,j],[float(itype)])
                        count += 1
            else:
                for i, ires in atom_ind[key1]:
                    for j, jres in atom_ind[key2]:
                        if ires == jres:
                            continue
                        force.addBond([i,j],[float(itype)])
                        count += 1
        if count:
            system.addForce(force)

        self.print_log('number of dfire interactions: %d'%count)

    def add_stap_force(self,system):
        tor_tables = []
        table_ind = {}
        aa_list = (
                'ALA', 'ARG', 'ASN', 'ASP', 'ASH', 'CYS', 'CYX', 'GLN', 'GLU', 'GLH',
                'GLY', 'HIS', 'HID', 'HIP', 'HIE', 'ILE', 'LEU', 'LYS', 'MET', 'PHE',
                'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL'
        )
        stap_list = self.p['stap_list']
        atom_ind = {}
        residues = self.parmed_structure.residues
        stap_params = []
        for i, residue in enumerate(residues):
            resname = residue.name
            # convert AMBER-style residue names to standard ones
            if resname == 'ASH':
                resname = 'ASP'
            elif resname in ('HID', 'HIP', 'HIE'):
                resname = 'HIS'
            elif resname == 'CYX':
                resname = 'CYS'
            if resname not in aa_list:
                # not a protein residue
                continue
            (
                phi_inds, psi_inds, chi1_inds, chi2_inds
            ) = get_torsion_inds(residues, i)
            if 'phipsi' in stap_list and phi_inds is not None \
                    and psi_inds is not None:
                key = ( resname, 'phipsi' )
                if key not in table_ind:
                    tfile = sys.prefix + f'/csa/data/stap/phipsi/{resname}.dat'
                    tor_tables.append(read_stap_table(tfile))
                    table_ind[key] = len(tor_tables)-1
                tind = table_ind[key]
                stap_params.append( (phi_inds, psi_inds, tind) )
            if 'phichi1' in stap_list and phi_inds is not None \
                    and chi1_inds is not None:
                key = ( resname, 'phichi1' )
                if key not in table_ind:
                    tfile = sys.prefix + f'/csa/data/stap/phichi1/{resname}.dat'
                    tor_tables.append(read_stap_table(tfile))
                    table_ind[key] = len(tor_tables)-1
                tind = table_ind[key]
                stap_params.append( (phi_inds, chi1_inds, tind) )
            if 'psichi1' in stap_list and psi_inds is not None \
                    and chi1_inds is not None:
                key = ( resname, 'psichi1' )
                if key not in table_ind:
                    tfile = sys.prefix + f'/csa/data/stap/psichi1/{resname}.dat'
                    tor_tables.append(read_stap_table(tfile))
                    table_ind[key] = len(tor_tables)-1
                tind = table_ind[key]
                stap_params.append( (psi_inds, chi1_inds, tind) )
            if 'chi1chi2' in stap_list and chi1_inds is not None \
                    and chi2_inds is not None:
                key = ( resname, 'chi1chi2' )
                if key not in table_ind:
                    tfile = sys.prefix + f'/csa/data/stap/chi1chi2/{resname}.dat'
                    tor_tables.append(read_stap_table(tfile))
                    table_ind[key] = len(tor_tables)-1
                tind = table_ind[key]
                stap_params.append( (chi1_inds, chi2_inds, tind) )

        if not stap_params:
            return

        tor_tables.append(tor_tables[0]) # dummy because it should be periodic
        tor_tables = np.array(tor_tables) # type, 1st_tor, 2nd_tor (x, y, z)
        tor_tables *= self.p['stap_weight']
        ntype, n_ang1, n_ang2 = tor_tables.shape
        # xsize, ysize, zsize, values, xmin, xmax, ymin, ymax, zmin, zmax
        pi = np.pi
        stap_table = mm.Continuous3DFunction(ntype, n_ang1, n_ang2,
                tor_tables.T.ravel(), 0.0, float(ntype)-1, -pi, pi, -pi, pi,
                periodic=True)
        force = mm.CustomCompoundBondForce(8,
                f'stap_func(itype,tor1,tor2);'
                f'tor1=dihedral(p1,p2,p3,p4);'
                f'tor2=dihedral(p5,p6,p7,p8);'
        )
        force.addPerBondParameter('itype')
        force.addTabulatedFunction('stap_func', stap_table)
        fgroup_num = self.set_force_group('stap')
        force.setForceGroup(fgroup_num)
        for atoms1, atoms2, itype in stap_params:
            force.addBond(atoms1 + atoms2, [float(itype)])
        system.addForce(force)


    def add_contact_force(self,system):
        contact_rest = self.p['contact_rest']
        n_param = len(contact_rest)
        if n_param == 2:
            cutoff, k = contact_rest
            tol_ratio = 0.0
        elif n_param == 3:
            cutoff, k, tol_ratio = contact_rest
        else:
            raise ValueError("ERROR: cannot parse 'contact_rest'")

        k = ( k * unit.kilocalorie_per_mole / unit.angstrom**2 ).value_in_unit(unit.kilojoule_per_mole/unit.nanometer**2)
        fgroup_num = self.set_force_group('contact')
        force = mm.CustomBondForce('step(r0-r)*k*(r-r0)^2 + step(r-r1)*k*(r-r1)^2')
        force.addPerBondParameter('k')
        force.addPerBondParameter('r0')
        force.addPerBondParameter('r1')
        force.setForceGroup(fgroup_num)

        amap_data = self.xyzmap['data']
        atom_list = []
        resnums = []
        for resnum in sorted(amap_data):
            try:
                ind = amap_data[resnum]['CA'][0]-1
            except:
                continue
            atom_list.append(ind)
            resnums.append(resnum)

        natom = len(atom_list)
        cutoff2 = cutoff**2
        count = 0
        amap_raw = self.xyzmap['raw_data']
        for i in range(natom-1):
            indi = atom_list[i]
            ri = resnums[i]
            imol = amap_raw[i][7]
            for j in range(i+1,natom):
                jmol = amap_raw[i][7]
                if self.p['contact_intra_only']  and imol != jmol:
                    continue
                rj = resnums[j]
                if abs( ri - rj ) < 2: # residue separation
                    continue
                indj = atom_list[j]
                distance2 = np.sum( ( self.initvar[indi] - self.initvar[indj] )**2 )
                if distance2 < cutoff2:
                    count += 1
                    distance = np.sqrt(distance2) / 10.0 # nanometer
                    tol = distance * (1.0+tol_ratio)
                    r0 = distance-tol
                    r1 = distance+tol
                    force.addBond(indi, indj, [k,r0,r1])
        system.addForce(force)
        self.print_log('number of contact_restraints: %d'%count)

    def add_symmetry_force(self,system):
        self.find_molecule_unit()
        unit_size = self.mol_unit_size
        nunit = self.n_mol_unit
        fgroup_num = self.set_force_group('symmetry')
        lintracut, uintracut, lintercut, uintercut = self.p['symmetry_cutoff']
        kintra, kinter = self.p['symmetry_weight']
        if self.p['softcore'] and False:
            force = mm.CustomCompoundBondForce(4,'1.0e8*(dd2/(dd2+1.0e8/k)); dd2= (distance(p1,p2) - distance(p3,p4))^2')
        else:
            force = mm.CustomCompoundBondForce(4,'k*( distance(p1,p2) - distance(p3,p4) )^2')
        force.addPerBondParameter('k')

        heavy_atom_ind = []
        for i in range(unit_size):
            an = self.structure.atoms[i].atomic_number
            if an == 1: # hydrogen
                continue
            heavy_atom_ind.append(i)
        nheavy = len(heavy_atom_ind)

        pos = self.initvar

        # inter-molecular symmetry restraints
        lcutoff2 = lintercut**2
        ucutoff2 = uintercut**2
        pair = {}
        used_heavy_atom_ind = set()
        for iunit in range(nunit-1):
            for i in range(nheavy):
                ii = heavy_atom_ind[i] + iunit*unit_size
                for junit in range(iunit+1,nunit):
                    for j in range(nheavy):
                        jj = heavy_atom_ind[j] + junit*unit_size
                        distance2 = np.sum( ( pos[ii] - pos[jj] )**2 )
                        if distance2 < ucutoff2 and distance2 > lcutoff2:
                            distance = np.sqrt(distance2)
                            if i <= j:
                                key = (i,j)
                            else:
                                key = (j,i)
                            if key not in pair:
                                pair[key] = []
                            pair[key].append((ii,jj,distance))
                            used_heavy_atom_ind.add(heavy_atom_ind[i])
                            used_heavy_atom_ind.add(heavy_atom_ind[j])

        count = 0
        for key in pair:
            pair_list = sorted( pair[key], key=lambda x: x[2] )
            nlist = len(pair_list)
            if nlist < nunit:
                continue
            if nlist > nunit:
                # more than 'nunit' distance pairs
                # This means that there are more than two distances for the same atom-atom pairs
                # Check if their average distances are separated cleanly (3-sigma check).
                nset = int(nlist/nunit)
                dlist = [None] * nset
                for iset in range(nset):
                    dis = np.empty(nunit,dtype=float)
                    for i in range(nunit):
                        dis[i] = pair_list[iset*nunit+i][2]
                    mean = np.mean(dis)
                    std = np.std(dis)
                    dlist[iset] = (mean,std)
                # check dlist
                dist_sep_ok = True
                for i in range(nset-1):
                    # 3-sigma cut
                    if    dlist[i][0] + dlist[i][1]*3.0 > dlist[i+1][0] \
                       or dlist[i+1][0] - dlist[i+1][1]*3.0 < dlist[i][0]:
                        # not OK
                        dist_sep_ok = False
                        break
                if not dist_sep_ok:
                    self.print_log( "WARN: Ambiguous symmetry " + str(pair_list) + "... skipped" )
                    continue
            while len(pair_list) >= nunit:
                plset = pair_list[:nunit]
                pair_list = pair_list[nunit:]
                mean_dis = np.mean( [x[2] for x in plset] )
                for i in range(1,nunit):
                    i1, j1, d1 = plset[i-1]
                    i2, j2, d2 = plset[i]
                    diffratio = abs(d1-d2)/((d1+d2)*0.5)
                    if diffratio > 0.2: # more than 20% difference
                        self.print_log( 'WARNING: distance mismatch distance(%d-%d)=%f and distance(%d-%d)=%f'%(i1,j1,d1,i2,j2,d2) )
                    count += 1
                    # sigma = 20% of the distance
                    k = (kinter*unit.kilocalorie_per_mole/(2.0*(0.2*mean_dis*unit.angstrom)**2)).value_in_unit( unit.kilojoule_per_mole / unit.nanometer**2 )
                    force.addBond((i1,j1,i2,j2),(k,))

        self.print_log( 'The number of inter-molecular symmetry restraints: %d'%count )

        # intra-molecular symmetry restraints
        lcutoff2 = lintracut**2
        ucutoff2 = uintracut**2
        if self.p['symmetry_contact_only']:
            heavy_atom_ind = sorted( used_heavy_atom_ind )
            nheavy = len(used_heavy_atom_ind)
        count = 0
        for i in range(nheavy-1):
            ii = heavy_atom_ind[i]
            for j in range(i+1,nheavy):
                jj = heavy_atom_ind[j]
                distance2 = np.sum( ( pos[ii] - pos[jj] )**2 )
                if distance2 < ucutoff2 and distance2 > lcutoff2:
                    distance = np.sqrt(distance2)
                    for iunit in range(1,nunit):
                        i1 = ii + (iunit-1)*unit_size
                        j1 = jj + (iunit-1)*unit_size
                        i2 = ii + iunit*unit_size
                        j2 = jj + iunit*unit_size
                        count += 1
                        k = (kintra*unit.kilocalorie_per_mole/(2.0*(0.2*distance*unit.angstrom)**2)).value_in_unit( unit.kilojoule_per_mole / unit.nanometer**2 )
                        force.addBond((i1,j1,i2,j2),(k,))

        self.print_log( 'The number of intra-molecular symmetry restraints: %d'%count )

        force.setForceGroup(fgroup_num)
        system.addForce(force)

    def minimizef(self,var,perturb_name):
        ncalls = 0
        if self.p['minimize_chiral']:
            if self.p['neb']:
                for i in range(self.p['neb_nseq']):
                    ene, var[i], nc = self.minimize_chiral(var[i])
                ncalls += nc
            else:
                ene, var, nc = self.minimize_chiral(var)
                ncalls += nc

        if hasattr(self,'rbank_minimization') and self.rbank_minimization:
            maxit = self.p['rbank_min_maxiter']
        else:
            maxit = self.p['bank_min_maxiter']
        if self.p['min_method'] == 'lbfgsb' or self.p['neb']:
            # use scipy's L-BFGS-B
            # var in Angstrom should be converted in the unit of nm
            self.ncalls = 0
            if self.p['neb']:
                # reshape might be better?
                svar = neb_util.serialize_var(var)
            else:
                svar = var.ravel()

            self.energy_group = None

            if self.p['softcore']:
                self.context = self.context_softcore
                maxit_backup = maxit
                maxit = int(maxit_backup*0.2)

            if self.p['md_maxiter'] == 0 or perturb_name != 'no_perturb' or not self.p['neb']:
                if self.p['symmetry_min']:
                    min_result = optimize.minimize(self.energy_function_symmetry, svar, method='L-BFGS-B',
                                     jac=True, options=dict(maxiter=int(maxit*0.2), disp=self.p['report_min'], gtol=0.1))
                    svar = min_result.x
                    min_result = optimize.minimize(self.energy_function, svar, method='L-BFGS-B',
                                     jac=True, options=dict(maxiter=int(maxit*0.8), disp=self.p['report_min'], gtol=0.1))
                else:
                    min_result = optimize.minimize(self.energy_function, svar, method='L-BFGS-B',
                                     jac=True, options=dict(maxiter=maxit, disp=self.p['report_min'], gtol=0.1))

                if self.p['softcore']:
                    svar = min_result.x
                    maxit = int(maxit_backup*0.8)
                    self.context = self.context_original
                    min_result = optimize.minimize(self.energy_function, svar, method='L-BFGS-B',
                                     jac=True, options=dict(maxiter=maxit, disp=self.p['report_min'], gtol=0.1))

            ncalls += self.ncalls
            # MD
            if self.p['neb']:
                minvar = min_result.x.reshape((self.p['neb_nseq'],-1,3))
                min_energy = min_result.fun
                total_ene, total_frc, decomp_ene = neb_util.neb_energy_force(self,minvar)
            else:
                try:
                    minvar = min_result.x.reshape((-1,3))
                except:
                    minvar = var
                try:
                    min_energy = min_result.fun
                except:
                    min_energy = self.get_energy(minvar)
                self.context.setPositions(unit.Quantity(minvar,unit.angstrom))
                if self.p['md_maxiter']:
                    self.context.setVelocitiesToTemperature(self.temperature*unit.kelvin)
                    try:
                        if self.p['md_maxiter'] > 1000:
                            self.simulation.step(1000)
                            st = self.context.getState(getEnergy=True)
                            md_energy = st.getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)
                            if np.isnan(md_energy):
                                raise BaseException
                            else:
                                self.simulation.step(self.p['md_maxiter']-1000)
                        else:
                            self.simulation.step(self.p['md_maxiter'])
                        ncalls += self.p['md_maxiter']
                        st = self.context.getState(getPositions=True,getEnergy=True)
                        minvar = np.array( st.getPositions(asNumpy=True).value_in_unit(unit.angstrom) )
                        min_energy = st.getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)
                    except:
                        self.context.setPositions(unit.Quantity(minvar,unit.angstrom))
        else:
            # use openMM's L-BFGS
            self.context.setPositions(unit.Quantity(var,unit.angstrom))
            if self.p['md_maxiter'] == 0 or perturb_name != 'no_perturb':
                try:
                    self.simulation.minimizeEnergy(
                        tolerance=unit.Quantity(value=self.p['decut'],
                        unit=unit.kilocalorie_per_mole),maxIterations=maxit)
                    st = self.context.getState(getPositions=True,getEnergy=True)
                    minvar = np.array( st.getPositions(asNumpy=True).value_in_unit(unit.angstrom) )
                    min_success = True
                except:
                    # failed minimization
                    self.context.setPositions(unit.Quantity(var,unit.angstrom))
                    st = self.context.getState(getPositions=True,getEnergy=True)
                    min_success = False
            else:
                st = self.context.getState(getPositions=True,getEnergy=True)
                minvar = var
                min_success = True

            if min_success:
                # MD
                if self.p['md_maxiter']:
                    self.context.setVelocitiesToTemperature(self.temperature*unit.kelvin)
                    try:
                        if self.p['md_maxiter'] > 1000:
                            self.simulation.step(1000)
                            st = self.context.getState(getEnergy=True)
                            md_energy = st.getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)
                            if np.isnan(md_energy):
                                raise BaseException
                            else:
                                self.simulation.step(self.p['md_maxiter']-1000)
                        else:
                            self.simulation.step(self.p['md_maxiter'])
                        st = self.context.getState(getPositions=True,getEnergy=True)
                    except:
                        self.context.setPositions(unit.Quantity(minvar,unit.angstrom))
                        st = self.context.getState(getPositions=True,getEnergy=True)
            else:
                self.context.setPositions(unit.Quantity(var,unit.angstrom))
                st = self.context.getState(getPositions=True,getEnergy=True)

            if self.p['md_maxiter']:
                md_energy = st.getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)
                if np.isnan(md_energy):
                    # Energy is 'NAN' so MD is blown up
                    # Take the minimization energy instead
                    self.context.setPositions(unit.Quantity(minvar,unit.angstrom))
                    st = self.context.getState(getPositions=True,getEnergy=True)
                    minvar = np.array( st.getPositions(asNumpy=True).value_in_unit(unit.angstrom) )
                    min_energy = st.getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)
                else:
                    minvar = np.array( st.getPositions(asNumpy=True).value_in_unit(unit.angstrom) )
                    min_energy = md_energy
            else:
                minvar = np.array( st.getPositions(asNumpy=True).value_in_unit(unit.angstrom) )
                min_energy = st.getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)

        if np.isnan(min_energy):
            min_energy = sys.float_info.max

        if self.p['neb']:
            ene_mc, spring_ene1, spring_ene2, spring_ene3 = decomp_ene
            neb_ene = np.empty(self.p['neb_nseq']*2+5)
            j = 4
            for i in range(self.p['neb_nseq']):
                j += 1
                neb_ene[j] = ene_mc[i]
                j += 1
                neb_ene[j] = spring_ene1[i] + spring_ene2[i] + spring_ene3[i]
            ssum1 = np.sum(spring_ene1)
            ssum2 = np.sum(spring_ene2)
            ssum3 = np.sum(spring_ene3)
            esum = np.sum(ene_mc)
            neb_ene[0] = esum + ssum1 + ssum2 + ssum3
            neb_ene[1] = esum
            neb_ene[2] = ssum1
            neb_ene[3] = ssum2
            neb_ene[4] = ssum3
            decomp_energy = (self.decomp_energy_name, tuple(neb_ene))
        else:
            dene_dict = self.energy_decomposition(self.context)
            dene = np.empty(len(self.decomp_energy_name),dtype=np.double)
            for i in range(len(self.decomp_energy_name)):
                ename = self.decomp_energy_name[i]
                dene[i] = dene_dict[ename]
            decomp_energy = (self.decomp_energy_name,dene)
            ncalls += 1

        if self.p['min_method'] != 'lbfgsb':
            ncalls = 1 # count number of minimization instead

        return min_energy, minvar, decomp_energy, ncalls

    def eval_local_update(self,i,j):
        if self.p['md_maxiter'] and self.perturb_info[i][0] == 'no_perturb':
            # When MD simulation is turned on, continuous run is always accepted.
            if self.update_bvar[j] >= 0:
                # This bank(j) is already replaced, then discard imvar
                return False
            else:
                self.print_log( "m%d is a continuous run from b%d (S: %.5E)"%(i+1,j+1,self.imscore), level=3 )
                self.insert_new_var(i,j)
                return True
        else:
            super(csa_OPENMM,self).eval_local_update(i,j)

    def read_xyzmap(self):
        if hasattr(self,'xyzmap'):
            # already read
            return
        #prmtop = self.p['prmtop']
        #parm = read_parm(prmtop)

        topo = self.structure.topology
        natom = topo._numAtoms
        connect = read_connect(natom,topo)

        Res31 ={'ALA':'A','CYS':'C','ASP':'D','GLU':'E','PHE':'F','GLY':'G',
                'HIS':'H','ILE':'I','LYS':'K','LEU':'L','MET':'M','ASN':'N',
                'PRO':'P','GLN':'Q','ARG':'R','SER':'S','THR':'T','VAL':'V',
                'TRP':'W','TYR':'Y','ASX':'N','GLX':'Q','UNK':'X','INI':'K',
                'AAR':'R','ACE':'X','ACY':'G','AEI':'T','AGM':'R','ASQ':'D',
                'AYA':'A','BHD':'D','CAS':'C','CAY':'C','CEA':'C','CGU':'E',
                'CME':'C','CMT':'C','CSB':'C','CSD':'C','CSE':'C','CSO':'C',
                'CSP':'C','CSS':'C','CSW':'C','CSX':'C','CXM':'M','CYG':'C',
                'CYM':'C','DOH':'D','EHP':'F','FME':'M','FTR':'W','GL3':'G',
                'H2P':'H','HIC':'H','HIP':'H','HTR':'W','HYP':'P','KCX':'K',
                'LLP':'K','LLY':'K','LYZ':'K','M3L':'K','MEN':'N','MGN':'Q',
                'MHO':'M','MHS':'H','MIS':'S','MLY':'K','MLZ':'K','MSE':'M',
                'NEP':'H','NPH':'C','OCS':'C','OCY':'C','OMT':'M','OPR':'R',
                'PAQ':'Y','PCA':'Q','PHD':'D','PRS':'P','PTH':'Y','PYX':'C',
                'SEP':'S','SMC':'C','SME':'M','SNC':'C','SNN':'D','SVA':'S',
                'TPO':'T','TPQ':'Y','TRF':'W','TRN':'W','TRO':'W','TYI':'Y',
                'TYN':'Y','TYQ':'Y','TYS':'Y','TYY':'Y','YOF':'Y','FOR':'X',
                '---':'-','PTR':'Y','LCX':'K','SEC':'D','MCL':'K','LDH':'K',
                'D  ':'X',
        }

        # xyzmap dict
        # ['title']: title
        # ['nres']: nres
        # ['seq']: sequence
        # ['natom']: natom
        # ['data'][resnum][atomname]: list of columns
        # ['resrange'][resnum]: tuple of atom index range
        self.xyzmap = {}
        self.xyzmap['title'] = 'no title'
        self.xyzmap['nres'] = topo._numResidues
        self.xyzmap['seq'] = ''
        for res in topo.residues():
            try:
                self.xyzmap['seq'] += Res31[res.name]
            except:
                self.xyzmap['seq'] += 'X'
        self.xyzmap['natom'] = natom
        self.xyzmap['connect'] = connect

        # read continuous molecules
        mol = { 1:1 }
        imol = 1
        size = len(connect)
        while ( len(mol) < size ):
            updated = True
            while ( updated ):
                updated = False
                for sn in range(1,natom+1):
                    if ( sn in mol ): continue
                    conn = connect[sn-1]
                    for cn in conn:
                        if ( cn in mol ):
                            mol[sn] = mol[cn]
                            updated = True
            if len(mol) < size:
                imol += 1
                for sn in range(1,natom+1):
                    if ( sn in mol ): continue
                    mol[sn] = imol
                    break

        # read atom lines
        data = {}
        raw_data = []
        for atm in topo.atoms():
            ind = atm.index
            res = atm.residue
            c = []
            c.append(ind+1) # ind0 serial number in xyz file
            c.append(ind+1) # ind1 serial number in pdb file
            c.append(atm.name) # ind2
            c.append(res.name) # ind3
            c.append(res.index+1) # ind4
            c.append(0) # ind5 null
            c.append(0) # ind6 null
            c.append(mol[ind+1]) # ind7
            raw_data.append(c)
            if ( c[4] not in data ):
                data[c[4]] = {}
            data[c[4]][c[2]] = c # residue number -> atom name = serial number of xyz
        self.xyzmap['data'] = data
        self.xyzmap['raw_data'] = raw_data

        # resrange
        resrange = {}
        atom_index = 0
        for res in sorted(data):
            end_index = atom_index + len(data[res])
            resrange[res] = ( atom_index, end_index )
            atom_index = end_index
        self.xyzmap['resrange'] = resrange

    def report_iterate(self):
        # save bank conformations
        if ( mpirank != 0 ): return
        if ( self.p['write_diff'] and self.niter % self.p['write_diff'] == 0 ):
            self.write_diff()
        if ( self.p['write_bank'] ):
            if ( self.p['write_pdb'] ):
                self.savepdb('b',self.bvar,self.bscore,decomp_energy=self.decomp_bscore)
            if ( self.p['record_gmin'] and self.update_bvar[self.minscore_ind] >= 0 ):
                # gmin updated
                self.write_pdb('gmin%05d.pdb'%self.niter,self.bvar[self.minscore_ind],energy=self.minscore)
            if ( self.p['write_xyz'] ):
                self.savecrd('bank',self.bvar,self.bscore)
            if ( self.p['write_score_profile'] ):
                self.write_score_profile()
        if ( not self.history_fh ): return
        self.write_history_file()

    def write_history_file_header(self):
        pre = str(self.p['profile_precision'])
        fmt = '%6s %3s %10s %4s %4s %' + pre + 's %' + pre + 's %15s %4s %4s\n'
        if self.p['min_method'] == 'lbfgsb':
            self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_energy','max_energy','nee','iuse','nbk'))
        else:
            self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_energy','max_energy','ntrial','iuse','nbk'))
        self.history_fh.flush()

    def read_native_var(self):
        # native var
        if self.p['native_coord']:
            if isinstance(self.p['native_coord'],str):
                if self.p['native_coord'].endswith('.inpcrd'):
                    inpcrd = app.AmberInpcrdFile(self.p['native_coord'])
                    self.native_atom_index = None
                    self.native_var = [ np.array( inpcrd.positions.value_in_unit(unit.angstrom) ) ]
                elif self.p['native_coord'].endswith('.pdb'):
                    pdb = app.PDBFile(self.p['native_coord'])
                    raw_var = np.array( pdb.positions.value_in_unit(unit.angstrom) )
                    top = pdb.getTopology()
                    known_atoms = {}
                    atom_ind = 0
                    for residue in top.residues():
                        resnum = int(residue.id)
                        for atom in residue.atoms():
                            known_atoms[(resnum,atom.name)] = atom_ind
                            atom_ind += 1
                    self.native_atom_index = []
                    natom = self.xyzmap['natom']
                    nvar = np.zeros((natom,3))
                    self.native_pdb_index = []
                    for resnum in self.xyzmap['data']:
                        atoms = self.xyzmap['data'][resnum]
                        for atom_name in atoms:
                            key = (resnum, atom_name)
                            if key in known_atoms:
                                ind = atoms[atom_name][0]-1
                                xyz = raw_var[known_atoms[key]]
                                nvar[ind,:] = xyz
                                self.native_pdb_index.append(ind)
                    self.native_pdb_index = np.array(self.native_pdb_index)
                    if len(self.native_pdb_index) == 0:
                        self.print_error('native pdb does not match')
                    self.native_var = [nvar]
            else:
                self.native_var = []
                for inpcrd_file in self.p['native_coord']:
                    if inpcrd_file.endswith('.inpcrd'):
                        inpcrd = app.AmberInpcrdFile(inpcrd_file)
                    elif inpcrd_file.endswith('.pdb'):
                        inpcrd = app.PDBFile(inpcrd_file)
                    self.native_var.append( np.array( inpcrd.positions.value_in_unit(unit.angstrom) ) )

    def savecrd(self,prefix,vars,energy=None):
        var_size = len(vars)
        if self.p['neb']:
            outfmt = '%s%0' + str((max(int(np.log10(var_size))+1,3))) + 'd.mdcrd'
            for i in range(len(vars)):
                outfile = outfmt%(prefix,i+1)
                self.print_log('Writing %s'%(outfile),level=5)
                natom = self.xyzmap['natom']
                f = open(outfile,'w')
                if ( energy != None ):
                    f.write( 'Total Energy= %f\n'%energy[i] )
                else:
                    f.write('\n')
                for j in range(self.p['neb_nseq']):
                    coord = vars[i][j].ravel().tolist()
                    count = 0
                    while (coord):
                        f.write('%8.3f'%coord.pop(0))
                        count += 1
                        if ( count % 10 == 0 ):
                            f.write('\n')
                    if ( count % 10 != 0 ):
                        f.write('\n')
                f.close()
        else:
            outfmt = '%s%0' + '%d'%(max(int(np.log10(var_size))+1,3)) + 'd.inpcrd'
            for i in range(len(vars)):
                outfile = outfmt%(prefix,i+1)
                self.print_log('Writing %s'%(outfile),level=5)

                natom = self.xyzmap['natom']
                coord = vars[i].ravel().tolist()

                f = open(outfile,'w')
                if ( energy != None ):
                    f.write( 'Total Energy= %f\n'%energy[i] )
                else:
                    f.write('\n')
                f.write( '%6d\n' % (natom) )
                count = 0
                while (coord):
                    f.write('%12.7f'%coord.pop(0))
                    count += 1
                    if ( count % 6 == 0 ):
                        f.write('\n')
                if ( count % 6 != 0 ):
                    f.write('\n')
                f.close()

    def get_energy(self,var):
        self.context.setPositions(var*0.1) # nanometer
        state = self.context.getState(getEnergy=True)
        ene = state.getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)
        return ene

    def get_energy_force(self,var):
        self.context.setPositions(var*0.1) # nanometer
        try:
            if self.energy_group is None:
                state = self.context.getState(getEnergy=True, getForces=True)
            else:
                state = self.context.getState(getEnergy=True, getForces=True, groups=self.energy_group)
        except:
            # probably a coordinat is nan
            frc = np.zeros(var.shape,dtype=np.double)
            return 1.0e300, frc
        ene = state.getPotentialEnergy().value_in_unit(unit.kilocalorie_per_mole)
        frc = state.getForces(asNumpy=True).value_in_unit(
            unit.kilocalorie_per_mole/unit.angstrom)
        if np.isnan(ene):
            ene = 1.0e300
            frc = np.zeros(var.shape,dtype=np.double)
        return ene, frc

    def pack_min_input(self,i_list):
        min_input = []
        for i in i_list:
            try:
                perturb_name = self.perturb_info_backup[i][0]
            except:
                perturb_name = None
            min_input.append( (self.pvar[i],perturb_name) )
            # destroy self.pvar[i] to save memory
            self.pvar[i] = None
        return min_input

    def unpack_min_output(self,min_output,i_list):
        for ind in range(len(i_list)):
            i = i_list[ind]
            mo = min_output[ind]
            score, minvar, decomp_ene, nee = mo
            self.mvar.append(minvar)
            self.mscore.append(score)
            try:
                self.perturb_info.append(self.perturb_info_backup[i])
            except:
                pass
            self.decomp_mscore.append(decomp_ene)
            try:
                self.minimize_nee += nee
            except:
                self.minimize_nee = nee
            self.print_log("m%d score: %.5E"%(len(self.mvar),score), level=3)

    def check_param(self):
        super(csa_OPENMM,self).check_param()
        # neb
        if self.p['neb']:
            if self.p['min_method'] != 'lbfgsb':
                self.print_error( "Only 'min_method: lbfgsb' supports 'neb'" )
            if self.p['md_maxiter']:
                self.print_error( "MD is not supported for 'neb'")
        if self.p['symmetry_min'] and self.p['min_method'] != 'lbfgsb':
            self.print_error( "Only 'min_method: lbfgsb' supports 'symmetry_min'" )
        if self.p['softcore'] and self.p['nonbonded_method'] == 'pme':
            self.print_error("Softcore does not support periodic boundary.")
        if self.p['softcore'] and self.p['min_method'] != 'lbfgsb':
            self.print_error("Softcore support only lbfgsb minimization.")

    def find_molecule_unit(self):
        if hasattr(self,'mol_unit_size') and hasattr(self,'n_mol_unit'):
            # already calculated
            return
        # find molecule unit size
        nmol = self.xyzmap['raw_data'][-1][7]
        atoms_per_molecule = [ 0 for x in range(nmol) ]
        for atm in self.xyzmap['raw_data']:
            ind = atm[7]-1
            atoms_per_molecule[ind] += 1
        ms0 = atoms_per_molecule[0]
        for i in range(1,nmol):
            if atoms_per_molecule[i] == ms0:
                break
        unitsize = i
        count_unit = 0
        ms0 = 0
        for i in range(unitsize):
            ms0 += atoms_per_molecule[i]
        for i in range(0,nmol,unitsize):
            msi = 0
            for j in range(i,i+unitsize):
                msi += atoms_per_molecule[j]
            if msi == ms0:
                count_unit += 1
            else:
                break
        if count_unit == 1:
            self.print_error( 'ERROR: cannot guess the molecule unit' )
        self.print_log( 'Molecule unit size: %d'%ms0 )
        self.print_log( 'The number of unit: %d'%count_unit )
        self.mol_unit_size = ms0
        self.n_mol_unit = count_unit

    def save_system(self,outfile):
        open(outfile,'w').write( mm.XmlSerializer.serialize(self.system) )

    def read_archive_coord(self):
        varlist = []
        arcfile = self.p['init_bank']
        if isinstance(arcfile,str):
            # arcfile is a filename
            # TODO
            raise ValueError
        else: # arcfile is a container?
            for af in arcfile:
                if af.endswith('.pdb'):
                    var = np.array(app.PDBFile(af).positions.value_in_unit(unit.angstrom))
                    varlist.append(var)
                else:
                    # more file types?
                    raise ValueError
        # multi conformation
        for i in range(len(varlist)):
            if varlist[i].shape[0] > self.xyzmap['natom']:
                varlist[i] = varlist[i].reshape((-1,self.xyzmap['natom'],3))

        return varlist

def dfire_atom_key(resnam,atmnam):
    residx = 0
    atmidx = 0
    if (resnam=='CYS'):
       residx = 1
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='SG'):
          atmidx = 6
    elif (resnam=='MET'):
       residx = 2
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='SD'):
          atmidx = 7
       elif (atmnam=='CE'):
          atmidx = 8
    elif (resnam=='PHE'):
       residx = 3
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='CD1'):
          atmidx = 7
       elif (atmnam=='CD2'):
          atmidx = 8
       elif (atmnam=='CE1'):
          atmidx = 9
       elif (atmnam=='CE2'):
          atmidx = 10
       elif (atmnam=='CZ'):
          atmidx = 11
    elif (resnam=='ILE'):
       residx = 4
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG1'):
          atmidx = 6
       elif (atmnam=='CG2'):
          atmidx = 7
       elif (atmnam=='CD1'):
          atmidx = 8
    elif (resnam=='LEU'):
       residx = 5
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='CD1'):
          atmidx = 7
       elif (atmnam=='CD2'):
          atmidx = 8
    elif (resnam=='VAL'):
       residx = 6
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG1'):
          atmidx = 6
       elif (atmnam=='CG2'):
          atmidx = 7
    elif (resnam=='TRP'):
       residx = 7
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='CD1'):
          atmidx = 7
       elif (atmnam=='CD2'):
          atmidx = 8
       elif (atmnam=='NE1'):
          atmidx = 9
       elif (atmnam=='CE2'):
          atmidx = 10
       elif (atmnam=='CE3'):
          atmidx = 11
       elif (atmnam=='CZ2'):
          atmidx = 12
       elif (atmnam=='CZ3'):
          atmidx = 13
       elif (atmnam=='CH2'):
          atmidx = 14
    elif (resnam=='TYR'):
       residx = 8
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='CD1'):
          atmidx = 7
       elif (atmnam=='CD2'):
          atmidx = 8
       elif (atmnam=='CE1'):
          atmidx = 9
       elif (atmnam=='CE2'):
          atmidx = 10
       elif (atmnam=='CZ'):
          atmidx = 11
       elif (atmnam=='OH'):
          atmidx = 12
    elif (resnam=='ALA'):
       residx = 9
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
    elif (resnam=='GLY'):
       residx = 10
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
    elif (resnam=='THR'):
       residx = 11
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='OG1'):
          atmidx = 6
       elif (atmnam=='CG2'):
          atmidx = 7
    elif (resnam=='SER'):
       residx = 12
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='OG'):
          atmidx = 6
    elif (resnam=='GLN'):
       residx = 13
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='CD'):
          atmidx = 7
       elif (atmnam=='OE1'):
          atmidx = 8
       elif (atmnam=='NE2'):
          atmidx = 9
    elif (resnam=='ASN'):
       residx = 14
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='OD1'):
          atmidx = 7
       elif (atmnam=='ND2'):
          atmidx = 8
    elif (resnam=='GLU'):
       residx = 15
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='CD'):
          atmidx = 7
       elif (atmnam=='OE1'):
          atmidx = 8
       elif (atmnam=='OE2'):
          atmidx = 9
    elif (resnam=='ASP'):
       residx = 16
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='OD1'):
          atmidx = 7
       elif (atmnam=='OD2'):
          atmidx = 8
    elif (resnam=='HIS'):
       residx = 17
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='ND1'):
          atmidx = 7
       elif (atmnam=='CD2'):
          atmidx = 8
       elif (atmnam=='CE1'):
          atmidx = 9
       elif (atmnam=='NE2'):
          atmidx = 10
    elif (resnam=='ARG'):
       residx = 18
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='CD'):
          atmidx = 7
       elif (atmnam=='NE'):
          atmidx = 8
       elif (atmnam=='CZ'):
          atmidx = 9
       elif (atmnam=='NH1'):
          atmidx = 10
       elif (atmnam=='NH2'):
          atmidx = 11
    elif (resnam=='LYS'):
       residx = 19
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='CD'):
          atmidx = 7
       elif (atmnam=='CE'):
          atmidx = 8
       elif (atmnam=='NZ'):
          atmidx = 9
    elif (resnam=='PRO'):
       residx = 20
       if (atmnam=='N'):
          atmidx = 1
       elif (atmnam=='CA'):
          atmidx = 2
       elif (atmnam=='C'):
          atmidx = 3
       elif (atmnam=='O'):
          atmidx = 4
       elif (atmnam=='CB'):
          atmidx = 5
       elif (atmnam=='CG'):
          atmidx = 6
       elif (atmnam=='CD'):
          atmidx = 7

    return (residx,atmidx)

def get_connected_atoms(iatm,connect,catms):
    for jatm in connect[iatm-1]:
        if jatm < iatm:
            continue
        catms.add(jatm)
        get_connected_atoms(jatm,connect,catms)


def get_atom_inds(residue):
    inds = {}
    for atom in residue.atoms:
        inds[atom.name] = atom.idx
    return inds


def get_torsion_inds(residues, ires):
    residue = residues[ires]
    inds = get_atom_inds(residue)

    # prev_residue
    if 'H2' in inds:
        prev_residue = None
    else:
        try:
            prev_residue = residues[ires-1]
        except:
            prev_residue = None
    if prev_residue is not None:
        prev_inds = get_atom_inds(prev_residue)

    # next_residue
    if 'OXT' in inds:
        next_residue = None
    else:
        try:
            next_residue = residues[ires+1]
        except:
            next_residue = None
    if next_residue is not None:
        next_inds = get_atom_inds(next_residue)

    resname = residue.name
    # phi
    if prev_residue is not None and 'C' in prev_inds:
        phi_inds = ( prev_inds['C'], inds['N'], inds['CA'], inds['C'] )
    else:
        phi_inds = None
    # psi
    if next_residue is not None and 'N' in next_inds:
        psi_inds = ( inds['N'], inds['CA'], inds['C'], next_inds['N'] )
    else:
        psi_inds = None
    # chi1
    if resname in ('ARG', 'ASN', 'ASP', 'ASH', 'GLN', 'GLU', 'GLH', 'HIS',
            'HID', 'HIP', 'HIE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'TRP',
            'TYR'):
        ag = 'CG'
    elif resname in ('ILE', 'VAL'):
        ag = 'CG1'
    elif resname == 'THR':
        ag = 'OG1'
    elif resname in ('CYS', 'CYX'):
        ag = 'SG'
    elif resname == 'SER':
        ag = 'OG'
    else:
        ag = None
    if ag is not None:
        chi1_inds = ( inds['N'], inds['CA'], inds['CB'], inds[ag] )
    else:
        chi1_inds = None
    # chi2
    if resname in ('ARG', 'GLN', 'GLU', 'GLH', 'LYS', 'PRO'):
        ad = 'CD'
    elif resname in ('LEU', 'PHE', 'TRP', 'TYR'):
        ad = 'CD1'
    elif resname in ('ASN', 'ASP', 'ASH'):
        ad = 'OD1'
    elif resname in ( 'HIS', 'HIP', 'HIE', 'HID' ):
        ad = 'ND1'
    elif resname == 'MET':
        ad = 'SD'
    elif resname == 'ILE':
        ad = 'CD1'
    else:
        ad = None
    if ad is not None:
        chi2_inds = ( inds['CA'], inds['CB'], inds[ag], inds[ad] )
    else:
        chi2_inds = None

    return phi_inds, psi_inds, chi1_inds, chi2_inds


def read_stap_table(tfile):
    table = []
    with open(tfile,'r') as f:
        for line in f:
            line = line.strip()
            if line.startswith('#'):
                continue
            table.append(list(map(float,line.split())))
    return (
            np.array(table) *
            unit.kilocalorie_per_mole
    ).value_in_unit(unit.kilojoule_per_mole)


def _check_atom_inds(inds):
    for ind in inds:
        if ind is None:
            return False
    return True

def standard_radian(inp):
    """ shift randian to [-pi,pi] """
    twopi = 2.0*np.pi
    inp -= int(inp/twopi) * twopi
    if inp >= np.pi:
        inp -= twopi
    elif inp < -np.pi:
        inp += twopi
    return inp
