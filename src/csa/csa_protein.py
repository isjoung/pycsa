"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import division
from __future__ import absolute_import

from builtins import str
from builtins import range
from .csa_TINKER import *
from .protein_perturb import *
from .protein_rand import *
from . import mol_util
from . import tinker_util as tu
import numpy as np
from . import csa_protein_util

class csa_protein(csa_TINKER):
    """csa for optimizing protein conformation"""

    def randomize_conf(self,ivar,nconf=None):
        rand_conf_type = self.p['rand_conf_type'] 
        if rand_conf_type == 0:
            newvar = self.rand_type0.randomize(ivar)
        elif rand_conf_type == 1:
            newvar = self.rand_type1.randomize(ivar)
        elif rand_conf_type == 2:
            newvar = self.rand_type2.randomize(ivar)
        elif rand_conf_type == 'backtor':
            newvar = self.rand_backtor.randomize(ivar)
        else:
            raise ValueError('unknown rand_conf_type %s'%str(rand_conf_type))
        return newvar

    def minimizef(self,var,rinfo=None):
        ncalls = 0 # energy calls

        if self.p['minimize_chiral']:
            ene, var, nc = self.minimize_chiral(var)
            ncalls += nc

        if (    self.p['minimize_backtor']
             or ( self.rbank_minimization and self.p['rbank_minimize_backtor'] ) ):
            try:
                ene # energy calculated?
            except:
                ene = None
            ene, var, nc = self.minimize_backtor(var,energy=ene)
            ncalls += nc

            # 3-res fragment
            if self.p['minimize_backtor'] == 2:
                ene, var, nc = self.minimize_backtor(var,energy=ene,nfrag=3)
                ncalls += nc

        if self.p['minimize_torsion']:
            try:
                ene # energy calculated?
            except:
                ene = None
            ene, var, nc = self.minimize_torsion(var,energy=ene)
            ncalls += nc

        min_energy, minvar, decomp_energy, ncalls2 = super(csa_protein,self).minimizef(var,rinfo=rinfo)
        ncalls += ncalls2

        return min_energy, minvar, decomp_energy, ncalls

    def minimize_torsion(self,var,energy=None):
        # internal coordinates and empty container for the perturbed torsion angles (tor_new)
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var)
        tor_new = np.empty_like(ztors1)

        ncalls = 0
        # calculate the current energy
        if energy is None:
            ene = tu.tinker.energy() 
        else:
            ene = energy

        # repeat fragment assemblies until energy is not improved
        fa_success = True
        while fa_success:
            fa_success = False
            for itry in range(self.p['max_torsion_try']): 
                tor_new, ang_new, bond_new = self.perturb_randint.perturb_torsion(iz1,ztors1,zang1,zbond1)
                mol_util.setint(iz1,bond_new,ang_new,tor_new)
                newene = tu.tinker.energy() 
                ncalls += 1
                if newene < ene:
                    ene = newene
                    ztors1 = tor_new
                    zang1 = ang_new
                    zbond1 = bond_new
                    fa_success = True

        mol_util.setint(iz1,zbond1,zang1,ztors1)
        var = mol_util.getxyz()

        return ene, var, ncalls 

    def minimize_backtor(self,var,energy=None,nfrag=None):
        # TODO remove tinker_util dependency
        # internal coordinates and empty container for the perturbed torsion angles (tor_new)
        ( iz1, zbond1, zang1, ztors1 ) = tu.getint(var)
        tor_new = np.empty_like(ztors1)

        ncalls = 0
        # calculate the current energy
        if energy is None:
            ene = tu.tinker.energy() 
        else:
            ene = energy

        # repeat fragment assemblies until energy is not improved
        maxiter = self.p['backtor_min_maxiter']
        fa_success = True
        count = 0
        while fa_success:
            fa_success = False
            for resind in np.random.permutation(self.perturb_backtor.residue_index):
                tor_new[:] = ztors1
                self.perturb_backtor.perturb_residue(resind+1,iz1,ztors1,tor_new,nfrag=nfrag)
                count += 1
                tu.setint(iz1,zbond1,zang1,tor_new)
                newene = tu.tinker.energy() 
                ncalls += 1
                if newene < ene:
                    ene = newene
                    ztors1[:] = tor_new
                    fa_success = True
                if maxiter > 0 and count >= maxiter:
                    break
            if maxiter > 0 and count >= maxiter:
                break

        tu.setint(iz1,zbond1,zang1,ztors1)
        var = tu.getxyz()

        return ene, var, ncalls 

    def comparef(self,var1,var2):
        if ( self.p['dist_method'] == 'rmsd_ca' ):
            ca_index = self.get_CA_index()
            try:
                distance = calc_rmsd(var1[ca_index,:],var2[ca_index,:])[0]
            except:
                distance =99.9
        else:
            distance = super(csa_protein,self).comparef(var1,var2)

        return float(distance)

    def perturbvarf(self,ind1):
        natom = self.xyzmap['natom']

        nperturb = ( self.p['perturb_param1']['nperturb'],
                     self.p['perturb_param2']['nperturb'],
                     self.p['perturb_param3']['nperturb'],
                     self.p['perturb_param4']['nperturb'],
                     self.p['perturb_param5']['nperturb'],
                     self.p['perturb_param6']['nperturb'],
                     self.p['perturb_param7']['nperturb'],
                     (self.p['perturb_backtor']['nperturb'],self.p['perturb_backtor']['nperturb3']),
                     self.p['perturb_exbeta']['nperturb'],
                     self.p['perturb_randint']['nperturb'],
                     self.p['perturb_randint2']['nperturb'],
                   )
        newvars = []
        perturb_info = []
        iuse = self.count_unused_bank()

        # distance-weighted partner
        if ( self.p['weighted_partner'] ):
            bank_dist = self.bank_distance.get_2dmat()
        else:
            bank_dist = None 

        # type1
        if ( nperturb[0] ):
            ( newvar, pinfo ) = self.perturb_type1.perturb(self.bvar,self.rvar,ind1,nperturb=nperturb[0],bank_dist=bank_dist)
            newvars += newvar
            perturb_info += pinfo

        # type2
        if ( nperturb[1] ):
            ( newvar, pinfo ) = self.perturb_type2.perturb(self.bvar,self.rvar,ind1,nperturb=nperturb[1],bank_dist=bank_dist)
            newvars += newvar
            perturb_info += pinfo

        # type3
        if ( nperturb[2] ):
            if ( self.ncycle == 0 and iuse > len(self.bvar) - len(self.seed) - self.p['n_new_bank_cut'] ):
                nmin, nmax = self.p['perturb_param3']['n_range0']
                ( newvar, pinfo ) = self.perturb_type3.perturb(self.bvar,self.rvar,ind1,src='rr',nperturb=nperturb[2],nmin=nmin,nmax=nmax)
                newvars += newvar
                perturb_info += pinfo
            else:
                br_ratio = self.p['perturb_param3']['br_ratio']
                n1 = int( nperturb[2] * br_ratio )
                n2 = nperturb[2] - n1
                nmin, nmax = self.p['perturb_param3']['n_range1']
                ( newvar, pinfo ) = self.perturb_type3.perturb(self.bvar,self.rvar,ind1,src='br',nperturb=n1,nmin=nmin,nmax=nmax)
                newvars += newvar
                perturb_info += pinfo
                ( newvar, pinfo ) = self.perturb_type3.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=n2,nmin=nmin,nmax=nmax,bank_dist=bank_dist)
                newvars += newvar
                perturb_info += pinfo

        # type4
        if ( nperturb[3] ):
            if ( self.ncycle == 0 and iuse > len(self.bvar) - len(self.p['seed_mask']) - self.p['n_new_bank_cut'] ):
                ( newvar, pinfo ) = self.perturb_type4.perturb(self.bvar,self.rvar,ind1,src='rr',nperturb=nperturb[3])
                newvars += newvar
                perturb_info += pinfo
            else:
                n1 = int(nperturb[3]/2)
                n2 = int(nperturb[3]/2)
                n1 += nperturb[3] - n1 - n2
                ( newvar, pinfo ) = self.perturb_type4.perturb(self.bvar,self.rvar,ind1,src='br',nperturb=n1)
                newvars += newvar
                perturb_info += pinfo
                ( newvar, pinfo ) = self.perturb_type4.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=n2,bank_dist=bank_dist)
                newvars += newvar
                perturb_info += pinfo

        # type5
        if ( nperturb[4] ):
            ( newvar, pinfo ) = self.perturb_type5.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=nperturb[4],bank_dist=bank_dist)
            newvars += newvar
            perturb_info += pinfo

        # type6
        if ( nperturb[5] ):
            if ( self.ncycle == 0 and iuse > len(self.bvar) - len(self.p['seed_mask']) - self.p['perturb_param6']['n_new_bank_cut'] ):
                ( newvar, pinfo ) = self.perturb_type6.perturb(self.bvar,self.rvar,ind1,src='rr',nperturb=nperturb[5])
                newvars += newvar
                perturb_info += pinfo
            else:
                n1 = int(nperturb[5]/2)
                n2 = int(nperturb[5]/2)
                n1 += nperturb[5] - n1 - n2
                ( newvar, pinfo ) = self.perturb_type6.perturb(self.bvar,self.rvar,ind1,src='br',nperturb=n1)
                newvars += newvar
                perturb_info += pinfo
                ( newvar, pinfo ) = self.perturb_type6.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=n2,bank_dist=bank_dist)
                newvars += newvar
                perturb_info += pinfo

        # type7
        if ( nperturb[6] ):
            ret = self.perturb_type7.perturb(self.bvar,self.rvar,ind1,src='bb',nperturb=nperturb[6])
            if ret != None:
                ( newvar, pinfo ) = ret
                newvars += newvar
                perturb_info += pinfo

        # type_backtor
        if ( nperturb[7][0] or nperturb[7][1] ):
            np1 = nperturb[7][1]
            np2 = nperturb[7][0]
            ( newvar, pinfo ) = self.perturb_backtor.perturb(self.bvar,ind1,nperturb=np1,nfrag=3)
            newvars += newvar
            perturb_info += pinfo
            ( newvar, pinfo ) = self.perturb_backtor.perturb(self.bvar,ind1,nperturb=np2)
            newvars += newvar
            perturb_info += pinfo

        # type_exbeta
        if ( nperturb[8] ):
            ( newvar, pinfo ) = self.perturb_exbeta.perturb(self.bvar,ind1,nperturb=nperturb[8])
            newvars += newvar
            perturb_info += pinfo

        # type_randint
        if ( nperturb[9] ):
            ( newvar, pinfo ) = self.perturb_randint.perturb(self.bvar,self.rvar,ind1,src='b',nperturb=nperturb[9])
            newvars += newvar
            perturb_info += pinfo

        # type_randint2
        if ( nperturb[10] ):
            ( newvar, pinfo ) = self.perturb_randint2.perturb(self.bvar,self.rvar,ind1,src='b',nperturb=nperturb[10])
            newvars += newvar
            perturb_info += pinfo

        return ( newvars, perturb_info )

    def __init__(self,param={}):
        p = {
            # generating random conformations
            # 0: bonds, angles and torsions are perturbed by gaussian perturbation
            #    for torsion, they are phi, psi, chi1, ...
            # 1: x, y, and z are randomly moved to their maximum distance
            # 2: x, y, and z are randomly moved according to gaussian perturbation
            # backtor: take backbone torsions from backtor_file
            'rand_conf_type': 0,
            # random conformation parameters
            # rand_conf_type == 0: numbers are sigmas of bond, angle, and torsions
            #     if angle or torsion is None, it is randomly perturbed
            # rand_conf_type == 1: numbers are maximum distances of x, y, and z
            # rand_conf_type == 2: numbers are sigma distances of x, y and z
            'rand_param0': { 'sigma_bond': 0.1, 'sigma_angle': 10.0, 'sigma_torsion': None, 'discrete_omega': True, 'residue_ratio': 1.0, 'res_index': None },
            'rand_param1': { 'max_x': 3.0, 'max_y': 3.0, 'max_z': 3.0, 'res_index': None },
            'rand_param2': { 'sigma_x': 1.0, 'sigma_y': 1.0, 'sigma_z': 1.0, 'res_index': None },
            'rand_backtor': { 'res_index': None, 'sigma_torsion': 5.0, 'backtor_file': None, 'discrete_omega': False, 'weighted_frag': False },

            # perturbation
            'n_new_bank_cut': 5, 
            'perturb_param1': { 'nperturb': 8, 'res_index': None, 'partner_mask': [] },
            'perturb_param2': { 'nperturb': 6, 'res_index': None, 'partner_mask': [] },
            'perturb_param3': { 'nperturb': 6, 'res_index': None, 'n_range0': (3,7), 'n_range1': (1,5), 'partner_mask': [], 'br_ratio': 0.5 },
            'perturb_param4': { 'nperturb': 0, 'sigma_bond': 0.1, 'sigma_angle': 5.0, 'sigma_torsion': 10.0, 'max_ratio': 0.05, 'res_index': None, 'partner_mask': [] },
            'perturb_param5': { 'nperturb': 0, 'res_index': None, 'partner_mask': [], 'nmin': 1, 'nmax': None },
            'perturb_param6': { 'nperturb': 0, 'res_index': None, 'partner_mask': [], 'n_new_bank_cut': 5, 'nmin': 1, 'nmax': 5 },
            'perturb_param7': { 'nperturb': 0, 'res_index': None, 'partner_mask': [], 'nmin': 1, 'nmax': 3, 'distance_cut': 6.5, 'perturb_tor': True },
            'perturb_backtor': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': 1, 'backtor_file': 'backtor.dat', 'nperturb3': 0, 'sigma_torsion': 1.0 },
            'perturb_exbeta': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': 3 },
            'perturb_randint': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': None, 'max_ratio': 0.02, 'sigma_torsion': 5.0, 'sigma_angle': 0.001, 'sigma_bond': 0.001 },
            'perturb_randint2': { 'nperturb': 0, 'res_index': None, 'nmin': 1, 'nmax': 5, 'sigma_torsion': 30.0 },

            # record bank energy and tmscore history
            'bank_history_dist_method': 'tmscore',

            # pre-minimization
            'rbank_minimize_backtor': 0,
            'minimize_backtor': 0,  # 0: turn off, 1: 9-fragment, 2: 9-fragment then 3-fragment
            'backtor_min_maxiter': -1,
            'minimize_torsion': False,
            'max_torsion_try': 100,
        }

        self.p = dict_merge(p,param)

        # init for csa parameter
        super(csa_protein,self).__init__(self.p)

    def init_accessor(self):
        super(csa_protein,self).init_accessor()
        # get protein torsion angles
        self.get_torsion_angle()

        # initialize randomize types
        self.rand_type0 = protein_rand_type0(self.p['rand_param0'],self.xyzmap,self.torsion_angle)
        self.rand_type1 = mol_rand_type1(self.p['rand_param1'],self.xyzmap)
        self.rand_type2 = mol_rand_type2(self.p['rand_param2'],self.xyzmap)
        self.rand_backtor = protein_rand_backtor(self.p['rand_backtor'],self.xyzmap,self.torsion_angle)

        # initialize perturb types
        def energy_func(var):
            tu.setxyz(var)
            return tu.tinker.energy() 
        self.perturb_type1 = protein_perturb_type1(self.p['perturb_param1'],self.xyzmap)
        self.perturb_type2 = protein_perturb_type2(self.p['perturb_param2'],self.xyzmap)
        self.perturb_type3 = protein_perturb_type3(self.p['perturb_param3'],self.xyzmap)
        self.perturb_type4 = protein_perturb_type4(self.p['perturb_param4'],self.xyzmap,self.torsion_angle)
        self.perturb_type5 = protein_perturb_type5(self.p['perturb_param5'],self.xyzmap)
        self.perturb_type6 = protein_perturb_type6(self.p['perturb_param6'],self.xyzmap)
        self.perturb_type7 = protein_perturb_type7(self.p['perturb_param7'],self.xyzmap)
        self.perturb_backtor = protein_perturb_backtor(self.p['perturb_backtor'],self.xyzmap,energy_func=energy_func)
        self.perturb_exbeta = protein_perturb_exbeta(self.p['perturb_backtor'],self.xyzmap)
        self.perturb_randint = protein_perturb_randint(self.p['perturb_randint'],self.xyzmap,energy_func=energy_func)
        self.perturb_randint2 = protein_perturb_randint2(self.p['perturb_randint2'],self.xyzmap)

    def get_torsion_angle(self):
        torsion_angle = {}
        for resind in range(len(self.xyzmap['seq'])):
            resname = self.xyzmap['seq'][resind]
            resnum = resind + 1
            atoms = self.xyzmap['data'][resnum]
            try:
                phi = [ atoms['N'][0], atoms['CA'][0] ]
                psi = [ atoms['CA'][0], atoms['C'][0] ]
            except:
                self.print_log('unknown residue %s'%(resname))
                continue
            try:
                n_next = self.xyzmap['data'][resnum+1]['N'][0]
                omega = [ atoms['C'][0], n_next ]
            except:
                omega = [ atoms['C'][0], 0 ]
            if ( resname == 'A' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1 ]
            elif ( resname == 'V' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2a = [ atoms['CB'][0], atoms['CG1'][0] ]
                chi2b = [ atoms['CB'][0], atoms['CG2'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2a, chi2b ]
            elif ( resname == 'I' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2a = [ atoms['CB'][0], atoms['CG1'][0] ]
                chi2b = [ atoms['CB'][0], atoms['CG2'][0] ]
                chi3 = [ atoms['CG1'][0], atoms['CD1'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2a, chi2b, chi3 ]
            elif ( resname == 'L' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['CG'][0] ]
                chi3a = [ atoms['CG'][0], atoms['CD1'][0] ]
                chi3b = [ atoms['CG'][0], atoms['CD2'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2, chi3a, chi3b ]
            elif ( resname == 'M' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['CG'][0] ]
                chi3 = [ atoms['CG'][0], atoms['SD'][0] ]
                chi4 = [ atoms['SD'][0], atoms['CE'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2, chi3, chi4 ]
            elif ( resname == 'F' or resname == 'Y' or resname == 'W' or resname == 'H' or resname == 'D' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['CG'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2 ]
            elif ( resname == 'R' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['CG'][0] ]
                chi3 = [ atoms['CG'][0], atoms['CD'][0] ]
                chi4 = [ atoms['CD'][0], atoms['NE'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2, chi3, chi4 ]
            elif ( resname == 'K' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['CG'][0] ]
                chi3 = [ atoms['CG'][0], atoms['CD'][0] ]
                chi4 = [ atoms['CD'][0], atoms['CE'][0] ]
                chi5 = [ atoms['CE'][0], atoms['NZ'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2, chi3, chi4, chi5 ]
            elif ( resname == 'E' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['CG'][0] ]
                chi3 = [ atoms['CG'][0], atoms['CD'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2, chi3 ]
            elif ( resname == 'S' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['OG'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2 ]
            elif ( resname == 'T' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2a = [ atoms['CB'][0], atoms['OG1'][0] ]
                chi2b = [ atoms['CB'][0], atoms['CG2'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2a, chi2b ]
            elif ( resname == 'N' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['CG'][0] ]
                chi3 = [ atoms['CG'][0], atoms['ND2'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2, chi3 ]
            elif ( resname == 'Q' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['CG'][0] ]
                chi3 = [ atoms['CG'][0], atoms['CD'][0] ]
                chi4 = [ atoms['CD'][0], atoms['NE2'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2, chi3, chi4 ]
            elif ( resname == 'C' ):
                chi1 = [ atoms['CA'][0], atoms['CB'][0] ]
                chi2 = [ atoms['CB'][0], atoms['SG'][0] ]
                torsion_angle[resind] = [ phi, psi, omega, chi1, chi2 ]
            elif ( resname == 'G' or resname == 'P' ):
                torsion_angle[resind] = [ phi, psi, omega ]
            else:
                self.print_log('unknown residue %s'%(resname))
        self.torsion_angle = torsion_angle
