"""
This is an example CSA class for optimizing the Eggholder function.

It is fully commented and minimally implemented for the novice of csa class.
For a more detailed and advanced example, see csa_param. 
"""
from __future__ import division
from __future__ import absolute_import

# Import numpy since each solution is a numpy array
from builtins import range
import numpy as np
# Import everything from the parental csa class
from .csa import *
# a helper subroutine for perturbations
from perturb_util import get_available_partner
# scipy optimize module is used for local minimization
from scipy import optimize

# This is the objective function
def energy_function(x):
    # Evaluate the Eggholder function value
    xs = x[1:]
    x0 = x[:-1]
    val = np.sum( -(xs+47)*np.sin(np.sqrt(np.absolute(xs+0.5*x0+47)))
                  -x0*np.sin(np.sqrt(np.absolute(x0-xs-47))) ) 
    return val

class csa_example(csa):
    """CSA for optimizing Eggholder function"""

    def __init__(self,param={}):
        # Some default parameters can be defined here
        p = {
            'nbank': 15, # N_B, The number of new solutions added
            'nseed': 5,  # N_S, The number of seed solutions per CSA iteration
            'icmax': 3,  # The maximum number of rounds
            # If the number of new solutions is over 'iucut', CSA round stays.
            # Otherwise, the CSA round increases.
            'iucut': 10,
            # If the score improvement by a newly updated solution is less than 'decut',
            # the new solution is updated in the bank but the solution is not marked
            # as a new solution 
            'decut': 0.001,
            'dcut1': 2.0, # Initial Dcut is Davg/'dcut1'
            'dcut2': 5.0, # Lowest Dcut is Davg/'dcut2'
            'dcut_reduce': 0.983912, # R_D, Dcut decrement ratio
            'save_restart': 1, # The frequency of restart file saving
            'history_file': 'history', # Optimization progress is reported on the file

            #
            # Problem specific parameters
            #
            'nparam': 2, # The dimension of the egghold function
            'param_range': [(-512.,512.),(-512.,512.)], # Parameter ranges eg. [ (min1, max1), (min2, max2), ... ]
            'min_maxiter': 1000, # Max iteration for the local minimization

            #
            # Perturbation related parameters
            #
            # The first crossover perturbation parameters
            'perturb_crossover': { 'nperturb': 10, # The number of perturbations per seed
                                   'partner_mask': [], # The indexes of the disallowed partner solutions
                                   'max_ratio': 0.5, # Maximum crossover ratio
                                 },
            # The second crossover perturbation parameters
            'perturb_crossover2': { 'nperturb': 10, # The number of perturbations per seed
                                    'partner_mask': [], # The indexes of the disallowed partner solutions
                                    # If the number of old solutions is over 'n_new_bank_cut',
                                    # a crossover is performed between first-bank(seed) and
                                    # first-bank, otherwise it is performed between bank(seed)
                                    # and first-bank 
                                    'n_new_bank_cut': 1,
                                    'max_ratio': 0.2, # Maximum crossover ratio
                                  },
            # Random perturbation parameters
            'perturb_random': { 'nperturb': 10, # The number of perturbations per seed
                                'nmin': 1, # The minimum number of perturbed variables
                                'nmax': 3, # The maximum number of perturbed variables
                                # The maximum change of the random perturbation is
                                # 'perturb_ratio' times the domain range.
                                'perturb_ratio': 0.2,
                              },
        }

        self.p = dict_merge(p,param)

        # Initialize other CSA parameters
        super(csa_example,self).__init__(self.p)

    def comparef(self,var1,var2):
        # Return the Euclidean distance of the two solutions, var1 and var2.
        return np.sqrt(np.sum((var1-var2)**2))

    def minimizef(self,var):
        # Local minimization is performed using L-BFGS-B by scipy module.
        res = optimize.minimize(energy_function,
                                var,
                                method='L-BFGS-B',
                                bounds=self.p['param_range'],
                                options={'maxiter': self.p['min_maxiter']})
        # Minimized score
        score = res.fun
        if np.isnan(score):
            # When the local minimization is failed, a big number is assigned instead of NAN.
            score = np.finfo('d').max/1.e6
        # Minimized conformation
        minvar = res.x

        return score, minvar

    def randvarf(self):
        # Initial random solutions are generated 
        var = np.empty(self.p['nparam'],dtype=float) # An empty container for the variable
        for i in range(self.p['nparam']):
            r0, r1 = self.p['param_range'][i] # Retrieve the parameter bounds.
            var[i] = (r1-r0)*np.random.rand() + r0 # A random number in the bounds is filled.
        return var

    def perturb_crossover(self,ind1,pname,partner_mask,src='bb',max_ratio=0.5):
        # This is a helper subroutine for crossover perturbation used in perturbvarf.
        #
        # 'ind1' is the index of the seed solution
        # 'pname' is the perturbation name
        # 'partner_mask' is a list of indexes of the disallowed partner solutions.
        # 'src' defined the sources of the solutions, the first character is for the
        # seed and the second character is for the partner. 'b' means the bank and 'r'
        # means the first-bank.

        # self.bvar is a list holding bank solutions
        # self.rvar is a list holding first bank solutions
        nbank = len(self.bvar)
        # 'var1' is the seed solution
        if src[0] == 'b':
            var1 = self.bvar[ind1]
        else:
            var1 = self.rvar[ind1]

        # n_opt is the size of the solution vector
        n_opt = self.p['nparam']
        # A randomly selected exchange-size
        n_exchange = np.random.randint(1,max(2,n_opt*max_ratio))

        # 'ap' is a list of the indexes of the available partners
        ap = get_available_partner(nbank,ind1,partner_mask)
        # A partner solution is randomly picked
        ind2 = np.random.choice(ap)
        if src[1] == 'b':
            var2 = self.bvar[ind2]
        else:
            var2 = self.bvar[ind2]

        # Pick the indexes of the exchanged parameters in the solution vector randomly
        exind = np.random.choice( self.p['nparam'], n_exchange, replace=False )
        # 'newvar' will be the child solution
        newvar = var1.copy()
        # Some of the parameters are copied from the partner solution
        newvar[exind] = var2[exind]

        # Let's put the name of the perturbation to the perturbation information.
        # If more information is necessary for the post-analysis, you can modify this.
        pinfo = pname

        return newvar, pinfo 

    def perturb_random(self,ind1,pname,perturb_ratio,nmin,nmax):
        # This is a helper subroutine for random perturbation used in perturbvarf
        #
        # 'ind1' is the index of the seed solution
        # 'pname' is the perturbation name
        # 'perturb_ratio' is the maximum relative degree of the perturbation to the domain range.
        # 'nmin' and 'nmax' are the minimum and the maximum number of perturbed parameters.

        # A seed solution is taken
        var1 = self.bvar[ind1]
        # Randomly determine the number of perturbed parameters
        n_exchange = np.random.randint(nmin,nmax)
        # Randomly determine which parameters will be perturbed.
        exind = np.random.choice( self.p['nparam'], n_exchange, replace=False )

        # 'newvar' will be the child solution
        newvar = var1.copy()

        for ei in exind:
            r0, r1 = self.p['param_range'][ei]
            newvar[ei] = newvar[ei] + (r1-r0)*perturb_ratio * (2.0*np.random.random()-1.0)
            # Check the newly perturbed parameters are still within the domain range.
            # If they are outside of the boundaries, change them to the boundary values.
            if newvar[ei] < r0:
                newvar[ei] = r0
            elif newvar[ei] > r1:
                newvar[ei] = r1

        # Let's put the perturbation name to the perturbation info (pinfo)
        pinfo = pname

        return newvar, pinfo

    def perturbvarf(self,ind1):
        # Generate child solutions using a seed solution.
        # 'ind1' is the index of the seed solution in the bank.

        # Prepare an empty list of new variables
        newvars = []
        # Prepare an empty list of perturbation information
        # Perturbation information is for post-analysis of the
        # CSA run. It is accessible from self.perturb_info
        perturb_info = []

        # The first type crossover perturbation
        p_param = self.p['perturb_crossover']
        for i in range(p_param['nperturb']):
            ( newvar, pinfo ) = self.perturb_crossover(ind1,'crossover',p_param['partner_mask'],src='bb',max_ratio=p_param['max_ratio'])
            newvars.append(newvar)
            perturb_info.append(pinfo)

        # The second type crossover perturbation
        p_param = self.p['perturb_crossover2']
        for i in range(p_param['nperturb']):
            iuse = self.count_unused_bank() # count the number of old solutions
            iuse_cut = len(self.bvar)-len(self.seed)-p_param['n_new_bank_cut']
            if self.ncycle == 0 and iuse > iuse_cut:
                # Most of the solutions in bank are new
                ( newvar, pinfo ) = self.perturb_crossover(ind1,'crossover2',p_param['partner_mask'],src='rr',max_ratio=p_param['max_ratio'])
                newvars.append(newvar)
                perturb_info.append(pinfo)
            else:
                ( newvar, pinfo ) = self.perturb_crossover(ind1,'crossover2',p_param['partner_mask'],src='br',max_ratio=p_param['max_ratio'])
                newvars.append(newvar)
                perturb_info.append(pinfo)

        # The random perturbation
        p_param = self.p['perturb_random']
        for i in range(p_param['nperturb']):
            ( newvar, pinfo ) = self.perturb_random(ind1,'random',p_param['perturb_ratio'],p_param['nmin'],p_param['nmax'])
            newvars.append(newvar)
            perturb_info.append(pinfo)

        return newvars, perturb_info
