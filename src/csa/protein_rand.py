"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import absolute_import

from builtins import range
from .mol_rand import *
from . import mol_util
from csa import protein_frag_assembly as pfa
import perturb_util as pu

class protein_rand_type0(mol_rand):
    def __init__(self,param,amap,tor):
        p = {
            'sigma_bond': 0.1,
            'sigma_angle': 10.0,
            'sigma_torsion': None,
            'discrete_omega': True,
            'residue_ratio': 1.0,
        }
        p.update(param)
        super(protein_rand_type0,self).__init__(p,amap)
        self.torsion_angle = tor

    def randomize_single(self,initvar):
        sigma_bond = self.p['sigma_bond']
        sigma_angle = self.p['sigma_angle']
        sigma_torsion = self.p['sigma_torsion']
        res_index = self.p['res_index']
        if ( res_index == None ):
            res_index = list(range( self.xyzmap['nres']))

        mask = np.array( [ False for x in range(self.xyzmap['natom']) ] )
        for rind in res_index:
            res_num = rind + 1
            resatoms = self.xyzmap['data'][res_num]
            for atomname in resatoms:
                mask[ resatoms[atomname][0] - 1 ] = True
        
        ( iz, zbond, zang, ztors ) = mol_util.getint(initvar)
        index = np.where(mask)
        natom = len(index)
        nmask = mask.size
        nres = self.xyzmap['nres']
        if ( self.p['discrete_omega'] ):
            tor_list = ( 0, 1, 3, 4, 5, 6, 7 ) # phi, psi, chi1, ...
        else:
            tor_list = ( 0, 1, 2, 3, 4, 5, 6, 7 ) # phi, psi, omega, chi1, ...
        resinds = list(range(nres))
        np.random.shuffle(resinds)
        resinds = resinds[:int(nres*self.p['residue_ratio'])]
        for resind in resinds:
            try:
                tor_ang = self.torsion_angle[resind]
            except:
                continue # non-protein
            for i in tor_list:
                try:
                    ( a1, a2 ) = tor_ang[i]
                except:
                    continue
                ind = np.where( (iz[1,:nmask] == a1) * (iz[0,:nmask] == a2) * mask )
                if ( sigma_torsion == None ):
                    tor_delta = np.random.rand(1) * 360.0
                    ztors[ind] = correct_torsion( ztors[ind] + tor_delta )
                else:
                    ztors[ind] = correct_torsion( np.random.normal( ztors[ind], sigma_torsion ) )
            if ( not self.p['discrete_omega'] ): continue
            ( a1, a2 ) = tor_ang[2] # omega
            if ( a2 != 0 ):
                ind = np.where( (iz[1,:nmask] == a1) * (iz[0,:nmask] == a2) * mask )
                ca_ind = self.xyzmap['data'][resind+2]['CA'][0] - 1
                if ( self.xyzmap['seq'][resind+1] == 'P' ):
                    if ( resind >= 0 ):
                        resname = self.xyzmap['seq'][resind]
                        if ( resname == 'Y' ):
                            omega_tor = np.random.choice((0.0,180.0),p=(0.25,0.75))
                        elif ( resname == 'S' ):
                            omega_tor = np.random.choice((0.0,180.0),p=(0.11,0.89))
                        else:
                            omega_tor = np.random.choice((0.0,180.0),p=(0.065,0.935))
                    else:
                        omega_tor = np.random.choice((0.0,180.0),p=(0.065,0.935))
                else:
                    omega_tor = np.random.choice((0.0,180.0),p=(0.00029,0.99971))
                for i in ind[0].tolist():
                    if ( i == ca_ind ):
                        ztors[i] = omega_tor
                    else:
                        ztors[i] = 180.0 - omega_tor

        # bond
        zbond[index] = np.random.normal( zbond[index], sigma_bond )
        # angle
        if ( sigma_angle == None ):
            angle_delta = np.random.rand(natom) * 360.0
            zang[index] = correct_angle( zang[index] + angle_delta )   
        else:
            zang[index] = correct_angle( np.random.normal( zang[index], sigma_angle ) )
        mol_util.setint(iz,zbond,zang,ztors)
        newvar = mol_util.getxyz()
        return newvar

class protein_rand_backtor(mol_rand):
    def __init__(self,param,amap,tor):
        p = {
            'res_index': None,
            'sigma_torsion': 1.0,
            'backtor_file': 'backtor.dat',
            'discrete_omega': False,
            'weighted_frag': False,
        }
        p.update(param)
        super(protein_rand_backtor,self).__init__(p,amap)
        self.torsion_angle = tor

        # remove non-protein residues
        for i in range(len(amap['seq'])):
            if ( amap['seq'][i] == 'X' ):
                try:
                    self.residue_index = delete(self.residue_index,i,0)
                except:
                    pass

        # exchangeable atom indexes
        self.exchange_ind = np.array( pu.get_atom_index(self.residue_index,self.xyzmap), dtype=int )

        # read backtor_file
        self.backtor, self.nfrag = pfa.read_backtor_file(self.p['backtor_file'])

    def randomize_single(self,initvar):
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(initvar)

        # variables
        res_index = self.residue_index
        backtor = self.backtor
        sigma_torsion = self.p['sigma_torsion']
        e_ind = self.exchange_ind
        perturbed = zeros(ztors1.size,dtype=bool)
        natom = self.xyzmap['natom']

        tor_new = ztors1.copy()
        # randomize first
        tor_new = tor_new + np.random.rand(natom)*360.0
        sf_res_index = res_index[:]
        np.random.shuffle(sf_res_index) 
        for resind in sf_res_index:
            resnum = resind + 1
            if ( resnum in backtor ):
                pfa.perturb_residue(self.backtor,e_ind,self.xyzmap['data'],resnum,iz1,ztors1,tor_new,nfrag=self.nfrag,sigma_torsion=sigma_torsion)

        #omega
        if ( self.p['discrete_omega'] ):
            for resind in res_index:
                try:
                    tor_ang = self.torsion_angle[resind]
                except:
                    continue # non-protein
                try:
                    (a1,a2) = tor_ang[2]
                except:
                    continue
                if ( a2 != 0 ):
                    mask = logical_and(iz1[0,e_ind]==a2,iz1[1,e_ind]==a1)
                    ca_ind = self.xyzmap['data'][resind+2]['CA'][0] - 1
                    old_omega = ztors1[ca_ind]
                    if ( self.xyzmap['seq'][resind+1] == 'P' ):
                        if ( resind >= 0 ):
                            resname = self.xyzmap['seq'][resind]
                            if ( resname == 'Y' ):
                                omega_tor = np.random.choice((0.0,180.0),p=(0.25,0.75))
                            elif ( resname == 'S' ):
                                omega_tor = np.random.choice((0.0,180.0),p=(0.11,0.89))
                            else:
                                omega_tor = np.random.choice((0.0,180.0),p=(0.065,0.935))
                        else:
                            omega_tor = np.random.choice((0.0,180.0),p=(0.065,0.935))
                    else:
                        omega_tor = np.random.choice((0.0,180.0),p=(0.00029,0.99971))
                    tor_new[e_ind[mask]] = correct_torsion( ztors1[e_ind[mask]] + (omega_tor-old_omega) )
                    perturbed[e_ind[mask]] = True

        # perturb rest of the torsions
        for exind in e_ind:
            if ( perturbed[exind] ): continue
            mask = logical_and(iz1[0,e_ind]==iz1[0,exind],iz1[1,e_ind]==iz1[1,exind])
            tor_new[e_ind[mask]] = correct_torsion( ztors1[e_ind[mask]] + np.random.rand()*360.0 )
            perturbed[e_ind[mask]] = True

        mol_util.setint(iz1,zbond1,zang1,tor_new)
        newvar = mol_util.getxyz()

        return newvar
