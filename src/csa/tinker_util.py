"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import print_function

from builtins import range
import tinker
from numpy import *
from copy import deepcopy

tinker_initialized = False


def getxyz():
    # get xyz coordinates from tinker
    # returns nx3 matrix
    natom = int(tinker.atoms.n)
    xyz = empty( (natom,3) )
    xyz[:,0] = tinker.atoms.x[0:natom]
    xyz[:,1] = tinker.atoms.y[0:natom]
    xyz[:,2] = tinker.atoms.z[0:natom]
    return xyz

def setint(iz,zbond,zang,ztors):
    # pass internal coordinates to tinker
    natom = tinker.atoms.n
    tinker.zcoord.iz[:,:natom] = iz[:,:natom].copy()
    tinker.zcoord.zbond[:natom] = zbond[:natom].copy()
    tinker.zcoord.zang[:natom] = zang[:natom].copy()
    tinker.zcoord.ztors[:natom] = ztors[:natom].copy()
    tinker.makexyz()

def getint(var):
    setxyz(var)
    tinker.makeint(3) # 3: use dihedral angles in all cases
    # get internal coordinates from tinker
    natom = tinker.atoms.n
    iz = tinker.zcoord.iz[:,:natom].copy()
    zbond = tinker.zcoord.zbond[:natom].copy()
    zang = tinker.zcoord.zang[:natom].copy()
    ztors = tinker.zcoord.ztors[:natom].copy()
    return iz, zbond, zang, ztors

def setxyz(xyz):
    # pass xyz coordinates to tinker
    n = xyz.shape[0]
    tinker.atoms.x[0:n] = xyz[:,0] 
    tinker.atoms.y[0:n] = xyz[:,1]
    tinker.atoms.z[0:n] = xyz[:,2] 
    tinker.atoms.n = n

def readxyz(xyzfile):
    """ reads coordinates from xyz file """
    try:
        f = open(xyzfile,'r')
        lines = f.readlines()
        f.close()
    except:
        print(__name__ + " Error: cannot read " + xyzfile)
        return False

    line = lines.pop(0).rstrip('\n')
    natom = int(line[0:6])

    coord = []
    for i in range(natom):
        line = lines.pop(0).rstrip('\n')
        coord.append( [ float(line[11:23]), float(line[23:35]), float(line[35:47]) ] )

    return array(coord)

def tinker_minimize(grdmin,report_min):
    minimum, ncalls = tinker.py_minimize(grdmin,report_min)
    # add_goap
    if ( tinker.potent.add_goap ):
        tinker.potent.use_goap = True
        minimum = tinker.energy()
        tinker.potent.use_goap = False
        ncalls += 1
    return minimum, ncalls

def tinker_minimize_lbfgsb(grdmin,report_min):
    minimum, ncalls = tinker.py_minimize_lbfgsb(grdmin,report_min)
    # add_goap
    if ( tinker.potent.add_goap ):
        tinker.potent.use_goap = True
        minimum = tinker.energy()
        tinker.potent.use_goap = False
        ncalls += 1
    return minimum, ncalls

def tinker_monte(torsmove,size,temper,report_mon):
    minimum, ncalls = tinker.py_monte(torsmove,size,temper,report_mon)
    # add_goap
    if ( tinker.potent.add_goap ):
        tinker.potent.use_goap = True
        minimum = tinker.energy()
        tinker.potent.use_goap = False
        ncalls += 1
    return minimum, ncalls

def tinker_init(xyzfile):
    global tinker_initialized
    if not tinker_initialized:
        tinker.pycsa.use_pycsa = True
        tinker.initial()
        tinker.py_getxyz(xyzfile)
        tinker.mechanic()
        tinker_initialized = True

def rmsfit(var1,var2,index=None):
    MAXATM = len(tinker.atoms.x)
    n1 = var1.shape[0]
    n2 = var2.shape[0]
    x1 = empty(MAXATM)
    y1 = empty(MAXATM)
    z1 = empty(MAXATM)
    x2 = empty(MAXATM)
    y2 = empty(MAXATM)
    z2 = empty(MAXATM)
    x1[:n1] = var1[:,0]
    y1[:n1] = var1[:,1]
    z1[:n1] = var1[:,2]
    x2[:n2] = var2[:,0]
    y2[:n2] = var2[:,1]
    z2[:n2] = var2[:,2]

    if ( index == None ):
        nfit = min(n1,n2) 
        index = [ x for x in range(nfit) ] 
    else:
        nfit = len(index)

    for i in range(nfit):
        tinker.align.ifit[0,i] = index[i] + 1 
        tinker.align.ifit[1,i] = index[i] + 1 
        tinker.align.wfit[i] = 1.0

    tinker.align.nfit = nfit
        
    rmsd = tinker.impose(n1,x1,y1,z1,n2,x2,y2,z2)
    newvar2 = transpose( vstack((x2,y2,z2)) )[:n2,:]
    return rmsd, newvar2

def get_ecomp():
    if ( tinker.potent.add_goap ):
        tinker.potent.use_goap = True

    try:
        use_contact= tinker.contact.use_contact
        econtact=tinker.contact.econtact 
        contact_type = []
        for ct in tinker.contact.contact_type.T.reshape(6,10):
            contact_type.append(ct.tostring().strip())
    except:
        use_contact=[False,False,False,False,False,False]
        econtact=[0.0,0.0,0.0,0.0,0.0,0.0]
        contact_type =["","","","","",""]
    try:
        use_saxs=tinker.saxs.use_saxs
        esaxs=tinker.saxs.esaxs
    except:
        use_saxs=False
        esaxs=0.0

    energy_all = (
        ( True,                           'total',  tinker.energi.esum          ),
        ( tinker.potent.use_bond,         'bond',   tinker.energi.eb            ),
        ( tinker.potent.use_angle,        'angle',  tinker.energi.ea            ),
        ( tinker.potent.use_angle,        'strbnd', tinker.energi.eba           ),
        ( tinker.potent.use_urey,         'urey',   tinker.energi.eub           ),
        ( tinker.potent.use_angang,       'angang', tinker.energi.eaa           ),
        ( tinker.potent.use_opbend,       'opbend', tinker.energi.eopb          ),
        ( tinker.potent.use_opdist,       'opdist', tinker.energi.eopd          ),
        ( tinker.potent.use_improp,       'improp', tinker.energi.eid           ),
        ( tinker.potent.use_imptor,       'imptor', tinker.energi.eit           ),
        ( tinker.potent.use_tors,         'tors',   tinker.energi.et            ),
        ( tinker.potent.use_pitors,       'pitors', tinker.energi.ept           ),
        ( tinker.potent.use_strtor,       'strtor', tinker.energi.ebt           ),
        ( tinker.potent.use_tortor,       'tortor', tinker.energi.ett           ),
        ( tinker.potent.use_vdw
          or tinker.fastpair.use_fast_lj,     'vdw',    tinker.energi.ev        ),
        ( tinker.potent.use_vdw
          or tinker.fastpair.use_fast_lj,  'vdwrep',    tinker.vdwvar.ev_rep    ),
        ( tinker.potent.use_vdw
          or tinker.fastpair.use_fast_lj,  'vdwatt',    tinker.vdwvar.ev_att    ),
        ( tinker.potent.use_charge
          or tinker.fastpair.use_fast_charge, 'charge', tinker.energi.ec        ),
        ( tinker.potent.use_chgdpl,       'chgdpl', tinker.energi.ecd           ),
        ( tinker.potent.use_dipole,       'dipole', tinker.energi.ed            ),
        ( tinker.potent.use_mpole,        'mpole',  tinker.energi.em            ),
        ( tinker.potent.use_polar,        'polar',  tinker.energi.ep            ),
        ( tinker.potent.use_rxnfld,       'rxnfld', tinker.energi.er            ),
        ( tinker.potent.use_solv
          or tinker.fastpair.use_fast_gb,     'solv',   tinker.energi.es        ),
        ( tinker.potent.use_metal,        'metal',  tinker.energi.elf           ),
        ( tinker.potent.use_geom,         'geom',   tinker.energi.eg            ),
#        ( tinker.potent.use_extra,        'extra',  tinker.energi.ex            ),
        ( tinker.potent.use_dfa,          'dfa',    tinker.energi.edfa          ),
        ( tinker.potent.use_dfa,          'dfadis', tinker.energi.edfa_dist     ),
        ( tinker.potent.use_dfa,          'dfaang', tinker.energi.edfa_angle    ),
        ( tinker.potent.use_dfa,          'dfanei', tinker.energi.edfa_nei      ),
        ( tinker.potent.use_dfa,          'dfahpo', tinker.energi.edfa_nei_hp   ),
        ( tinker.potent.use_dfa,          'dfaaro', tinker.energi.edfa_nei_aro  ),
        ( tinker.potent.use_dfa,          'dfapol', tinker.energi.edfa_nei_pol  ),
        ( tinker.potent.use_dfa,          'dfasht', tinker.energi.edfa_beta     ),
        ( tinker.potent.use_dfire,        'dfire',  tinker.energi.edfire        ),
        ( tinker.potent.use_dfire,        'dfiren', tinker.energi.edfire_np     ),
        ( tinker.potent.use_dfire,        'dfirep', tinker.energi.edfire_pol_sc ),
        ( tinker.potent.use_hbond
          or tinker.hbond_vsgb.use_hbond_vsgb, 'hbond',  tinker.energi.ehbond   ),
        ( tinker.potent.use_hbond,        'hb_bb',  tinker.energi.ehbond_bb     ),
        ( tinker.potent.use_hbond,        'hb_bs',  tinker.energi.ehbond_bs     ),
        ( tinker.potent.use_hbond,        'hb_ss',  tinker.energi.ehbond_ss     ),
        ( tinker.potent.use_goap,         'goap',   tinker.energi.egoap         ),
        ( tinker.potent.use_geom,         'templ',  tinker.energi.e_templ       ),
        ( tinker.potent.use_geom,         'local',  tinker.energi.e_local       ),
        ( tinker.potent.use_multi,        'multi',  tinker.energi.emulti        ),
        ( tinker.potent.use_eef1,         'eef1',   tinker.energi.eeef1         ),
        ( tinker.potent.use_noe,          'enoe',   tinker.energi_nmr.enoe      ),
        ( tinker.potent.use_dih,          'edih',   tinker.energi_nmr.edih      ),
        #( tinker.potent.use_rdc,          'erdc',   tinker.energi_nmr.erdc      ),
        ( tinker.potent.use_sann,         'esann',  tinker.energi.esann         ),

        ( use_contact[0], contact_type[0], econtact[0] ),
        ( use_contact[1], contact_type[1], econtact[1] ),
        ( use_contact[2], contact_type[2], econtact[2] ),
        ( use_contact[3], contact_type[3], econtact[3] ),
        ( use_contact[4], contact_type[4], econtact[4] ),
        ( use_contact[5], contact_type[5], econtact[5] ),
        ( use_saxs,           'esaxs',  esaxs           ),

        ( tinker.unres_weight.use_unres,  'UNRESt', tinker.unres_variable.e_unres_total ),
        ( tinker.unres_weight.use_unres,  'Uvdwsc', tinker.unres_variable.e_unres_vdwscsc*tinker.unres_weight.unres_vdwscsc_weight ),
        ( tinker.unres_weight.use_unres,  'Uvdwscp', tinker.unres_variable.e_unres_vdwscp*tinker.unres_weight.unres_vdwscp_weight ),
        ( tinker.unres_weight.use_unres,  'Uvdwpp', tinker.unres_variable.e_unres_vdwpp*tinker.unres_weight.unres_vdwpp_weight ),
        ( tinker.unres_weight.use_unres,  'Uelepp', tinker.unres_variable.e_unres_elecpp*tinker.unres_weight.unres_elecpp_weight ),
        ( tinker.unres_weight.use_unres,  'Ustr',   tinker.unres_variable.e_unres_str*tinker.unres_weight.unres_str_weight ),
        ( tinker.unres_weight.use_unres,  'Ubend',  tinker.unres_variable.e_unres_bend*tinker.unres_weight.unres_bending_weight ),
        ( tinker.unres_weight.use_unres,  'Uscloc', tinker.unres_variable.e_unres_scloc*tinker.unres_weight.unres_scloc_weight ),
        ( tinker.unres_weight.use_unres,  'Utor',   tinker.unres_variable.e_unres_tor*tinker.unres_weight.unres_tor_weight ),
        ( tinker.unres_weight.use_unres,  'Utord',  tinker.unres_variable.e_unres_tord*tinker.unres_weight.unres_tord_weight ),
        ( tinker.unres_weight.use_unres,  'Uelloc', tinker.unres_variable.e_unres_elloc*tinker.unres_weight.unres_elloc_weight ),
        ( tinker.unres_weight.use_unres,  'Ucorr4', tinker.unres_variable.e_unres_corr4*tinker.unres_weight.unres_corr4_weight ),
        ( tinker.unres_weight.use_unres,  'Uturn3', tinker.unres_variable.e_unres_turn3*tinker.unres_weight.unres_turn3_weight ),
        ( tinker.unres_weight.use_unres,  'Uturn4', tinker.unres_variable.e_unres_turn4*tinker.unres_weight.unres_turn4_weight ),
        ( tinker.unres_weight.use_unres,  'Usccor', tinker.unres_variable.e_unres_sccor*tinker.unres_weight.unres_sccor_weight ),

        ( tinker.chemshift.use_chemshift, 'ecs',    tinker.chemshift.ecs        ),
        (    tinker.potent.use_chiral 
          or tinker.potent.use_chiral2
          or tinker.potent.use_chiral3,   'echir',  tinker.eothers.echir        ),
        ( tinker.restrain.nlorentzf,      'elorf',  tinker.restrain.elorentzf   ),
        ( tinker.native_contact.use_native_contact, 'enc', tinker.native_contact.e_nativecontact ),
        ( tinker.contact_order.use_contact_order, 'eco', tinker.contact_order.e_co ),
        ( tinker.rna.use_rna,             'erna',   tinker.rna.erna             ),
        ( tinker.rna.use_rnageom,         'egrna',  tinker.rna.erna_geom        ),
        ( tinker.rna.use_egrt,            'egrt',   tinker.rna.egrt             ),
        ( tinker.rna.use_egrd,            'egrd',   tinker.rna.egrd             ),
        ( tinker.rna.use_egra,            'egra',   tinker.rna.egra             ),
        ( tinker.rna.use_egrbt,           'egrbt',  tinker.rna.egrbt            ),
        ( tinker.rna.use_egrld,           'egrld',  tinker.rna.egrld            ),
        ( tinker.rna.use_egrld1,          'egrld1', tinker.rna.egrld1           ),
        ( tinker.rna.use_egrs1,           'egrs1',  tinker.rna.egrs1            ),
        ( tinker.rna.use_egrs2,           'egrs2',  tinker.rna.egrs2            ),
        ( tinker.rna.use_egrbp,           'egrbp',  tinker.rna.egrbp            ),
        ( tinker.rna.use_egrbp1,          'egrbp1', tinker.rna.egrbp1           ),
        ( tinker.py_energy.use_py_energy, 'py_ene', tinker.py_energy.py_ene     ),
        ( tinker.secondary.use_secondary, 'second', tinker.secondary.e_secondary          ),
        ( tinker.secondary.use_secondary, 'seconH', tinker.secondary.e_secondary_helix    ),
        ( tinker.secondary.use_secondary, 'seconE', tinker.secondary.e_secondary_beta     ),
        ( tinker.secondary.use_secondary, 'seconP', tinker.secondary.e_secondary_betapair ),
        ( tinker.rg.use_rg,               'rg',     tinker.rg.e_rg              ),
    )

    name_list = []
    energy_list = []
    for use_energy, e_name, energy in energy_all:
        if ( not use_energy ): continue
        name_list.append( e_name )
        energy_list.append( float(energy) ) # numpy -> float

    if ( tinker.potent.add_goap ):
        tinker.potent.use_goap = False

    return name_list, energy_list
