"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors: Keehyung Joo, Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import division
from __future__ import absolute_import

from builtins import range
from builtins import object
import sys
from .csa import dict_merge
from copy import deepcopy
import numpy as np
from perturb_util import get_atom_index, exchange_coord_int, exchange_coord_int2, exchange_coord_car, get_available_partner, pick_partner, exchange_chain_coord, get_atom_mask, calc_rmsd
from . import mol_util
from . import protein_frag_assembly as pfa

def perturb_average_int(var1,var2,interp_ratio):
    ir = interp_ratio
    cir = 1.0-ir
    iz0, zbond0, zang0, ztors0 = mol_util.getint(var1)
    iz1, zbond1, zang1, ztors1 = mol_util.getint(var2)

    zbond_new = zbond0 * cir + zbond1 * ir
    zang_new = zang0 * cir + zang1 * ir
    ztors_gap = ztors1 - ztors0
    ztors_gap -= np.floor(ztors_gap/360.0) * 360.0
    ztors_gap[ztors_gap > 180.0] -= 360.0
    ztors_new = ztors0 + ztors_gap * ir

    mol_util.setint(iz0,zbond_new,zang_new,ztors_new)
    return mol_util.getxyz()

def perturb_average(var1,var2,interp_ratio):
    _, fitvar = calc_rmsd(var1,var2)
    return var1 * (1.0-interp_ratio) + fitvar * interp_ratio

class mol_perturb(object):
    pname = 'mol_perturb'
    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'res_index': None,
            'atom_index': None,
            'partner_mask': [],
        }
        self.p = dict_merge(p,param)
        self.xyzmap = amap

        # prepare self.atom_index
        if self.p['res_index'] is None:
            if self.p['atom_index'] is None:
                self.atom_index = np.arange(amap['natom'])
            else:
                self.atom_index = np.array(self.p['atom_index'])
        else:
            a_index = []
            for rind in self.p['res_index']:
                ratoms = amap['data'][rind+1]
                for atomname in ratoms:
                    a_index.append(ratoms[atomname][0]-1)
            if self.p['atom_index'] is not None:
                a_index += list(self.p['atom_index'])
            self.atom_index = np.array(sorted(set(a_index)))

        # prepare chain
        if len( amap['raw_data'][0] ) >= 8:
            chain_dict = {}
            for aind in self.atom_index:
                ic = amap['raw_data'][aind][7]
                if ic not in chain_dict:
                    chain_dict[ic] = []
                chain_dict[ic].append(aind)
            self.chain = tuple(chain_dict.values())
        else:
            # old-style mapfile
            self.chain = [ self.atom_index ]

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,bank_dist=None,multiconf=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
            nbank = len(rvar)
        else:
            var1 = bvar[ind1]
            nbank = len(bvar)

        # number of exchanged residues
        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        ap = get_available_partner(nbank,ind1,self.p['partner_mask'])
        for i in range(nperturb):
            try:
                src1 = src[1]
            except:
                src1 = None
            if src1 is not None:
                ind2 = pick_partner(ap,ind1,bank_dist=bank_dist)
                if src1 == 'b':
                    var2 = bvar[ind2]
                else:
                    var2 = rvar[ind2]
            if multiconf is not None:
                newvar = [None] * multiconf
                res_source = [None] * multiconf
                nconf = np.random.randint(1,3) # 1 or 2 conf is perturbed
                pmask = np.zeros(multiconf,dtype=np.uint8)
                pmask[ np.random.choice(multiconf,nconf,replace=False) ] = True
                for j in range(multiconf):
                    if pmask[j]:
                        if src1 is not None:
                            newvar[j], exchange = self.perturb_single(var1[j],var2[j])
                            if isinstance(exchange,np.ndarray):
                                # this is an exchanged atom mask
                                natom = len(exchange)
                                r2 = float(np.count_nonzero(exchange))/natom
                                r1 = 1.0-r2
                                res_source[j] = [ (src[0],ind1,r1), (src1,ind2,r2) ]
                            else:
                                res_source[j] = [ (src[0],ind1) for x in range(self.xyzmap['nres']) ]
                                for rind in exchange:
                                    res_source[j][rind] = (src1,ind2)
                        else:
                            newvar[j], exchange = self.perturb_single(var1[j])
                            if isinstance(exchange,np.ndarray):
                                # this is an exchanged atom mask
                                natom = len(exchange)
                                r2 = float(np.count_nonzero(exchange))/natom
                                r1 = 1.0-r2
                                res_source[j] = [ (src[0],ind1,r1), (None,None,r2) ]
                            else:
                                res_source[j] = [ (src[0],ind1) for x in range(self.xyzmap['nres']) ]
                                for rind in exchange:
                                    res_source[j][rind] = (None,None)
                    else:
                        newvar[j] = var1[j].copy()
            else:
                if src1 is not None:
                    newvar, exchange = self.perturb_single(var1,var2)
                    if isinstance(exchange,np.ndarray):
                        # this is an exchanged atom mask
                        natom = len(exchange)
                        r2 = float(np.count_nonzero(exchange))/natom
                        r1 = 1.0-r2
                        res_source = [ (src[0],ind1,r1), (src1,ind2,r2) ]
                    else:
                        res_source = [ (src[0],ind1) for x in range(self.xyzmap['nres']) ]
                        for rind in exchange:
                            res_source[rind] = (src1,ind2)
                else:
                    newvar, exchange = self.perturb_single(var1)
                    if isinstance(exchange,np.ndarray):
                        # this is an exchanged atom mask
                        natom = len(exchange)
                        r2 = float(np.count_nonzero(exchange))/natom
                        r1 = 1.0-r2
                        res_source = [ (src[0],ind1,r1) ]
                    else:
                        res_source = [ (src[0],ind1) for x in range(self.xyzmap['nres']) ]
                        for rind in exchange:
                            res_source[rind] = (None,None)
            newvars.append(newvar)
            perturb_info.append((self.pname,res_source))

        return newvars, perturb_info

class mol_perturb_type0(mol_perturb):
    pname = 'no_perturb'

    # no perturbation
    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,multiconf=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
        else:
            var1 = bvar[ind1]

        nres = self.xyzmap['nres']

        if ( nperturb == None ):
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []

        if multiconf is not None:
            for i in range(nperturb):
                newvars.append( [ var1.copy() for x in range(multiconf) ] )
                res_source = [ [ (src[0],ind1) for x in range(nres) ] for y in range(multiconf) ]
                perturb_info.append( (self.pname,res_source) )
        else:
            for i in range(nperturb):
                newvars.append(var1.copy())
                res_source = [ (src[0],ind1) for x in range(nres) ]
                perturb_info.append( (self.pname,res_source) )

        return newvars, perturb_info

class mol_perturb_inv(mol_perturb):
    pname = 'inverse'

    # no perturbation
    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,multiconf=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
        else:
            var1 = bvar[ind1]

        nres = self.xyzmap['nres']

        if ( nperturb == None ):
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []

        if multiconf is not None:
            for i in range(nperturb):
                newvars.append( [ -var1 for x in range(multiconf) ] )
                res_source = [ [ (src[0],ind1) for x in range(nres) ] for y in range(multiconf) ]
                perturb_info.append( (self.pname,res_source) )
        else:
            for i in range(nperturb):
                newvars.append(-var1)
                res_source = [ (src[0],ind1) for x in range(nres) ]
                perturb_info.append( (self.pname,res_source) )

        return newvars, perturb_info

class mol_perturb_int_cross(mol_perturb):
    pname = 'int_cross'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'res_index': None,
            'atom_index': None,
            'nmax': None,
            'nmin': None,
            'max_ratio': 0.4,
            'partner_mask': [],
        }
        self.p = dict_merge(p,param)

        super(mol_perturb_int_cross,self).__init__(self.p,amap)

        self.natom = amap['natom']
        self.nres = amap['nres']

        # atoms that can be crossed over
        use_atom = np.zeros(self.natom,dtype=np.uint8)

        # prepare self.use_atom
        for aind in self.atom_index:
            use_atom[aind] = True
            resind = amap['raw_data'][aind][4]-1
        self.use_atom = use_atom
        self.n_use_atom = np.count_nonzero(self.use_atom)

        # count number of mol and figure out the mol range
        self.nmol = 1
        for atm in amap['raw_data']:
            if self.nmol < atm[7]:
                self.nmol = atm[7]
        self.mol_atom_range = [ [self.natom,0] for x in range(self.nmol) ]
        self.mol_residue_range = [ [self.nres,0] for x in range(self.nmol) ]
        for atm in amap['raw_data']:
            imol = atm[7]-1
            amr = self.mol_atom_range[imol]
            aind = atm[0]-1
            if aind < amr[0]:
                amr[0] = aind
            if aind >= amr[1]:
                amr[1] = aind+1
            rmr = self.mol_residue_range[imol]
            rind = atm[4]-1
            if rind < rmr[0]:
                rmr[0] = rind
            if rind >= rmr[1]:
                rmr[1] = rind+1

        # nmin and nmax
        self.nmin = self.p['nmin']
        if self.nmin is None:
            self.nmin = 3 #int(self.natom/np.count_nonzero(use_residue))
        self.nmax = self.p['nmax']
        if self.nmax is None:
            self.nmax = int(self.p['max_ratio'] * self.n_use_atom)
        if self.nmax < self.nmin:
            self.nmax = self.nmin

    def perturb_single(self,var1,var2):
        # determine the size of exchange
        esize = np.random.randint(self.nmin,self.nmax+1) # number of residues

        istart = np.random.randint(self.atom_index.size-esize)
        istop = istart + esize
        esizes = []
        for imol in range(self.nmol):
            count = 0
            r0, r1  = self.mol_atom_range[imol]
            for i in range(istart,istop):
                ind = self.atom_index[i]
                if  ind >= r0 and ind < r1:
                    count += 1
            esizes.append(count)

        #    mask = np.zeros(self.natom,dtype=np.uint8)
        #    istart = np.random.randint(self.natom-esize+1)
        #    istop = istart + esize
        #    for i in range(istart,istop):
        #        mask[i] = 1
        #    mask = np.logical_and(mask,self.use_atom)
        #    esize = np.count_nonzero(mask)
        #    if esize:


        #        esizes = []
        #        for imol in range(self.nmol):
        #            count = 0
        #            for iatom in range(*self.mol_atom_range[imol]):
        #                if mask[iatom]:
        #                    count += 1
        #            esizes.append(count)
        #        break

        newvar = var1.copy()
        ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)
        ex_atom_mask_all = np.zeros(self.natom,dtype=np.uint8)
        for imol in range(self.nmol):
            ( iz1, bond_new, ang_new, tor_new ) = mol_util.getint(newvar)
            # exchange size for each molecule
            es = esizes[imol]
            if es == 0:
                continue

            r0, r1 = self.mol_atom_range[imol]
            istart_ind = np.random.randint((r1-r0)-es+1) + r0
            ex_atom_mask = np.zeros(self.natom,dtype=np.uint8)
            count = 0
            iatom = istart_ind
            while count < es:
                if self.use_atom[iatom]:
                    count += 1
                    ex_atom_mask[iatom] = 1
                iatom += 1
                if iatom == r1:
                    iatom = r0
            ex_atom_mask_all += ex_atom_mask

            tor_new, ang_new, bond_new = exchange_coord_int2(ex_atom_mask,iz1,tor_new,ang_new,bond_new,ztors2,zang2,zbond2)
            # save int
            # assuming iz1 and iz2 are identical
            mol_util.setint(iz1,bond_new,ang_new,tor_new)
            newvar = exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())

        return newvar, ex_atom_mask_all[self.atom_index]

class mol_perturb_car_residue(mol_perturb):
    pname = 'car_residue'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'nmax': 5,
            'nmin': 1,
            'max_ratio': 0.1,
            'partner_mask': [],
            'residue_base': True,
        }
        self.p = dict_merge(p,param)
        self.natom = amap['natom']

        super(mol_perturb_car_residue,self).__init__(self.p,amap)

        if self.p['residue_base']:
            # prepare atom index by residue
            aind_by_res = {}
            for aind in self.atom_index:
                ires = self.xyzmap['raw_data'][aind][4]
                if ires not in aind_by_res:
                    aind_by_res[ires] = []
                aind_by_res[ires].append(aind)
            self.atom_index_by_res = list(aind_by_res.values())
            index_size = len(self.atom_index_by_res)
        else:
            index_size = len(self.atom_index)

        nmax = self.p['nmax']
        nmin = self.p['nmin']
        if nmax is None:
            nmax = int( index_size * self.p['max_ratio'] )
        if nmax > index_size:
            nmax = index_size
        if nmin is None:
            nmin = 1
        if nmin > index_size:
            nmin = index_size
        if nmax < nmin:
            nmax = nmin
        self.nmax = nmax
        self.nmin = nmin

    def perturb_single(self,var1,var2):
        natom_change = np.random.randint(self.nmin,self.nmax+1)

        if self.p['residue_base']:
            # calculate number of exchange atoms
            nres_change = np.random.randint(self.nmin,self.nmax+1)
            # exchange atom index
            exind = np.array([ y for x in np.random.choice(self.atom_index_by_res,nres_change,replace=False) for y in x ])
        else:
            exind = np.random.choice(self.atom_index,natom_change,replace=False)

        # exchange atom index
        newvar = exchange_coord_car(exind,var1,var2)

        ex_mask = np.zeros(self.natom,dtype=np.uint8)
        ex_mask[exind] = True

        return newvar, ex_mask[self.atom_index]

class mol_perturb_int_residue(mol_perturb):
    pname = 'int_residue'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'res_index': None,
            'nmax': 5,
            'nmin': 1,
            'max_ratio': 0.1,
            'partner_mask': [],
            'residue_base': True,
        }
        self.p = dict_merge(p,param)
        self.natom = amap['natom']

        super(mol_perturb_int_residue,self).__init__(self.p,amap)

        if self.p['residue_base']:
            # prepare atom index by residue
            aind_by_res = {}
            for aind in self.atom_index:
                ires = self.xyzmap['raw_data'][aind][4]
                if ires not in aind_by_res:
                    aind_by_res[ires] = []
                aind_by_res[ires].append(aind)
            self.atom_index_by_res = list(aind_by_res.values())
            index_size = len(self.atom_index_by_res)
        else:
            index_size = len(self.atom_index)

        nmax = self.p['nmax']
        nmin = self.p['nmin']
        if nmax is None:
            nmax = int( index_size * self.p['max_ratio'] )
        if nmax > index_size:
            nmax = index_size
        if nmin is None:
            nmin = 1
        if nmin > index_size:
            nmin = index_size
        if nmax < nmin:
            nmax = nmin
        self.nmax = nmax
        self.nmin = nmin

    def perturb_single(self,var1,var2):
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)
        ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)

        if self.p['residue_base']:
            # calculate number of exchange atoms
            nres_change = np.random.randint(self.nmin,self.nmax+1)
            # exchange atom index
            exind = []
            for x in np.random.choice(self.atom_index_by_res,nres_change,
                    replace=False):
                for y in x:
                    exind.append(y)
            #exind = np.array([ y for x in np.random.choice(self.atom_index_by_res,nres_change,replace=False) for y in x ])
        else:
            natom_change = np.random.randint(self.nmin,self.nmax+1)
            exind = np.random.choice(self.atom_index,natom_change,replace=False)

        # exchange
        tor_new, ang_new, bond_new = exchange_coord_int(exind,iz1,ztors1,zang1,zbond1,ztors2,zang2,zbond2)
        # save int
        # assuming iz1 and iz2 are identical
        mol_util.setint(iz1,bond_new,ang_new,tor_new)
        newvar = exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())

        ex_mask = np.zeros(self.natom,dtype=np.uint8)
        ex_mask[exind] = True

        return newvar, ex_mask[self.atom_index]

class mol_perturb_torsion(mol_perturb):
    #
    # use mol_perturb_int_residue instead
    #
    pname = 'torsion'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'res_index': None,
            'nmax': 5,
            'nmin': 1,
            'max_ratio': 0.01,
            'partner_mask': [],
        }
        self.p = dict_merge(p,param)
        super(mol_perturb_torsion,self).__init__(self.p,amap)

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,nmin=None,nmax=None,bank_dist=None,multiconf=None):
        if nmax is None:
            self.nmax = self.p['nmax']
            if self.nmax is None:
                self.nmax = int( self.atom_index.size * self.p['max_ratio'] )
        else:
            self.nmax = nmax
        if nmin is None:
            self.nmin = self.p['nmin']
            if self.nmin is None:
                self.nmin = 1
        else:
            self.nmin = nmin
        if self.nmax < self.nmin:
            self.nmax = self.nmin

        return super(mol_perturb_torsion,self).perturb(bvar,rvar,ind1,src=src,nperturb=nperturb,bank_dist=None,multiconf=multiconf)

    def perturb_single(self,var1,var2):
        nres = self.xyzmap['nres']
        xyzmap_raw_data = self.xyzmap['raw_data']

        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)
        ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)

        # calculate number of exchange atoms
        n_exchange = min( np.random.randint(self.nmin,self.nmax+1), self.atom_index.size )
        exind = np.random.permutation(self.atom_index)[:n_exchange]
        exresind = set()
        for ei in exind:
            exresind.add( xyzmap_raw_data[ei][4] - 1 )

        # exchange
        tor_new, ang_new, bond_new = exchange_coord_int(exind,iz1,ztors1,zang1,zbond1,ztors2,zang2,zbond2)
        # save int
        # assuming iz1 and iz2 are identical
        mol_util.setint(iz1,bond_new,ang_new,tor_new)
        newvar = exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())

        return newvar, exresind

class mol_perturb_car_cross(mol_perturb):
    pname = 'car_cross'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'res_index': None,
            'nmax': None,
            'nmin': 15,
            'max_ratio': 0.4,
            'partner_mask': [],
        }
        self.p = dict_merge(p,param)

        super(mol_perturb_car_cross,self).__init__(self.p,amap)

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,nmin=None,nmax=None,bank_dist=None,multiconf=None):
        if nmax is None:
            self.nmax = self.p['nmax']
            if self.nmax is None:
                self.nmax = int( len(self.atom_index) * self.p['max_ratio'] )
        if nmin is None:
            self.nmin = self.p['nmin']
            if self.nmin is None:
                self.nmin = 3
        if self.nmax < self.nmin:
            self.nmax = self.nmin

        return super(mol_perturb_car_cross,self).perturb(bvar,rvar,ind1,src=src,nperturb=nperturb,bank_dist=None,multiconf=multiconf)

    def perturb_single(self,var1,var2):
        # calculate number of exchange atoms
        natom_change = np.random.randint(self.nmin,self.nmax+1)

        ra_center_ind = np.random.choice(self.atom_index)

        # calc distance from the representative residue
        ra_var2 = var2[self.atom_index,:]
        ra_center = var2[ra_center_ind,:]
        ra_distance2 = np.sum( ( ra_var2 - ra_center )**2, axis=1 )

        # exchange residue index
        ex_mask = np.argsort(ra_distance2)[:natom_change]
        exind = self.atom_index[ex_mask]

        # exchange atom index
        newvar = exchange_coord_car(exind,var1,var2)

        ex_atom = np.zeros(self.atom_index.size,dtype=np.uint8)
        ex_atom[ex_mask] = 1

        return newvar, ex_atom

class mol_perturb_randcar(mol_perturb):
    pname = 'randcar'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'sigma': 0.5,
            'repeat': 1,
            'res_index': None,
            'nmax': None,
            'nmin': None,
            'max_ratio': 0.05,
        }
        self.p = dict_merge(p,param)
        self.natom = amap['natom']

        super(mol_perturb_randcar,self).__init__(self.p,amap)

        nmax = self.p['nmax']
        nmin = self.p['nmin']
        if nmax is None:
            nmax = int( self.atom_index.size * self.p['max_ratio'] )
        if nmin is None:
            nmin = 1
        if nmax < nmin:
            nmax = nmin
        self.nmin = nmin
        self.nmax = nmax

        if self.nmin > self.atom_index.size:
            self.nmin = self.atom_index.size
        if self.nmax > self.atom_index.size:
            self.nmax = self.atom_index.size

    def perturb_single(self,var1):
        newvar = var1.copy()

        natom_change = np.random.randint(self.nmin,self.nmax+1)
        ind = np.random.choice(self.atom_index,natom_change,replace=False)

        theta = np.random.uniform(low=0.0,high=2.0*np.pi,size=natom_change)
        phi = np.random.uniform(low=0.0,high=2.0*np.pi,size=natom_change)
        sin_theta = np.sin(theta)
        scale = np.random.normal(0.0,self.p['sigma'],natom_change)

        newvar[ind,0] += sin_theta * np.cos(phi) * scale
        newvar[ind,1] += sin_theta * np.sin(phi) * scale
        newvar[ind,2] += np.cos(theta) * scale

        ex_mask = np.zeros(self.natom,dtype=np.uint8)
        ex_mask[ind] = True

        return newvar, ex_mask[self.atom_index]


class mol_perturb_randint(mol_perturb):
    pname = 'randint'

    def __init__(self,param,amap,energy_func=None):
        p = {
            'nperturb': 0,
            'sigma_bond': 0.001,
            'sigma_angle': 0.001,
            'sigma_torsion': 5.0,
            'repeat': 1,
            'max_try': 100,
            'max_ratio': 0.05,
            'res_index': None,
            'nmax': None,
            'nmin': None,
            'max_energy_diff': None,
        }
        self.p = dict_merge(p,param)

        super(mol_perturb_randint,self).__init__(self.p,amap)

        # energy function
        if energy_func is None:
            self.max_energy_diff = None
        else:
            self.energy_func = energy_func
            self.max_energy_diff = self.p['max_energy_diff']

        nmax = self.p['nmax']
        nmin = self.p['nmin']
        if ( nmax == None ): nmax = int( self.atom_index.size * self.p['max_ratio'] )
        if ( nmin == None ): nmin = 1
        if ( nmax < nmin ): nmax = nmin
        self.nmin = nmin
        self.nmax = nmax

        if self.nmin > self.atom_index.size:
            self.nmin = self.atom_index.size
        if self.nmax > self.atom_index.size:
            self.nmax = self.atom_index.size

    def perturb_single(self,var1):
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        # energy check?
        if self.max_energy_diff is not None:
            ene = self.energy_func(var1)
            ene_cut = ene + self.max_energy_diff
        else:
            ene_cut = None

        nres = self.xyzmap['nres']
        xyzmap_raw_data = self.xyzmap['raw_data']

        cur_tor = ztors1.copy()
        cur_ang = zang1.copy()
        cur_bond = zbond1.copy()

        for irepeat in range(self.p['repeat']):
            best_ene = sys.float_info.max
            best_tor = None
            best_ang = None
            best_bond = None
            best_exresind = None
            success = False

            for itry in range(self.p['max_try']):
                tor_new, ang_new, bond_new, exresind = self.perturb_torsion(iz1,cur_tor,cur_ang,cur_bond,resind=True)

                if ene_cut is not None:
                    mol_util.setint(iz1,bond_new,ang_new,tor_new)
                    newvar = mol_util.getxyz()
                    newene = self.energy_func(newvar)
                    if newene <= ene_cut:
                        cur_tor = tor_new
                        cur_ang = ang_new
                        cur_bond = bond_new
                        cur_exresind = exresind
                        success = True
                        break
                    elif newene < best_ene:
                        best_ene = newene
                        best_tor = tor_new
                        best_ang = ang_new
                        best_bond = bond_new
                        best_exresind = exresind
                else:
                    cur_tor = tor_new
                    cur_ang = ang_new
                    cur_bond = bond_new
                    cur_exresind = exresind
                    success = True
                    break

            if not success:
                cur_tor = tor_new
                cur_ang = ang_new
                cur_bond = bond_new
                cur_exresind = best_exresind
                ene = best_ene
                ene_cut = ene + self.max_energy_diff
                break # if energy fails, then exit immediately

        # save int
        mol_util.setint(iz1,cur_bond,cur_ang,cur_tor)
        newvar = exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())

        return newvar, cur_exresind

    def perturb_torsion(self,iz1,cur_tor,cur_ang,cur_bond,resind=False):
        n_change = min( np.random.randint(self.nmin,self.nmax+1), self.atom_index.size )
        exind = np.random.choice(self.atom_index,n_change,replace=False)

        ztors2 = cur_tor.copy()
        zang2 = cur_ang.copy()
        zbond2 = cur_bond.copy()

        if resind:
            exresind = []
            xyzmap_raw_data = self.xyzmap['raw_data']
            for ei in exind:
                exresind.append( xyzmap_raw_data[ei][4] - 1 )

        if self.p['sigma_torsion'] is not None:
            ztors2[exind] += np.random.normal(0.0,self.p['sigma_torsion'],exind.size)
        if self.p['sigma_angle'] is not None:
            zang2[exind] += np.random.normal(0.0,self.p['sigma_angle'],exind.size)
        if self.p['sigma_bond'] is not None:
            zbond2[exind] += np.random.normal(0.0,self.p['sigma_bond'],exind.size)

        # exchange
        if resind:
            tor_new, ang_new, bond_new = exchange_coord_int(exind,iz1,cur_tor,cur_ang,cur_bond,ztors2,zang2,zbond2)
            return tor_new, ang_new, bond_new, exresind
        else:
            return exchange_coord_int(exind,iz1,cur_tor,cur_ang,cur_bond,ztors2,zang2,zbond2)


class mol_perturb_trans_rot(mol_perturb):
    pname = 'trans_rot'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'sigma_trans': 0.5,
            'sigma_rot': 10.0,
        }
        self.p = dict_merge(p,param)

        super(mol_perturb_trans_rot,self).__init__(self.p,amap)

    def perturb_single(self,var1):
        newvar = var1.copy()
        var_sel = newvar[self.atom_index,:]

        center = np.mean(var_sel,axis=0)
        var_sel -= center

        # rotate
        theta_x = np.random.normal(0.0,self.p['sigma_rot']) / 180.0 * np.pi
        cos_t = np.cos(theta_x)
        sin_t = np.sin(theta_x)
        rx = np.array((
            (1.,0.,0.),
            (0.,cos_t,-sin_t),
            (0.,sin_t,cos_t),
        ))
        theta_y = np.random.normal(0.0,self.p['sigma_rot']) / 180.0 * np.pi
        cos_t = np.cos(theta_y)
        sin_t = np.sin(theta_y)
        ry = np.array((
            (cos_t,0.,sin_t),
            (0.,1.,0.),
            (-sin_t,0.,cos_t),
        ))
        theta_z = np.random.normal(0.0,self.p['sigma_rot']) / 180.0 * np.pi
        cos_t = np.cos(theta_z)
        sin_t = np.sin(theta_z)
        rz = np.array((
            (cos_t,-sin_t,0.),
            (sin_t,cos_t,0.),
            (0.,0.,1.),
        ))
        r = np.dot(np.dot(rz,ry),rx)
        var_sel[:,:] = np.dot(r,var_sel.T).T

        # translate
        theta = np.random.uniform(low=0.0,high=2.0*np.pi)
        phi = np.random.uniform(low=0.0,high=2.0*np.pi)
        sin_theta = np.sin(theta)
        scale = np.random.normal(0.0,self.p['sigma_trans'])
        ranvec = np.array((
            sin_theta * np.cos(phi),
            sin_theta * np.sin(phi),
            np.cos(theta),
        )) * scale

        newvar[self.atom_index,:] = var_sel + (center + ranvec)

        return newvar, np.ones(self.atom_index.size)


class mol_perturb_average(mol_perturb):
    pname = 'average'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'partner_mask': [],
        }
        self.p = dict_merge(p,param)
        super(mol_perturb_average,self).__init__(self.p,amap)

        self.natom = amap['natom']
        self.nres = amap['nres']

        # count number of mol and figure out the mol range
        self.nmol = 1
        for atm in amap['raw_data']:
            if self.nmol < atm[7]:
                self.nmol = atm[7]
        self.mol_atom_range = [ [self.natom,0] for x in range(self.nmol) ]
        self.mol_residue_range = [ [self.nres,0] for x in range(self.nmol) ]
        for atm in amap['raw_data']:
            imol = atm[7]-1
            amr = self.mol_atom_range[imol]
            aind = atm[0]-1
            if aind < amr[0]:
                amr[0] = aind
            if aind >= amr[1]:
                amr[1] = aind+1
            rmr = self.mol_residue_range[imol]
            rind = atm[4]-1
            if rind < rmr[0]:
                rmr[0] = rind
            if rind >= rmr[1]:
                rmr[1] = rind+1

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None,bank_dist=None,interp_ratio=None,multiconf=None):
        if interp_ratio is None:
            self.interp_ratio = np.random.rand() * 0.8 + 0.1 # [0.1,0.9)
        else:
            self.interp_ratio = interp_ratio
        return super(mol_perturb_average,self).perturb(bvar,rvar,ind1,src=src,nperturb=nperturb,bank_dist=bank_dist,multiconf=None)

    def perturb_single(self,var1,var2):
        interp_ratio = self.interp_ratio
        newvar = perturb_average_int(var1,var2,interp_ratio)
        refvar = var1 * (1.0-interp_ratio) + var2
        for imol in range(self.nmol):
            i, j = self.mol_atom_range[imol]
            _, fitvar = calc_rmsd(refvar[i:j,:],newvar[i:j,:])
            newvar[i:j,:] = fitvar
        return newvar, (0,)

class mol_perturb_native_torsion(mol_perturb):
    pname = 'native_torsion'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'res_index': None,
            'nmax': None,
            'nmin': 1,
            'max_ratio': 0.5,
        }
        self.p = dict_merge(p,param)
        super(mol_perturb_native_torsion,self).__init__(self.p,amap)

        nmax = self.p['nmax']
        nmin = self.p['nmin']
        if ( nmax == None ): nmax = int( self.atom_index.size * self.p['max_ratio'] )
        if ( nmin == None ): nmin = 1
        if ( nmax < nmin ): nmax = nmin
        self.nmax = nmax
        self.nmin = nmin

    def perturb(self,bvar,native_var,ind1,native_ind,src='b',nperturb=None,nmin=None,nmax=None,multiconf=None):
        var1 = bvar[ind1]
        var2 = native_var[native_ind]

        if nmax is None:
            nmax = self.nmax
        if nmin is None:
            nmin = self.nmin
        if nmax < nmin:
            nmax = nmin

        # number of exchanged residues
        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        for i in range(nperturb):
            if multiconf is not None:
                newvar = [None] * multiconf
                res_source = [None] * multiconf
                for j in range(multiconf):
                    res_source[j] = [ (src[0],ind1) for x in range(self.xyzmap['nres']) ]
                    newvar[j], exresind = self.perturb_single(var1[j],var2) # var2 is single
                    for rind in exresind:
                        res_source[j][rind] = ('native',native_ind)
            else:
                res_source = [ (src[0],ind1) for x in range(self.xyzmap['nres']) ]
                newvar, exresind = self.perturb_single(var1,var2)
                for rind in exresind:
                    res_source[rind] = ('native',native_ind)
            newvars.append(newvar)
            perturb_info.append((self.pname,res_source))

        return newvars, perturb_info

    def perturb_single(self,var1,var2):
        xyzmap_raw_data = self.xyzmap['raw_data']
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)
        ( iz2, zbond2, zang2, ztors2 ) = mol_util.getint(var2)

        # calculate number of exchange atoms
        n_exchange = min( np.random.randint(self.nmin,self.nmax+1), self.atom_index.size )
        exind = np.random.permutation(self.atom_index)[:n_exchange]
        exresind = []
        for ei in exind:
            exresind.append( xyzmap_raw_data[ei][4] - 1 )

        # exchange
        tor_new, ang_new, bond_new = exchange_coord_int(exind,iz1,ztors1,zang1,zbond1,ztors2,zang2,zbond2)
        # save int
        # assuming iz1 and iz2 are identical
        mol_util.setint(iz1,bond_new,ang_new,tor_new)
        newvar = exchange_chain_coord(self.chain,self.xyzmap,var1,mol_util.getxyz())

        return newvar, exresind

class mol_perturb_native_car_cross(mol_perturb_car_cross):
    pname = 'native_car_cross'

    def __init__(self,param,amap):
        p = {
            'nperturb': 0,
            'res_index': None,
            'nmax': None,
            'nmin': 1,
            'max_ratio': 0.5,
        }
        self.p = dict_merge(p,param)

        super(mol_perturb_native_car_cross,self).__init__(self.p,amap)

    def perturb(self,bvar,native_var,ind1,native_ind,src='b',nperturb=None,nmin=None,nmax=None,multiconf=None):
        var1 = bvar[ind1]
        var2 = native_var[native_ind]

        if nmax is None:
            self.nmax = self.p['nmax']
            if self.nmax is None:
                self.nmax = int( len(self.atom_index) * 0.4 ) # 40%
        if nmin is None:
            self.nmin = self.p['nmin']
            if self.nmin is None:
                self.nmin = 1
        if self.nmax < self.nmin:
            self.nmax = self.nmin

        # number of exchanged residues
        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        for i in range(nperturb):
            if multiconf is not None:
                newvar = [None] * multiconf
                res_source = [None] * multiconf
                for j in range(multiconf):
                    res_source[j] = [ (src[0],ind1) for x in range(self.xyzmap['nres']) ]
                    newvar[j], exresind = self.perturb_single(var1[j],var2) # var2 is single
                    for rind in exresind:
                        res_source[j][rind] = ('native',native_ind)
            else:
                res_source = [ (src[0],ind1) for x in range(self.xyzmap['nres']) ]
                newvar, exresind = self.perturb_single(var1,var2)
                for rind in exresind:
                    res_source[rind] = ('native',native_ind)
            newvars.append(newvar)
            perturb_info.append((self.pname,res_source))

        return newvars, perturb_info

class protein_perturb_backtor(mol_perturb):
    pname = 'backtor'

    # construct beta sheet
    def __init__(self,param,amap,energy_func=None):
        p = {
            'nperturb': 6,
            'res_index': None,
            'nmax': 3,
            'nmin': 1,
            'backtor_file': None,
            'sigma_torsion': 1.0,
            'weighted_frag': False,
            'max_energy_diff': None,
        }
        self.p = dict_merge(p,param)
        super(protein_perturb_backtor,self).__init__(self.p,amap)

        # energy function
        if energy_func is None:
            self.max_energy_diff = None
        else:
            self.energy_func = energy_func
            self.max_energy_diff = self.p['max_energy_diff']

        self.residue_index = []
        for aind in self.atom_index:
            atom_name = amap['raw_data'][aind][2]
            if atom_name == 'CA':
                self.residue_index.append(amap['raw_data'][aind][4]-1)

        # remove non-protein residues
        for i in range(len(amap['seq'])):
            if ( amap['seq'][i] == 'X' ):
                try:
                    self.residue_index = delete(self.residue_index,i,0)
                except:
                    pass

        # exchangeable atom indexes
        e_ind = get_atom_index(self.residue_index,self.xyzmap)
        self.exchange_ind = np.array(e_ind)

        # read backtor_file
        self.backtor, self.nfrag = pfa.read_backtor_file(self.p['backtor_file'])

        if self.backtor is not None:
            # exchangeable residue indexes
            mol_nres = self.xyzmap['nres']
            min_res = int(self.nfrag/2)
            max_res = mol_nres-int(self.nfrag/2)
            e_res_ind = []
            for ri in self.residue_index:
                if ri >= min_res and ri < max_res:
                    e_res_ind.append(ri)
            self.exchange_res_ind = e_res_ind

    def perturb(self,bvar,ind1,nperturb=None,nmin=None,nmax=None,nfrag=None,multiconf=None):
        if nfrag is None:
            nfrag = self.nfrag
        var1 = bvar[ind1]

        # number of exchanged residues
        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        for i in range(nperturb):
            if multiconf is not None:
                newvar = [None] * multiconf
                res_source = [None] * multiconf
                for j in range(multiconf):
                    res_source[j] = [ ('b',ind1) for x in range(self.xyzmap['nres']) ]
                    newvar[j], exresind = self.perturb_single(var1[j],nmin=nmin,nmax=nmax,nfrag=nfrag)
                    for rind in exresind:
                        res_source[j][rind] = ('backtor',0)
            else:
                res_source = [ ('b',ind1) for x in range(self.xyzmap['nres']) ]
                newvar, exresind = self.perturb_single(var1,nmin=nmin,nmax=nmax,nfrag=nfrag)
                for rind in exresind:
                    res_source[rind] = ('backtor',0)
            newvars.append(newvar)
            perturb_info.append((self.pname,res_source))

        return newvars, perturb_info

    def perturb_single(self,var1,nmin=None,nmax=None,nfrag=None):
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        # energy check?
        if self.max_energy_diff is not None:
            ene = self.energy_func(var1)
            ene_cut = ene + self.max_energy_diff
        else:
            ene_cut = None

        nres = len(self.residue_index)

        newvars = []
        perturb_info = []
        if nmax is None:
            nmax = self.p['nmax']
            if nmax is None:
                nmax = int(( nres - 1 ) / 2)
        if nmin is None:
            nmin = self.p['nmin']
            if nmin is None:
                nmin = 1
        if nmax < nmin:
            nmax = nmin

        nres_change = np.random.randint(nmin,nmax+1)
        cur_tor_new = ztors1.copy()
        cur_res_source = np.zeros(self.xyzmap['nres'],dtype=bool)
        cur_exresind = []

        for ires_change in range(nres_change):
            best_ene = sys.float_info.max
            best_tor_new = None
            best_res_source = None
            success = False

            exresind = np.random.choice(self.exchange_res_ind,len(self.exchange_res_ind),replace=False)
            for resind in exresind:
                tor_new = cur_tor_new.copy()
                res_source = deepcopy(cur_res_source)
                res_source[resind] = True
                self.perturb_residue(resind+1,iz1,ztors1,tor_new,nfrag=nfrag)

                if ene_cut is not None:
                    mol_util.setint(iz1,zbond1,zang1,tor_new)
                    newvar = mol_util.getxyz()
                    newene = self.energy_func(newvar)
                    if newene <= ene_cut:
                        cur_tor_new = tor_new
                        cur_res_source = res_source
                        success = True
                        break
                    else:
                        if newene < best_ene:
                            best_ene = newene
                            best_tor_new = tor_new
                            best_res_source = res_source
                else:
                    cur_tor_new = tor_new
                    cur_res_source = res_source
                    success = True
                    break

            if not success:
                cur_tor_new = best_tor_new
                cur_res_source = best_res_source
                ene = best_ene
                ene_cut = ene + self.max_energy_diff
                break # energy fails, then exit immediately

        mol_util.setint(iz1,zbond1,zang1,cur_tor_new)
        newvar = mol_util.getxyz()

        return newvar, np.where(cur_res_source)[0].tolist()

    def perturb_residue(self,resnum,iz1,ztors1,tor_new,nfrag=None):
        if nfrag is None:
            nfrag = self.nfrag
        if resnum in self.backtor:
            pfa.perturb_residue(self.backtor,self.exchange_ind,self.xyzmap['data'],resnum,iz1,ztors1,tor_new,nfrag=nfrag,sigma_torsion=self.p['sigma_torsion'],weighted_frag=self.p['weighted_frag'])

class protein_perturb_backtor_cg(mol_perturb):
    pname = 'backtor'

    # construct beta sheet
    def __init__(self,param,amap,energy_func=None):
        p = {
            'nperturb': 0,
            'res_index': None,
            'nmax': 3,
            'nmin': 1,
            'backtor_file': None,
            'sigma_torsion': 1.0,
            'sigma_angle': 1.0,
            'weighted_frag': False,
            'max_energy_diff': None,
        }
        self.p = dict_merge(p,param)
        super(protein_perturb_backtor_cg,self).__init__(self.p,amap)

        # energy function
        if energy_func is None:
            self.max_energy_diff = None
        else:
            self.energy_func = energy_func
            self.max_energy_diff = self.p['max_energy_diff']

        # remove non-protein residues
        for i in range(len(amap['seq'])):
            if ( amap['seq'][i] == 'X' ):
                try:
                    self.residue_index = delete(self.residue_index,i,0)
                except:
                    pass

        self.residue_index = []
        for aind in self.atom_index:
            atom_name = amap['raw_data'][aind][2]
            if atom_name == 'CA':
                self.residue_index.append(amap['raw_data'][aind][4]-1)

        # exchangeable atom indexes
        e_ind = get_atom_index(self.residue_index,self.xyzmap)
        self.exchange_ind = np.array(e_ind)

        # read backtor_file
        self.backtor, self.nfrag = pfa.read_backtor_file(self.p['backtor_file'],cg=True)

        if self.backtor is not None:
            # exchangeable residue indexes
            mol_nres = self.xyzmap['nres']
            min_res = int(self.nfrag/2)
            max_res = mol_nres-int(self.nfrag/2)
            e_res_ind = []
            for ri in self.residue_index:
                if ri >= min_res and ri < max_res:
                    e_res_ind.append(ri)
            self.exchange_res_ind = e_res_ind

    def perturb(self,bvar,ind1,nperturb=None,nmin=None,nmax=None,nfrag=None,multiconf=None):
        if nfrag is None:
            nfrag = self.nfrag
        var1 = bvar[ind1]

        # number of exchanged residues
        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        for i in range(nperturb):
            if multiconf is not None:
                newvar = [None] * multiconf
                res_source = [None] * multiconf
                for j in range(multiconf):
                    res_source[j] = [ ('b',ind1) for x in range(self.xyzmap['nres']) ]
                    newvar[j], exresind = self.perturb_single(var1[j],nmin=nmin,nmax=nmax,nfrag=nfrag)
                    for rind in exresind:
                        res_source[j][rind] = ('backtor',0)
            else:
                res_source = [ ('b',ind1) for x in range(self.xyzmap['nres']) ]
                newvar, exresind = self.perturb_single(var1,nmin=nmin,nmax=nmax,nfrag=nfrag)
                for rind in exresind:
                    res_source[rind] = ('backtor',0)
            newvars.append(newvar)
            perturb_info.append((self.pname,res_source))

        return newvars, perturb_info

    def perturb_single(self,var1,nmin=None,nmax=None,nfrag=None):
        ( iz1, zbond1, zang1, ztors1 ) = mol_util.getint(var1)

        # energy check?
        if self.max_energy_diff is not None:
            ene = self.energy_func(var1)
            ene_cut = ene + self.max_energy_diff
        else:
            ene_cut = None

        nres = len(self.residue_index)

        newvars = []
        perturb_info = []
        if nmax is None:
            nmax = self.p['nmax']
            if nmax is None:
                nmax = ( nres - 1 ) / 2
        if nmin is None:
            nmin = self.p['nmin']
            if nmin is None:
                nmin = 1
        if nmax < nmin:
            nmax = nmin

        nres_change = np.random.randint(nmin,nmax+1)
        cur_tor_new = ztors1.copy()
        cur_ang_new = zang1.copy()
        cur_res_source = np.zeros(self.xyzmap['nres'],dtype=bool)
        cur_exresind = []

        for ires_change in range(nres_change):
            best_ene = sys.float_info.max
            best_tor_new = None
            best_ang_new = None
            best_res_source = None
            success = False

            exresind = np.random.choice(self.exchange_res_ind,len(self.exchange_res_ind),replace=False)
            for resind in exresind:
                tor_new = cur_tor_new.copy()
                ang_new = cur_ang_new.copy()
                res_source = deepcopy(cur_res_source)
                res_source[resind] = True
                self.perturb_residue(resind+1,iz1,ztors1,tor_new,zang1,ang_new,nfrag=nfrag)

                if ene_cut is not None:
                    mol_util.setint(iz1,zbond1,zang1,tor_new)
                    newvar = mol_util.getxyz()
                    newene = self.energy_func(newvar)
                    if newene <= ene_cut:
                        cur_tor_new = tor_new
                        cur_ang_new = ang_new
                        cur_res_source = res_source
                        success = True
                        break
                    else:
                        if newene < best_ene:
                            best_ene = newene
                            best_tor_new = tor_new
                            best_ang_new = ang_new
                            best_res_source = res_source
                else:
                    cur_tor_new = tor_new
                    cur_ang_new = ang_new
                    cur_res_source = res_source
                    success = True
                    break

            if not success:
                cur_tor_new = best_tor_new
                cur_ang_new = best_ang_new
                cur_res_source = best_res_source
                ene = best_ene
                ene_cut = ene + self.max_energy_diff
                break # energy fails, then exit immediately

        mol_util.setint(iz1,zbond1,cur_ang_new,cur_tor_new)
        newvar = mol_util.getxyz()

        return newvar, np.where(cur_res_source)[0].tolist()

    def perturb_residue(self,resnum,iz1,ztors1,tor_new,zang1,ang_new,nfrag=None):
        if nfrag is None:
            nfrag = self.nfrag
        if resnum in self.backtor:
            pfa.perturb_residue_cg(self.backtor,self.exchange_ind,self.xyzmap['data'],resnum,iz1,ztors1,tor_new,zang1,ang_new,nfrag=nfrag,sigma_torsion=self.p['sigma_torsion'],sigma_angle=self.p['sigma_angle'],weighted_frag=self.p['weighted_frag'])

class multiconf_perturb(object):
    pname = 'multiconf_perturb'
    def __init__(self,param,multiconf):
        p = {
            'nperturb': 0,
            'partner_mask': [],
        }
        self.multiconf = multiconf
        self.p = dict_merge(p,param)

class multiconf_perturb_cross(multiconf_perturb):
    pname = 'multiconf_perturb_cross'
    def __init__(self,param,multiconf):
        p = {
            'nperturb': 0,
            'nmax': None,
            'nmin': 1,
            'max_ratio': 0.4,
            'max_shift_ratio': 0.3,
            'partner_mask': [],
        }
        self.p = dict_merge(p,param)

        super(multiconf_perturb_cross,self).__init__(self.p,multiconf)

        nmax = self.p['nmax']
        nmin = self.p['nmin']
        if nmax is None:
            nmax = int(self.multiconf * self.p['max_ratio'])
        if nmax > self.multiconf:
            nmax = self.multiconf
        if nmin is None:
            nmin = 1
        if nmax < nmin:
            nmax = nmin
        self.nmin = nmin
        self.nmax = nmax

        self.max_shift = self.p['max_shift_ratio'] * self.multiconf

        # exchange size weight
        self.esize_weight = np.empty(nmax-nmin+1,dtype=np.double)
        for i in range(nmax-nmin+1):
            esize = i + nmin
            self.esize_weight[i] = self.multiconf - esize + 1
        self.esize_weight /= np.sum(self.esize_weight)

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
            nbank = len(rvar)
        else:
            var1 = bvar[ind1]
            nbank = len(bvar)

        # number of exchanged residues
        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        ap = get_available_partner(nbank,ind1,self.p['partner_mask'])
        for i in range(nperturb):
            ind2 = pick_partner(ap,ind1)
            if src[1] == 'b':
                var2 = bvar[ind2]
            else:
                var2 = rvar[ind2]

            # determine the size of exchange
            esize = np.random.choice(self.esize_weight.size,p=self.esize_weight) + self.nmin

            # residue indices to be exchanged
            istart_ind = np.random.randint(self.multiconf-esize+1)
            istop_ind = istart_ind + esize
            # shift the alignment a bit between var1 and var2
            nshift = np.random.randint(-self.max_shift,self.max_shift+1)
            if istart_ind + nshift < 0:
                nshift = -istart_ind
            if istop_ind + nshift >= self.multiconf:
                nshift = self.multiconf - istop_ind - 1

            # exchange residue index
            exmask = np.zeros(self.multiconf,dtype=np.uint8)
            exmask[istart_ind:istop_ind] = True

            newvar = [None] * self.multiconf
            conf_source = [None] * self.multiconf
            for i in range(self.multiconf):
                if exmask[i]:
                    newvar[i] = var2[i+nshift].copy()
                    conf_source[i] = (src[1],ind2)
                else:
                    newvar[i] = var1[i].copy()
                    conf_source[i] = (src[0],ind1)

            newvars.append(np.array(newvar))
            perturb_info.append( (self.pname,conf_source) )

        return newvars, perturb_info

class multiconf_perturb_residue(multiconf_perturb):
    pname = 'multiconf_perturb_residue'
    def __init__(self,param,multiconf):
        p = {
            'nperturb': 0,
            'nmax': 3,
            'nmin': 1,
            'max_shift_ratio': 0.3,
            'partner_mask': [],
        }
        self.p = dict_merge(p,param)

        super(multiconf_perturb_residue,self).__init__(self.p,multiconf)

        nmax = self.p['nmax']
        nmin = self.p['nmin']
        if nmax is None:
            nmax = self.multiconf
        if nmin is None:
            nmin = 1
        if nmax < nmin:
            nmax = nmin
        self.nmin = nmin
        self.nmax = nmax

        self.max_shift = self.p['max_shift_ratio'] * self.multiconf

    def perturb(self,bvar,rvar,ind1,src='bb',nperturb=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
            nbank = len(rvar)
        else:
            var1 = bvar[ind1]
            nbank = len(bvar)

        # number of exchanged residues
        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        ap = get_available_partner(nbank,ind1,self.p['partner_mask'])
        for i in range(nperturb):
            ind2 = pick_partner(ap,ind1)
            if src[1] == 'b':
                var2 = bvar[ind2]
            else:
                var2 = rvar[ind2]

            # determine the size of exchange
            esize = np.random.randint(self.nmin,self.nmax+1)

            # exchange residue index
            exmask = np.zeros(self.multiconf,dtype=np.uint8)
            ind = np.random.choice(self.multiconf,esize,replace=False)
            minind = np.amin(ind)
            maxind = np.amax(ind)
            exmask[ind] = True
            # shift the alignment a bit between var1 and var2
            nshift = np.random.randint(-self.max_shift,self.max_shift+1)
            if minind + nshift < 0:
                nshift = -minind
            if maxind + nshift >= self.multiconf:
                nshift = self.multiconf - maxind - 1

            newvar = [None] * self.multiconf
            conf_source = [None] * self.multiconf
            for i in range(self.multiconf):
                if exmask[i]:
                    newvar[i] = var2[i+nshift].copy()
                    conf_source[i] = (src[1],ind2)
                else:
                    newvar[i] = var1[i].copy()
                    conf_source[i] = (src[0],ind1)

            newvars.append(np.array(newvar))
            perturb_info.append( (self.pname,conf_source) )

        return newvars, perturb_info

class multiconf_perturb_2opt(multiconf_perturb):
    pname = 'multiconf_perturb_2opt'
    def __init__(self,param,multiconf):
        p = {
            'nperturb': 0,
        }
        self.p = dict_merge(p,param)

        super(multiconf_perturb_2opt,self).__init__(self.p,multiconf)

    def perturb(self,bvar,rvar,ind1,src='b',nperturb=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
            nbank = len(rvar)
        else:
            var1 = bvar[ind1]
            nbank = len(bvar)

        # number of exchanged residues
        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        for i in range(nperturb):
            ind = np.random.choice(self.multiconf,2,replace=False)
            start_ind = ind[0]
            stop_ind = ind[1]
            if start_ind > stop_ind:
                start_ind, stop_ind = stop_ind, start_ind

            newvar = [None] * self.multiconf
            conf_source = [(src[0],ind1)] * self.multiconf
            for i in range(start_ind):
                newvar[i] = var1[i].copy()
            for i in range(start_ind,stop_ind+1):
                newvar[i] = var1[stop_ind-i+start_ind].copy()
            for i in range(stop_ind+1,self.multiconf):
                newvar[i] = var1[i].copy()

            newvars.append(np.array(newvar))
            perturb_info.append( (self.pname,conf_source) )

        return newvars, perturb_info

class multiconf_perturb_average(multiconf_perturb):
    pname = 'multiconf_perturb_average'
    def __init__(self,param,multiconf):
        p = {
            'nperturb': 0,
            'internal_coord': True,
        }
        self.p = dict_merge(p,param)

        super(multiconf_perturb_average,self).__init__(self.p,multiconf)

    def perturb(self,bvar,rvar,nv0,nv1,ind1,src='b',nperturb=None):
        if ( src[0] == 'r' ):
            var1 = rvar[ind1]
            nbank = len(rvar)
        else:
            var1 = bvar[ind1]
            nbank = len(bvar)

        # number of exchanged residues
        if nperturb is None:
            nperturb = self.p['nperturb']

        newvars = []
        perturb_info = []
        for i in range(nperturb):
            ind = np.random.randint(self.multiconf)
            if ind == 0:
                v0 = nv0
            else:
                v0 = var1[ind-1]
            if ind == self.multiconf-1:
                v1 = nv1
            else:
                v1 = var1[ind+1]

            newvar = deepcopy(var1)
            if self.p['internal_coord']:
                newvar[ind] = perturb_average_int(v0,v1,0.5)
            else:
                newvar[ind] = ( v0 + v1 ) * 0.5

            conf_source = [(src[0],ind1)] * self.multiconf

            newvars.append(newvar)
            perturb_info.append( (self.pname,conf_source) )

        return newvars, perturb_info

