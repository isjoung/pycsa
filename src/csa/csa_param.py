"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung and Seung Hwan Hong
Contributors: Jooyoung Lee

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import division
from __future__ import absolute_import
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import range
import os
import numpy as np
import pickle
from .csa import *
import param_util as pu
from perturb_util import get_available_partner
import copy
from scipy import optimize
from math import log, exp

BIG = np.finfo('d').max/1.0e6

class csa_param(csa):
    """csa for parameter optimization"""

    def __init__(self,param={}):
        p = {
            'nbank': 50,
            'nseed': 30,
            'icmax': 4,
            'iucut': 5,
            'decut': 0.001,
            'dcut1': 2.0,
            'dcut2': 3.0,
            'dcut_reduce': (2./3.)**(1.0/20),
            'nbank_add': 0,

            'save_restart': 1,

            'history_file': 'history',
            'append_history': False,

            'perturb_crossover': { 'nperturb': 0, 'partner_mask': [], 'max_ratio': 0.5, },
            'perturb_crossover2': { 'nperturb': 0, 'partner_mask': [], 'n_new_bank_cut': 2, 'max_ratio': 0.2, },
            'perturb_random': { 'nperturb': 0, 'nmin': 1, 'nmax': 3, 'perturb_ratio': 0.2, },
            'perturb_differential_evolution': { 'nperturb': 0, 'mutation': (0.5,1.0), 'recombination': 0.5, 'n_cross': 1, 'partner_mask': [] },

            'write_bank': False,
            'write_score_profile': True,
            'bank_history_precision': 12,
            'record_gmin': False,
            'profile_precision': 12,

            'parallel_minimization': 2,

            'minimize_seed': False,
            'native_var': None,

            # problem specific parameters
            'param_range': [], # eg. [ (min1, max1), (min2, max2), ... ]
            'min_maxiter': 2000, # max iter for nelder-mead minimization
            'min_perturb_ratio': 0.1, # number or list of steps
            'param_scale': 'linear', # linear/log or list of linear/log
            'min_tol': 0.0001, # minimum distances between solutions in nelder-mead minimization
            'energy_function': None,

            'min_method': 'nelder-mead', # nelder-mead, L-BFGS-B, TNC, SLSQP, Powell
        }

        self.p = dict_merge(p,param)

        # check energy function
        self.energy_function = self.p['energy_function']

        # init for csa parameter
        super(csa_param,self).__init__(self.p)

        # check param_range
        self.nparam = len(self.p['param_range'])

        # min_perturb_step
        if isinstance( self.p['min_perturb_ratio'], list ) or isinstance( self.p['min_perturb_ratio'], tuple ):
            if len(self.p['min_perturb_ratio']) != self.nparam:
                self.print_error("min_perturb_ratio does not match param_range")
            self.min_perturb_ratio = copy.copy(self.p['min_perturb_ratio'])
        else:
            self.min_perturb_ratio = [ self.p['min_perturb_ratio'] ] * self.nparam

        # param_scale
        if isinstance( self.p['param_scale'], list ) or isinstance( self.p['param_scale'], tuple ):
            if len(self.p['param_scale']) != self.nparam:
                self.print_error("param_scale does not match param_range")
            self.param_scale = copy.copy(self.p['param_scale'])
        else:
            self.param_scale = [ self.p['param_scale'] ] * self.nparam

        # prepare minimization
        pu.prepare_minimization(self)

    def check_param(self):
        super(csa_param,self).check_param()
        if self.energy_function is None:
            self.print_error('energy_function is not specified')

    def minimizef(self,var):
        mm = self.p['min_method']
        if mm == 'nelder-mead':
            score, minvar, nfe = pu.min_neldermead(var)
        else:
            x0 = var.copy()
            N = len(x0)

            # bound check for SLSQP
            if mm == 'SLSQP':
                pr = self.p['param_range']
                for i in range(N):
                    if x0[i] < pr[i][0]:
                        x0[i] = pr[i][0]
                    elif x0[i] > pr[i][1]:
                        x0[i] = pr[i][1]

            # log scale
            param_scale = self.p['param_scale']
            for i in range(N):
                if param_scale[i] == 'log':
                    x0[i] = log(x0[i])

            opt = {'maxiter': self.p['min_maxiter']}
            #opt = {'maxiter': self.p['min_maxiter'], 'gtol': self.p['min_tol'], 'xtol': self.p['min_tol']}
            if mm == 'L-BFGS-B' or mm == 'TNC': # or mm == 'SLSQP': # 'SLSQP bounds' is not so reliable
                res = optimize.minimize(pu.eval_score_nobound, x0, method=mm, bounds=self.p['param_range'], options=opt)
            else:
                res = optimize.minimize(pu.eval_score_bound, x0, method=mm, options=opt)

            score = res.fun
            if np.isnan(score):
                score = BIG
            minvar = res.x
            nfe = res.nfev

            # change logarithmic x0 back to linear scale
            for i in range(N):
                if param_scale[i] == 'log':
                    minvar[i] = exp(minvar[i])

            # bound check for SLSQP
            if mm == 'SLSQP':
                pr = self.p['param_range']
                fixed = False
                for i in range(N):
                    if minvar[i] < pr[i][0]:
                        minvar[i] = pr[i][0]
                        fixed = True
                    elif minvar[i] > pr[i][1] or np.isnan(minvar[i]):
                        minvar[i] = pr[i][1]
                        fixed = True
                if fixed:
                    score = self.energy_function(minvar)
                    nfe += 1

        return score, minvar, nfe

    def unpack_min_output(self,min_output,i_list):
        for ind in range(len(i_list)):
            i = i_list[ind]
            mo = min_output[ind]
            score, minvar, nfe = mo
            self.mvar.append(minvar)
            self.mscore.append(score)
            try:
                # In case of randomization, perturb_info is absent.
                self.perturb_info.append( self.perturb_info_backup[i] )
            except:
                pass
            try:
                self.minimize_nfe += nfe
            except:
                self.minimize_nfe = nfe
            self.print_log("m%d score: %.5E"%(len(self.mvar),score), level=3)

    def restart_key_list(self):
        return super(csa_param,self).restart_key_list() + [ 'minimize_nfe' ]

    def write_history_file_header(self):
        pre = str(self.p['profile_precision'])
        fmt = '%6s %3s %10s %4s %4s %' + pre + 's %' + pre + 's %15s %4s %4s\n'
        self.history_fh.write(fmt%('#niter','cyc','cutdiff','imn','imx','min_score','max_score','nfe','iuse','nbk'))

    def write_history_file(self):
        self.print_log( "Writing history", level=4 )
        pre0 = self.p['profile_precision']
        pre1 = pre0 - 7
        fmt_e = ' %' + str(pre0) + '.' + str(pre1) + 'E'
        fmt = '%6d %3d %10.4E %4d %4d' + fmt_e + fmt_e + ' %15d %4d %4d\n'
        self.history_fh.write(fmt
            %(self.niter,self.ncycle,self.dcut,self.minscore_ind+1,self.maxscore_ind+1,self.minscore,self.maxscore,self.minimize_nfe,self.count_unused_bank(),self.count_bank()))
        self.history_fh.flush()

    def comparef(self,var1,var2):
        distance = pu.param_distance(var1,var2)
        return distance

    def randvarf(self):
        var = np.empty(self.nparam,dtype=float)
        for i in range(self.nparam):
            r0, r1 = self.p['param_range'][i]
            pscale = self.param_scale[i]
            if pscale == 'linear':
                var[i] = (r1-r0)*np.random.rand() + r0
            elif pscale == 'log':
                var[i] = exp((log(r1)-log(r0))*np.random.rand() + log(r0))
            else:
                raise ValueError('wrong param_scale')
        return var

    def perturbvarf(self,ind1):
        nbank = len(self.bvar)
        newvars = []
        perturb_info = []

        # perturb_crossover
        p_param = self.p['perturb_crossover']
        for i in range(p_param['nperturb']):
            ( newvar, pinfo ) = self.perturb_crossover(ind1,'crossover',p_param['partner_mask'],src='bb',max_ratio=p_param['max_ratio'])
            newvars.append(newvar)
            perturb_info.append(pinfo)

        # perturb_crossover2
        p_param = self.p['perturb_crossover2']
        for i in range(p_param['nperturb']):
            iuse = self.count_unused_bank()
            iuse_cut = len(self.bvar)-len(self.seed)-p_param['n_new_bank_cut']
            if self.ncycle == 0 and iuse > iuse_cut:
                ( newvar, pinfo ) = self.perturb_crossover(ind1,'crossover2',p_param['partner_mask'],src='rr',max_ratio=p_param['max_ratio'])
                newvars.append(newvar)
                perturb_info.append(pinfo)
            else:
                ( newvar, pinfo ) = self.perturb_crossover(ind1,'crossover2',p_param['partner_mask'],src='br',max_ratio=p_param['max_ratio'])
                newvars.append(newvar)
                perturb_info.append(pinfo)

        # perturb_random
        p_param = self.p['perturb_random']
        for i in range(p_param['nperturb']):
            ( newvar, pinfo ) = self.perturb_random(ind1,'random',p_param['perturb_ratio'],p_param['nmin'],p_param['nmax'])
            newvars.append(newvar)
            perturb_info.append(pinfo)

        # perturb_crossover
        p_param = self.p['perturb_differential_evolution']
        for i in range(p_param['nperturb']):
            ( newvar, pinfo ) = self.perturb_differential_evolution(ind1,'DE',p_param['partner_mask'],mutation=p_param['mutation'],recombination=p_param['recombination'],n_cross=p_param['n_cross'])
            newvars.append(newvar)
            perturb_info.append(pinfo)

        return newvars, perturb_info

    def write_bank(self,prefix,suffix,vars,scores=None):
        for i in range(len(vars)):
            var = vars[i]
            outfile = '%s%03d%s'%(prefix,i+1,suffix)
            self.print_param(var,outfile,scores[i])
        if ( scores ):
            imin = scores.index(min(scores))
            outfile = 'gmin%s'%suffix
            self.print_param(vars[imin],outfile)

    def print_param(self,var,outfile,score=None):
        f = open(outfile,'w')
        # param
        f.write("# parameters\n")
        for j in range(self.nparam):
            f.write('%f\n'%var[j])
        f.write('\n')
        # score
        if score is not None:
            f.write('score: %f\n'%score)

    def eval_update_final(self):
        super(csa_param,self).eval_update_final()

        # calculate acceptace ratio
        count = {}
        accept = {}
        for i in range(len(self.update_mvar)):
            try:
                # dict perturb info
                pname = self.perturb_info[i]['name']
            except:
                pname = self.perturb_info[i][0]
            status = self.update_mvar[i]
            if ( pname not in count ):
                count[pname] = 0
            if ( pname not in accept ):
                accept[pname] = 0
            if ( status > 0 ):
                accept[pname] += 1
            count[pname] += 1
        # print acceptance ratio
        for pname in sorted( count.keys() ):
            if ( count[pname] == 0 ):
                ratio = 0.0
            else:
                ratio = float(accept[pname]) / float(count[pname])
            try:
                self.accept_ratio[pname] = ratio
            except:
                self.accept_ratio = {}
                self.accept_ratio[pname] = ratio
            self.print_log( "acceptance ratio of %s is %f"%(pname,ratio) )
        # print detailed perturb info
        for i in range(len(self.update_bvar)):
            mi = self.update_bvar[i]
            if ( mi < 0 ): continue
            delta_score = self.bscore[i]-self.mscore[mi]
            line = 'new b%d: %s'%(i+1,pname)
            if isinstance(self.perturb_info[mi],dict):
                pratio = self.perturb_info[mi]['ratio']
                for key in pratio:
                    bank_type, bank_ind = key
                    line += ' %s%d(%.2f)'%(bank_type,bank_ind+1,pratio[key])
            else:
                pname, src = self.perturb_info[mi]
                for s in src:
                    line += ' %s(%.2f)'%s
            line += ' S(%.7E) dS(%.7E)'%(self.mscore[mi],delta_score)
            self.print_log(line)

    def report_iterate(self):
        super(csa_param,self).report_iterate()
        if mpirank != 0:
            return
        if ( self.p['record_gmin'] and self.update_bvar[self.minscore_ind] >= 0 ):
            self.print_param(self.bvar[self.minscore_ind],'gmin%05d'%self.niter,score=self.minscore)

    def perturbvar(self):
        super(csa_param,self).perturbvar()

        if mpirank != 0: return

        if self.p['minimize_seed']:
            # add seed conformations to the perturbed conformation list
            for ind1 in self.seed:
                self.mvar.append(self.bvar[ind1].copy())
                self.perturb_info.append( ( 'seed', ('b%d'%(ind1+1),1.0) ) )

    def perturb_crossover(self,ind1,pname,partner_mask,src='bb',max_ratio=0.5):
        nbank = len(self.bvar)
        if src[0] == 'b':
            var1 = self.bvar[ind1]
        else:
            var1 = self.rvar[ind1]

        n_opt = self.nparam
        n_exchange = np.random.randint(1,max(2,n_opt*max_ratio))

        ap = get_available_partner(nbank,ind1,partner_mask)
        ind2 = np.random.choice(ap)
        if src[1] == 'b':
            var2 = self.bvar[ind2]
        else:
            var2 = self.bvar[ind2]

        exind = np.random.choice( self.nparam, n_exchange, replace=False )
        newvar = var1.copy()
        newvar[exind] = var2[exind]

        #pinfo = ( pname, ( ('%s%d'%(src[0],ind1+1),float(n_opt-n_exchange)/n_opt), ('%s%d'%(src[1],ind2+1),float(n_exchange)/n_opt) ) )
        pinfo = { 'name': pname,
                  'seed': ind1,
                  'ratio': { (src[0],ind1): float(n_opt-n_exchange)/n_opt,
                             (src[1],ind2): float(n_exchange)/n_opt }
                }

        return newvar, pinfo

    def perturb_differential_evolution(self,ind1,pname,partner_mask,mutation=(0.5,1.0),recombination=0.7,n_cross=1):
        nbank = len(self.bvar)
        var1 = self.bvar[ind1]

        n_opt = self.nparam

        ap = get_available_partner(nbank,ind1,partner_mask)
        ind2, ind3, ind4 = np.random.choice(ap,3,replace=False)
        var2 = self.bvar[ind2]
        var3 = self.bvar[ind3]
        var4 = self.bvar[ind4]

        mut = np.random.uniform( mutation[0], mutation[1] )
        yvar = var2 + mut * ( var3 - var4 )

        n_exchange = max(n_cross, np.count_nonzero( np.random.rand(n_opt) < recombination ))

        newvar = var1.copy()
        for i in np.random.choice(n_opt,n_exchange,replace=False):
            newvar[i] = yvar[i]

        #pinfo = ( pname, ( ('b%d'%(ind1+1),float(n_opt-n_exchange)/n_opt), ('b%d'%(ind2+1),float(n_exchange)/n_opt) ) )
        pinfo = { 'name': pname,
                  'seed': ind1,
                  'ratio': { ('b',ind1): float(n_opt-n_exchange)/n_opt,
                             ('b',ind2): float(n_exchange)/n_opt }
                }

        return newvar, pinfo

    def perturb_random(self,ind1,pname,perturb_ratio,nmin,nmax):
        var1 = self.bvar[ind1]
        n_exchange = np.random.randint(nmin,nmax)
        exind = np.random.choice( self.nparam, n_exchange, replace=False )
        newvar = var1.copy()

        for ei in exind:
            r0, r1 = self.p['param_range'][ei]
            if self.param_scale[ei] == 'linear':
                newvar[ei] = newvar[ei] + (r1-r0)*perturb_ratio * (2.0*np.random.random()-1.0)
            elif self.param_scale[ei] == 'log':
                newvar[ei] = newvar[ei] + np.exp( (log(r1)-log(r0))*perturb_ratio * (2.0*np.random.random()-1.0) )
            else:
                raise ValueError('wrong param_scale')
            if newvar[ei] < r0:
                newvar[ei] = r0
            elif newvar[ei] > r1:
                newvar[ei] = r1

        #pinfo = ( pname, ( ('b%d'%(ind1+1),float(self.nparam-n_exchange)/self.nparam), ) )
        pinfo = { 'name': pname,
                  'seed': ind1,
                  'ratio': { ('b',ind1): float(self.nparam-n_exchange)/self.nparam }
                }

        return newvar, pinfo
