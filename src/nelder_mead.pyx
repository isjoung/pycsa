# cython: profile=False, boundscheck=False, wraparound=False, language_level=3
"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung, Scipy developers
Contributors:

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import numpy as np
cimport numpy as cnp
cimport cython

# global variables
#cdef cfptr nm_function
#cdef int maxiter
#cdef double [:] max_param
#cdef double [:] min_param
#cdef double perturb_ratio
#cdef double xtol

@cython.wraparound(True)
cdef tuple minimize_neldermead(cnp.ndarray [cnp.double_t,ndim=1] x0):
    cdef cnp.ndarray [cnp.double_t,ndim=1] y
    cdef cnp.ndarray [cnp.double_t,ndim=1] xr
    cdef cnp.ndarray [cnp.double_t,ndim=1] xe
    cdef cnp.ndarray [cnp.double_t,ndim=1] xc
    cdef cnp.ndarray [cnp.double_t,ndim=1] xcc
    cdef cnp.ndarray [cnp.double_t,ndim=1] xbar
    cdef int N = x0.size
    cdef int rho, chi
    cdef double psi, sigma
    cdef list one2np1
    cdef cnp.ndarray [cnp.double_t,ndim=2] sim  # (N+1,N)
    cdef cnp.ndarray [cnp.double_t,ndim=1] fsim # N+1
    cdef int i, j, k
    cdef int iterations
    cdef cnp.ndarray [cnp.int64_t,ndim=1] ind
    cdef double fxr, fxe
    cdef bint doshrink
    cdef int eval_count

    global maxiter
    global max_param
    global min_param
    global perturb_ratio
    global xtol
    global nm_function

    """ This subroutine is copied from scipy and modified """

    eval_count = 0

    rho = 1
    chi = 2
    psi = 0.5
    sigma = 0.5
    one2np1 = list(range(1, N + 1))

    sim = np.zeros((N + 1, N), dtype=x0.dtype)
    fsim = np.zeros((N + 1,), dtype=np.double)
    sim[0] = x0
    fsim[0] = nm_function(x0)
    eval_count += 1

    for k in range(0, N):
        y = np.empty(N,dtype=np.double)
        for i in range(N):
            y[i] = ( max_param[i] - min_param[i] ) * perturb_ratio  * ( 2.0 * np.random.random() - 1.0 ) + x0[i]
        sim[k + 1] = y
        f = nm_function(y)
        eval_count += 1
        fsim[k + 1] = f

    ind = np.argsort(fsim)
    fsim = np.take(fsim, ind, 0)
    # sort so sim[0,:] has the lowest function value
    sim = np.take(sim, ind, 0)

    iterations = 1

    while iterations < maxiter:
        if np.max(np.ravel(np.abs(np.exp(sim[1:]) - np.exp(sim[0])))) <= xtol:
            break

        xbar = np.add.reduce(sim[:-1], 0) / N
        xr = (1 + rho) * xbar - rho * sim[-1]
        fxr = nm_function(xr)
        eval_count += 1
        doshrink = 0

        if fxr < fsim[0]:
            xe = (1 + rho * chi) * xbar - rho * chi * sim[-1]
            fxe = nm_function(xe)
            eval_count += 1

            if fxe < fxr:
                sim[-1] = xe
                fsim[-1] = fxe
            else:
                sim[-1] = xr
                fsim[-1] = fxr
        else:  # fsim[0] <= fxr
            if fxr < fsim[-2]:
                sim[-1] = xr
                fsim[-1] = fxr
            else:  # fxr >= fsim[-2]
                # Perform contraction
                if fxr < fsim[-1]:
                    xc = (1 + psi * rho) * xbar - psi * rho * sim[-1]
                    fxc = nm_function(xc)
                    eval_count += 1

                    if fxc <= fxr:
                        sim[-1] = xc
                        fsim[-1] = fxc
                    else:
                        doshrink = 1
                else:
                    # Perform an inside contraction
                    xcc = (1 - psi) * xbar + psi * sim[-1]
                    fxcc = nm_function(xcc)
                    eval_count += 1

                    if fxcc < fsim[-1]:
                        sim[-1] = xcc
                        fsim[-1] = fxcc
                    else:
                        doshrink = 1

                if doshrink:
                    for j in one2np1:
                        sim[j] = sim[0] + sigma * (sim[j] - sim[0])
                        fsim[j] = nm_function(sim[j])
                        eval_count += 1

        ind = np.argsort(fsim)
        sim = np.take(sim, ind, 0)
        fsim = np.take(fsim, ind, 0)
        iterations += 1

    x0 = sim[0]
    var = x0.copy()

    return np.min(fsim), var, eval_count
