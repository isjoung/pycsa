# cython: language_level=3
"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung
Contributors:

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import numpy as np
cimport numpy as np
from libc.math cimport sqrt

def initialize_neb(self):
    self.neb_var0 = self.native_var[ self.p['neb_end'][0] ].copy()
    self.neb_ene0 = self.get_energy(self.neb_var0)
    self.neb_var1 = self.native_var[ self.p['neb_end'][1] ].copy()
    self.neb_ene1 = self.get_energy(self.neb_var1)
    if self.p['neb_fit'] is None:
        self.neb_fit_ind = np.arange(self.xyzmap['natom'],dtype=np.int32)
    else:
        self.neb_fit_ind = np.unique(self.p['neb_fit']).astype(np.int32)
    if self.p['neb_rms'] is None:
        self.neb_rms_ind = np.arange(self.xyzmap['natom']).astype(np.int32)
    else:
        self.neb_rms_ind = np.unique(self.p['neb_rms']).astype(np.int32)

    # center neb coord
    center_coord(self.neb_var0, self.neb_fit_ind)
    center_coord(self.neb_var1, self.neb_fit_ind)

cdef void center_coord( np.ndarray [np.double_t,ndim=2] var, np.ndarray [np.int32_t,ndim=1] neb_fit_ind):
    cdef int n_coord = var.shape[0]
    cdef int n_ind = neb_fit_ind.size
    cdef int i, j
    cdef double [:] center = np.zeros(3,dtype=np.double)

    for j in range(n_ind):
        i = neb_fit_ind[j]
        center[0] += var[i,0]
        center[1] += var[i,1]
        center[2] += var[i,2]

    center[0] /= n_ind
    center[1] /= n_ind
    center[2] /= n_ind

    for i in range(n_coord):
        var[i,0] -= center[0]
        var[i,1] -= center[1]
        var[i,2] -= center[2]

def neb_energy_force(self, np.ndarray [np.double_t,ndim=3] input):
    cdef np.ndarray [np.double_t,ndim=2] end_var0
    cdef np.ndarray [np.double_t,ndim=2] end_var1
    cdef np.ndarray [np.double_t,ndim=2] var0
    cdef np.ndarray [np.double_t,ndim=2] var1
    cdef np.ndarray [np.int32_t,ndim=1] neb_fit_ind
    cdef np.ndarray [np.int32_t,ndim=1] neb_rms_ind
    cdef np.ndarray [np.double_t,ndim=3] mcrot
    cdef np.ndarray [np.double_t,ndim=2] rot
    cdef double neb_fc1, neb_fc2, neb_fc3, rcut
    cdef double fc1, fc2p, fc2m
    cdef double total_ene, ene0, ene1, ene
    cdef int nconf, natom, naxis
    cdef int n_fit_ind, n_rms_ind
    cdef np.ndarray [np.double_t,ndim=3] total_frc
    cdef np.ndarray [np.double_t,ndim=2] taum
    cdef np.ndarray [np.double_t,ndim=2] taup
    cdef np.ndarray [np.double_t,ndim=2] tau
    cdef np.ndarray [np.double_t,ndim=1] ene_mc
    cdef double d0, d1, dd, ddcutp, ddcutm, dij
    cdef int iseq, jseq, irms, j, k
    cdef np.ndarray [np.double_t,ndim=1] spring_ene1
    cdef np.ndarray [np.double_t,ndim=1] spring_ene2
    cdef np.ndarray [np.double_t,ndim=1] spring_ene3
    cdef np.ndarray [np.double_t,ndim=1] dmax
    cdef double vmax, vmin, dtau
    cdef double facp, facm
    cdef double edx, edy, edz, xrot, yrot, zrot

    neb_fit_ind = self.neb_fit_ind
    neb_rms_ind = self.neb_rms_ind
    n_fit_ind = neb_fit_ind.size
    n_rms_ind = neb_rms_ind.size
    nconf = input.shape[0]
    natom = input.shape[1]
    naxis = input.shape[2]
    total_frc = np.zeros((nconf,natom,naxis),dtype=np.double)
    mcrot = np.empty((nconf,naxis,naxis),dtype=np.double)
    dmax = np.empty(nconf,dtype=np.double)

    spring_ene1 = np.zeros(nconf,dtype=np.double)
    spring_ene2 = np.zeros(nconf,dtype=np.double)
    spring_ene3 = np.zeros(nconf,dtype=np.double)

    ene_mc = np.empty(nconf,dtype=np.double)
     
    end_var0 = self.neb_var0
    end_var1 = fit_neb_coord(input,self.neb_var0,self.neb_var1,self.neb_fit_ind)
    end_ene0 = self.neb_ene0
    end_ene1 = self.neb_ene1

    neb_fc1, neb_fc2, neb_fc3 = self.p['neb_force']
    rcut = self.p['neb_rcut']

    total_ene = 0.0
    for iseq in range(nconf):
        var = input[iseq]
        ene_mc[iseq], total_frc[iseq] = self.get_energy_force(var)
        total_ene += ene_mc[iseq]

    taum = np.empty((natom,naxis),dtype=np.double)
    taup = np.empty((natom,naxis),dtype=np.double)
    tau = np.empty((natom,naxis),dtype=np.double)

    for iseq in range(nconf):
        var = input[iseq]
        ene = ene_mc[iseq]
        var0, var1, ene0, ene1 = get_neb_neighbor(iseq, input, ene_mc, end_var0, end_var1, end_ene0, end_ene1)
        d1 = 0.0
        d0 = 0.0
        for irms in range(n_rms_ind):
            j = neb_rms_ind[irms]
            for k in range(naxis):
                # tau_minus and tau_plus
                taum[j,k] = var[j,k] - var0[j,k]
                taup[j,k] = var1[j,k] - var[j,k]
                # calculate d1 = abs(R(i+1)-R(i)), d0 = abs(R(i)-R(i-1))
                d0 += taum[j,k]**2
                d1 += taup[j,k]**2
        d1 = sqrt(d1)
        d0 = sqrt(d0)
        dd = d1 - d0 
        ddcutp = max( d1 - rcut, 0.0 )
        ddcutm = max( d0 - rcut, 0.0 )
        fc1 = neb_fc1 * dd
        fc2p = neb_fc2 * ddcutp
        fc2m = neb_fc2 * ddcutm
        spring_ene1[iseq] = fc1 * dd * 0.5
        spring_ene2[iseq] = ( fc2p * ddcutp + fc2m * ddcutm ) * 0.5
        total_ene += spring_ene1[iseq] + spring_ene2[iseq]
        if d0 > d1:
            dmax[iseq] = d0
        else:
            dmax[iseq] = d1

        # calculate tau
        if ene1 > ene and ene > ene0:
            # tau is tau_plus
            for irms in range(n_rms_ind):
                j = neb_rms_ind[irms]
                for k in range(naxis):
                    tau[j,k] = taup[j,k]
        elif ene1 < ene and ene < ene0:
            # tau is tau_minus
            for irms in range(n_rms_ind):
                j = neb_rms_ind[irms]
                for k in range(naxis):
                    tau[j,k] = taum[j,k]
        else:
            vmax = abs(ene1-ene)
            vmin = abs(ene0-ene)
            if vmin > vmax:
                vmin, vmax = vmax, vmin
            if ene1 > ene0:
                for irms in range(n_rms_ind):
                    j = neb_rms_ind[irms]
                    for k in range(naxis):
                        tau[j,k] = taup[j,k] * vmax + taum[j,k] * vmin
            else:
                for irms in range(n_rms_ind):
                    j = neb_rms_ind[irms]
                    for k in range(naxis):
                        tau[j,k] = taup[j,k] * vmin + taum[j,k] * vmax

        # calculate the length of tau
        dtau = 0.0
        for irms in range(n_rms_ind):
            j = neb_rms_ind[irms]
            for k in range(naxis):
                dtau += tau[j,k]**2
        dtau = sqrt(dtau)
        if dtau == 0.0:
            dtau = 1e-50

        # remove force parallel and add spring parallel force
        # calcuate the length of tau
        facp = fc2p/d1
        facm = fc2m/d0
        for irms in range(n_rms_ind):
            j = neb_rms_ind[irms]
            for k in range(naxis):
               # fc1 * tau/dtau: spring parallel derivative
               # deriv * tau/dtau: original parallel derivative 
               # facm * taum - facp * taup: derivative for the absolute length of the chain
               total_frc[iseq,j,k] +=   ( fc1 - total_frc[iseq,j,k] ) * tau[j,k]/dtau \
                                      - facm * taum[j,k] + facp * taup[j,k]

    if neb_fc3 > 0.0:
        for iseq in range(nconf):
            mcrot[iseq,0,0] = 1.0
            mcrot[iseq,0,1] = 0.0
            mcrot[iseq,0,2] = 0.0
            mcrot[iseq,1,0] = 0.0
            mcrot[iseq,1,1] = 1.0
            mcrot[iseq,1,2] = 0.0
            mcrot[iseq,2,0] = 0.0
            mcrot[iseq,2,1] = 0.0
            mcrot[iseq,2,2] = 1.0

        for iseq in range(nconf):
            var0 = input[iseq]
            for jseq in range(iseq+2,nconf):
                var1 = input[jseq]
                rot = rms_rotate(var0,var1)
                # forward rotation matrix
                mcrot[jseq] = np.dot(mcrot[jseq], rot)
                dij = 0.0
                for irms in range(n_rms_ind):
                    j = neb_rms_ind[irms]
                    for k in range(naxis):
                        tau[j,k] = var0[j,k] - var1[j,k]
                        dij += tau[j,k]**2
                dij = sqrt(dij)
                d0 = dmax[iseq] - dij
                d1 = dmax[jseq] - dij
                fac = 0.0
                if d0 > 0.0:
                    ene = neb_fc3 * d0*d0
                    spring_ene3[iseq] += ene*0.5
                    spring_ene3[jseq] += ene*0.5
                    total_ene += ene
                    fac += d0
                if d1 > 0.0:
                    ene = neb_fc3 * d1*d1
                    spring_ene3[iseq] += ene*0.5
                    spring_ene3[jseq] += ene*0.5
                    total_ene += ene
                    fac += d1
                if fac > 0.0:
                    fac *= neb_fc3 * 2.0 / dij
                    for irms in range(n_rms_ind):
                        j = neb_rms_ind[irms]
                        edx = tau[j,0]*fac
                        edy = tau[j,1]*fac
                        edz = tau[j,2]*fac
                        # inverse transformation
                        xrot = edx*mcrot[iseq,0,0] + edy*mcrot[iseq,0,1] + edz*mcrot[iseq,0,2]
                        yrot = edx*mcrot[iseq,1,0] + edy*mcrot[iseq,1,1] + edz*mcrot[iseq,1,2]
                        zrot = edx*mcrot[iseq,2,0] + edy*mcrot[iseq,2,1] + edz*mcrot[iseq,2,2]
                        total_frc[iseq,j,0] += xrot
                        total_frc[iseq,j,1] += yrot
                        total_frc[iseq,j,2] += zrot
                        # inverse transformation
                        xrot = edx*mcrot[jseq,0,0] + edy*mcrot[jseq,0,1] + edz*mcrot[jseq,0,2]
                        yrot = edx*mcrot[jseq,1,0] + edy*mcrot[jseq,1,1] + edz*mcrot[jseq,1,2]
                        zrot = edx*mcrot[jseq,2,0] + edy*mcrot[jseq,2,1] + edz*mcrot[jseq,2,2]
                        total_frc[jseq,j,0] -= xrot
                        total_frc[jseq,j,1] -= yrot
                        total_frc[jseq,j,2] -= zrot

    return total_ene, total_frc, (ene_mc, spring_ene1, spring_ene2, spring_ene3)

cdef tuple get_neb_neighbor( int i, 
    np.ndarray [np.double_t,ndim=3] input,
    np.ndarray [np.double_t,ndim=1] ene,
    np.ndarray [np.double_t,ndim=2] end_var0,
    np.ndarray [np.double_t,ndim=2] end_var1,
    double end_ene0, end_ene1 ):
    cdef int nconf = input.shape[0]

    if i == 0:
        return end_var0, input[i+1], end_ene0, ene[i+1]
    elif i == nconf-1:
        return input[i-1], end_var1, ene[i-1], end_ene1
    else:
        return input[i-1], input[i+1], ene[i-1], ene[i+1]
 
cdef np.ndarray [np.double_t,ndim=2] fit_neb_coord(input,end_var0,end_var1,neb_fit_ind):
    """ fit input coordinates and return the coordinates of the second end point """
    cdef int nconf = input.shape[0]
    cdef np.ndarray [np.double_t,ndim=2] var0
    cdef np.ndarray [np.double_t,ndim=2] var1
    cdef np.ndarray [np.double_t,ndim=2] U
    cdef int i

    # center
    for i in range(nconf):
        center_coord(input[i], neb_fit_ind)
    
    # rms fit
    var0 = end_var0
    for i in xrange(nconf):
        var1 = input[i]
        U = rms_rotate(var0,var1)
        input[i] = np.dot( var1, U )
        var0 = input[i]
    U = rms_rotate(var0,end_var1)
    var1 = np.dot( end_var1, U )

    return var1

cdef np.ndarray [np.double_t,ndim=2] rms_rotate( np.ndarray [np.double_t,ndim=2] ref_coord,
    np.ndarray [np.double_t,ndim=2] coord ):
    cdef int ncoord = ref_coord.shape[0]
    cdef int naxis  = ref_coord.shape[1]
    cdef np.ndarray [np.double_t,ndim=2] C = np.zeros((naxis,naxis),dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=2] r1
    cdef np.ndarray [np.double_t,ndim=1] s
    cdef np.ndarray [np.double_t,ndim=2] r2
    cdef np.ndarray [np.double_t,ndim=2] U = np.zeros((naxis,naxis),dtype=np.double)

    # covariance matrix
    # cythonized
    #C = np.dot( coord.T, ref_coord )
    for i in range(naxis):
        for j in range(naxis):
            for k in range(ncoord):
                C[i,j] += coord[k,i] * ref_coord[k,j]

    # Singular Value Decomposition
    ( r1, s, r2 ) = np.linalg.svd(C)

    # compute sign (remove mirroring)
    if ( np.linalg.det(C) < 0.0 ):
        i = naxis - 1
        for j in range(naxis):
            r2[i,j] *= -1.0

    # cythonized
    #U = np.dot( r1, r2 )
    for i in range(naxis):
        for j in range(naxis):
            for k in range(naxis):
                U[i,j] += r1[i,k] * r2[k,j]

    return U

def serialize_var(var):
    cdef int d0 = len(var)
    cdef int d1, d2
    cdef np.ndarray [np.double_t,ndim=1] svar 
    cdef np.ndarray [np.double_t,ndim=2] ivar
    cdef int ind, i, j, k

    d1, d2 = var[0].shape
    svar = np.empty(d0*d1*d2,dtype=np.double)

    ind = 0
    for i in range(d0):
        ivar = var[i]
        for j in range(d1):
            for k in range(d2):
                svar[ind] = ivar[j,k]
                ind += 1

    return svar
