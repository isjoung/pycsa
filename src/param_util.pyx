# cython: profile=False, boundscheck=False, wraparound=False, language_level=3
"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung, Scipy developers
Contributors:

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import numpy as np
cimport numpy as np
cimport cython
cimport nelder_mead
from libc.math cimport sqrt, log, exp

# global variables
cdef double BIG = np.finfo('d').max/1.0e6
cdef double [:] max_param
cdef double [:] min_param
cdef list param_scale
cdef object energy_function

def prepare_minimization(self):
    cdef int i
    global max_param
    global min_param
    global param_scale
    global energy_function

    max_param = np.empty(self.nparam,dtype=np.double)
    min_param = np.empty(self.nparam,dtype=np.double)
    for i in range(self.nparam): 
        if self.param_scale[i] == 'linear':
            min_param[i] = self.p['param_range'][i][0]
            max_param[i] = self.p['param_range'][i][1]
        else: # log
            min_param[i] = log(self.p['param_range'][i][0])
            max_param[i] = log(self.p['param_range'][i][1])
            
    perturb_ratio = self.p['min_perturb_ratio']
    param_scale = self.param_scale
    energy_function = self.p['energy_function']

    # prepare nelder_mead
    nelder_mead.max_param = max_param
    nelder_mead.min_param = min_param
    nelder_mead.perturb_ratio = self.p['min_perturb_ratio']
    nelder_mead.xtol = self.p['min_tol']
    nelder_mead.maxiter = self.p['min_maxiter']

cdef double eval_score(np.ndarray [np.double_t,ndim=1] v):
    global param_scale
    global max_param
    global min_param
    global BIG
    global energy_function

    cdef int i
    cdef int nv = v.size
    cdef np.ndarray [np.double_t,ndim=1] var = np.empty(nv,dtype=np.double)
    cdef double val

    for i in range(nv):
        if max_param[i] < v[i]:
            return BIG
        if min_param[i] > v[i]:
            return BIG
        if param_scale[i] == 'log':
            var[i] = exp(v[i])
        else: # linear
            var[i] = v[i]
    val = energy_function(var)
    return val

def eval_score_bound(np.ndarray [np.double_t,ndim=1] v):
    return eval_score(v)

def eval_score_nobound(np.ndarray [np.double_t,ndim=1] v):
    global param_scale
    global max_param
    global min_param
    global BIG
    global energy_function

    cdef int i
    cdef int nv = v.size
    cdef np.ndarray [np.double_t,ndim=1] var = np.empty(nv,dtype=np.double)
    cdef double val

    for i in range(nv):
        if param_scale[i] == 'log':
            var[i] = exp(v[i])
        else: # linear
            var[i] = v[i]

    val = energy_function(var)
    return val

def min_neldermead(np.ndarray [np.double_t,ndim=1] var):
    cdef double score
    cdef np.ndarray [np.double_t,ndim=1] minvar
    cdef np.ndarray [np.double_t,ndim=1] x0 = var.copy()
    cdef int count
    cdef int N = var.size

    # prepare x0 in logscale
    for i in range(N):
        if param_scale[i] == 'log':
            x0[i] = log(x0[i])
    
    nelder_mead.nm_function = &eval_score
    score, minvar, count = nelder_mead.minimize_neldermead(x0)

    # change logarithmic x0 back to linear scale
    for i in range(N):
        if param_scale[i] == 'log':
            minvar[i] = exp(minvar[i])

    return score, minvar, count

def param_distance(
    np.ndarray [np.double_t,ndim=1] var1,
    np.ndarray [np.double_t,ndim=1] var2):

    cdef n_size = var1.size
    cdef int i
    cdef double diff, distance

    distance = 0
    for i in range(n_size):
        if param_scale[i] == 'log':
            diff = log(var1[i]) - log(var2[i])
        else: # linear
            diff = var1[i] - var2[i]
        distance += diff*diff
    distance = sqrt(distance)

    return distance
