import numpy as np
from symmetric2d import Symmetric2d

# prepare a symmetric 2d matrix
a = np.arange(100) + 10
a = a.reshape((10,10)).astype(np.double)
for i in range(10):
    a[i,i] = 0.0
    for j in range(10):
        a[j,i] = a[i,j]

# upper half
b = a[np.triu_indices(10,k=1)]

# initiate
s = Symmetric2d(10,b)

assert(s.average() == np.mean(b))

for i in range(10):
    for j in range(10):
        assert( s.get_elem(i,j) == a[i,j] )

mm = np.array([0,4,8])
assert( s.amin_mask(mm) == np.amin(np.delete(np.delete(a,mm,0),mm,1)[np.triu_indices(7,k=1)]) )

s.adjust_size(13)
assert( s.get_2dmat().shape == (13,13) )

s.adjust_size(6)
assert( s.get_2dmat().shape == (6,6) )

a = a[:6,:6]
b = a[np.triu_indices(6,k=1)]

bmin = np.amin(b)
bmax = np.amax(b)
assert( s.amin() == bmin ) 
assert( s.amax() == bmax )
assert( s.aminmax() == (bmin,bmax) )

col = np.arange(6).astype(np.double)
col[2] = 0.0
a[:,2] = col
a[2,:] = col
s.set_column(2,col)
assert( np.all(s.get_2dmat() == a) )

assert( np.sort(a[3,:])[1] == s.amin_column(3) )
assert( np.sum( np.partition(a,2,axis=0)[:2,:] )/a.shape[0] == s.average_min() )
assert( np.amax(np.sort(np.partition(a,2,axis=0)[:2,:],axis=0)[1,:]) == s.max_min() )

ind = np.array([0,1,4])
mask = np.ones(a.shape[0],dtype=bool)
mask[ind] = 0
b = a[mask,:][:,mask]
assert( np.amin(b[np.triu_indices(b.shape[0],k=1)]) == s.amin_mask(ind) )

s.remove_column(3)
a = np.delete(np.delete(a,3,axis=0),3,axis=1)
assert( np.all( a == s.get_2dmat() ) )
