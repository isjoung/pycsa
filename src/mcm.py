import sys
import numpy as np
from math import log, exp
import param_util as pu
from scipy import optimize
from copy import deepcopy, copy
import cPickle

def dict_merge(a, b):
    '''recursively merges dict's. not just simple a['key'] = b['key'], if
    both a and b have a key who's value is a dict then dict_merge is called
    on both values and the result stored in the returned dictionary.'''
    if not isinstance(b,dict):
        return b
    result = deepcopy(a)
    for k, v in b.iteritems():
        if k in result and isinstance(result[k], dict):
            result[k] = dict_merge(result[k], v)
        else:
            result[k] = deepcopy(v)
    return result

class MCM(object):
    def __init__(self,param={}):
        p = { 'init_temperature': 1000.0,
              'final_temperature': 10.0,
              'boltzmann_const': 1.0, # 1.9872066e-3
              'param_range': [], # eg. [ (min1, max1), (min2, max2), ... ]
              'param_scale': [], # eg. [ 'linear', 'log', 'linear', ... ]
              'min_perturb_ratio': 0.1,
              'maxiter': 10000, 
              'maxcycle': 10,
              'min_tol': 0.0001,
              'min_maxiter': 1000,
              'energy_function': None,
              'schedule': 'exponetial',
              'min_method': 'L-BFGS-B',
              'perturb': { 'nmin': 1, 'nmax': 3, 'perturb_ratio': 0.2, },
              'history_file': 'history',
              'restart_file': 'sa_restart.dat',
              'restart': 1,
              'history_freq': 100,
              'save_restart': 100,
            }

        self.p = dict_merge(p,param)

        self.nparam = len(self.p['param_range'])
        if self.nparam < 1:
            raise ValueError('param %d'%self.nparam)

        # param_scale
        if isinstance( self.p['param_scale'], list ) or isinstance( self.p['param_scale'], tuple ):
            if len(self.p['param_scale']) != self.nparam:
                self.print_error("param_scale does not match param_range")
            self.param_scale = copy(self.p['param_scale'])
        else:
            self.param_scale = [ self.p['param_scale'] ] * self.nparam

        # energy function
        self.energy_function = self.p['energy_function']

        # prepare minimization
        pu.prepare_minimization(self)

        # temperature
        self.init_temperature = self.p['init_temperature']
        if self.p['schedule'] == 'linear':
            self.temp_slope = (self.p['final_temperature'] - self.p['init_temperature'])/self.p['maxiter']
        else:
            self.temp_factor = 1.0/self.p['maxiter'] * log(self.p['final_temperature']/self.p['init_temperature']) + 1.0

    def __del__(self):
        try:
            self.history_fh.close()
        except:
            pass

    def print_log(self,message):
        print "| %s"%message

    def check_param(self):
        self.print_log("checking input parameters")
        # print input parameters
        self.print_log("Input parameters")
        for key in sorted(self.p):
            if isinstance(self.p[key],str):
                self.print_log("'%s' = %s"%(key,repr(self.p[key])))
            else:
                self.print_log("'%s' = %s"%(key,str(self.p[key])))

    def write_minvar(self):
        with open('gmin','w') as f:
            for i in xrange(self.minvar.size):
                f.write('%16.9E\n'%self.minvar[i])
            f.write('\n%16.9E\n'%self.minscore)

    def save_restart(self):
        keylist = ( 'iter', 'temperature', 'nfe', 'var', 'minvar', 'score', 'minscore', 'cycle' )  
        restart_file = self.p['restart_file']
        f = open( restart_file, 'w' )
        savedata = {}
        for key in keylist:
            try:
                savedata[key] = self.__dict__[key]
            except:
                pass
        cPickle.dump(savedata,f,2)
        f.close()

    def load_restart(self):
        # read the restart file
        restart_file = self.p['restart_file']
        try:
            f = open(restart_file,'r')
        except:
            return False
        loaded_self = cPickle.load(f)
        for key in loaded_self.keys():
            self.__dict__[key] = loaded_self[key]
            # destroy the key to save memory
            del loaded_self[key]
        return True

    def run(self):
        self.check_param()
        self.temperature = self.init_temperature
        self.iter = 0
        self.cycle = 0
        self.var = self.randvar()
        self.score, self.var, self.nfe = self.minimize(self.var)
        self.minscore = self.score
        self.minvar = self.var
        pp = self.p['perturb']

        if self.p['restart']:
            restarted = self.load_restart()
        else:
            restarted = False
        if restarted:
            self.open_history(append=True)
        else:
            self.open_history(append=False)
            self.write_history_header()
       
        while self.cycle < self.p['maxcycle']:
            while self.iter < self.p['maxiter']:
                pvar = self.perturb(self.var,pp['perturb_ratio'],pp['nmin'],pp['nmax'])
                new_score, minvar, nfe = self.minimize(pvar)
                self.nfe += nfe
                if (    new_score < self.score
                     or exp((self.score-new_score)/(self.p['boltzmann_const']*self.temperature)) > np.random.rand() ):
                    # accept
                    self.var = minvar
                    self.score = new_score
                if self.minscore > self.score:
                    self.minscore = self.score
                    self.minvar = self.var
                    self.write_minvar()
                if self.iter % self.p['history_freq'] == 0:
                    self.write_history()
                if self.iter % self.p['save_restart'] == 0:
                    self.save_restart()
                self.adjust_temperature()
                self.iter += 1
            self.iter = 0
            self.temperature = self.init_temperature
            self.cycle += 1
            self.save_restart()

    def open_history(self,append=False):
        if self.p['history_file']:
            if append:
                self.history_fh = open('history','a')
            else:
                self.history_fh = open('history','w')
        else:
            self.history_fh = sys.stdout

    def write_history_header(self):
        self.history_fh.write('#%5s %3s %16s %16s %16s %16s\n'%('iter','cyc','temperature','minscore','score','nfe'))
        self.history_fh.flush()

    def write_history(self):
        self.history_fh.write('%6d %3d %16.9E %16.9E %16.9E %16d\n'%(self.iter,self.cycle,self.temperature,self.minscore,self.score,self.nfe))
        self.history_fh.flush()

    def adjust_temperature(self):
        if self.p['schedule'] == 'linear':
            self.temperature = self.temp_slope * self.iter + self.init_temperature
        else:
            self.temperature = self.init_temperature * exp((self.temp_factor-1)*self.iter)

    def minimize(self,var):
        mm = self.p['min_method']
        if mm == 'nelder-mead':
            score, minvar, nfe = pu.min_neldermead(var)
        else:
            x0 = var.copy()
            N = len(x0)

            # bound check for SLSQP
            if mm == 'SLSQP':
                pr = self.p['param_range']
                for i in xrange(N):
                    if x0[i] < pr[i][0]:
                        x0[i] = pr[i][0]
                    elif x0[i] > pr[i][1]:
                        x0[i] = pr[i][1]

            # log scale
            param_scale = self.p['param_scale']
            for i in xrange(N):
                if param_scale[i] == 'log':
                    x0[i] = log(x0[i])

            opt = {'maxiter': self.p['min_maxiter'], 'gtol': self.p['min_tol'], 'xtol': self.p['min_tol']}
            if mm == 'L-BFGS-B' or mm == 'TNC': # or mm == 'SLSQP': # 'SLSQP bounds' is not so reliable
                res = optimize.minimize(pu.eval_score_nobound, x0, method=mm, bounds=self.p['param_range'], options=opt)
            else:
                res = optimize.minimize(pu.eval_score_bound, x0, method=mm, options=opt)

            score = res.fun
            if np.isnan(score):
                score = BIG
            minvar = res.x
            nfe = res.nfev

            # change logarithmic x0 back to linear scale
            for i in range(N):
                if param_scale[i] == 'log':
                    minvar[i] = exp(minvar[i])

            # bound check for SLSQP
            if mm == 'SLSQP':
                pr = self.p['param_range']
                fixed = False
                for i in xrange(N):
                    if minvar[i] < pr[i][0]:
                        minvar[i] = pr[i][0]
                        fixed = True
                    elif minvar[i] > pr[i][1] or np.isnan(minvar[i]):
                        minvar[i] = pr[i][1]
                        fixed = True
                if fixed:
                    score = self.energy_function(minvar)
                    nfe += 1

        return score, minvar, nfe

    def randvar(self):
        var = np.empty(self.nparam,dtype=float)
        for i in xrange(self.nparam):
            r0, r1 = self.p['param_range'][i]
            pscale = self.param_scale[i]
            if pscale == 'linear':
                var[i] = (r1-r0)*np.random.rand() + r0
            elif pscale == 'log':
                var[i] = exp((log(r1)-log(r0))*np.random.rand() + log(r0))
            else:
                raise ValueError('wrong param_scale')
        return var

    def perturb(self,var,perturb_ratio,nmin,nmax):
        n_exchange = np.random.randint(nmin,nmax)
        exind = np.random.choice( self.nparam, n_exchange, replace=False )
        newvar = var.copy()

        for ei in exind:
            r0, r1 = self.p['param_range'][ei]
            if self.param_scale[ei] == 'linear':
                newvar[ei] = newvar[ei] + (r1-r0)*perturb_ratio * (2.0*np.random.random()-1.0)
            elif self.param_scale[ei] == 'log':
                newvar[ei] = newvar[ei] + np.exp( (log(r1)-log(r0))*perturb_ratio * (2.0*np.random.random()-1.0) )
            else:
                raise ValueError('wrong param_scale')
            if newvar[ei] < r0:
                newvar[ei] = r0
            elif newvar[ei] > r1:
                newvar[ei] = r1

        return newvar
