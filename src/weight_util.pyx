# cython: profile=False, boundscheck=False, wraparound=False, language_level=3
"""
This is part of the pycsa program.

Portions copyright (c) 2015-2016 Korea Institute for Advanced Study and
the Authors
Authors: InSuk Joung, Seung Hwan Hong
Contributors:

Permission is hereby granted to any person obtaining a copy of this
software and associated documentation files (the "Software") to deal in
the Software for research purposes only. You may not use the Software
for commercial purposes without the express permission of the authors.
The Software and portions may not be sold nor redistributed to third
parties without the express permission of the author.  Reports or
publications resulting from use of this Software must contain an
acknowledgement in the form commonly used in academic research.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport sqrt, log, exp
cimport nelder_mead

# global variables
cdef double BIG = 1.0e99
cdef list input_data
cdef np.uint8_t [:] opt_mask
cdef int n_best
cdef double n_best_ratio
cdef int score_type
cdef double harmonic_weight
cdef int penalty_type
cdef bint minimize_score
cdef bint minimize_energy
cdef double [:] default_weight
cdef double [:] max_weight
cdef double [:] max_weight_inv
cdef np.ndarray global_var
cdef np.ndarray ref_scores 
cdef double fail_score_slope

@cython.wraparound(True)
def prepare_minimization(self):
    cdef np.ndarray [np.double_t,ndim=1] max_param
    cdef int nopt_var
    cdef int i

    global input_data
    global opt_mask
    global n_best
    global n_best_ratio
    global score_type
    global harmonic_weight
    global penalty_type
    global minimize_score
    global minimize_energy
    global default_weight
    global max_weight
    global max_weight_inv
    global ref_scores
    global fail_score_slope

    input_data = self.input_data
    opt_mask = self.opt_mask.astype(np.uint8)
    if isinstance(self.p['n_best'],str) and self.p['n_best'][-1] == '%':
        n_best_ratio = float(self.p['n_best'][:-1]) / 100.0
    else:
        n_best_ratio = 0.0
        n_best = self.p['n_best']
    score_type = self.p['score_type']
    harmonic_weight = self.p['harmonic_weight']
    penalty_type = self.p['penalty_type']
    minimize_score = self.p['minimize_score']
    minimize_energy = self.p['minimize_energy']
    default_weight = self.default_weight
    max_weight = self.max_weight
    max_weight_inv = 1.0 / self.max_weight

    # prepare nelder_mead
    nopt_var = max_weight.size
    max_param = np.empty(nopt_var,dtype=np.double)
    min_param = np.empty(nopt_var,dtype=np.double)
    for i in range(nopt_var):
        max_param[i] = log(max_weight[i])
        min_param[i] = log(max_weight_inv[i])
    nelder_mead.max_param = max_param
    nelder_mead.min_param = min_param
    nelder_mead.perturb_ratio = self.p['min_perturb_ratio']
    nelder_mead.xtol = self.p['min_tol']
    nelder_mead.maxiter = self.p['min_maxiter']
    if self.p['ref_scores'] is not None:
        ref_scores = np.array(self.p['ref_scores'],dtype=np.double)
    fail_score_slope = self.p['fail_score_slope']

def eval_score_slice2(np.ndarray [np.double_t,ndim=1] x0):
    return eval_score_slice(x0)

def set_global_var(np.ndarray [np.double_t,ndim=1] var):
    global global_var
    # prepare global_var for eval_score
    # some variables not in opt_mask will be taken from global_var
    global_var = var.copy()

cdef double eval_score_slice(np.ndarray [np.double_t,ndim=1] x0):
    cdef double score[2]
    cdef int i, j
    cdef int v_size
    cdef np.ndarray [np.double_t,ndim=1] var

    global opt_mask

    var = global_var
    v_size = var.size

    j = 0
    for i in range(v_size):
        if opt_mask[i] == 0: # False
            continue
        var[i] = exp(x0[j])
        j += 1

    eval_score(var,score)
    return score[0] + score[1]

cdef void eval_score(np.ndarray [np.double_t,ndim=1] var, double* all_score):
    cdef str data_file
    cdef np.ndarray [np.double_t,ndim=1] score_data
    cdef np.ndarray [np.double_t,ndim=2] sum_data
    cdef int n_data
    cdef int n_param = var.size
    cdef np.ndarray [np.double_t,ndim=1] weight = np.empty(n_param,dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=1] data_sum
    cdef np.ndarray [np.int_t,ndim=1] best_ind
    cdef int i, j, nb, itarget
    cdef double score, sco, p_score
    cdef np.ndarray [np.double_t,ndim=1] rsco
    cdef list ret

    global BIG
    global input_data
    global n_best
    global n_best_ratio
    global harmonic_weight
    global minimize_score
    global minimize_energy
    global default_weight
    global max_weight
    global ref_scores
    global fail_score_slope

    rsco = ref_scores

    for j in range(n_param):
        # check bound
        if var[j] < max_weight_inv[j] or var[j] > max_weight[j]:
            all_score[0] = BIG
            return
        # calc weight
        weight[j] = var[j] * default_weight[j]

    score = 0.0
    for itarget in range(len(input_data)):
        data_file, score_data, sum_data = input_data[itarget]
        n_data = score_data.size
        if n_best_ratio > 0.0:
            nb = max( min( <int> ( n_data * n_best_ratio ), n_data ), 1 )
        else:
            if n_best > 0:
                nb = min(n_best,n_data)
            else:
                nb = n_data
        data_sum = np.zeros(n_data,dtype=np.double)
        if minimize_energy:
            for i in range(n_data):
                for j in range(n_param):
                    data_sum[i] += sum_data[i,j] * weight[j]
        else:
            for i in range(n_data):
                for j in range(n_param):
                    data_sum[i] -= sum_data[i,j] * weight[j]

        if nb == n_data:
            best_ind = np.arange(nb,dtype=np.int)
        else:
            best_ind = np.argpartition(data_sum,nb)

        if score_type==1:
            # average of nb
            sco = 0.0
            for i in range(nb):
                sco += score_data[best_ind[i]]
            sco /= nb
        elif score_type==2:
            # best out of nb
            sco = score_data[best_ind[0]]
            if minimize_score:
                for i in range(1,nb):
                    if score_data[best_ind[i]]<sco :
                        sco=score_data[best_ind[i]]
            else:
                for i in range(1,nb):
                    if score_data[best_ind[i]]>sco :
                        sco=score_data[best_ind[i]]
        elif score_type==3:
            # worst out of nb
            sco = score_data[best_ind[0]]
            if minimize_score:
                for i in range(1,nb):
                    if score_data[best_ind[i]]>sco :
                        sco=score_data[best_ind[i]]
            else:
                for i in range(1,nb):
                    if score_data[best_ind[i]]<sco :
                        sco=score_data[best_ind[i]]
        elif score_type == 4:
            # correlation coefficient
            sco = - calc_correlation(data_sum[best_ind],score_data[best_ind])
        elif score_type == 5:
            # average of nb
            # give more penalty to the failed target
            sco = 0.0
            for i in range(nb):
                sco += score_data[best_ind[i]]
            sco /= nb
            sco -= rsco[itarget]
            if minimize_score:
                if sco > 0.0:
                    # failed target
                    sco *= fail_score_slope
            else:
                if sco < 0.0:
                    sco *= fail_score_slope

        score += sco
    score /= len(input_data)

    if not minimize_score:
        score *= -1.0

    # penalty for big weights
    p_score = 0.0
    if penalty_type == 0:
        for j in range(n_param):
            p_score += harmonic_weight * var[j]**2
    elif penalty_type == 1:
        for j in range(n_param):
            p_score += harmonic_weight * (var[j]-1.0)**2
    elif penalty_type == 2:
        # correlation coefficient of the lowest energy conformation in bins
        for data_file, score_data, sum_data in input_data:
            if minimize_score:
                p_score -= harmonic_weight * calc_histo_correlation(data_sum,score_data) 
            else:
                p_score += harmonic_weight * calc_histo_correlation(data_sum,score_data) 
    else:
        raise

    all_score[0] = score
    all_score[1] = p_score

def eval_score_detail(np.ndarray [np.double_t,ndim=1] var):
    cdef str data_file
    cdef np.ndarray [np.double_t,ndim=1] score_data
    cdef np.ndarray [np.double_t,ndim=2] sum_data
    cdef int n_data
    cdef int n_param = var.size
    cdef np.ndarray [np.double_t,ndim=1] weight = np.empty(n_param,dtype=np.double)
    cdef np.ndarray [np.double_t,ndim=1] data_sum
    cdef np.ndarray [np.int_t,ndim=1] best_ind
    cdef int i, j, nb, itarget
    cdef sco_avg, sco_min, sco_max, sco_pickone, s
    cdef double sco, ene_min
    cdef double score[2]
    cdef list ret

    global input_data
    global n_best
    global n_best_ratio
    global minimize_energy
    global default_weight

    for j in range(n_param):
        # calc weight
        weight[j] = var[j] * default_weight[j]

    ret = []
    for itarget in range(len(input_data)):
        data_file, score_data, sum_data = input_data[itarget]
        n_data = score_data.size
        if n_best_ratio > 0.0:
            nb = max( min( <int> ( n_data * n_best_ratio ), n_data ), 1 )
        else:
            if n_best > 0:
                nb = min(n_best,n_data)
            else:
                nb = n_data
        data_sum = np.zeros(n_data,dtype=np.double)
        if minimize_energy:
            for i in range(n_data):
                for j in range(n_param):
                    data_sum[i] += sum_data[i,j] * weight[j]
        else:
            for i in range(n_data):
                for j in range(n_param):
                    data_sum[i] -= sum_data[i,j] * weight[j]

        if nb == n_data:
            best_ind = np.arange(nb,dtype=np.int)
        else:
            best_ind = np.argpartition(data_sum,nb)

        s = score_data[best_ind[0]]
        ene_min = data_sum[best_ind[0]]
        sco_pickone = s
        sco_min = s
        sco_max = s
        sco_avg = s
        for i in range(1,nb):
            s = score_data[best_ind[i]]
            sco_avg += s
            if s < sco_min:
                sco_min = s
            if s > sco_max:
                sco_max = s
            if ene_min > data_sum[best_ind[i]]:
                sco_pickone = s
                ene_min = data_sum[best_ind[i]]
        sco_avg /= nb
        ret.append( (data_file, sco_avg, sco_min, sco_max, sco_pickone) )

    eval_score(var,score)

    return score[0], score[1], ret

cdef double calc_correlation(double [:] x, double [:] y):
    cdef double xsum, ysum
    cdef double x2sum, y2sum
    cdef double xysum
    cdef int nsize = x.size
    cdef int i
    cdef double r

    xsum = 0.0
    ysum = 0.0
    x2sum = 0.0
    y2sum = 0.0
    xysum = 0.0
    for i in range(nsize):
        xsum += x[i]
        ysum += y[i]
        x2sum += x[i]**2
        y2sum += y[i]**2
        xysum += x[i]*y[i]

    r =   ( nsize * xysum - xsum * ysum ) \
        / ( sqrt( nsize*x2sum - xsum**2 ) * sqrt( nsize*y2sum - ysum**2 ) )
    return r

def minimize_neldermead(np.ndarray [np.double_t,ndim=1] var):
    cdef double score
    cdef np.ndarray [np.double_t,ndim=1] minvar
    cdef np.ndarray [np.double_t,ndim=1] x0
    cdef int count
    cdef int N
    cdef int v_size
    cdef int i, j

    global global_var
    global opt_mask

    v_size = var.size

    # prepare x0 in logscale
    x0 = np.empty(v_size,dtype=np.double)
    N = 0
    for i in range(v_size):
        if opt_mask[i] == 0: # False
            continue
        x0[N] = log(var[i])
        N += 1
    x0 = x0[:N]

    # prepare global_var for eval_score
    # some variables not in opt_mask will be taken from global_var
    global_var = var.copy()

    nelder_mead.nm_function = &eval_score_slice
    score, x0, count = nelder_mead.minimize_neldermead(x0)

    # change logarithmic x0 back to linear scale
    minvar = global_var
    j = 0
    for i in range(v_size):
        if opt_mask[i] == 0: # False
            continue
        minvar[i] = exp(x0[j])
        j += 1

    return score, minvar, count

def weight_distance(
    np.ndarray [np.double_t,ndim=1] var1,
    np.ndarray [np.double_t,ndim=1] var2):

    cdef n_size = var1.size
    cdef int i
    cdef double diff, distance

    distance = 0
    for i in range(n_size):
        diff = log(var1[i]) - log(var2[i])
        distance += diff*diff
    distance = sqrt(distance)

    return distance

cdef double calc_histo_correlation(np.ndarray [np.double_t,ndim=1] data_sum, np.ndarray [np.double_t,ndim=1] score_data):
    cdef int nbin = 20
    cdef int ndata = data_sum.size
    cdef int i, j
    cdef double min_score, max_score, bin_gap
    cdef double sco, dsum
    cdef np.ndarray [np.double_t,ndim=1] x
    cdef np.ndarray [np.double_t,ndim=1] y
    cdef double corr
    cdef int xysize

    global minimize_energy

    # min/max
    min_score = score_data[0]
    max_score = score_data[0]
    for i in range(1,ndata):
        sco = score_data[i]
        if sco > max_score:
            max_score = sco
        if sco < min_score:
            min_score = sco

    if min_score < -BIG or max_score > BIG:
        return 0.0

    # bin_gap
    bin_gap = (max_score - min_score)/nbin
    if bin_gap == 0.0:
        return 0.0

    x = np.empty(nbin,dtype=np.double)
    y = np.empty(nbin,dtype=np.double)
    if minimize_energy:
        for i in range(nbin):
            y[i] = BIG
    else:
        for i in range(nbin):
            y[i] = -BIG

    for i in range(nbin):
        x[i] = (i+0.5) * bin_gap + min_score

    for i in range(ndata):
        sco = score_data[i]
        binind = <int> ( ( sco - min_score )/bin_gap )
        if binind == nbin:
            binind -= 1
        dsum = data_sum[i]
        if minimize_energy:
            if y[binind] > dsum:
                x[binind] = sco
                y[binind] = dsum
        else:
            if y[binind] < dsum:
                x[binind] = sco
                y[binind] = dsum

    # remove not updated
    xysize = 0
    for i in range(nbin):
        if minimize_energy:
            if y[i] == BIG:
                continue
        else:
            if y[i] == -BIG:
                continue
        x[xysize] = x[i]
        y[xysize] = y[i]
        xysize += 1

    if xysize < 2:
        return 0.0

    corr = calc_correlation(x[:xysize],y[:xysize])
    return corr
