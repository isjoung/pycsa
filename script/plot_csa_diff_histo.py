#!/usr/bin/env python
import Gnuplot
import argparse
import numpy as np

def main():
    parser = argparse.ArgumentParser(description='plot csa diff histogram')
    parser.add_argument('data_files', metavar='data_file', nargs='*', help='diff files')
    parser.add_argument('--nbin', dest='nbin', type=int, default=100 )
    #parser.add_argument('--min', dest='min', type=float, default=None )
    #parser.add_argument('--max', dest='max', type=float, default=None )
    args = parser.parse_args()

    data_files = args.data_files
    if ( len(data_files) == 0 ):
        data_files = ['diff']

    plot_data = []
    raw_data = []
    minbin = None
    maxbin = None
    for dfile in data_files:
        data = read_diff_file(dfile)
        raw_data.append(data)
        dmin = np.amin(data)
        dmax = np.amax(data)
        if ( minbin == None or minbin > dmin ):
            minbin = dmin
        if ( maxbin == None or maxbin < dmax ):
            maxbin = dmax

    for data in raw_data:
        histo = np.histogram(data,bins=args.nbin,range=(minbin,maxbin))
        x = histo[1][1:]
        hist_data = np.transpose( np.vstack( ( x, histo[0]/float(np.sum(histo[0])) ) ) )
        plot_data.append( Gnuplot.PlotItems.Data(hist_data,with_='linespoints') )
        
    p = Gnuplot.Gnuplot(debug=1)
    #p("set terminal pdf color enhanced font 'Helvetica,15' dl 3.0"),
    #p("set encoding iso_8859_1",
    #p("set output 'out.pdf'",
    p.plot(*plot_data)
    raw_input()

def read_diff_file(dfile):
    f = open(dfile,'r')
    data = []
    for line in f:
        c = line.split()
        if ( len(c) == 3 ):
            c[0] = int(c[0])
            c[1] = int(c[1])
            if ( c[0] < c[1] ):
                data.append(float(c[2]))
    f.close()
    return data

if __name__ == '__main__':
    main()

