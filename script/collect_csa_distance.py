#!/usr/bin/env python
import sys
import re
import argparse

def main():
    """
    collects distances from a log file
    """
    parser = argparse.ArgumentParser(description='Collects distances from a log file.')
    parser.add_argument('logfile', default='log', nargs='?', help='log filename')
    args = parser.parse_args()

    logfile = args.logfile
    with open(logfile,'r') as f:
        niter = 0
        dcut = 0.0
        davg = 0.0 
        dmin = 0.0
        dmax = 0.0
        distance = []
        for line in f:
            m = re.search(r'Iteration step: (\d+)',line)
            if ( m ):
                distance.append( (niter,dcut,davg,dmin,dmax) )
                niter = int( m.group(1) )
            m = re.search(r'The average distance between bank conformations is (\S+)',line)
            if ( m ):
                davg = float( m.group(1) )
            m = re.search(r'Average bank distance: (\S+)',line)
            if ( m ):
                davg = float( m.group(1) )
            m = re.search(r'Minimum bank distance: (\S+)',line)
            if ( m ):
                dmin = float( m.group(1) )
            m = re.search(r'Maximum bank distance: (\S+)',line)
            if ( m ):
                dmax = float( m.group(1) )
            m = re.search(r'(?:Initial Dcut| D_cut): (\S+)',line)
            if ( m ):
                dcut = float( m.group(1) )

    print '# iter Dcut Davg Dmin Dmax'
    for niter,dcut,davg,dmin,dmax in distance:
        print "%d %f %f %f %f"%(niter,dcut,davg,dmin,dmax)

if __name__ == '__main__':
    main()
