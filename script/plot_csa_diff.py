#!/usr/bin/env python
from Gnuplot import Gnuplot
import tempfile
import copy

def main():
    data = read_diff_file('diff')
    # grow data
    temp = copy.deepcopy(data[0])
    data.insert(0,temp)
    for x in data[0]:
        x[0] -= 1
    for i in data:
        i.insert(0,copy.copy(i[0]))
    for i in data:
        i[0][1] -= 1
    data[0][0][2] = 0.0

    # write data
    fout = tempfile.NamedTemporaryFile(delete=True)
    dfile = fout.name
    write_diff_file(data,fout)
    fout.flush()

    p = Gnuplot(debug=1)
    #p("set terminal pdf color enhanced font 'Helvetica,15' dl 3.0"),
    #p("set encoding iso_8859_1",
    #p("set output 'out.pdf'",
    p("set pm3d map corners2color c4")
    p("splot '%s'"%dfile)
    p("set size square")
    p("pause mouse")

    raw_input()

def write_diff_file(data,fout):
    for i in data:
        for j in i:
            fout.write('%d %d %f\n'%(j[0],j[1],j[2]))
        fout.write('\n')

def read_diff_file(dfile):
    f = open(dfile,'r')
    ret = []
    block = []
    for line in f:
        c = line.split()
        if ( len(c) == 3 ):
            c[0] = int(c[0])
            c[1] = int(c[1])
            c[2] = float(c[2]) 
            block.append(c)
        else:
            ret.append(block)
            block = []
    f.close()
    return ret

if __name__ == '__main__':
    main()

