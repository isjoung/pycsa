#!/usr/bin/env python
import sys
import re

def main():
    """
collects acceptance ratios from log file
"""
    logfile = 'log'
    if ( len(sys.argv) > 1 ):
        logfile = sys.argv[1]

    f = open(logfile,'r')
    niter = 0
    accept_ratio = []
    c_ratio = {}
    perturb_type = set()
    for line in f:
        m = re.search(r'Iteration step: (\d+)',line)
        if ( m ):
            if ( len(c_ratio) > 0 ):
                accept_ratio.append( (niter,c_ratio) )
                c_ratio = {}
            niter = int( m.group(1) )
        m = re.search(r'acceptance ratio of (\S+) is (\S+)',line)
        if ( m ):
            ptype = m.group(1)
            perturb_type.add(ptype)
            c_ratio[ptype] = float( m.group(2) ) 
    f.close()

    perturb_type = list( sorted(perturb_type) )
    print '#',
    for ptype in perturb_type:
        print ptype,
    print
    for niter, c_ratio in accept_ratio:
        print '%d'%niter,
        for ptype in perturb_type:
            try:
                print ' %.6f'%c_ratio[ptype],
            except:
                print ' %.6f'%0.0,
        print

if __name__ == '__main__':
    main()

