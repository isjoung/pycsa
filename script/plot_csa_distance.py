#!/usr/bin/env python
import Gnuplot
import argparse
import numpy as np

def main():
    parser = argparse.ArgumentParser(description='plot csa distance')
    parser.add_argument('data_files', metavar='data_file', nargs='*', help='diff files')
    #parser.add_argument('--min', dest='min', type=float, default=None)
    #parser.add_argument('--max', dest='max', type=float, default=None)
    parser.add_argument('-p',dest='pdf', default=None)
    args = parser.parse_args()

    data_files = args.data_files
    if ( len(data_files) == 0 ):
        data_files = ['distance.dat']

    plot_line = 'plot'
    for df in data_files:
        plot_line += " '%s' u 1:2 ti 'D_{cut}', '' u 1:3 ti 'D_{avg}', '' u 1:4 ti 'D_{min}', '' u 1:5 ti 'D_{max}',"%df
    plot_line = plot_line[:-1]
        
    p = Gnuplot.Gnuplot(debug=1)
    if args.pdf:
        p("set terminal pdf color enhanced font 'Helvetica,15' dl 3.0 lw 2.0"),
        p("set encoding iso_8859_1"),
        p("set output '%s'"%args.pdf),
    p("set style data lines")
    p("set xlabel 'CSA Iteration'")
    p("set ylabel 'Distance'")
    p(plot_line)
    if not args.pdf:
       raw_input()

def read_diff_file(dfile):
    f = open(dfile,'r')
    data = []
    for line in f:
        c = line.split()
        if ( len(c) == 3 ):
            c[0] = int(c[0])
            c[1] = int(c[1])
            if ( c[0] < c[1] ):
                data.append(float(c[2]))
    f.close()
    return data

if __name__ == '__main__':
    main()

