#!/usr/bin/env python
import sys
import re
from math import log10

def main():
    """
    collects n_updates from a log file
    """
    logfile = 'log'
    if ( len(sys.argv) > 1 ):
        logfile = sys.argv[1]

    f = open(logfile,'r')
    niter = 0
    n_local = 0
    n_cluster = 0
    n_top_removed = 0
    n_added = 0
    max_n = 1
    data = []
    for line in f:
        m = re.search(r'Iteration step: (\d+)',line)
        if ( m ):
            data.append( (niter,n_local,n_cluster,n_top_removed,n_added) )
            niter = int( m.group(1) )
        m = re.search(r'n_local_update: (\d+)',line)
        if ( m ):
            n_local = int( m.group(1) )
            max_n = max(max_n,n_local)
        m = re.search(r'n_cluster_update: (\d+)',line)
        if ( m ):
            n_cluster = int( m.group(1) )
            max_n = max(max_n,n_cluster)
        m = re.search(r'n_top_removed: (\d+)',line)
        if ( m ):
            n_top_removed = int( m.group(1) )
            max_n = max(max_n,n_top_removed)
        m = re.search(r'n_added: (\d+)',line)
        if ( m ):
            n_added = int( m.group(1) )
            max_n = max(max_n,n_added)
    f.close()

    niter_size = int(log10(niter))+1
    n_size = int(log10(max_n))+1

    print '# iter n_local n_cluster n_top_removed n_added'
    for d in data:
        #print "%d %d %d %d"%d
        print ('%'+str(niter_size)+'d %'+str(n_size)+'d %'+str(n_size)+'d %'+str(n_size)+'d %'+str(n_size)+'d')%d

if __name__ == '__main__':
    main()

