#!/usr/bin/env python
import sys
import re

def main():
    """
    collects ecut from a log file
    """
    logfile = 'log'
    if ( len(sys.argv) > 1 ):
        logfile = sys.argv[1]

    f = open(logfile,'r')
    niter = 0
    ecut = None
    data = []
    for line in f:
        m = re.search(r'Iteration step: (\d+)',line)
        if ( m ):
            data.append( (niter,ecut) )
            niter = int( m.group(1) )
        m = re.search(r' increase_bank_energy_cut: (\S+)',line)
        if ( m ):
            ecut = float( m.group(1) )
        if ecut == None:
            m = re.search(r'increase_bank_energy_cut = (\S+)',line)
            if m:
                ecut = float( m.group(1) )
    f.close()

    print '# iter Dcut Davg'
    for niter,ecut in data:
        print "%d %f"%(niter,ecut)

if __name__ == '__main__':
    main()

