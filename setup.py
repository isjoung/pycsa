from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy
import os
from glob import glob

pyx_list = [
    'src/zmatrix_c.pyx','src/perturb_util.pyx','src/csa_util.pyx',
    'src/csa_protein_util.pyx','src/weight_util.pyx','src/nelder_mead.pyx',
    'src/param_util.pyx', 'src/neb_util.pyx', 'src/symmetric2d.pyx']

numpy_include = numpy.get_include()

ext = []
for pyx in pyx_list:
    ext.append( Extension( os.path.basename(pyx[:-4]), [pyx], include_dirs=[numpy_include,'.','csa'] ) )

setup(
    name = "pycsa",
    version = "0.2.1",
    packages = ['csa', 'csa.utils'],
    package_dir = {'': 'src'},
    include_dirs = ['src','src/csa'],
    ext_modules = cythonize(ext),
    data_files = [
        ('csa/data/stap/phipsi', glob('data/stap/phipsi/*.dat')),
        ('csa/data/stap/phichi1', glob('data/stap/phichi1/*.dat')),
        ('csa/data/stap/psichi1', glob('data/stap/psichi1/*.dat')),
        ('csa/data/stap/chi1chi2', glob('data/stap/chi1chi2/*.dat')),
    ],
    install_requires = ['numpy', 'mpi4py', 'scipy', 'cython']
)
