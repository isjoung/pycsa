#!/usr/bin/env python
from csa import read_restart_param
from csa.csa_TINKER import csa_TINKER

bank_size = 50
nseed = 30
max_bank_size = 100
nbank_add = 0
nperturb_int_cross = 8
nperturb_int_residue = 4
nperturb_torsion = 4
nperturb_car_cross = 4
nperturb_randint = 0
bank_min_maxiter = 1000
rbank_min_maxiter = 1000

def main():
    p = read_restart_param('csa_restart.dat')
    if len(p) == 0:
        p['csa_stage'] = (0,0)
        p['nbank'] = bank_size
        p['seed_mask'] = []
        p['icmax'] = 3

    nbank = p['nbank']
    csa_stage = p['csa_stage']

    c = csa_TINKER({
        'xyzfile': 'model.xyz',
        'xyzmapfile': 'model.map',

        'parallel_minimization': 3,
        'smart_pmunit': 1,

        'dcut_reduce': 0.4**(1.0/333.333),
        'dcut1': 2.0,
        'dcut2': 5.0,
        'dist_method': 'dihedral',
        'icmax': p['icmax'],
        'iucut': 10,

        'nbank': p['nbank'],
        'nseed': nseed,
        'nbank_add': nbank_add,

        'seed_mask': p['seed_mask'],

        'restart_mode': 2,
        'save_restart': 1,

        'rand_conf_type': 0,
        'write_diff': False,
        'write_pdb': True,
        'write_xyz': False,

        'bank_min_maxiter': bank_min_maxiter,
        'rbank_min_maxiter': rbank_min_maxiter,

        'verbose': 2,

        'minimize_seed': False,
        'decut': 0.1,

        'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': p['seed_mask'] },
        'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': p['seed_mask'] },
        'perturb_torsion':     { 'nperturb': nperturb_torsion,     'partner_mask': p['seed_mask'] },
        'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': p['seed_mask'] },
        'perturb_randint':     { 'nperturb': nperturb_randint },

        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        # parameters
        c.run()
        csa_stage = next_csa_stage(csa_stage)
        nbank = (csa_stage[0]+1) * bank_size 
        if csa_stage[1] == 0:
            icmax = 0
            seed_mask = range(nbank-bank_size)
        else:
            icmax = 2
            nbank = c.p['nbank']
            seed_mask = []

        c.set_param({
            'csa_stage': csa_stage,
            'nbank': nbank,
            'seed_mask': seed_mask,
            'icmax': icmax,
            'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': seed_mask },
            'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': seed_mask },
            'perturb_torsion':     { 'nperturb': nperturb_torsion,     'partner_mask': seed_mask },
            'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': seed_mask },
        })
        if csa_stage[1] == 0:
            c.ncycle = 0
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

if __name__ == '__main__':
    main()
