#!/usr/bin/env python
from csa import read_restart_param
from csa.csa_OPENMM import csa_OPENMM

bank_size = 50
nseed = 30
max_bank_size = 400
nbank_add = 50
nperturb_int_cross = 6
nperturb_int_residue = 2
nperturb_torsion = 4
nperturb_car_cross = 5
nperturb_randint = 3

bank_min_maxiter = 1000
rbank_min_maxiter = 1000

def main():
    p = read_restart_param('csa_restart.dat')
    if len(p) == 0:
        p['csa_stage'] = (0,0)
        p['nbank'] = bank_size
        p['icmax'] =  2
        p['seed_mask'] = []
        
    nbank = p['nbank']
    csa_stage = p['csa_stage']

    c = csa_OPENMM({
        # force field / initial coordinates
        'topology': 'model.prmtop',
        'init_coord': 'model.inpcrd',
        'implicit_solvent': 'HCT',
        'charge': True,
        'restrain': 'restrain',
        'nucleicacid_chiral_rest': True,
        'contact_rest': None,

        # openmm platform
        'openmm_platform': 'CUDA',

        # minimization method
        'min_method': 'openmm',

        # parallel minimization
        'parallel_minimization': 2,
        'smart_pmunit': 1,

        # dcut
        'dcut_reduce': 0.4**(1.0/333.333),
        'dcut1': 2.0,
        'dcut2': 5.0,
        'dist_method': 'rmsd',

        # iucut
        'icmax': p['icmax'],
        'iucut': 10,

        # nbank / seed
        'nbank': nbank,
        'nseed': nseed,
        'nbank_add': nbank_add,
        'seed_mask': p['seed_mask'],

        # restart
        'restart_mode': 2,
        'save_restart': 1,

        # generation of initial conformations
        'rand_conf_type': 0,
        'rand_param0': {'sigma_torsion': 360.0, 'residue_ratio': 1.0, 'discrete_omega': True, 'sigma_angle': 10.0, 'res_index': None, 'sigma_bond': 0.1},

        # verbose
        'verbose': 2,

        # minimize param
        'minimize_seed': False,
        'decut': 0.1,
        'bank_min_maxiter': bank_min_maxiter,
        'rbank_min_maxiter': rbank_min_maxiter,

        # perturbation
        'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': p['seed_mask'] },
        'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': p['seed_mask'] },
        'perturb_torsion':     { 'nperturb': nperturb_torsion,     'partner_mask': p['seed_mask'] },
        'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': p['seed_mask'] },
        'perturb_randint':     { 'nperturb': nperturb_randint },

        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        c.run()
        csa_stage = next_csa_stage(csa_stage)
        nbank = ( csa_stage[0] + 1 ) * bank_size
        if csa_stage[1] == 0:
            icmax = 0
            seed_mask = range(nbank-bank_size)
        else:
            icmax = 2
            seed_mask = []
        c.set_param({
            'icmax': icmax,
            'seed_mask': seed_mask,
            'nbank': nbank,

            'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': seed_mask },
            'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': seed_mask },
            'perturb_torsion':     { 'nperturb': nperturb_torsion,     'partner_mask': seed_mask },
            'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': seed_mask },
            'perturb_randint':     { 'nperturb': nperturb_randint },

            'csa_stage': csa_stage,
        })

        if csa_stage[1] == 0:
            c.ncycle = 0
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

if __name__ == '__main__':
    main()
