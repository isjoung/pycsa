#!/usr/bin/env python
import mdtraj as mdt
from glob import glob
import numpy as np
import argparse

def main():
    parser = argparse.ArgumentParser(description='calculate rmsd')
    parser.add_argument('-r', dest='use_rbank', action='store_true', help='rbank')
    args = parser.parse_args()

    ref = mdt.load('complex.pdb') 
    rec_ind = ref.topology.select("protein")
    lig_ind = ref.topology.select("not protein")
    ref_lig_coord = ref.xyz[0,lig_ind,:]
    if args.use_rbank:
        bank_files = sorted(glob('r*.pdb'))
    else:
        bank_files = sorted(glob('b*.pdb'))
    for bank_file in bank_files:
        bank = mdt.load(bank_file)
        bank.superpose(ref, atom_indices=rec_ind)
        bank_lig_coord = bank.xyz[0,lig_ind,:]
        rmsd = np.sqrt(np.mean(np.sum((ref_lig_coord - bank_lig_coord)**2, axis=1)))
        print(bank_file,rmsd)
        

main()
 
