#!/usr/bin/env python
from csa import read_restart_param
from csa.csa_OPENMM import csa_OPENMM, read_connect
import numpy as np
from simtk.openmm import app
from simtk import unit

bank_size = 25
nseed = 10
max_bank_size = 50
nbank_add = bank_size * 2
nperturb_car_cross = 4
nperturb_int_cross = 4
nperturb_car_residue = 2
nperturb_int_residue = 2
nperturb_randcar = 2
nperturb_randint = 2
nperturb_trans_rot = 2

bank_min_maxiter = 1000
rbank_min_maxiter = 1000

fix_radius = 15.0
receptor_max_coord = 0.5

ligand_perturb_type = 'int'
ligand_max_coord = 5.0 # it applies when lignad_perturb_type == 'car'

prmtop = 'complex.prmtop'
inpcrd = 'complex.inpcrd'


def main():
    p = read_restart_param('csa_restart.dat')
    if len(p) == 0:
        p['csa_stage'] = (0,0)
        p['nbank'] = bank_size
        p['icmax'] =  2
        p['seed_mask'] = []
        
    nbank = p['nbank']
    csa_stage = p['csa_stage']

    # find fix index
    structure = app.AmberPrmtopFile(prmtop)
    topo = structure.topology
    natom = topo._numAtoms
    connect = read_connect(natom,topo)

    # read continuous molecules
    mol = { 1:1 }
    imol = 1
    size = len(connect)
    while ( len(mol) < size ):
        updated = True
        while ( updated ):
            updated = False
            for sn in range(1,natom+1):
                if ( sn in mol ): continue
                conn = connect[sn-1]
                for cn in conn:
                    if ( cn in mol ):
                        mol[sn] = mol[cn]
                        updated = True
        if len(mol) < size:
            imol += 1
            for sn in range(1,natom+1):
                if ( sn in mol ): continue
                mol[sn] = imol
                break

    ligand_mol = imol
    receptor_index = []
    ligand_index = []
    for ind in mol:
        if mol[ind] == ligand_mol:
            ligand_index.append(ind-1)
        else:
            receptor_index.append(ind-1)
    ligand_index = np.array(ligand_index)
    receptor_index = np.array(receptor_index)
        
    init_coord = app.AmberInpcrdFile(inpcrd)
    initvar = np.array( init_coord.positions.value_in_unit(unit.angstrom) )
    ligand_center = np.mean(initvar[ligand_index,:],axis=0)

    mask = np.sum((ligand_center - initvar[receptor_index,:])**2,axis=1) > fix_radius**2
    fix_index = receptor_index[mask]
    non_fix_index = receptor_index[np.logical_not(mask)]
    perturb_index = np.concatenate((non_fix_index,ligand_index))

    c = csa_OPENMM({
        # force field / initial coordinates
        'topology': prmtop,
        'init_coord': inpcrd,
        'implicit_solvent': 'GBn2',
        'charge': True,
        'restrain': None, #'restrain',
        'protein_chiral_rest': True,
        'contact_rest': None, #[ 10.0, 10.0, 0.2 ],
        'positional_rest': True,
        'positional_weight': 100.0,

        'fix_index': fix_index,

        # openmm platform
        'openmm_platform': 'CUDA',

        # minimization method
        'min_method': 'lbfgsb',#'openmm', #'lbfgsb'

        # parallel minimization
        'parallel_minimization': 2,
        'smart_pmunit': 1,

        # dcut
        'dcut_reduce': None,
        'dcut_reduce_method': 'linear',
        'dcut_reduce_steps': 300,
        'dcut1': 0.40,
        'dcut2': 3.0,
        'dist_method': 'rmsd',
        'random_seed': 2,

        # iucut
        'icmax': p['icmax'],
        'iucut': 10,

        # nbank / seed
        'nbank': nbank,
        'nseed': nseed,
        'nbank_add': nbank_add,
        'seed_mask': p['seed_mask'],

        # restart
        'restart_mode': 2,
        'save_restart': 1,

        # generation of initial conformations
        'rand_conf_type': 'receptor_ligand',
        'rand_param_receptor_ligand': {
            'ligand_mol': ligand_mol,
            'receptor_max_coord': receptor_max_coord,
            'ligand_max_coord': ligand_max_coord,
            'rand_radius': fix_radius,
            'ligand_perturb_type': ligand_perturb_type },

        # verbose
        'verbose': 3,

        # minimize param
        'minimize_seed': False,
        'decut': 0.1,
        'bank_min_maxiter': bank_min_maxiter,
        'rbank_min_maxiter': rbank_min_maxiter,

        # perturbation
        'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': p['seed_mask'], 'atom_index': ligand_index },
        'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': p['seed_mask'], 'atom_index': ligand_index, 'residue_base': False },
        'perturb_randint':     { 'nperturb': nperturb_randint, 'nmin': 1, 'nmax': 3, 'atom_index': ligand_index },
        'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': p['seed_mask'], 'atom_index': perturb_index },
        'perturb_car_residue': { 'nperturb': nperturb_car_residue, 'partner_mask': p['seed_mask'], 'atom_index': perturb_index, 'residue_base': False },
        'perturb_randcar':     { 'nperturb': nperturb_randcar, 'nmin': 1, 'nmax': 3, 'atom_index': perturb_index },
        'perturb_trans_rot':   { 'nperturb': nperturb_trans_rot, 'sigma_trans': 0.5, 'sigma_rot': 10.0, 'atom_index': ligand_index },

        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        c.run()
        csa_stage = next_csa_stage(csa_stage)
        nbank = ( csa_stage[0] + 1 ) * bank_size
        if csa_stage[1] == 0:
            icmax = 0
            seed_mask = range(nbank-bank_size)
        else:
            icmax = 2
            seed_mask = []
        c.set_param({
            'icmax': icmax,
            'seed_mask': seed_mask,
            'nbank': nbank,

            'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': seed_mask },
            'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': seed_mask },
            'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': seed_mask },
            'perturb_car_residue':   { 'nperturb': nperturb_car_residue,   'partner_mask': seed_mask },
            'perturb_randint':     { 'nperturb': nperturb_randint },

            'csa_stage': csa_stage,
        })

        if csa_stage[1] == 0:
            c.ncycle = 0
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )




if __name__ == '__main__':
    main()


