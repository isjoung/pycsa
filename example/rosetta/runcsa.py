#!/usr/bin/env python
from csa import read_restart_param
from csa.csa_rosetta import csa_rosetta

bank_size = 50
nseed = 30
nbank_add = 50
max_bank_size = 400
nperturb_int_cross = 6
nperturb_int_residue = 2
nperturb_torsion = 4
nperturb_car_cross = 5
nperturb_randint = 3


def main():
    p = read_restart_param('csa_restart.dat')
    if len(p) == 0:
        p['csa_stage'] = (0,0)
        p['nbank'] = bank_size
        p['icmax'] =  2
        p['seed_mask'] = []
        
    nbank = p['nbank']
    csa_stage = p['csa_stage']

    c = csa_rosetta({
        # model pdb
        'pose': 'model.pdb',
        'native_coord': None,

        # basic csa parameters
        'nbank': 50,
        'nbank_add': 30,
        'nseed': 30,

        # termination
        'icmax': 3,  # If ncycle reaches icmax, iteration stops
        'iucut': 10, # the number of bank update is below iucut, ncycle increments 

        # measuring distances between conformations
        # dihedral: dihedral distance
        # rmsd: root-mean-square distance
        'dist_method': 'rmsd',
        'bb_distance_weight': 1.0,

        # dcut
        'dcut_reduce': 0.997252158427478, # dcut is reduced by dcut_reduce every iteration 0.4**(1./333) = 0.997...

        # restart
        'restart_file': 'csa_restart.dat',
        'save_restart': 1,
        'restart_mode': 2,

        'init_bank': None,
        'include_init_bank': True, # add init_bank to rbank
        'rand_init_bank': False, # randomize conformations in init_bank. if False, the conformations are
                                 # added without perturbation
        'grdmin': 0.1, # minimum RMS gradient per atom 

        # log
        'report_min': 0, # report minimization step frequency
        'history_file': 'history', # history filename
        'append_history': False, # to force appending to the existing history file
        'write_pdb': True, # writes pdb files
        'append_coord': False, # append coordinates to bank pdb files
        'write_score_profile': True, # writes energy profile
        'profile_file': 'profile.energy',
        'record_gmin': False, # save gmin###.pdb

        # randomize/rbank
        # random conformation parameters
        'rand_conf_type': 0,
        # rand_conf_type == 0: numbers are sigmas of bond, angle, and torsions
        #     if angle or torsion is None, it is randomly perturbed
        # rand_conf_type == 1: numbers are maximum distances of x, y, and z
        # rand_conf_type == 2: numbers are sigma distances of x, y and z
        'rand_param0': { 'sigma_bond': 0.1, 'sigma_angle': 10.0, 'sigma_torsion': None, 'discrete_omega': True, 'residue_ratio': 1.0, 'res_index': None },
        'rand_param1': { 'max_x': 3.0, 'max_y': 3.0, 'max_z': 3.0, 'res_index': None },

        # perturb
        'minimize_seed': True, # seed variables are added to the perturbed variable lists
        'weighted_partner': False,

        # fix residue
        'fix_residue': None,

        # record bank energy and distance history
        'bank_history': None,
        'bank_history_dist_method': 'rmsd',

        # update is rejected if the condition is met
        'update_cut': None,
        'update_cut_method': 'rmsd',

        # metadynamics
        'ap_dist_method': 'rmsd',

        'verbose': 1,
        'log_caller': False,  

        # perturbation
        'perturb_int_cross': { 'nperturb': nperturb_int_cross, 'res_index': None, 'partner_mask': [] },
        'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'res_index': None, 'partner_mask': [] },
        'perturb_torsion': { 'nperturb': nperturb_torsion, 'res_index': None, 'n_range0': (3,7), 'n_range1': (1,5), 'partner_mask': [], 'br_ratio': 0.5, 'n_new_bank_cut': 5 },
        'perturb_car_cross': { 'nperturb': nperturb_car_cross, 'res_index': None, 'partner_mask': [], 'nmin': 1, 'nmax': None },
        'perturb_randint': { 'nperturb': nperturb_randint, 'res_index': None, 'nmin': 1, 'nmax': None, 'max_ratio': 0.02,
                             'sigma_bond': 0.001, 'sigma_angle': 0.001, 'sigma_torsion': 5.0 },

        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        c.run()
        csa_stage = next_csa_stage(csa_stage)
        nbank = ( csa_stage[0] + 1 ) * bank_size
        if csa_stage[1] == 0:
            icmax = 0
            seed_mask = range(nbank-bank_size)
        else:
            icmax = 2
            seed_mask = []
        c.set_param({
            'icmax': icmax,
            'seed_mask': seed_mask,
            'nbank': nbank,

            'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': seed_mask },
            'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': seed_mask },
            'perturb_torsion':     { 'nperturb': nperturb_torsion,     'partner_mask': seed_mask },
            'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': seed_mask },
            'perturb_randint':     { 'nperturb': nperturb_randint },

            'csa_stage': csa_stage,
        })

        if csa_stage[1] == 0:
            c.ncycle = 0
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

if __name__ == '__main__':
    main()
