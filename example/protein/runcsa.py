#!/usr/bin/env python
from csa import read_restart_param
from csa.csa_protein import csa_protein

bank_size = 50
nseed = 30
max_bank_size = 100
nbank_add = 30
nperturb_type1 = 8
nperturb_type2 = 6
nperturb_type3 = 6
nperturb_type4 = 0
nperturb_type5 = 0
nperturb_type6 = 0
nperturb_type7 = 0
nperturb_backtor = 0
nperturb_randint = 0
nperturb_exbeta = 0
nperturb_randint = 0
nperturb_randint2 = 0
bank_min_maxiter = 1000
rbank_min_maxiter = 1000

def main():
    p = read_restart_param('csa_restart.dat')
    if len(p) == 0:
        p['csa_stage'] = (0,0)
        p['nbank'] = bank_size
        p['seed_mask'] = []
        p['icmax'] = 3

    nbank = p['nbank']
    csa_stage = p['csa_stage']

    c = csa_protein({
        'xyzfile': 'model.xyz',
        'xyzmapfile': 'model.map',

        'smart_pmunit': 1,
        'parallel_minimization': 3,
        'xyzfile': 'model.xyz',
        'dcut_reduce': (0.4)**(1./333.333),
        'dcut1': 2.0,
        'dcut2': 5.0,
        'dist_method': 'dihedral',
        'icmax': p['icmax'],
        'iucut': 10,

        'nseed': nseed,
        'seed_mask': p['seed_mask'],
        'nbank': nbank,
        'nbank_add': nbank_add,


        'save_restart': 1,

        'rand_conf_type': 0,
        'write_diff': False,
        'write_pdb': True,
        'write_xyz': False,

        'bank_min_maxiter': bank_min_maxiter,
        'rbank_min_maxiter': rbank_min_maxiter,

        'verbose': 2,

        'perturb_param1': { 'nperturb': nperturb_type1, 'partner_mask': p['seed_mask'] },
        'perturb_param2': { 'nperturb': nperturb_type2, 'partner_mask': p['seed_mask'] },
        'perturb_param3': { 'nperturb': nperturb_type3, 'partner_mask': p['seed_mask'] },
        'perturb_param4': { 'nperturb': nperturb_type4, 'partner_mask': p['seed_mask'] },
        'perturb_param5': { 'nperturb': nperturb_type5, 'partner_mask': p['seed_mask'] },
        'perturb_param6': { 'nperturb': nperturb_type6, 'partner_mask': p['seed_mask'] },
        'perturb_param7': { 'nperturb': nperturb_type7, 'partner_mask': p['seed_mask'] },
        'perturb_exbeta': { 'nperturb': nperturb_exbeta, 'partner_mask': p['seed_mask'] },
        'perturb_backtor': { 'nperturb': nperturb_backtor },
        'perturb_randint': { 'nperturb': nperturb_randint },
        'perturb_randint': { 'nperturb': nperturb_randint, 'nmin': 1, 'nmax': 5, 'sigma_torsion': 5.0 },
        'perturb_randint2': {'nmin': 1, 'nmax': 3, 'sigma_torsion': None, 'nperturb': nperturb_randint2 },

        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        c.run()
        csa_stage = next_csa_stage(csa_stage)
        nbank = (csa_stage[0]+1) * bank_size 
        if csa_stage[1] == 0:
            icmax = 0
            seed_mask = range(nbank-bank_size)
        else:
            icmax = 2
            nbank = c.p['nbank']
            seed_mask = []

        c.set_param({
            'csa_stage': csa_stage,
            'icmax': icmax,

            'seed_mask': seed_mask,
            'max_diversity_mask': seed_mask,
            'nbank': nbank,
            'seed_mask': seed_mask,
            'icmax': icmax,

            'perturb_param1': { 'nperturb': nperturb_type1, 'partner_mask': seed_mask },
            'perturb_param2': { 'nperturb': nperturb_type2, 'partner_mask': seed_mask },
            'perturb_param3': { 'nperturb': nperturb_type3, 'partner_mask': seed_mask },
            'perturb_param4': { 'nperturb': nperturb_type4, 'partner_mask': seed_mask },
            'perturb_param5': { 'nperturb': nperturb_type5, 'partner_mask': seed_mask },
            'perturb_param6': { 'nperturb': nperturb_type6, 'partner_mask': seed_mask },
            'perturb_param7': { 'nperturb': nperturb_type7, 'partner_mask': seed_mask },
            'perturb_exbeta': { 'nperturb': nperturb_exbeta, 'partner_mask': seed_mask },
        })

        if csa_stage[1] == 0:
            c.ncycle = 0
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

if __name__ == '__main__':
    main()
