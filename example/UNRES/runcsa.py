#!/usr/bin/env python
from csa.csa_UNRES import csa_UNRES
import cPickle

bank_size = 5
nseed = 5
max_bank_size = 5
nbank_add = 1
nperturb_int_cross = 6
nperturb_int_residue = 2
nperturb_torsion = 4
nperturb_car_cross = 5
nperturb_randint = 3
min_maxiter = 1000

def main():
    p = read_restart_param('csa_restart.dat')
    csa_stage = p['csa_stage']
    nbank = p['nbank']

    c = csa_UNRES({
        'parallel_minimization': 3,

        'pdbfile': 'model.pdb',
        'dcut_reduce': (0.4)**(1./333.333),
        'dcut1': 2.0,
        'dcut2': 5.0,
#        'dist_method': 'rmsd',
        'dist_method': 'dihedral',

        'restart_mode': 2,
        'icmax': p['icmax'],
        'iucut': 10,

        'nseed': nseed,
        'seed_mask': p['seed_mask'],
        'max_diversity_mask': p['seed_mask'],
        'nbank': nbank,
        'nbank_add': nbank_add,
        'random_seed': 0,

        'save_restart': 1,
        'write_pdb': True,

        'rand_conf_type': 0,
        'rand_param0': {'sigma_bond': 0.1, 'sigma_angle': 10.0, 'sigma_torsion': 20.0, 'discrete_omega': True, 'residue_ratio':1.0, 'res_index': None },
#        'rand_param1': {'res_index': None, 'max_z': 3.0, 'max_x': 3.0, 'max_y': 3.0},
        'min_maxiter': min_maxiter,
        'verbose': 2,
        'init_bank': None,
        'bank_history': 'bank.history',
        'native_pdb': 'native.pdb',
        'pick_rbank': 'sort',
        'minimize_seed': True,
        'max_diversity': False,
        'decut': 0.1,

        'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': p['seed_mask'] },
        'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': p['seed_mask'] },
        'perturb_torsion':     { 'nperturb': nperturb_torsion,     'partner_mask': p['seed_mask'] },
        'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': p['seed_mask'] },
        'perturb_randint':     { 'nperturb': nperturb_randint },

        'csa_stage': csa_stage,
        'bank_history_dist_method':'tmscore',
        'tmscore_cut': None,
        'super_native': False,

        'min_method':'lbfgsb',

    })

    while nbank <= max_bank_size:
        c.run()
        csa_stage = next_csa_stage(csa_stage)
        nbank = ( csa_stage[0] + 1 ) * bank_size
        if ( csa_stage[1] == 0 ):
            icmax = 0
            seed_mask = range(nbank-bank_size)
        else:
            icmax = 1
            seed_mask = []
        c.set_param({
            'nbank': nbank,
            'max_diversity_mask': seed_mask,
            'seed_mask': seed_mask,
            'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': seed_mask },
            'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': seed_mask },
            'perturb_torsion':     { 'nperturb': nperturb_torsion,     'partner_mask': seed_mask },
            'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': seed_mask },
            'perturb_randint':     { 'nperturb': nperturb_randint },
            'icmax': icmax,
            'csa_stage': csa_stage,
        })
        c.ncycle = 0
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

def read_restart_param(rfile):
    p = {
        'csa_stage': (0,0),
        'nbank': bank_size,
        'icmax': 2,
        'seed_mask': [],
    }

    try:
        f = open(rfile,'r')
    except:
        return p
    restart = cPickle.load(f)
    f.close()

    return restart['p']

if __name__ == '__main__':
    main()
