#!/usr/bin/env python
from csa import read_restart_param
from csa.csa_weight import csa_weight
from glob import glob

bank_size = 15
max_bank_size = 2000
nseed = 6
nperturb_crossover = 10
nperturb_crossover2 = 10
nperturb_random = 10
score_name = 'tmscore'
data_files = glob('data/*.dat')
# num tmscore ddfire dope goap rf_ha_srs rwplus
decomp_params = "ddfire dope goap rf_ha_srs rwplus"
opt_params = "dope goap rf_ha_srs rwplus"

decomp_params = decomp_params.split()
opt_params = opt_params.split()
def main():
    p = read_restart_param('csa_restart.dat')
    if len(p) == 0:
        p['csa_stage'] = (0,0)
        p['nbank'] = bank_size 
        p['seed_mask'] = []
        p['icmax'] = 3
    nbank = p['nbank']
    csa_stage = p['csa_stage']

    c = csa_weight({
        'profile_precision': 15,
        'data_files': data_files,
        'decomp_params': decomp_params,
        'opt_params': opt_params,
        'score_name': score_name,
        'n_best': 1,
        'minimize_score': False,
        'minimize_energy': True,
        'harmonic_weight': 1e-6, # try to minimize the weight if possible
        'penalty_type': 1,
        'min_maxiter': 2000,
        'min_perturb_ratio': 0.1, # 10% of max_weight
        'min_method': 'nelder-mead',
        'max_weight': 100.0,
        'input_data_file': None, #'input.dat',
        'skip_data_file': False,
        'default_weight': [ 1.0 for x in range(len(decomp_params)) ],

        'parallel_minimization': 2,
        'restart_mode': 2,
        'dcut_reduce': (2.0/3.0)**(1.0/25.0),

        'write_diff': False,
        'write_bank': True,

        'dcut1': 2.0,
        'dcut2': 5.0,

        'seed_mask': p['seed_mask'],
        'minimize_seed': False,

        'nbank': nbank,
        'nseed': nseed,
        'icmax': p['icmax'],
        'iucut': 10,
        'verbose': 2,

        'perturb_crossover': { 'nperturb': nperturb_crossover, 'partner_mask': [], 'max_ratio': 0.5, },
        'perturb_crossover2': { 'nperturb': nperturb_crossover2, 'partner_mask': [], 'n_new_bank_cut': 2, 'max_ratio': 0.2, },
        'perturb_random': { 'nperturb': nperturb_random, 'nmin': 1, 'nmax': 3, 'perturb_ratio': 0.2, },

        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        c.run()
        csa_stage = next_csa_stage(csa_stage)

        nbank = ( csa_stage[0] + 1 ) * bank_size
        if csa_stage[1] == 0:
            seed_mask = range(nbank-bank_size)
            icmax = 0
        else:
            seed_mask = []
            icmax = 2

        c.set_param({
            'nbank': nbank,
            'icmax': icmax,
            'seed_mask': seed_mask,
            'max_diversity_mask': seed_mask,
            'perturb_crossover': { 'nperturb': nperturb_crossover, 'partner_mask': seed_mask },
            'perturb_crossover2': { 'nperturb': nperturb_crossover2, 'partner_mask': seed_mask },
            'csa_stage': csa_stage,
        })

        if csa_stage[1] == 0:
            c.ncycle = 0
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

if __name__ == '__main__':
    main()
