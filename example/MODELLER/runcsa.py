#!/usr/bin/env python
from csa import read_restart_param
from csa.csa_MODELLER import csa_MODELLER

bank_size = 50
nseed = 30
max_bank_size = 100
nbank_add = 30
nperturb_int_cross = 6
nperturb_int_residue = 2
nperturb_torsion = 4
nperturb_car_cross = 5
nperturb_randint = 3
min_maxiter = 1000

def main():
    p = read_restart_param('csa_restart.dat')
    if len(p) == 0:
        p['csa_stage'] = (0,0)
        p['nbank'] = bank_size
        p['icmax'] =  2
        p['seed_mask'] = []
        
    nbank = p['nbank']
    csa_stage = p['csa_stage']

    c = csa_MODELLER({
        'pdbfile': 'model.pdb',
        'rsrfile': 'model.rsr',
        'dcut_reduce': (0.4)**(1./333.333),
        'dcut1': 2.0,
        'dcut2': 5.0,
        'dist_method': 'rmsd',

        'icmax': p['icmax'],
        'iucut': 10,

        'nseed': nseed,
        'seed_mask': p['seed_mask'],
        'max_diversity_mask': p['seed_mask'],
        'nbank': nbank,
        'nbank_add': nbank_add,
        'random_seed': 0,

        'save_restart': 1,
        'write_pdb': True,

        'rand_conf_type': 0,

        'min_maxiter': min_maxiter,
        'min_method': 'cg',
        'parallel_minimization': 3,

        'verbose': 2,
        'pick_rbank': 'sort',
        'minimize_seed': True,
        'decut': 0.1,

        'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': p['seed_mask'] },
        'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': p['seed_mask'] },
        'perturb_torsion':     { 'nperturb': nperturb_torsion,     'partner_mask': p['seed_mask'] },
        'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': p['seed_mask'] },
        'perturb_randint':     { 'nperturb': nperturb_randint },

        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        c.run()
        csa_stage = next_csa_stage(csa_stage)
        nbank = ( csa_stage[0] + 1 ) * bank_size
        if ( csa_stage[1] == 0 ):
            icmax = 0
            seed_mask = range(nbank-bank_size)
        else:
            icmax = 2
            seed_mask = []
        c.set_param({
            'nbank': nbank,
            'seed_mask': seed_mask,
            'perturb_int_cross':   { 'nperturb': nperturb_int_cross,   'partner_mask': seed_mask },
            'perturb_int_residue': { 'nperturb': nperturb_int_residue, 'partner_mask': seed_mask },
            'perturb_torsion':     { 'nperturb': nperturb_torsion,     'partner_mask': seed_mask },
            'perturb_car_cross':   { 'nperturb': nperturb_car_cross,   'partner_mask': seed_mask },
            'perturb_randint':     { 'nperturb': nperturb_randint },
            'icmax': icmax,
            'csa_stage': csa_stage,
        })

        if csa_stage[1] == 0:
            c.ncycle = 0
        # Save the restart file
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

if __name__ == '__main__':
    main()
