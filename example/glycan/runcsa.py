#!/usr/bin/env python
from csa_glycan import csa_glycan
import cPickle

bank_size = 50
nseed = 30
max_bank_size = 100
nbank_add = 0
nperturb_int_cross = 8
nperturb_int_residue = 6
nperturb_torsion = 6
min_maxiter = 1000
max_diversity_dcut = False
res_index = range(241, 251)

def main():
    p = read_restart_param('csa_restart.dat')
    csa_stage = p['csa_stage']
    nbank = p['nbank']

    c = csa_glycan({
        'pdbfile': 'native.pdb',
        'max_diversity_dcut': max_diversity_dcut,
        'dcut_reduce': (0.4)**(1./333.333),
        'dcut1': 2.0,
        'dcut2': 5.0,
        'dist_method': 'rmsd',

        'restart_mode': 2,
        'icmax': p['icmax'],
        'iucut': 10,

        'nseed': nseed,
        'seed_mask': p['seed_mask'],
        'max_diversity_mask': p['seed_mask'],
        'nbank': nbank,
        'nbank_add': nbank_add,
        'random_seed': 0,

        'save_restart': 1,
        'write_pdb': True,

        'rand_conf_type': 0,

	'bank_min_maxiter': min_maxiter,
        'rbank_min_maxiter': min_maxiter,
        'min_method': 'tinker',

        'verbose': 2,
        'pick_rbank': 'sort',
        'minimize_seed': True,
        'decut': 0.1,

        'perturb_int_cross':   { 'res_index': res_index, 'nperturb': nperturb_int_cross,   'partner_mask': p['seed_mask'] },
        'perturb_int_residue': { 'res_index': res_index, 'nperturb': nperturb_int_residue, 'partner_mask': p['seed_mask'] },
        'perturb_torsion':     { 'res_index': res_index, 'nperturb': nperturb_torsion,     'partner_mask': p['seed_mask'] },

        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        c.run()
        csa_stage = next_csa_stage(csa_stage)
        nbank = ( csa_stage[0] + 1 ) * bank_size
        if ( csa_stage[1] == 0 ):
            icmax = 0
            seed_mask = range(nbank-bank_size)
        else:
            icmax = 1
            seed_mask = []
        c.set_param({
            'nbank': nbank,
            'max_diversity_mask': seed_mask,
            'seed_mask': seed_mask,
            'perturb_int_cross':   { 'res_index': res_index, 'nperturb': nperturb_int_cross,   'partner_mask': seed_mask },
            'perturb_int_residue': { 'res_index': res_index, 'nperturb': nperturb_int_residue, 'partner_mask': seed_mask },
            'perturb_torsion':     { 'res_index': res_index, 'nperturb': nperturb_torsion,     'partner_mask': seed_mask },
            'icmax': icmax,
            'csa_stage': csa_stage,
        })
        c.ncycle = 0
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

def read_restart_param(rfile):
    p = {
        'csa_stage': (0,0),
        'nbank': bank_size,
        'icmax': 2,
        'seed_mask': [],
    }

    try:
        f = open(rfile,'r')
    except:
        return p
    restart = cPickle.load(f)
    f.close()

    return restart['p']

if __name__ == '__main__':
    main()
