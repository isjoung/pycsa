#!/usr/bin/env python
import numpy as np
from csa import read_restart_param
from csa.csa_param import csa_param, mpicomm, mpirank

bank_size = 15
max_bank_size = 100
nseed = 6
nperturb_crossover = 10
nperturb_crossover2 = 10
nperturb_random = 10
min_maxiter = 1000
nvar = 5

# param range
param_range = [ (-512., 512.) ] * nvar
# param scale
param_scale = [ 'linear' ] * nvar
# native var
native_var = np.array([0.0]*nvar)

exp1 = np.exp(1)
const = exp1 + 20.0
def energy_function(x):
    #print 'x', x
    xs = x[1:]
    x0 = x[:-1]
    val = np.sum( -(xs+47)*np.sin(np.sqrt(np.absolute(xs+0.5*x0+47)))-x0*np.sin(np.sqrt(np.absolute(x0-xs-47))) ) 
    return val

class mycsa(csa_param):
    def read_native_var(self):
        self.native_var = [ native_var ]

def main():
    p = read_restart_param('csa_restart.dat')
    if len(p) == 0:
        p['csa_stage'] = (0,0)
        p['nbank'] = bank_size 
        p['seed_mask'] = []
        p['icmax'] = 3

    csa_stage = p['csa_stage']
    nbank = p['nbank']

    c = mycsa({
        'bank_history': 'bank_history',
        'param_range': param_range,
        'min_maxiter': min_maxiter,
        'min_perturb_ratio': 0.1,
        'param_scale': param_scale,
        'min_method': 'L-BFGS-B',
        'energy_function': energy_function,

        'parallel_minimization': 2,
        'restart_mode': 2,
        'dcut_reduce': (2.0/3.0)**(1.0/25.0),

        'write_diff': False,
        'write_bank': True,

        'dcut1': 2.0,
        'dcut2': 5.0,

        'seed_mask': p['seed_mask'],
        'minimize_seed': False,

        'nbank': nbank,
        'nseed': nseed,
        'icmax': p['icmax'],
        'iucut': 10,
        'verbose': 2,

        'perturb_crossover': { 'nperturb': nperturb_crossover, 'partner_mask': [], 'max_ratio': 0.5, },
        'perturb_crossover2': { 'nperturb': nperturb_crossover2, 'partner_mask': [], 'n_new_bank_cut': 1, 'max_ratio': 0.2, },
        'perturb_random': { 'nperturb': nperturb_random, 'nmin': 1, 'nmax': 3, 'perturb_ratio': 0.2, },

        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        c.run()
        csa_stage = next_csa_stage(csa_stage)

        nbank = ( csa_stage[0] + 1 ) * bank_size
        if ( csa_stage[1] == 0 ):
            seed_mask = range(nbank-bank_size)
            icmax = 0
        else:
            seed_mask = []
            icmax = 2

        c.set_param({
            'nbank': nbank,
            'icmax': icmax,
            'seed_mask': seed_mask,
            'max_diversity_mask': seed_mask,
            'perturb_crossover': { 'nperturb': nperturb_crossover, 'partner_mask': seed_mask, 'max_ratio': 0.5, },
            'perturb_crossover2': { 'nperturb': nperturb_crossover2, 'partner_mask': seed_mask, 'n_new_bank_cut': 2, 'max_ratio': 0.2, },
            'perturb_random': { 'nperturb': nperturb_random, 'nmin': 1, 'nmax': 3, 'perturb_ratio': 0.2, },
            'csa_stage': csa_stage,
        })
        if csa_stage[1] == 0:
            c.ncycle = 0
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

if __name__ == '__main__':
    main()
