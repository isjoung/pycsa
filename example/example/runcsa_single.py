#!/usr/bin/env python
from csa.csa_example import csa_example

nvar = 2
c = csa_example({
    'nparam': nvar,
    'param_range': [(-512.,512.)]*nvar,
})
c.run()
