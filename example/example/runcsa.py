#!/usr/bin/env python
from csa import read_restart_param
from csa.csa_example import csa_example

bank_size = 15            # N_B, the number of solutions added at every CSA stage
max_bank_size = 30        # The maximum size of bank
nseed = 6                 # N_S
nperturb_crossover = 10   # The number of the first type crossover perturbation per seed
nperturb_crossover2 = 10  # The number of the second type crossover perturbation per seed
nperturb_random = 10      # The number of random perturbation per seed
min_maxiter = 1000        # The number of maximum iteration for a single local minimization
nvar = 5                  # The dimension of the eggholder function

def main():
    # Try to read a restart file
    p = read_restart_param('csa_restart.dat')
    if len(p) == 0:
        # A restart file is not read, which means that this is the first run
        # or the restart file is not readable.
        #
        # Assign default values for the following CSA parameters
        #
        # 'csa_stage' is introduced. The first number is for the size of bank
        # ie. nbank = ( csa_stage[0] + 1 ) * bank_size
        # The second number can be either 0 or 1. Except for the first stage (0,0),
        # the n-th stage has two sub-stages (n,0) and (n,1). At the stage of (n,0),
        # 'seed_mask' is set but at the stage of (n,1) 'seed_mask' is not set.
        # 'seed_mask' is not set for the first stage (0,0), too.
        p['csa_stage'] = (0,0)
        p['nbank'] = bank_size 
        # Indexes in the 'seed_mask' is not selected as a seed nor as a partner
        # solution in crossover.
        p['seed_mask'] = []
        p['icmax'] = 3

    nbank = p['nbank']
    csa_stage = p['csa_stage']

    # Instantiate the csa_example class
    c = csa_example({
        'nbank': nbank,
        'nseed': 5,
        'seed_mask': p['seed_mask'],
        'icmax': p['icmax'],
        'iucut': 10,
        'decut': 0.001,
        'dcut1': 2.0,
        'dcut2': 5.0,
        'dcut_reduce': 0.983912,
        'save_restart': 1,
        'history_file': 'history',
        'nparam': nvar,
        'param_range': [(-512.,512.)]*nvar,
        'min_maxiter': 1000,
        'perturb_crossover': { 'nperturb': 10, 'partner_mask': [], 'max_ratio': 0.5 },
        'perturb_crossover2': { 'nperturb': 10, 'partner_mask': [], 'n_new_bank_cut': 1,
                                'max_ratio': 0.2 },
        'perturb_random': { 'nperturb': 10, 'nmin': 1, 'nmax': 3, 'perturb_ratio': 0.2 },

        # 'csa_stage' parameter for controlling the CSA flow
        'csa_stage': csa_stage,
    })

    while nbank <= max_bank_size:
        # Run CSA
        c.run()

        # The next 'csa_stage'
        csa_stage = next_csa_stage(csa_stage)

        # Adjust the size of bank
        nbank = ( csa_stage[0] + 1 ) * bank_size
        if csa_stage[1] == 0:
            # This stage is the first round of a new CSA stage
            seed_mask = range(nbank-bank_size)
            icmax = 0
        else:
            # This stage is the beginning of the second round of the current CSA stage
            seed_mask = []
            icmax = 2

        # update CSA parameters properly for the next stage
        c.set_param({
            'nbank': nbank,
            'icmax': icmax,
            'seed_mask': seed_mask,
            'perturb_crossover': { 'nperturb': nperturb_crossover, 'partner_mask': seed_mask },
            'perturb_crossover2': { 'nperturb': nperturb_crossover2, 'partner_mask': seed_mask },
            'perturb_random': { 'nperturb': nperturb_random },
            'csa_stage': csa_stage,
        })

        if csa_stage[1] == 0:
            # reset the round(cycle) number if necessary
            c.ncycle = 0
        # Save the restart file
        c.save_restart()

def next_csa_stage(csa_stage):
    if ( csa_stage[0] == 0 ):
        return ( 1, 0 )
    elif ( csa_stage[1] == 0 ):
        return ( csa_stage[0], 1 )
    else:
        return ( csa_stage[0]+1, 0 )

if __name__ == '__main__':
    main()
